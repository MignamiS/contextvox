Index 
------

[Project configuration](config_dev.md)
:	Project setup for **Eclipse**.

[ContextVox website](http://contextvox.dti.supsi.ch/)
:	ContextVox's official webpage.

[Quick start guide](http://contextvox.dti.supsi.ch/index.php/guide/)
:	Some information about ContextVox and how to use it.
