Project configuration
---------------------
Below the procedure to configure **Eclipse**.

1. Clone repository
2. Create a Plug-in project on Eclipse PDE
	- name and version indifferent
	- create an Activator
	- no template
3. Copy/paste repository folder content, replacing all duplicated files
4. Delete Activator and update plug-in informations (name, version, ...) if not consistent
5. Fix library dependencies on *project > properties > Java build path > Add JAR*. Add all the JAR files on the */lib* folder.
6. Clean and build


### Unit test ###
Put the */test/org/contextvox/replacer/dictionaries/res* folder into the source folder on the Java build path, in order to run the JUnit tests. It contains a shorter version of the messages folder.

Also add the JUnit library if it is the first time you run it:
Project properties -> Java build path -> Libraries -> Add library -> JUnit.

