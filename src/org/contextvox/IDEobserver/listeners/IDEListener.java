package org.contextvox.IDEobserver.listeners;

/**
 * Common ContextVox IDEObserver interface.
 */
public interface IDEListener {

	/**
	 * Returns if the observer is still connected to its target
	 *
	 * @return boolean
	 */
	public boolean isConnected();

	/**
	 * Connect the observer.
	 *
	 * @return whether the connection success
	 */
	public boolean start();

	/**
	 * Disconnect the observer from its target.
	 *
	 * @return whether the disconnection success
	 */
	public boolean stop();

}
