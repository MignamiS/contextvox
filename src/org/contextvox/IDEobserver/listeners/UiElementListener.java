package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.contentoutline.ContentOutline;

/**
 * Provides a listener for Activeshell elements like tables, links or trees.
 */
@SuppressWarnings("restriction")
public class UiElementListener implements Listener, IDEListener {

	private boolean connected;
	private final StimulusReceiver receiver;

	private Display display;

	private Control lastControl = null;

	public UiElementListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void handleEvent(final Event event) {
		final Control control = display.getFocusControl();
		if (control != null && !control.equals(lastControl)) {
			lastControl = control;
			switchCase(control);
		}
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public boolean start() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		display = workbench.getDisplay();
		// FIXME addListener should be used instead, but this listener is not
		// triggered by focus events.
		display.addFilter(SWT.FocusIn, this);
		this.connected = true;
		return true;
	}

	@Override
	public boolean stop() {
		if (this.display == null) {
			final IWorkbench workbench = PlatformUI.getWorkbench();
			display = workbench.getDisplay();
		}
		display.removeFilter(SWT.FocusIn, this);
		this.connected = false;
		return true;
	}

	private void caseTable(final Table table) {
		final String msg = Messages.getSingleMessage("tableSelected");
		receiver.processRequest(new NavigationRequest(MessageSource.TABLE, msg));
		table.addSelectionListener(new TableSelListener(table));
	}

	private void caseTree(final Tree tree) {
		final IWorkbenchPart wp = PluginElements.getActivePart();
		// this cases are alredy processed in another listener
		if (wp instanceof PackageExplorerPart || wp instanceof ContentOutline)
			return;

		/*
		 * FIXME SM questa condition evita un bug che si ottiene quando il focus
		 * e' sull'outline e viene cambiato editor (e di conseguenza cambia
		 * anche l'outline), ma sebbene dovrebbe essere la ContentOutline ad
		 * essere nel focus, viene focusata questa WorkbenchPart che permette a
		 * questo listener di registrare un secondo listener nell'Outline.
		 * Occorre trovare un sistema meno "brutale" di questo...
		 */
		if (wp instanceof CompilationUnitEditor)
			return;

		this.receiver
				.processRequest(new NavigationRequest(MessageSource.TREE, Messages.getSingleMessage("treeSelected")));
		// register a new selectionListener on the yet discovered tree
		tree.addSelectionListener(new TreeSelListener());
		tree.addListener(SWT.Expand, new TreeExpandListener());
		tree.addListener(SWT.Collapse, new TreeCollapseListener());
	}

	private void switchCase(final Control control) {
		// check if the selected element is complex
		if (control instanceof Tree) {
			caseTree((Tree) control);
		} else if (control instanceof Table) {
			caseTable((Table) control);
		} else {
			// the element is simple, read directly
			if (control instanceof Text) {
				this.receiver.processRequest(new NavigationRequest(MessageSource.TEXT, null, control));
			} else if (control instanceof Link) {
				this.receiver.processRequest(new NavigationRequest(MessageSource.LINK, null, control));
			}

		}
	}

	private final class TableSelListener implements SelectionListener {
		private final Table table;

		private TableSelListener(final Table table) {
			this.table = table;
		}

		@Override
		public void widgetDefaultSelected(final SelectionEvent e) {
		}

		@Override
		public void widgetSelected(final SelectionEvent e) {
			// final TableItem[] tableItems = table.getSelection();
			// for (final TableItem tableItem : tableItems)
			// TODO not really useful
			// contextVox.read(tableItem.getText(), MessageSource.TABLE_ITEM);
		}
	}

	private final class TreeCollapseListener implements Listener {
		@Override
		public void handleEvent(final Event event) {
			receiver.processRequest(new NavigationRequest(MessageSource.TREE_ITEM, null, event));
		}
	}

	private final class TreeExpandListener implements Listener {
		@Override
		public void handleEvent(final Event event) {
			final NavigationRequest request = new NavigationRequest(MessageSource.TREE_ITEM, null, event);
			receiver.processRequest(request);
		}
	}

	private final class TreeSelListener implements SelectionListener {
		@Override
		public void widgetDefaultSelected(final SelectionEvent e) {
		}

		@Override
		public void widgetSelected(final SelectionEvent e) {
			final NavigationRequest request = new NavigationRequest(MessageSource.TREE_ITEM, null, e);
			receiver.processRequest(request);
		}
	}
}