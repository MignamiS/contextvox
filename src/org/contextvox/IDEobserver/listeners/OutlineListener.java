package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.OperationType;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.views.contentoutline.ContentOutline;

public class OutlineListener implements ISelectionChangedListener, IDEListener {

	private boolean connected;
	private final StimulusReceiver receiver;

	// the listened outline
	private final ContentOutline outline;

	public OutlineListener(final StimulusReceiver receiver, final ContentOutline outline) {
		this.receiver = receiver;
		this.outline = outline;
		// starts already connected (see PartSelectionListener)
		this.connected = true;
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public void selectionChanged(final SelectionChangedEvent event) {
		final NavigationRequest request = new NavigationRequest(MessageSource.OUTLINE);
		request.setOperation(OperationType.TREEITEM_SELECTED);
		request.setSelectionChangeEvent(event);
		receiver.processRequest(request);
	}

	@Override
	public boolean start() {
		// already started by PartSelectionListener
		this.connected = true;
		return true;
	}

	@Override
	public boolean stop() {
		this.outline.removeSelectionChangedListener(this);
		this.connected = false;
		return true;
	}

}