package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.OperationType;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;

/**
 * Listener for generic trees
 */
public class VoxTreeListener implements TreeListener, SelectionListener {

	private final StimulusReceiver receiver;

	public VoxTreeListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void widgetSelected(final SelectionEvent event) {
		if (event.widget == null)
			return;
		final NavigationRequest request = new NavigationRequest(MessageSource.TREE);
		request.setOperation(OperationType.TREEITEM_SELECTED);
		request.widget = event.widget;
		this.receiver.processRequest(request);
	}

	@Override
	public void widgetDefaultSelected(final SelectionEvent event) {
	}

	@Override
	public void treeCollapsed(final TreeEvent event) {
		if (event.widget == null)
			return;
		final NavigationRequest request = new NavigationRequest(MessageSource.TREE);
		request.setOperation(OperationType.TREEITEM_COLLAPSED);
		request.widget = event.widget;
		this.receiver.processRequest(request);
	}

	@Override
	public void treeExpanded(final TreeEvent event) {
		if (event.widget == null)
			return;
		final NavigationRequest request = new NavigationRequest(MessageSource.TREE);
		request.setOperation(OperationType.TREEITEM_EXPANDED);
		request.widget = event.widget;
		this.receiver.processRequest(request);
	}

}
