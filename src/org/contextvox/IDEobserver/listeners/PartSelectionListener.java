package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.views.contentoutline.ContentOutline;

@SuppressWarnings("restriction")
public class PartSelectionListener implements ISelectionListener, IDEListener {
	// TODO refactor

	private boolean connected;
	private final StimulusReceiver receiver;

	private final IWorkbenchPart lastPart = null;

	public PartSelectionListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	// it happens only when the part is different
	private void processOnceForPart(final IWorkbenchPart part, final ISelection selection) {
		if (part instanceof ContentOutline) {
			// register outline listener
			final ContentOutline outline = (ContentOutline) part;
			outline.addSelectionChangedListener(new OutlineListener(receiver, outline));
		}
		// register document listener
		// if (part instanceof ITextEditor) {
		// final ITextEditor te = (ITextEditor) part;
		// final IDocumentProvider provider = te.getDocumentProvider();
		// final IEditorInput editorInput = te.getEditorInput();
		// final IDocument doc = provider.getDocument(editorInput);
		// doc.addDocumentListener(JavaDocumentModificationListener.getInstance());
		// } else
		// register console listener
		// else if (part instanceof ConsoleView) {
		// new ConsoleSelection((ConsoleView) part).execute(selection);
		// }

		// if (part instanceof ITextEditor) {
		/*
		 * proceed only if the selection is a text-selection and if the text
		 * selected is longer than 0; that means only text selection
		 * "strictly speaking" and NOT cursor motion.
		 */
		// if (selection instanceof ITextSelection) {
		// final ITextSelection selection2 = (ITextSelection) selection;
		// if (selection2.getLength() > 0) {
		// // updateEditorStateSelection((ITextEditor) part,
		// // selection);
		// final AnalyzeRequest request = new
		// AnalyzeRequest(MessageSource.EDITOR_SELECTION);
		// request.setSelection(selection);
		// request.setEditor((ITextEditor) part);
		// ContextVoxProgram.readFocus(request);
		// }
		// else {
		// // reset selection data
		// AnalyzeState.resetSelection();
		// }
		// }
	}

	private void announcePartTitle(final IWorkbenchPart part, final ISelection selection) {
		final NavigationRequest request = new NavigationRequest(MessageSource.VIEW, "", part);
		request.setElementSelection(selection);
		receiver.processRequest(request);
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public void selectionChanged(final IWorkbenchPart part, final ISelection selection) {
		if (!part.equals(lastPart))
			processOnceForPart(part, selection);
		processAlways(part, selection);
	}

	@Override
	public boolean start() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage workbenchPage = window.getActivePage();
		if (workbenchPage != null) {
			workbenchPage.addSelectionListener(this);
			this.connected = true;
			return true;
		}
		return false;
	}

	@Override
	public boolean stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage workbenchPage = window.getActivePage();
		if (workbenchPage != null) {
			workbenchPage.removeSelectionListener(this);
			this.connected = false;
			return true;
		}
		return false;
	}

	private void processAlways(final IWorkbenchPart part, final ISelection selection) {
		if (part instanceof ITextEditor && selection instanceof ITextSelection) {
			final ITextSelection textSel = (ITextSelection) selection;
			final AnalyzeRequest request = new AnalyzeRequest(MessageSource.EDITOR_SELECTION);
			request.setSelection(textSel);
			request.setEditor((ITextEditor) part);
			System.out.println("select " + System.currentTimeMillis());
			this.receiver.processRequest(request);
		}
	}

	private void updateEditorStateSelection(final ITextEditor editor, final ISelection selection) {
		final ITextSelection sel = (ITextSelection) selection;
		final IDocumentProvider provider = editor.getDocumentProvider();
		final IEditorInput editorInput = editor.getEditorInput();
		final IDocument doc = provider.getDocument(editorInput);
		// doc.addDocumentListener(JavaDocumentModificationListener.getInstance());
		try {
			final int textLength = sel.getLength();
			final int textOffset = sel.getOffset();
			final int startLine = sel.getStartLine();
			final int lineOffset = doc.getLineOffset(startLine);
			final int lineLenght = doc.getLineLength(startLine);
			final int documentLenght = doc.getLength();

			// final SelectionEvent state = new SelectionEvent(startLine,
			// lineOffset, lineLenght, textOffset,
			// documentLenght, textLength);
			// FIXME build a request and use the textual cont. service
			// ContextState.addJavaEditorEvent(editor, state);
		} catch (final BadLocationException e) {
			// VoxLogger.log(LogLevel.ERROR, "Error accessing the document with
			// the given start line", e);
		}

	}

}
