package org.contextvox.IDEobserver.listeners.selections.state;

public class ConsoleState {

	private static ConsoleState consoleState;

	private int line = -1;
	private int lineOffset = -1;
	private int lineLenght = -1;
	private int textOffset = -1;
	private int textLenght = -1;
	private int documentLenght = -1;

	private boolean sameLine = false;
	private boolean rightArrow = false;

	private ConsoleState() {
	}

	public static synchronized ConsoleState getInstance() {
		if (consoleState == null)
			consoleState = new ConsoleState();
		return consoleState;
	}

	public void setStartLine(final int line) {
		if (this.line == line)
			sameLine = true;
		else
			sameLine = false;
		this.line = line;
	}

	public int getStartLine() {
		return line;
	}

	public void setLineOffset(final int lineOffset) {
		this.lineOffset = lineOffset;
	}

	public int getLineOffset() {
		return lineOffset;
	}

	public void setLineLenght(final int lineLenght) {
		this.lineLenght = lineLenght;
	}

	public int getLineLenght() {
		return lineLenght;
	}

	public void setTextOffset(final int textOffset) {
		if (this.textOffset < textOffset)
			rightArrow = true;
		else
			rightArrow = false;
		this.textOffset = textOffset;
	}

	public int getTextOffset() {
		return textOffset;
	}

	public void setTextLenght(final int textLenght) {
		this.textLenght = textLenght;
	}

	public int getTextLenght() {
		return textLenght;
	}

	public void setDocumentLenght(final int documentLenght) {
		this.documentLenght = documentLenght;
	}

	public int getDocumentLenght() {
		return documentLenght;
	}

	public boolean isSameLine() {
		return sameLine;
	}

	public boolean isRightArrow() {
		return rightArrow;
	}

}
