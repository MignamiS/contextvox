package org.contextvox.IDEobserver.listeners.selections;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.listeners.selections.state.ConsoleState;
import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.preferences.PreferenceConstants;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.debug.internal.ui.views.console.ProcessConsole;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.internal.console.ConsoleView;

@SuppressWarnings("restriction")
public class ConsoleSelection {

	private final ConsoleView consoleView;
	private IDocument document;

	public ConsoleSelection(final ConsoleView consoleView) {
		this.consoleView = consoleView;
	}

	public void execute(final ISelection selection) {
		final IConsole console = consoleView.getConsole();
		final ProcessConsole processConsole = (ProcessConsole) console;
		if (processConsole != null)
			document = processConsole.getDocument();
		updateJavaEditorState(selection);
		updateContext(selection);
		manageSpeech(selection);
	}

	private void updateJavaEditorState(final ISelection selection) {
		final ITextSelection textSelection = (ITextSelection) selection;
		final int textLenght = textSelection.getLength();
		final int textOffset = textSelection.getOffset();
		final int startLine = textSelection.getStartLine();
		ConsoleState.getInstance().setTextLenght(textLenght);
		ConsoleState.getInstance().setTextOffset(textOffset);
		ConsoleState.getInstance().setStartLine(startLine);
		try {
			final int lineOffset = document.getLineOffset(startLine);
			final int lineLenght = document.getLineLength(startLine);
			final int documentLenght = document.getLength();
			ConsoleState.getInstance().setLineOffset(lineOffset);
			ConsoleState.getInstance().setLineLenght(lineLenght);
			ConsoleState.getInstance().setDocumentLenght(documentLenght);
		} catch (final BadLocationException e) {
			e.printStackTrace();
		}
	}

	private void updateContext(final ISelection selection) {
		final ITextSelection textSelection = (ITextSelection) selection;
		final CompilationUnit root = getActiveEditorClassRoot();
		final int offset = textSelection.getOffset();
		final int lenght = textSelection.getLength();
		final ASTNode node = NodeFinder.perform(root, offset, lenght);
		Context.getInstance().setStartNode(node);
	}

	private IEditorPart getActiveEditor() {
		final Activator activator = Activator.getDefault();
		final IWorkbench workbench = activator.getWorkbench();
		final IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		if (window != null) {
			final IWorkbenchPage page = window.getActivePage();
			if (page != null)
				return page.getActiveEditor();
		}
		return null;
	}

	private ITypeRoot getJavaInput(final IEditorPart part) {
		final IEditorInput editorInput = part.getEditorInput();
		if (editorInput != null) {
			final IJavaElement input = JavaUI.getEditorInputJavaElement(editorInput);
			if (input instanceof ITypeRoot)
				return (ITypeRoot) input;
		}
		return null;
	}

	// get content to Editor
	private CompilationUnit getActiveEditorClassRoot() {
		final ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final ITypeRoot input = getJavaInput(getActiveEditor());
		if (input instanceof ICompilationUnit)
			parser.setSource((ICompilationUnit) input);
		else
			parser.setSource((IClassFile) input);
		parser.setResolveBindings(true);
		return (CompilationUnit) parser.createAST(null);
	}

	private void manageSpeech(final ISelection selection) {
		if (!isDynamic())
			return;
		try {
			readLine();
		} catch (final Exception e) {
		}
	}

	private boolean isDynamic() {
		final Activator activator = Activator.getDefault();
		final IPreferenceStore preferenceStore = activator.getPreferenceStore();
		final String dynamicReader = PreferenceConstants.DYNAMIC_READER_KEY;
		return preferenceStore.getBoolean(dynamicReader);
	}

	private void readLine() throws BadLocationException {
		final int lineOffset = ConsoleState.getInstance().getLineOffset();
		final int lineLenght = ConsoleState.getInstance().getLineLenght();
		String message = document.get(lineOffset, lineLenght);
		message = switchCase(message);
		Activator.getServiceHandler().getTTSService().read(message, MessageSource.CONSOLE);
	}

	private String switchCase(final String message) {
		if (isEmptyLine(message))
			return Messages.getSingleMessage("emptyLine");
		return Activator.getServiceHandler().getReplacerService().replaceSymbols(message);
	}

	private boolean isEmptyLine(final String message) {
		for (int i = 0; i < message.length(); i++) {
			if (!Character.isWhitespace(message.charAt(i)))
				return false;
		}
		return true;
	}

}