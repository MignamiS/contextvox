package org.contextvox.IDEobserver.listeners.selections;

import org.contextvox.Activator;
import org.contextvox.core.nativeVox.translator.Symbol;
import org.contextvox.plugin.PluginElements;
import org.eclipse.core.resources.IMarker;
import org.eclipse.ui.internal.views.markers.ProblemsView;

@SuppressWarnings("restriction")
public class ProblemsSelection {

	private static ProblemsSelection problemsSelection = null;

	private ProblemsSelection() {
	}

	public static synchronized ProblemsSelection getInstance() {
		if (problemsSelection == null)
			problemsSelection = new ProblemsSelection();
		return problemsSelection;
	}

	public String getResourceName() {
		final ProblemsView problemsView = PluginElements.getProblems();
		final IMarker[] markers = problemsView.getSelectedMarkers();
		if (markers.length == 0)
			return Symbol.spaceStr;
		return Activator.getServiceHandler().getReplacerService().replaceSymbols(markers[0].getResource().getName());
	}

}
