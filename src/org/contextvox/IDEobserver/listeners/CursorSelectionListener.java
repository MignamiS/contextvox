package org.contextvox.IDEobserver.listeners;

import org.contextvox.Activator;
import org.contextvox.plugin.PluginElements;
import org.contextvox.preferences.PreferenceConstants;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.ui.javaeditor.EditorUtility;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Class which listen the change in outline and change the selection on the
 * Package Explorer for coherence with the cursor on editor
 */
@SuppressWarnings("restriction")
public class CursorSelectionListener implements ISelectionChangedListener {

	private static CursorSelectionListener instance;

	private CursorSelectionListener() {
	}

	public static synchronized CursorSelectionListener getInstance() {
		if (instance == null)
			instance = new CursorSelectionListener();
		return instance;
	}

	public void start() {
		PluginElements.getOutline().addSelectionChangedListener(this);
	}

	public void stop() {
		PluginElements.getOutline().removeSelectionChangedListener(this);
	}

	@Override
	public void selectionChanged(final SelectionChangedEvent arg0) {

		final IEditorPart activeEditor = PluginElements.getActiveEditor();
		if (activeEditor == null || !(activeEditor instanceof JavaEditor))
			return;

		final ITypeRoot rootJavaElement = EditorUtility.getEditorInputJavaElement(activeEditor, false);

		final ITextSelection treeSelection = (ITextSelection) ((ITextEditor) activeEditor).getSelectionProvider()
				.getSelection();

		IJavaElement element;
		try {
			element = rootJavaElement.getElementAt(treeSelection.getOffset());

			// check if is necessary to expand Package Expl. hierarchy while
			// navigating
			final boolean compulsivePackageExplorerExpander = isCompulsiveExpander();
			if (compulsivePackageExplorerExpander && element != null) {
				PluginElements.getPackageExplorer().selectAndReveal(element);
			} else {
				return;
			}

		} catch (final JavaModelException e) {
			return;
		}
	}

	/*
	 * Look in Preferences if it is necessary to expand all elements in Package
	 * Explorer when visiting each in code
	 */
	private boolean isCompulsiveExpander() {
		final Activator activator = Activator.getDefault();
		if (activator == null) {
			return false; // default value, do not expande package expl.
		}
		final IPreferenceStore preferenceStore = activator.getPreferenceStore();
		return preferenceStore.getBoolean(PreferenceConstants.COMPULSIVE_EXPANDER_KEY);
	}

}
