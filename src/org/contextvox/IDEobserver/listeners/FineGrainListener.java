package org.contextvox.IDEobserver.listeners;

import java.lang.reflect.Field;

import org.contextvox.Activator;
import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.OperationType;
import org.contextvox.plugin.requests.SpecialRequest;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.views.contentoutline.ContentOutline;

/**
 * This listener provides awareness of fine-grain events such keyboard input. Is
 * inserted directly on the SWT's main loop.
 */
public class FineGrainListener implements Listener, IDEListener {

	private boolean connected;
	private final StimulusReceiver receiver;

	public FineGrainListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void handleEvent(final Event event) {

		switch (event.type) {
		case SWT.Selection:
			// processSelection(event);
			break;

		case SWT.KeyDown:
			extracted(event);
			break;

		case SWT.KeyUp:
			extracted(event);
			break;
		}

	}

	private void extracted(final Event event) {
		final Field[] interfaceFields = SWT.class.getFields();
		for (final Field f : interfaceFields) {
			if (f.getType().equals(int.class))
				try {
					final int c = f.getInt(SWT.class);
					if (event.keyCode == c)
						System.out.println("key " + f.getName());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}

		}
		System.out.println();
	}

	private void processSelection(final Event event) {
		// avoid request production on special views
		if (!focusMonitor())
			return;
		// filter selection
		final Widget widget = event.widget;
		if (widget instanceof Tree) {
			final NavigationRequest request = new NavigationRequest(MessageSource.TREE);
			request.setOperation(OperationType.TREEITEM_SELECTED);
			request.widget = widget;
			this.receiver.processRequest(request);
		}
	}

	/**
	 * Checks if the part has already a listener
	 *
	 * @return <code>true</code> if the focused view (part) does not has already
	 *         a listener
	 */
	@SuppressWarnings("restriction")
	private boolean focusMonitor() {
		/*
		 * FIXME probably the check won't be accurate because. although the part
		 * is active, a dialog may has the focus, as explained here:
		 * https://wiki.eclipse.org/
		 * FAQ_How_do_I_find_out_what_view_or_editor_is_selected%3F
		 */
		final IWorkbenchPart part = PluginElements.getActivePart();
		if (part instanceof PackageExplorerPart || part instanceof ContentOutline)
			return false;
		return true;
	}

	private void processKeyboardInput(final Event event) {
		// TODO refactor
		/*
		 * This method is responsible for every event on the editor, except for
		 * selection event.
		 */
		if (!Activator.getDefault().contextVox().isVoiceEnabled())
			return;

		// intercept quick stop command
		if (event.type == SWT.KeyDown && event.keyCode == SWT.CTRL)
			this.receiver.processRequest(SpecialRequest.buildStopRequest(MessageSource.UI_ELEMENT));

		// TODO insert a timestamp threshold
		final IWorkbench wb = PlatformUI.getWorkbench();
		final IWorkbenchWindow wbw = wb.getActiveWorkbenchWindow();
		final IWorkbenchPage ap = wbw.getActivePage();
		final IWorkbenchPart part = ap.getActivePart();
		// if the java editor is selected, trigger the analysis state
		if (part instanceof ITextEditor) {
			// TODO insert boolean flag to check if it is already registered.
			// register document listener
			final ITextEditor te = (ITextEditor) part;
			final IDocumentProvider provider = te.getDocumentProvider();
			final IEditorInput editorInput = te.getEditorInput();
			final IDocument doc = provider.getDocument(editorInput);
			// doc.addDocumentListener(JavaDocumentModificationListener.getInstance());

			// filter event
			if (filter(event)) {
				// motion
				updateEditorStateMotion(event, (ITextSelection) ap.getSelection(), doc, (ITextEditor) part);
				final AnalyzeRequest request = new AnalyzeRequest(MessageSource.EDITOR);
				request.setEditor((ITextEditor) part);
				request.setEvent(event);
				request.setSelection(ap.getSelection());
				this.receiver.processRequest(request);
			}
		} else {
			// do not read anything
			// CoreProxy.free();
		}
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public boolean start() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		// final IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		// final IWorkbenchPage workbenchPage = window.getActivePage();
		// workbenchPage.addSelectionListener(this);

		// register the punctual event listener
		final Display display = workbench.getDisplay();
		display.addFilter(SWT.KeyUp, this);
		display.addFilter(SWT.KeyDown, this);
		// display.addFilter(SWT.KeyDown, this);
		// TODO check if key up is necessary
		// display.addFilter(SWT.KeyUp, this);
		// display.addFilter(SWT.Selection, this);
		this.connected = true;
		return true;
	}

	@Override
	public boolean stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		// final IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		// final IWorkbenchPage workbenchPage = window.getActivePage();
		// workbenchPage.removeSelectionListener(this);
		final Display display = workbench.getDisplay();
		display.removeFilter(SWT.KeyDown, this);
		display.removeFilter(SWT.KeyUp, this);
		display.removeFilter(SWT.Selection, this);
		this.connected = false;
		return true;
	}

	// check if the event should be forwarded
	private boolean filter(final Event event) {
		if (event.keyCode == SWT.ARROW_UP || event.keyCode == SWT.ARROW_DOWN || event.keyCode == SWT.ARROW_RIGHT
				|| event.keyCode == SWT.ARROW_LEFT)
			return true;
		else if (event.keyCode == SWT.PAGE_UP || event.keyCode == SWT.PAGE_DOWN)
			return true;
		else
			return false;
	}

	private void updateEditorStateMotion(final Event event, final ITextSelection selection, final IDocument document,
			final ITextEditor editor) {
		try {
			final int textOffset = selection.getOffset();
			final int startLine = selection.getStartLine();
			final int lineOffset = document.getLineOffset(startLine);
			final int lineLenght = document.getLineLength(startLine);
			final int documentLenght = document.getLength();

			// System.out.println(String.format(
			// "text offset: %d%nstart line: %d%nline offset: %d%nline length:
			// %d%ndocument length: %d",
			// textOffset, startLine, lineOffset, lineLenght, documentLenght));

			// final MotionEvent state = new MotionEvent(startLine, lineOffset,
			// lineLenght, textOffset, documentLenght,
			// event);
			// ContextState.addJavaEditorEvent(editor, state);
		} catch (final BadLocationException e) {
			// VoxLogger.log(LogLevel.ERROR, "Error accessing the document with
			// the given start line", e);
		}
	}

}