package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.OperationType;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;

/**
 * Notifies Package Explorer navigation.
 */
@SuppressWarnings("restriction")
public class PackageExplorerListener implements ITreeViewerListener, ISelectionChangedListener, IDEListener {

	private boolean connected;
	// where to pass requests
	private final StimulusReceiver receiver;
	private PackageExplorerPart packageExplorer;

	public PackageExplorerListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public void selectionChanged(final SelectionChangedEvent e) {
		/*
		 * Added to avoid reaction when the package explorer is not selected
		 * (e.g. the package explorer is expanded by the
		 * ChangeSelectionOnPackageExplorerListener)
		 */
		if (!PluginElements.getPackageExplorer().getTreeViewer().getTree().isFocusControl())
			return;

		final NavigationRequest request = new NavigationRequest(MessageSource.PACKAGE_EXPLORER);
		request.setOperation(OperationType.TREEITEM_SELECTED);
		request.setSelectionChangeEvent(e);
		receiver.processRequest(request);
	}

	@Override
	public boolean start() {
		packageExplorer = PluginElements.getPackageExplorer();
		if (packageExplorer == null)
			return false;
		final TreeViewer treeViewer = packageExplorer.getTreeViewer();
		treeViewer.addTreeListener(this);
		treeViewer.addSelectionChangedListener(this);
		this.connected = true;
		return true;
	}

	@Override
	public boolean stop() {
		if (this.packageExplorer == null)
			return false;
		final TreeViewer treeViewer = packageExplorer.getTreeViewer();
		treeViewer.removeTreeListener(this);
		treeViewer.removeSelectionChangedListener(this);
		this.connected = false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.jface.viewers.ITreeViewerListener#treeCollapsed(org.eclipse
	 * .jface.viewers.TreeExpansionEvent)
	 */
	@Override
	public void treeCollapsed(final TreeExpansionEvent e) {
		if (!packageExplorer.getTreeViewer().getTree().isFocusControl())
			return;

		final NavigationRequest request = NavigationRequest.buildTreeExpansionRequest(MessageSource.PACKAGE_EXPLORER, e,
				false);
		this.receiver.processRequest(request);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.jface.viewers.ITreeViewerListener#treeExpanded(org.eclipse
	 * .jface.viewers.TreeExpansionEvent)
	 */
	@Override
	public void treeExpanded(final TreeExpansionEvent e) {
		if (!this.packageExplorer.getTreeViewer().getTree().isFocusControl())
			return;

		final NavigationRequest request = NavigationRequest.buildTreeExpansionRequest(MessageSource.PACKAGE_EXPLORER, e,
				true);
		this.receiver.processRequest(request);
	}

}