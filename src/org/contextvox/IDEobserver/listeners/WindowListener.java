package org.contextvox.IDEobserver.listeners;

import org.contextvox.IDEobserver.helpers.WidgetsHelper;
import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * Tracks focus motion on the active window (shell). The window may be the
 * principal or a dialog.
 */
public class WindowListener implements Listener, IDEListener {

	private boolean connected;
	private Display display;
	private Shell lastShell = new Shell();
	private final StimulusReceiver receiver;

	public WindowListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void handleEvent(final Event event) {
		final Shell shell = display.getActiveShell();
		// notify only the first time
		if (!lastShell.equals(shell)) {
			MessageSource source;
			if (shell.getParent() == null)
				// principal window
				source = MessageSource.WINDOW;
			else
				// is a dialog
				source = MessageSource.DIALOG;

			receiver.processRequest(new VoxRequest(source));
		}
		lastShell = shell;
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public boolean start() {
		display = WidgetsHelper.getDisplay();
		display.addListener(SWT.FocusIn, this);
		this.connected = true;
		return true;
	}

	@Override
	public boolean stop() {
		if (display == null)
			display = PlatformUI.getWorkbench().getDisplay();
		display.removeListener(SWT.FocusIn, this);
		this.connected = false;
		return true;
	}

}
