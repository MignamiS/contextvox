package org.contextvox.IDEobserver.listeners;

import java.util.ArrayList;

import org.contextvox.contextualizer.commandLine.commandlinev2.CommandExecutor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;

/**
 * Listener attached to a document to track modifications on it, used by the
 * Spot Changes command. Each instance continue to spot changes until the report
 * is get, then no more event can be spotted and this object becomes read-only.
 * 
 * @see CommandExecutor#spotChanges()
 */
public class SpotChangesListener implements IDocumentListener {

	private static final String ADDED = "added: ";
	private static final String DELETED = "deleted: ";
	private static final int MAX_TEXT_SIZE = 80;
	private static final int MAX_HISTORY_SIZE = 30;

	private final IDocument owner;

	private final ArrayList<Record> history = new ArrayList<>();
	// counts events that exceed the max history siza
	private int oversize = 0;
	private String cachedReport = null;

	// when true no more event can be spotted, read-only
	private boolean lock = false;

	public SpotChangesListener(IDocument owner) {
		this.owner = owner;
	}

	/**
	 * Detaches this listener from its target document.
	 */
	public void detach() {
		if (this.owner != null)
			this.owner.removeDocumentListener(this);
		;
	}

	@Override
	public void documentChanged(DocumentEvent event) {
	}

	@Override
	public void documentAboutToBeChanged(DocumentEvent event) {
		if (lock)
			return;

		// do not insert other events when history size exceeded
		if (history.size() < MAX_HISTORY_SIZE) {
			// is adding or deleting text?
			final boolean deleting = event.getText().equals("");
			final String text = getText(event, deleting);

			if (history.size() == 0) {
				// store first event
				final Record rec = new Record(deleting);
				rec.add(text);
				history.add(rec);
			} else {
				// look for preexisting records
				final Record last = history.get(history.size() - 1);
				if (deleting == last.isDeleting() && text.length() == 1) {
					// concatenate same events
					last.add(text);
				} else {
					// store new record
					final Record rec = new Record(deleting);
					rec.add(text);
					history.add(rec);
				}
			}
		} else {
			oversize++;
		}
	}

	private String getText(DocumentEvent event, boolean deleting) {
		String text = "";
		if (deleting) {
			final int offset = event.getOffset();
			final int len = event.getLength();
			text = "";
			try {
				text = event.getDocument().get(offset, len);
			} catch (final BadLocationException e) {
				e.printStackTrace();
			}
		} else {
			text = event.getText();
		}

		return text;
	}

	/**
	 * Produces a printable report of the first n change event spotted on the
	 * document. This method locks this instance. Further call of this method
	 * will produce the same report.
	 * 
	 * @return the printable String containing events separated by a new line
	 */
	public String getSpottedChanges() {
		if (cachedReport == null) {
			lock = true;
			final StringBuilder sb = new StringBuilder();
			for (final Record item : history) {
				sb.append(String.format("%s%n", item.toString()));
			}
			if (oversize > 0)
				sb.append(String.format("And other %d events", oversize));
			cachedReport = sb.toString();
		}
		return cachedReport;
	}

	/**
	 * History record
	 */
	private class Record {
		private final boolean deleting;
		private final StringBuilder sb;

		public Record(boolean deleting) {
			this.deleting = deleting;
			this.sb = new StringBuilder();
		}

		public void add(String s) {
			this.sb.append(s);
		}

		public boolean isDeleting() {
			return this.deleting;
		}

		@Override
		public String toString() {
			// convert into a sentence and store it
			String message = "";
			String text = sb.toString();
			text = reduceString(text);
			if (this.deleting)
				message = DELETED + text;
			else
				message = ADDED + text;
			return message;

		}

		// reduce string size and escape some character
		private String reduceString(String str) {
			// reduce size
			if (str.length() > MAX_TEXT_SIZE) {
				final int oversize = str.length() - MAX_TEXT_SIZE;
				str = str.substring(0, MAX_TEXT_SIZE);
				str += String.format(" plus other %d characters", oversize);
			}
			// escape
			str = str.replaceAll("\\r\\n|\\r|\\n|\\t", " ");
			// removed multiple whitespaces
			str = str.replaceAll("\\s+", " ");
			return str;
		}
	}
}
