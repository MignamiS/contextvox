package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * This listener is activated when the cursor browses a menu.
 */
public class MenuBarListener implements Listener, IDEListener {

	private boolean connected;

	private final StimulusReceiver receiver;

	public MenuBarListener(final StimulusReceiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void handleEvent(final Event event) {
		final NavigationRequest request = new NavigationRequest(MessageSource.MENU, null, event);
		receiver.processRequest(request);
	}

	@Override
	public boolean isConnected() {
		return this.connected;
	}

	@Override
	public boolean start() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.addFilter(SWT.Arm, this);
		this.connected = true;
		return true;
	}

	@Override
	public boolean stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.removeFilter(SWT.Arm, this);
		this.connected = false;
		return true;
	}

}