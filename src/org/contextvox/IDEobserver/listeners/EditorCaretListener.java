package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.StimulusReceiver;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.swt.custom.CaretEvent;
import org.eclipse.swt.custom.CaretListener;
import org.eclipse.ui.texteditor.AbstractTextEditor;

/**
 * Listen to caret motion inside a document.
 *
 * @author Simone Mignami
 *
 */
public final class EditorCaretListener implements CaretListener, IDocumentListener {

	private final StimulusReceiver receiver;
	private final AbstractTextEditor editor;

	public EditorCaretListener(final StimulusReceiver receiver, final AbstractTextEditor editor) {
		this.receiver = receiver;
		this.editor = editor;
	}

	@Override
	public void caretMoved(final CaretEvent event) {
		System.out.println("move " + System.currentTimeMillis());

		final int offset = event.caretOffset;
		final AnalyzeRequest request = new AnalyzeRequest(MessageSource.EDITOR);
		request.setEditor(editor);
		request.setOffset(offset);
		this.receiver.processRequest(request);
	}

	@Override
	public void documentAboutToBeChanged(final DocumentEvent event) {
		System.out.println("change " + System.currentTimeMillis());

		boolean delete = false;
		String text = "";

		// determine if the user is writing or deleting text
		if (event.getText() == "") {
			// deleting
			final int offset = event.getOffset();
			final int len = event.getLength();
			text = null;
			try {
				text = event.getDocument().get(offset, len);
				delete = true;
			} catch (final BadLocationException e) {
				e.printStackTrace();
			}
		} else {
			// writing
			text = event.getText();
			delete = false;
		}

		final AnalyzeRequest request = new AnalyzeRequest(MessageSource.EDITOR_TYPING);
		request.setTextDeleted(delete);
		request.setEditor(editor);
		this.receiver.processRequest(request);
	}

	@Override
	public void documentChanged(final DocumentEvent event) {
	}
}