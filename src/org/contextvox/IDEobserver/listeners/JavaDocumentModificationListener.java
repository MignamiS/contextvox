package org.contextvox.IDEobserver.listeners;

import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Observers changement on the document.
 */
public class JavaDocumentModificationListener implements IDocumentListener {
	// TODO remove it

	private static JavaDocumentModificationListener singleton;

	public static JavaDocumentModificationListener getInstance() {
		if (singleton == null)
			singleton = new JavaDocumentModificationListener();
		return singleton;
	}

	@Override
	public void documentAboutToBeChanged(final DocumentEvent event) {
		boolean delete = false;
		String text = "";

		// determine if the user is writing or deleting text
		if (event.getText() == "") {
			// deleting
			final int offset = event.getOffset();
			final int len = event.getLength();
			text = null;
			try {
				text = event.getDocument().get(offset, len);
				delete = true;
			} catch (final BadLocationException e) {
				// TODO print some debug error
			}
		} else {
			// writing
			text = event.getText();
			delete = false;
		}

		// update java editor state with modification event
		final IWorkbench wb = PlatformUI.getWorkbench();
		final IWorkbenchWindow wbw = wb.getActiveWorkbenchWindow();
		final IWorkbenchPage ap = wbw.getActivePage();
		final IWorkbenchPart part = ap.getActivePart();
		final ITextEditor editor = (ITextEditor) part;
		final ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
		// final ModificationEvent state = new ModificationEvent(event, text,
		// delete);
		// FIXME use the new textual cont. service, set the new event with a
		// request
		// ContextState.addJavaEditorEvent(editor, state);

		// send request
		// FIXME something wrong when creating the request, some reference is
		// missing.
		final AnalyzeRequest request = new AnalyzeRequest(MessageSource.EDITOR_TYPING);
		request.setTextDeleted(delete);
		// request.setEvent(event);
		request.setSelection(selection);
		request.setEditor(editor);
		ContextVoxProgram.readFocus(request);
	}

	@Override
	public void documentChanged(final DocumentEvent event) {
	}

}
