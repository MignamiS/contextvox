package org.contextvox.IDEobserver.listeners;

import java.util.HashMap;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.preferences.PreferenceConstants;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.JavaCore;

/**
 * Class which allows you to listen the changes of the Java elements
 */
public class JavaChangeListener implements IElementChangedListener {
	// FIXME not working properly.
	// For the moment this listener is inactive, because generates annoying
	// feedbacks when writing code. May be useful later when dealing with
	// refactors, to notify the user about the changes introduced by hers/his
	// actions.

	/**
	 * The shared instance
	 */
	private static JavaChangeListener instance;

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static JavaChangeListener getInstance() {
		if (instance == null)
			instance = new JavaChangeListener();
		return instance;
	}

	/**
	 * The constructor, private for the singleton pattern
	 */
	private JavaChangeListener() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.jdt.core.IElementChangedListener#elementChanged(org.eclipse
	 * .jdt.core.ElementChangedEvent)
	 */
	@Override
	public void elementChanged(final ElementChangedEvent event) {
		// enabled in preferences?
		final boolean jc = Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.NOTIFY_JAVACHANGE_KEY);
		if (!jc)
			return;

		final IJavaElementDelta delta = event.getDelta();
		final IJavaElementDelta[] children = delta.getAffectedChildren();
		final Job job = new Job("") {
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				for (int i = 0; i < children.length; i++) {
					traverseAndPrint(children[i]);
				}
				return Status.OK_STATUS;
			}
		};
		job.setPriority(Job.SHORT);
		job.schedule();
	}

	/**
	 * This method starts itself
	 */
	public void start() {
		JavaCore.addElementChangedListener(this);
	}

	/**
	 * This method shutdowns itself
	 */
	public void stop() {
		JavaCore.removeElementChangedListener(this);
	}

	/**
	 * @param delta
	 *            recursive method for traverse the java element that are
	 *            changed
	 */
	private void traverseAndPrint(final IJavaElementDelta delta) {
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		String text = "";
		boolean check = true;
		final int elementType = delta.getElement().getElementType();

		switch (delta.getKind()) {
		case IJavaElementDelta.CHANGED:
			/*
			 * if ((delta.getFlags() & IJavaElementDelta.F_CHILDREN) != 0) { sb
			 * = new StringBuilder(); } if ((delta.getFlags() &
			 * IJavaElementDelta.F_CONTENT) != 0) { sb = new StringBuilder(); }
			 */
			/* Others flags can also be checked */
			check = false;
			break;
		case IJavaElementDelta.REMOVED:
			data.put(SentenceToken.NAME, delta.getElement().getElementName());
			data.put(SentenceToken.STATUS, Messages.getSingleMessage("removed"));
			break;

		case IJavaElementDelta.ADDED:
			data.put(SentenceToken.NAME, delta.getElement().getElementName());
			data.put(SentenceToken.STATUS, Messages.getSingleMessage("added"));
			break;
		}

		switch (elementType) {
		case IJavaElement.COMPILATION_UNIT:
			text = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FILE, data);
			break;
		case IJavaElement.TYPE:
			text = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data);
			break;
		case IJavaElement.METHOD:
			text = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.METHOD, data);
			break;
		case IJavaElement.FIELD:
			text = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FIELD, data);
			break;
		default:
			check = false;
			break;
		}

		if (check)
			Activator.getServiceHandler().getTTSService().read(text, MessageSource.JAVACHANGE_EVENT);

		final IJavaElementDelta[] children = delta.getAffectedChildren();
		for (int i = 0; i < children.length; i++) {
			traverseAndPrint(children[i]);
		}

	}

}