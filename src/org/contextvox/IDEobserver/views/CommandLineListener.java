package org.contextvox.IDEobserver.views;

import java.io.StringReader;
import java.util.ArrayList;

import org.contextvox.Activator;
import org.contextvox.contextualizer.commandLine.parser.Parser;
import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.requests.MessageSource;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.widgets.Display;

/**
 * Class which listen to the command line, it says every word (after space),
 * every command (after enter). It executes the command and says the answer of
 * the command. It keeps the entire list of the commands which are executed.
 */
public class CommandLineListener implements KeyListener {
	// TODO when the new CommandLineKeyListenr is build, delete this file

	/**
	 * List of commands which are executed
	 */
	private final ArrayList<String> commandList;
	/**
	 * Counter for the accumulators
	 */
	private int counter;
	/**
	 * Accumulator for the last word, for the synthesizer
	 */
	private StringBuilder wordAccumulator;

	/**
	 * The constructor, reset every field
	 */
	public CommandLineListener() {
		counter = 0;
		commandList = new ArrayList<>();
		wordAccumulator = new StringBuilder();
		new StringBuilder();
	}

	/**
	 * It is executed when Enter is pressed, it update the accumulator.
	 */
	private void checkToSpeechCR() {
		new StringBuilder(PluginElements.getContextVoxView().getCommandLineText());
		wordAccumulator = new StringBuilder();
		new StringBuilder();
	}

	/**
	 * It is executed when Space is pressed, it update the accumulator.
	 */
	private void checkToSpeechSPACE() {
		counter++;
		new StringBuilder(PluginElements.getContextVoxView().getCommandLineText());
		Activator.getServiceHandler().getTTSService().read(wordAccumulator.toString(), MessageSource.COMMANDLINE);
		wordAccumulator = new StringBuilder();
	}

	/**
	 * It is executed when Enter is pressed, it executes the right command and
	 * update the Speacking view. It send the sentences to say to the
	 * SpeakingHandler.
	 */
	private void executeCommand() {
		final ContextVoxView contextVoxView = PluginElements.getContextVoxView();
		final String commandLineText = contextVoxView.getCommandLineText();
		if (!commandLineText.equals("")) {

			final int actualOffset = contextVoxView.getCommandAreaOffset();
			final Device device = Display.getCurrent();
			final Color red = new Color(device, 165, 0, 0);

			final StyleRange[] tmpRanges = contextVoxView.getCommandAreaStyleRanges();
			final StyleRange[] ranges = new StyleRange[tmpRanges.length + 1];
			for (int i = 0; i < tmpRanges.length; i++) {
				ranges[i] = tmpRanges[i];
			}
			contextVoxView.writeOnConsole(commandLineText);

			final StyleRange styleRange = new StyleRange();
			styleRange.start = actualOffset;
			styleRange.length = commandLineText.length();
			styleRange.fontStyle = SWT.BOLD;
			styleRange.foreground = red;
			ranges[ranges.length - 1] = styleRange;

			contextVoxView.setOutputAreaStyleRanges(ranges);
			contextVoxView.resetInputArea();

			final StyleRange[] textStyleRanges = contextVoxView.getCommandAreaStyleRanges();

			// REMARK next line commented because using advanced feature
			// final CommandParser parser = new CommandParser(new
			// ByteArrayInputStream(commandLineText.getBytes()));

			String rensponse = "";
			try {

				// REMARK next line commented because using advanced feature
				// rensponse = parser.parse();
				rensponse = new Parser(new StringReader(commandLineText)).parse().eval();

				// REMARK next line commented because useless
				// contextVoxView.addTextOnCommandArea(rensponse);
				contextVoxView.setOutputAreaStyleRanges(textStyleRanges);
				commandList.add(commandLineText);
				if (!rensponse.equals("")) {
					Activator.getServiceHandler().getTTSService().read(rensponse, MessageSource.COMMANDLINE);
				}

				// REMARK next lines commented because using advanced feature
				/*
				 * Messages.traduceText(commandLineText + "." // commandLineText
				 * + "." + System.getProperty("line.separator") + rensponse); }
				 * catch (final ParseException e) {
				 * SpeakingHandler.getInstance()
				 * .addToQueue(Messages.syntaxError); }
				 */
			} catch (final Exception e) {
				// REMARK next line commented because using advanced feature
				// SpeakingHandler.getInstance().addToQueue(Messages.syntaxError);
				Activator.getServiceHandler().getTTSService().read(e.getMessage(), MessageSource.COMMANDLINE);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.swt.events.KeyListener#keyPressed(org.eclipse.swt.events.
	 * KeyEvent)
	 */
	@Override
	public void keyPressed(final KeyEvent event) {
		final ContextVoxView contextVoxView = PluginElements.getContextVoxView();
		switch (event.keyCode) {
		case SWT.CR:
			checkToSpeechCR();
			executeCommand();
			counter = commandList.size();
			break;
		case SWT.SPACE:
			checkToSpeechSPACE();

			break;
		case SWT.ARROW_UP:
			if (counter > 0) {
				counter--;
				contextVoxView.setInputLineText(commandList.get(counter));
				Activator.getServiceHandler().getTTSService().read(commandList.get(counter), MessageSource.COMMANDLINE);
			}

			break;
		case SWT.ARROW_DOWN:
			if (counter < commandList.size() - 1) {
				counter++;
				contextVoxView.setInputLineText(commandList.get(counter));
				Activator.getServiceHandler().getTTSService().read(commandList.get(counter), MessageSource.COMMANDLINE);
			}

			break;
		default:
			wordAccumulator.append(event.character);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events
	 * .KeyEvent)
	 */
	@Override
	public void keyReleased(final KeyEvent event) {
		switch (event.keyCode) {
		case SWT.BS:
			final String tmpStr = PluginElements.getContextVoxView().getCommandLineText();
			wordAccumulator = new StringBuilder(tmpStr.substring(tmpStr.lastIndexOf(' ') + 1, tmpStr.length()));
			break;
		}
	}

}
