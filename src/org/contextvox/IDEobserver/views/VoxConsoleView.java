package org.contextvox.IDEobserver.views;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.PluginElements;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.part.ViewPart;

/**
 * Console that prints messages from ContextVox, for debug or demos purpose.
 */
public class VoxConsoleView extends ViewPart {

	private static final String SPACE_REPLACE = "-";
	private StyledText consoleArea;
	// model
	private static final List<String> consoleBuffer;
	private static final int CONSOLEBUFFER_SIZE = 100;

	static {
		consoleBuffer = Collections.synchronizedList(new LinkedList<String>());
	}

	@Override
	public void createPartControl(final Composite parent) {
		final GridLayout grid = new GridLayout();
		// final GridData horizontalFill = new GridData(SWT.FILL, SWT.FILL,
		// true,
		// false);
		final GridData fill = new GridData(SWT.FILL, SWT.FILL, true, true);
		parent.setLayout(grid);

		consoleArea = new StyledText(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY | SWT.WRAP);
		consoleArea.setText("Vox Console\n");
		consoleArea.setLayoutData(fill);
		// set font
		final FontData[] data = consoleArea.getFont().getFontData();
		data[0].setHeight(14);
		consoleArea.setFont(new Font(Display.getCurrent(), data[0]));

		// get data from buffer
		for (final String item : consoleBuffer)
			consoleArea.append(item);
	}

	@Override
	public void setFocus() {
		// consoleArea.setFocus();
	}

	public static void writeOnConsole(String text) {
		// process whitespaces for a better views
		if (ContextVoxProgram.isShowWhitespacesOnConsole()) {
			text = text.replaceAll(" ", SPACE_REPLACE);
		}

		// update buffer, keeping size under maximum buffer size
		if (consoleBuffer.size() >= CONSOLEBUFFER_SIZE) {
			consoleBuffer.remove(0);
			consoleBuffer.add(text);
		} else {
			consoleBuffer.add(text);

		}

		// update current view, if enabled
		final VoxConsoleView vcv = PluginElements.getVoxConsoleView();
		if (vcv != null) {
			vcv.consoleArea.append(text);
			vcv.consoleArea.setTopIndex(vcv.consoleArea.getLineCount() - 1);
		}

		// consoleArea.setText(consoleArea.getText()
		// + System.getProperty("line.separator") + text);
	}
}
