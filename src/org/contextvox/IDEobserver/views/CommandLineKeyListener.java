package org.contextvox.IDEobserver.views;

import org.contextvox.Activator;
import org.contextvox.contextualizer.commandLine.commandlinev2.CommandLineParser;
import org.contextvox.contextualizer.commandLine.commandlinev2.ParseException;
import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.requests.MessageSource;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

/**
 * This class will substitute the old CommandLineListener
 */
public class CommandLineKeyListener implements KeyListener {

	// view associated with this listener
	private final ContextVoxView view;

	public CommandLineKeyListener() {
		view = PluginElements.getContextVoxView();
	}

	@Override
	public void keyPressed(final KeyEvent event) {
		// ENTER pressed
		switch (event.keyCode) {
		case SWT.CR:
		case SWT.KEYPAD_CR:
			processCommand(view.getCommandLineText() + '\n');
			break;
		case SWT.ARROW_UP:
			selectCommand(true);
			break;
		case SWT.ARROW_DOWN:
			selectCommand(false);
			break;
		}

		// shortcut
		if (event.keyCode >= '0' && event.keyCode <= '9') {
			if ((event.stateMask & SWT.ALT) == SWT.ALT) {
				final int key = Character.getNumericValue(event.keyCode);
				final String command = PluginElements.getContextVoxView().getShortcut(key);
				if (command != null) {
					processCommand(command + '\n');
				} else {
					final String message = "No shortcut is registered for this combination";
					Activator.getServiceHandler().getTTSService().read(message, MessageSource.COMMANDLINE);
					PluginElements.getContextVoxView().writeOnConsole(message);
				}
			}
		}
	}

	private void selectCommand(final boolean forward) {
		this.view.selectCommand(forward);
	}

	private void processCommand(final String command) {
		try {
			CommandLineParser.parse(command);
		} catch (final ParseException e) {
			final String message = "Error on the command, see the help";
			view.writeOnConsole(message);
			Activator.getServiceHandler().getTTSService().read(message, MessageSource.COMMANDLINE);
			return;
		}

		// update view
		view.resetInputArea();

		// add command to history
		this.view.addCommandToHistory(command);
	}

	@Override
	public void keyReleased(final KeyEvent arg0) {
	}

}
