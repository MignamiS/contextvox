package org.contextvox.IDEobserver.views;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

import org.contextvox.Activator;
import org.contextvox.plugin.requests.MessageSource;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

/**
 * Command line view
 */
public class ContextVoxView extends ViewPart {

	private static final String START_MESSAGE = "Command line started.";

	/** Number of saved commands */
	private static final int HISTORY_SIZE = 30;

	// TextArea for output
	private StyledText outputArea;
	// Field in which the user write the commands
	private Text inputArea;

	// command history
	private final LinkedList<String> commandHistory = new LinkedList<String>();
	private ListIterator<String> historyIterator;

	// shortcut list
	private final String[] shortcuts = new String[10];

	public ContextVoxView() {
		super();
	}

	public void addCommandToHistory(final String command) {
		this.historyIterator = null;
		// trim command removing carriage return
		final String trimmed = command.replaceAll("[\n\r]", "");
		this.commandHistory.addFirst(trimmed);
		// trim history
		if (this.commandHistory.size() > HISTORY_SIZE) {
			this.commandHistory.removeLast();
		}
	}

	/**
	 * Set the command for the given key pression.
	 *
	 * @param key
	 *            from 0 to 9
	 * @param command
	 */
	public void addShortcut(final int key, final String command) {
		if (key >= 0 && key <= 9)
			this.shortcuts[key] = command;
	}

	/**
	 * Clears all the printed text on the output area.
	 */
	public void clearOutputArea() {
		this.outputArea.setText("");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.
	 * widgets .Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {
		final GridLayout gridLay = new GridLayout();
		final GridData horizontalFill = new GridData(SWT.FILL, SWT.FILL, true, false);
		final GridData fill = new GridData(SWT.FILL, SWT.FILL, true, true);
		parent.setLayout(gridLay);

		// text area
		outputArea = new StyledText(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY | SWT.WRAP);

		outputArea.setText(START_MESSAGE);
		outputArea.setLayoutData(fill);

		// command line
		inputArea = new Text(parent, SWT.SINGLE | SWT.BORDER);
		inputArea.setEditable(true);
		inputArea.setText("");
		inputArea.setLayoutData(horizontalFill);
		// inputText.addKeyListener(new CommandLineListener());
		inputArea.addKeyListener(new CommandLineKeyListener());
	}

	/**
	 * Return a list of the registered shortcut.
	 *
	 * @return ArrayList of shortcuts, formatted to be printed.
	 */
	public ArrayList<String> getAllShortcuts() {
		final ArrayList<String> result = new ArrayList<>();
		for (int i = 0; i < this.shortcuts.length; i++) {
			if (this.shortcuts[i] != null)
				result.add(String.format("Alt+%d %s", i, this.shortcuts[i]));
		}
		return result;
	}

	/**
	 * Returns the offset of the command area
	 *
	 * @return the offset of the command area
	 */
	public int getCommandAreaOffset() {
		return outputArea.getText().length() + 1;
	}

	/**
	 * Returns the styles of the command area
	 *
	 * @return the styles of the command area
	 */
	public StyleRange[] getCommandAreaStyleRanges() {
		return outputArea.getStyleRanges();
	}

	/**
	 * Returns the string on the input line.
	 *
	 * @return String
	 */
	public String getCommandLineText() {
		return inputArea.getText();
	}

	/**
	 * Return the last inserted command, that has been successfully parsed.
	 *
	 * @return String the last command or NULL if noone is present.
	 */
	public String getLastCommand() {
		if (this.commandHistory.size() == 0)
			return null;
		else
			return this.commandHistory.getFirst();
	}

	public String getShortcut(final int key) {
		if (key >= 0 && key <= 9)
			return this.shortcuts[key];
		else
			return null;
	}

	/**
	 * Clear the text input field.
	 */
	public void resetInputArea() {
		inputArea.setText("");
	}

	/**
	 * Scroll the command area to the bottom
	 */
	private void scrollCommandAreaBottom() {
		outputArea.setTopIndex(outputArea.getLineCount() - 1);
	}

	/**
	 * Select and show the previous/next command on the history. If there are
	 * not command on the history, nothing will happen.
	 *
	 * @param forward
	 *            indicates whether the next or previous command should be taken
	 */
	public void selectCommand(final boolean forward) {
		if (this.commandHistory.size() == 0)
			return;
		// get the iterator
		if (this.historyIterator == null)
			this.historyIterator = this.commandHistory.listIterator(0);
		// look for the nexte element
		String selected = null;
		if (forward) {
			if (this.historyIterator.hasNext())
				selected = this.historyIterator.next();
		} else {
			if (this.historyIterator.hasPrevious())
				selected = this.historyIterator.previous();
		}

		// display it
		if (selected != null) {
			Activator.getServiceHandler().getTTSService().read(selected, MessageSource.COMMANDLINE);
			this.inputArea.setText(selected);
		}
	}

	/**
	 * Set the system focus on the input line.
	 */
	@Override
	public void setFocus() {
		inputArea.setFocus();
	}

	/**
	 * Set the text in the input line.
	 *
	 * @param str
	 *            the text to set
	 */
	public void setInputLineText(final String str) {
		inputArea.setText(str);
	}

	/**
	 * Sets the styles of the text in the output area.
	 *
	 * @param ranges
	 *            styles to set in the command area
	 */
	public void setOutputAreaStyleRanges(final StyleRange[] ranges) {
		outputArea.setStyleRanges(ranges);
	}

	/**
	 * Write the given text into the console text area (output area). Note that
	 * this operation has to be performed on the UI Thread, so this method
	 * should be call in a UIJob.
	 *
	 * @param text
	 */
	public void writeOnConsole(final String text) {
		outputArea.append("\n" + text);
		scrollCommandAreaBottom();
	}

}