package org.contextvox.IDEobserver.helpers;

import java.util.HashMap;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.exceptions.ClassMultipleException;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.contextvox.utils.LogLevel;
import org.contextvox.utils.VoxLogger;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

/**
 * Class which retrieves the informations of the classes in the workspace with
 * static methods
 */
public class ClassInformations {

	/**
	 * Returns the class (ICompilationUnit) with the class name, the project and
	 * the package in which it is.
	 *
	 * @param javaProject
	 *            the project in which the class is
	 * @param thePackage
	 *            the pacakge in which the class is
	 * @param className
	 *            the class name
	 * @return the class in ICompilationUnit object
	 * @throws ClassMultipleException
	 *             exception thrown if there are multiple classes found
	 */
	public static ICompilationUnit getClass(final IJavaProject javaProject, final IPackageFragment thePackage,
			final String className) throws ClassMultipleException {
		ICompilationUnit theClass = null;

		int i = 0;
		try {
			final IPackageFragment[] packages = javaProject.getPackageFragments();

			for (final IPackageFragment mypackage : packages) {
				if (mypackage.equals(thePackage) || thePackage == null) {
					for (final ICompilationUnit unit : mypackage.getCompilationUnits()) {
						if (unit.getElementName().equals(className + ".java")) {
							if (i > 1) {
								throw new ClassMultipleException();
							} else {
								theClass = unit;
								i++;
							}
						}

					}
				}
			}
		} catch (final JavaModelException e) {
			VoxLogger.log(LogLevel.ERROR, "Error while manipulating the Java Model", e);
		}

		return theClass;
	}

	/**
	 *
	 * Returns the general information about the classes in a given package and
	 * project
	 *
	 * @param javaProject
	 *            the project in which the classes are
	 * @param thePackage
	 *            the package in which the classes are
	 * @return the string with the informations about the classes
	 */
	public static String getGeneralInfo(final IJavaProject javaProject, final IPackageFragment thePackage) {
		final StringBuilder sb = new StringBuilder();
		final StringBuilder first = new StringBuilder();

		int i = 0;

		try {
			final IPackageFragment[] packages = javaProject.getPackageFragments();

			for (final IPackageFragment mypackage : packages) {
				if (mypackage.equals(thePackage) || thePackage == null) {
					for (final ICompilationUnit unit : mypackage.getCompilationUnits()) {
						i++;
						sb.append(i + ", " + unit.getElementName() + ".");
						sb.append(System.getProperty("line.separator"));
					}
				}
			}
		} catch (final JavaModelException e) {
			VoxLogger.log(LogLevel.ERROR, "Error during Java Model manipulation", e);
		}

		if (i > 1) {
			first.append(String.valueOf(i) + " " + Messages.getSingleMessage("classes") + ".");
		} else {
			// TODO da vedere se lasciare cosi o no
			first.append(String.valueOf(i) + " " + "class" + ".");
		}
		first.append(System.getProperty("line.separator"));
		return first.toString() + sb.toString();
	}

	/**
	 * Returns the informations about a given class
	 *
	 * @param javaProject
	 *            the project in which the class is
	 * @param thePackage
	 *            the package in which the class is
	 * @param className
	 *            the class name
	 * @return the string with the informations about the class
	 */
	public static String getPunctalInfo(final IJavaProject javaProject, final IPackageFragment thePackage,
			final String className) {

		final StringBuilder sb = new StringBuilder();
		ICompilationUnit theClass;
		try {
			theClass = getClass(javaProject, thePackage, className);
		} catch (final ClassMultipleException e) {
			return Messages.getSingleMessage("multipleClass");
		}
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		data.put(SentenceToken.NAME, className);
		if (theClass == null) {
			data.put(SentenceToken.STATUS, Messages.getSingleMessage("notexpanded"));
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data);
		}
		if (theClass.isOpen()) {
			data.put(SentenceToken.STATUS, Messages.getSingleMessage("opened"));
			sb.append(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data));
			sb.append(System.getProperty("line.separator"));
		} else {
			data.put(SentenceToken.STATUS, Messages.getSingleMessage("closed"));
			sb.append(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data));
			sb.append(System.getProperty("line.separator"));
		}

		if (theClass.isReadOnly()) {
			data.put(SentenceToken.STATUS, Messages.getSingleMessage("readOnly"));
			sb.append(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data));
			sb.append(System.getProperty("line.separator"));
		}

		try {
			int numberOfMethods = 0;
			for (final IType type : theClass.getAllTypes()) {
				numberOfMethods += type.getMethods().length;
			}
			sb.append(numberOfMethods + " " + Messages.getSingleMessage("methods") + ".");
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

}
