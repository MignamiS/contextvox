package org.contextvox.IDEobserver.helpers;

import java.util.HashMap;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

/**
 * Helper class that retreives Java element information.
 */
public class JavaElementsHelper {

	/**
	 * Produces a message from a Java element.
	 *
	 * @param javaElement
	 *            can be a project, a folder, a compilation unit, a field or
	 *            such
	 * @return feedback message
	 */
	public static String extractInfo(final IJavaElement javaElement) {
		final Map<SentenceToken, String> data = new HashMap<>();
		final String elementName = javaElement.getElementName();

		// TODO refactor using parameterized sentences
		switch (javaElement.getElementType()) {
		case IJavaElement.JAVA_PROJECT:
			return Messages.getSingleMessage("project") + " " + elementName;
		case IJavaElement.PACKAGE_FRAGMENT_ROOT:
			data.put(SentenceToken.NAME, elementName);
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FOLDER, data);
		case IJavaElement.PACKAGE_FRAGMENT:
			return Messages.getSingleMessage("package") + " " + elementName;
		case IJavaElement.COMPILATION_UNIT:
		case IJavaElement.CLASS_FILE:
			return Messages.getSingleMessage("file") + " " + elementName;
		case IJavaElement.PACKAGE_DECLARATION:
			return processpackageDeclaration(elementName);
		case IJavaElement.TYPE:
			return processJavaType(javaElement, data, elementName);
		case IJavaElement.FIELD:
			return processField(javaElement, data, elementName);
		case IJavaElement.METHOD:
			processMethod(javaElement, data, elementName);
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.METHOD, data);
		default:
			return Messages.getSingleMessage("unknownJavaElement");
		}
	}

	private static String processpackageDeclaration(final String elementName) {
		return Messages.getSingleMessage("package") + " " + elementName;
	}

	private static String processField(final IJavaElement javaElement, final Map<SentenceToken, String> data,
			final String elementName) {
		final IField field = (IField) javaElement;
		try {
			data.put(SentenceToken.NAME, elementName);
			data.put(SentenceToken.MODIFIERS, Flags.toString(field.getFlags()));
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FIELD, data);
		} catch (final JavaModelException e) {
			return "";
		}
	}

	private static String processJavaType(final IJavaElement javaElement, final Map<SentenceToken, String> data,
			final String elementName) {
		final IType theClass = (IType) javaElement;
		try {
			data.put(SentenceToken.MODIFIERS, Flags.toString(theClass.getFlags()));
			data.put(SentenceToken.NAME, elementName);
			if (theClass.isInterface()) {
				return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.INTERFACE, data);
			} else if (theClass.isEnum()) {
				return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.ENUM, data);
			} else {
				return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data);
			}
		} catch (final Exception e) {
			return "";
		}
	}

	private static void processMethod(final IJavaElement javaElement, final Map<SentenceToken, String> data,
			final String elementName) {
		// TODO refactor
		final IMethod method = (IMethod) javaElement;
		try {
			data.put(SentenceToken.MODIFIERS, Flags.toString(method.getFlags()));
		} catch (final JavaModelException e2) {
			e2.printStackTrace();
		}
		data.put(SentenceToken.NAME, elementName);
		try {
			data.put(SentenceToken.RETURNTYPE, Signature.getSignatureSimpleName(method.getReturnType()));
		} catch (final JavaModelException e1) {
			e1.printStackTrace();
		}
		try {
			if (method.getParameters().length != 0) {
				final MessageBuilder mb = new MessageBuilder();
				for (int i = 0; i < method.getParameters().length; i++) {
					mb.add(method.getParameterNames()[i]);
				}
				data.put(SentenceToken.PARAMETERS, mb.toString());
			}
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

}
