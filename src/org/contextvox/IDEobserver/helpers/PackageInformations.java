package org.contextvox.IDEobserver.helpers;

import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.utils.LogLevel;
import org.contextvox.utils.VoxLogger;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;

/**
 * Class which retrieves the informations of the pakages in the workspace with
 * static methods
 */
public class PackageInformations {

	/**
	 * Returns the general information about the classes in a given project
	 *
	 * @param javaProject
	 *            the project in which the packages are
	 * @return the string with the informations about the packages
	 */
	public static String getGeneralInfo(final IJavaProject javaProject) {
		final StringBuilder sb = new StringBuilder();
		final StringBuilder first = new StringBuilder();

		int i = 0;

		try {
			final IPackageFragment[] packages = javaProject
					.getPackageFragments();

			for (final IPackageFragment thePackage : packages) {
				try {
					if (thePackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
						// FIX Added LG: The default package is ignored if it's
						// empty. All other packages are announced even if
						// they're empty
						if (thePackage.isDefaultPackage()
								&& !thePackage.containsJavaResources())
							continue;
						i++;
						sb.append(i
								+ ", "
								+ (thePackage.isDefaultPackage() ? "default package"
										: thePackage.getElementName()) + ".");
						sb.append(System.getProperty("line.separator"));

					}
				} catch (final JavaModelException e) {
					VoxLogger.log(LogLevel.ERROR,
							"Error during Java Model manipulation", e);
				}

			}

		} catch (final JavaModelException e) {
			VoxLogger.log(LogLevel.ERROR,
					"Error during Java Model manipulation", e);
		}
		if (i > 1) {
			first.append(String.valueOf(i) + " "
					+ Messages.getSingleMessage("packages") + ".");
		} else {
			first.append(String.valueOf(i) + " "
					+ Messages.getSingleMessage("package") + ".");
		}
		return first.toString() + sb.toString();
	}

	/**
	 * Returns the package (IPackageFragment) with the package name and the
	 * project in which it is.
	 *
	 * @param javaProject
	 *            the project in which the package is
	 * @param packageName
	 *            the package name
	 * @return the package in IPackageFragment object
	 * @throws JavaModelException
	 *             exception thrown if there are problems with the JavaModel
	 */
	public static IPackageFragment getPackage(final IJavaProject javaProject,
			final String packageName) throws JavaModelException {
		IPackageFragment[] packages;
		packages = javaProject.getPackageFragments();

		IPackageFragment thePackage = null;
		for (final IPackageFragment thePackageTmp : packages) {
			if (thePackageTmp.getElementName().equals(packageName)) {
				thePackage = thePackageTmp;
				break;
			}
		}
		return thePackage;
	}

	/**
	 * Returns the informations about a given pacakge
	 *
	 * @param javaProject
	 *            the project in which the package is
	 * @param packageName
	 *            the package name
	 * @return the string with the informations about the pacakge
	 */
	public static String getPunctalInfo(final IJavaProject javaProject,
			final String packageName) {

		final StringBuilder sb = new StringBuilder();
		try {
			final IPackageFragment thePackage = getPackage(javaProject,
					packageName);

			if (thePackage == null)
				return Messages.getSingleMessage("packageNotFound");

			if (thePackage.isOpen())
				sb.append(Messages.getSingleMessage("package") + " "
						+ Messages.getSingleMessage("opened") + ".");
			else
				sb.append(Messages.getSingleMessage("package") + " "
						+ Messages.getSingleMessage("closed") + ".");

			if (thePackage.getCompilationUnits().length > 1) {
				sb.append(thePackage.getCompilationUnits().length + " classes.");
			} else {
				sb.append(thePackage.getCompilationUnits().length + " class.");
			}

			sb.append(System.getProperty("line.separator"));
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

}
