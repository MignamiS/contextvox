package org.contextvox.IDEobserver.helpers;

import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * Helper class for widgets elaboration.
 */
public class WidgetsHelper {

	/**
	 * Returns the level of the given item inside the tree. Level 0 corresponds
	 * to the root.
	 *
	 * @param selected
	 *            the tree item taken in account
	 * @return the level number
	 * @deprecated use {@link #computeTreeLevel(ITreeSelection)} instead, is
	 *             more generic and does not require unsafe casts
	 */
	@Deprecated
	public static int computeTreeLevel(final TreeItem selected) {
		// TODO remove it when no longer needed
		int level = 0;
		if (selected.getParentItem() == null) {
			// root
			return level;
		}

		// explore tree
		TreeItem tmp = selected.getParentItem();
		do {
			tmp = tmp.getParentItem();
			level++;
		} while (tmp != null);

		return level;
	}

	/**
	 * Computes how many levels there are from the given tree item to the tree
	 * root.
	 *
	 * @param selection
	 * @return an integer starting from 0 (root), or <code>-1</code> if there is
	 *         an error
	 */
	public static int computeTreeLevel(final ITreeSelection selection) {
		int steps = -1;
		final TreePath[] pathss = selection.getPaths();
		if (pathss.length > 0)
			steps = pathss[0].getSegmentCount();
		// 0 = starting point, but this starts from 1
		return steps - 1;
	}

	/**
	 * Retrieves the workbench part (view or editor) that is currently active.
	 *
	 * @return active part or <code>null</code> if this method is called outside
	 *         the UI-thread
	 */
	public static IWorkbenchPart getActivePart() {
		final IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (activeWorkbenchWindow != null) {
			final IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
			if (activePage != null)
				return activePage.getActivePart();
		}
		return null;
	}

	/**
	 * Returns the current display of Eclipse.
	 *
	 * @return the display
	 */
	public static Display getDisplay() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		return workbench.getDisplay();
	}
}
