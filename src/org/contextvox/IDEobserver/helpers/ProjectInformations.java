package org.contextvox.IDEobserver.helpers;

import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

/**
 * Class which retrieves the informations of the projects in the workspace with
 * static methods
 */
public class ProjectInformations {

	/**
	 * Returns the general information about the projects in the workspace
	 *
	 * @return the string with the informations about the projects
	 */
	public static String getGeneralInfo() {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot root = workspace.getRoot();
		final IProject[] projects = root.getProjects();
		final StringBuilder sb = new StringBuilder();

		if (projects.length > 1) {
			sb.append(projects.length + " "+ Messages.getSingleMessage("projects") + ".");
		} else {
			sb.append(projects.length + " "+ Messages.getSingleMessage("project") + ".");
		}
		sb.append(System.getProperty("line.separator"));
		int i = 1;
		for (final IProject project : projects) {

			sb.append(i + ", " + project.getName() + ".");
			sb.append(System.getProperty("line.separator"));
			i++;
		}

		return sb.toString();

	}

	/**
	 * Returns the informations about a given project
	 *
	 * @param projectName
	 *            the project name
	 * @return the string with the informations about the project
	 */
	public static String getPunctalInfo(final String projectName) {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot root = workspace.getRoot();
		final IProject project = root.getProject(projectName);
		final StringBuilder sb = new StringBuilder();

		if (project == null)
			return Messages.getSingleMessage("natureProject");

		if (project.isOpen())
			sb.append(Messages.getSingleMessage("project") + " " + Messages.getSingleMessage("opened")+".");
		else
			sb.append(Messages.getSingleMessage("project") + " " + Messages.getSingleMessage("closed") + ".");

		sb.append(System.getProperty("line.separator"));

		try {

			final IProjectDescription projectDescription = project
					.getDescription();

			final String[] natures = projectDescription.getNatureIds();

			if (natures.length > 0)
				sb.append(Messages.getSingleMessage("natureProject") + " ");
			for (int i = 0; i < natures.length; i++) {
				switch (natures[i]) {
				case "org.eclipse.jdt.core.javanature":
					sb.append("Java" + ".");
					break;

				default:
					sb.append(natures[i] + ".");
					break;
				}

			}

			final String projectDescriptionComment = projectDescription
					.getComment();
			if (projectDescriptionComment != null)
				sb.append(projectDescriptionComment);

		} catch (final CoreException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

}
