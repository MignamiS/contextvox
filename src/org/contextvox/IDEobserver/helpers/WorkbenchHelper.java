package org.contextvox.IDEobserver.helpers;

import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * Helper for workbench references retreival.
 *
 * @author Simone Mignami
 *
 */
public class WorkbenchHelper {

	public static ISelectionService getSelectionService() {
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null)
			return window.getSelectionService();
		return null;
	}
}
