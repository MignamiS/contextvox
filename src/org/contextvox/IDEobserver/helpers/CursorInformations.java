package org.contextvox.IDEobserver.helpers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.plugin.PluginElements;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.internal.ui.javaeditor.EditorUtility;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Class which retrieves the informations of the cursor in the active editor
 * with static methods
 */
@SuppressWarnings("restriction")
public class CursorInformations {

	/**
	 * Returns the informations about the cursor located in the active editor,
	 * return in which element is located and the name of this element
	 *
	 * @return the string with the informations about the cursor
	 */
	public static String getGeneralInfo() {
		// FIX modified LG:
		// http://stackoverflow.com/questions/10938956/get-selected-java-element-from-editor-in-eclipse

		final IEditorPart activeEditor = PluginElements.getActiveEditor();

		if (activeEditor == null || !(activeEditor instanceof JavaEditor))
			return Messages.getSingleMessage("noOpenedEditor");

		final ITypeRoot rootJavaElement = EditorUtility.getEditorInputJavaElement(activeEditor, false);

		final ITextSelection selection = (ITextSelection) ((ITextEditor) activeEditor).getSelectionProvider()
				.getSelection();

		IJavaElement element;
		try {
			element = rootJavaElement.getElementAt(selection.getOffset());
		} catch (final JavaModelException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		if (element.getElementType() == IJavaElement.TYPE) {
			final IType theClass = (IType) element;
			data.put(SentenceToken.NAME, theClass.getElementName());
			try {
				data.put(SentenceToken.MODIFIERS, Flags.toString(theClass.getFlags()));
				// check the type of the class
				if (theClass.isInterface()) {
					return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.INTERFACE,
							data);
				} else if (theClass.isEnum()) {
					return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.ENUM, data);
				}
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data);

		} else if (element.getElementType() == IJavaElement.METHOD) {
			final IMethod method = (IMethod) element;
			data.put(SentenceToken.NAME, method.getElementName());
			try {
				data.put(SentenceToken.MODIFIERS, Flags.toString(method.getFlags()));
				data.put(SentenceToken.RETURNTYPE, method.getReturnType());
				data.put(SentenceToken.PARAMETERS,
						Arrays.toString(method.getParameterNames()).replace("[", "").replace("]", ""));
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.METHOD, data);

		} else if (element.getElementType() == IJavaElement.FIELD) {
			final IField field = (IField) element;
			data.put(SentenceToken.NAME, field.getElementName());
			try {
				data.put(SentenceToken.MODIFIERS, Flags.toString(field.getFlags()));
				final String temp = Signature.getSignatureSimpleName(field.getTypeSignature());
				if (temp.contains("<")) {
					// temp=TokenReplacer.replaceGenerics(temp);
				}
				data.put(SentenceToken.TYPE, temp);
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FIELD, data);
		} else {
			return Messages.getSingleMessage("unknownJavaElement");
		}
	}
}
