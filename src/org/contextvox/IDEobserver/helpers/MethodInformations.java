package org.contextvox.IDEobserver.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.exceptions.InvalidSelectionException;
import org.contextvox.IDEobserver.exceptions.MethodNotFoundException;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

/**
 * Class which retrieves the informations of the methods in the workspace with
 * static methods
 */
public class MethodInformations {

	/**
	 * Returns the general information about the methods in a given class
	 *
	 * @param element
	 * @return
	 * @throws JavaModelException
	 * @throws InvalidSelectionException
	 */
	public static String getGeneralInfo(final IJavaElement element)
			throws JavaModelException, InvalidSelectionException {
		IMethod methods[] = null;

		switch (element.getElementType()) {
		case IJavaElement.COMPILATION_UNIT:
			final ArrayList<IMethod> tmpMethods = new ArrayList<>();
			final ICompilationUnit compUnit = (ICompilationUnit) element;
			final IType[] allTypes = compUnit.getAllTypes();
			for (final IType iType : allTypes)
				tmpMethods.addAll(Arrays.asList(iType.getMethods()));
			methods = tmpMethods.toArray(new IMethod[tmpMethods.size()]);
			break;
		case IJavaElement.CLASS_FILE:
			methods = ((IClassFile) element).getType().getMethods();
			break;
		case IJavaElement.TYPE:
			methods = ((IType) element).getMethods();
			break;
		default:
			throw new InvalidSelectionException();
		}

		if (methods == null || methods.length == 0)
			return Messages.getSingleMessage("noMethodsAvailable");

		final int methodsNum = methods.length;
		String text = "";
		if (methodsNum > 1) {
			text += methodsNum + " " + Messages.getSingleMessage("methods") + ".";
		} else {
			text += methodsNum + " method.";
		}
		text += System.getProperty("line.separator");

		for (int i = 0; i < methods.length; i++) {
			text += i + 1 + ", " + methods[i].getElementName() + ".";
			text += System.getProperty("line.separator");
		}
		return text;
	}

	/**
	 * Returns the method (IMethod) with the method naem and the class in which
	 * it is.
	 *
	 * @param theClass
	 *            the class in which the method is
	 * @param methodName
	 *            the method name
	 * @return the method in IMethod object
	 * @throws MethodNotFoundException
	 *             exception thrown if it doesn't found the method
	 * @throws InvalidSelectionException
	 */
	public static ArrayList<IMethod> getMethod(final IJavaElement element, final String methodName)
			throws MethodNotFoundException, InvalidSelectionException {
		final ArrayList<IMethod> listOfMethods = new ArrayList<>();
		switch (element.getElementType()) {
		case IJavaElement.COMPILATION_UNIT:
			final ICompilationUnit compUnit = (ICompilationUnit) element;

			try {
				for (final IType type : compUnit.getAllTypes()) {
					final IMethod[] methods = type.getMethods();

					for (final IMethod tmpMethod : methods) {
						if (tmpMethod.getElementName().equals(methodName))
							listOfMethods.add(tmpMethod);
					}

				}
				if (listOfMethods.size() > 0)
					return listOfMethods;
			} catch (final JavaModelException e) {
				throw new MethodNotFoundException();
			}
			break;
		case IJavaElement.CLASS_FILE:
			final IClassFile classFile = (IClassFile) element;
			try {
				final IType type = classFile.getType();
				final IMethod[] methods = type.getMethods();

				for (final IMethod tmpMethod : methods) {
					if (tmpMethod.getElementName().equals(methodName))
						listOfMethods.add(tmpMethod);
				}
				if (listOfMethods.size() > 0)
					return listOfMethods;
			} catch (final JavaModelException e) {
				throw new MethodNotFoundException();
			}
			break;
		case IJavaElement.TYPE:
			final IType type = (IType) element;
			IMethod[] methods;
			try {
				methods = type.getMethods();

				for (final IMethod tmpMethod : methods) {
					if (tmpMethod.getElementName().equals(methodName))
						listOfMethods.add(tmpMethod);
				}
				if (listOfMethods.size() > 0)
					return listOfMethods;
			} catch (final JavaModelException e) {
				throw new MethodNotFoundException();
			}
			break;
		default:
			throw new InvalidSelectionException();
		}
		throw new MethodNotFoundException();
	}

	/**
	 *
	 * Returns the informations about a given method
	 *
	 * @param theClass
	 *            the class in which the method is
	 * @param methodName
	 *            the method name
	 * @return the string with the informations about the method
	 *
	 */
	public static String getPunctalInfo(final ArrayList<IMethod> methods) {

		final StringBuilder sb = new StringBuilder();

		if (methods.size() > 1) {
			sb.append(methods.size() + " ");
			final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
			data.put(SentenceToken.NAME, methods.get(0).getElementName());
			try {
				data.put(SentenceToken.MODIFIERS, Flags.toString(methods.get(0).getFlags()));
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			sb.append(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.METHOD, data));
			sb.append(System.getProperty("line.separator"));
		}

		int j = 1;
		for (final IMethod method : methods) {
			if (methods.size() > 1) {
				sb.append(String.valueOf(j) + ", ");
				j++;
			}
			try {
				final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
				if (method.isConstructor()) {
					data.put(SentenceToken.NAME, method.getElementName());
					data.put(SentenceToken.MODIFIERS, Flags.toString(method.getFlags()));
				}

				if (method.isMainMethod()) {
					data.put(SentenceToken.NAME, method.getElementName());
					data.put(SentenceToken.MODIFIERS, Flags.toString(method.getFlags()));
				}

				final StringBuilder parameters = new StringBuilder();
				final ILocalVariable[] params = method.getParameters();
				final int paramsCnt = params.length;
				if (paramsCnt != 0) {
					parameters.append(paramsCnt + " ");
					for (final ILocalVariable param : params) {
						parameters.append(Signature.getSignatureSimpleName(param.getTypeSignature()) + ", "
								+ param.getElementName() + ". ");
					}
					data.put(SentenceToken.PARAMETERS, parameters.toString());
				}
				if (!method.isConstructor()) {
					final String type = Signature.getSignatureSimpleName(method.getReturnType());
					if (type.contains("<")) {
						// type=TokenReplacer.replaceGenerics(type);
					}
					data.put(SentenceToken.RETURNTYPE, type);
					sb.append(
							Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.METHOD, data));
				} else {
					sb.append(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CONSTRUCTOR,
							data));
				}

			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
