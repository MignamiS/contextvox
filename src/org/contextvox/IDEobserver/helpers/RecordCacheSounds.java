package org.contextvox.IDEobserver.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import org.contextvox.core.nativeVox.translator.CharCache;
import org.contextvox.services.replacer.dictionaries.Messages;

import com.gtranslate.Audio;
import com.gtranslate.Language;

/**
 * Questa classe non � direttamente usata da ContextVox. Serve solamente a
 * salvare dei files mp3 che verranno poi utilizzati nella cache presente nella
 * classe CharCache.
 */
public class RecordCacheSounds {

	private static void saveLetters(final String language) {
		InputStream inputStream = null;
		String encodedText;
		final Audio audio = Audio.getInstance();
		try {
			for (char i = 'a'; i <= 'z'; i++) {
				encodedText = URLEncoder.encode("" + i, "UTF-8");
				inputStream = audio.getAudio(encodedText, language);

				final OutputStream outstream = new FileOutputStream(
						new File(CharCache.LETTERS_PATH + language + "/" + i + ".mp3"));
				final byte[] buffer = new byte[4096];
				int len;
				while ((len = inputStream.read(buffer)) > 0) {
					outstream.write(buffer, 0, len);
				}
				outstream.close();
			}
		} catch (Exception | NoClassDefFoundError e) {
			e.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void saveNumbers(final String language) {
		InputStream inputStream = null;
		String encodedText;
		final Audio audio = Audio.getInstance();
		try {
			for (int i = 0; i <= 9; i++) {
				encodedText = URLEncoder.encode("" + i, "UTF-8");
				inputStream = audio.getAudio(encodedText, language);

				final OutputStream outstream = new FileOutputStream(
						new File(CharCache.NUMBERS_PATH + language + "/" + i + ".mp3"));
				final byte[] buffer = new byte[4096];
				int len;
				while ((len = inputStream.read(buffer)) > 0) {
					outstream.write(buffer, 0, len);
				}
				outstream.close();
			}
		} catch (Exception | NoClassDefFoundError e) {
			e.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void saveSpecialChars(final String language) {
		InputStream inputStream = null;
		String encodedText;
		final Audio audio = Audio.getInstance();
		final String specialChars[] = new String[CharCache.SPECIAL_CHARS.length];
		for (int i = 0; i < CharCache.SPECIAL_CHARS.length; i++)
			specialChars[i] = "";
		// TODO this method has been changed because broken
		// specialChars[i] =
		// SymbolReplacer.configNaturalLanguage().replace(CharCache.SPECIAL_CHARS[i]);

		try {
			for (int i = 0; i < specialChars.length; i++) {
				encodedText = URLEncoder.encode(specialChars[i], "UTF-8");
				inputStream = audio.getAudio(encodedText, language);

				final OutputStream outstream = new FileOutputStream(
						new File(CharCache.SPECIAL_CHARS_PATH + language + "/" + i + ".mp3"));
				final byte[] buffer = new byte[4096];
				int len;
				while ((len = inputStream.read(buffer)) > 0) {
					outstream.write(buffer, 0, len);
				}
				outstream.close();
			}
		} catch (Exception | NoClassDefFoundError e) {
			e.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void saveErrors() {
		InputStream inputStream = null;
		String encodedText;
		final Audio audio = Audio.getInstance();
		final String sentences[] = { "Unable to load the ContextVox plug-in. Please restart Eclipse.",
				"Impossibile caricare il plug-in ContextVox. Si prega di riavviare Eclipse",
				"Unable to connect to the internet. Please check your network connection.",
				"Impossibile collegarsi ad internet. Si prega di controllare la connessione di rete." };
		final String languages[] = { Language.ENGLISH, Language.ITALIAN, Language.ENGLISH, Language.ITALIAN };
		final String fileNames[] = { "errorLoadingContextVox", "errorLoadingContextVox", "errorOnNetworkConnection",
				"errorOnNetworkConnection" };
		try {
			for (int i = 0; i < sentences.length; i++) {
				encodedText = URLEncoder.encode(sentences[i], "UTF-8");
				inputStream = audio.getAudio(encodedText, languages[i]);

				final OutputStream outstream = new FileOutputStream(
						new File(CharCache.ERRORS_PATH + languages[i] + "/" + fileNames[i] + ".mp3"));
				final byte[] buffer = new byte[4096];
				int len;
				while ((len = inputStream.read(buffer)) > 0) {
					outstream.write(buffer, 0, len);
				}
				outstream.close();
			}
		} catch (Exception | NoClassDefFoundError e) {
			e.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(final String[] args) {
		// REMARK: language serve solo per il trovare il nome del folder nel
		// quale salvare i files. Per i caratteri speciali, prima di registrare
		// il suoni, bisogna impostare la lingua di default nella classe
		// Messages (all'interno dello static initializer, nella condizione if
		// (activator == null))
		final String language = Messages.getCurrentLanguage().getIdentifier();// Language.ENGLISH;
		// final String language = Language.ITALIAN ;
		saveLetters(language);
		saveNumbers(language);
		saveSpecialChars(language);
		saveErrors();
	}
}
