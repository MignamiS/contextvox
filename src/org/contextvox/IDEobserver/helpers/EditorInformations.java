package org.contextvox.IDEobserver.helpers;

import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Helper class which retreives editor information.
 */
public class EditorInformations {

	/**
	 * Given a text editor returns an eventual document inside of it.
	 *
	 * @param editor
	 *            a text editor
	 * @return a document, or <code>null</code> if no one is found
	 */
	public static IDocument getDocumentFromEditor(final ITextEditor editor) {
		final IDocumentProvider provider = editor.getDocumentProvider();
		final IEditorInput editorInput = editor.getEditorInput();
		return provider.getDocument(editorInput);
	}

	/**
	 * Returns the informations about the editors opened in the workspace, which
	 * are opened, which is focused
	 *
	 * @return the string with the informations about the editors
	 */
	public static String getGeneralInformations() {
		// TODO clean up a bit this legacy code...
		final StringBuilder sb = new StringBuilder();
		final IEditorReference[] editorReferences = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.getEditorReferences();
		for (final IEditorReference iEditorReference : editorReferences) {
			sb.append(iEditorReference.getName() + ", ");

		}
		sb.append(" " + Messages.getSingleMessage("areOpened") + ".");
		sb.append(System.getProperty("line.separator"));
		final IEditorPart editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.getActiveEditor();
		if (editor instanceof ITextEditor) {
			final ITextEditor textEditor = (ITextEditor) editor;

			sb.append(textEditor.getTitle() + " " + Messages.getSingleMessage("focused") + ".");
			return sb.toString();
		} else {
			return Messages.getSingleMessage("noOpenedEditor") + ".";
		}
	}
}
