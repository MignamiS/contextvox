package org.contextvox.IDEobserver.jobs;

import java.util.HashMap;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.contextualizer.ASTInfoConverter;
import org.contextvox.contextualizer.ASTProvider;
import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.ReadMode;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.preferences.PreferenceHelper;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceAlternative;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.contextvox.services.text.EditorState;
import org.contextvox.services.ttsengine.TTSService;
import org.contextvox.utils.SoundManager;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * This job is scheduled when the user navigates the editor. When scheduled, the
 * given selection will be read according with the current reading level.
 */
public class ReadEditorJob extends Job {

	/**
	 * Max number of char that are read when selecting. When this number is
	 * exceeded, read only the amount of character selected
	 */
	private static final int MAX_SELECTION_READ = 100;

	/** Contains the last typed word */
	private static final StringBuilder typingBuffer = new StringBuilder();

	private static String currentTextSelection = "";

	private final IDocument document;
	private final ITextEditor editor;
	/*
	 * java element root (compilation unit) corresponding to the document opened
	 * on the editor take in account
	 */
	private final ITypeRoot typeRoot;

	private String codeLine = "";
	private AnalyzeRequest request;

	private final TTSService ttsEngine;
	private final EditorState textualContext;

	/**
	 * An Eclipse's job that processes a request on the given editor.
	 *
	 * @param name
	 *            the name of the job
	 * @param element
	 *            the compilation unit enclosed by the editor
	 * @param document
	 *            the document contained by the editor
	 * @param request
	 *            the event request that generated this job
	 * @param editor
	 *            the editor that the job has to read
	 */
	public ReadEditorJob(final String name, final ITypeRoot element, final IDocument document, final VoxRequest request,
			final ITextEditor editor) {
		super(name);
		/*
		 * TODO Refactor: remove the document argument, that can be obtained
		 * directly by the editor. Also the TypeRoot may be extracted using the
		 * ASTASTProvider helper class.
		 */
		this.document = document;
		this.editor = editor;
		this.typeRoot = element;
		this.request = (AnalyzeRequest) request;
		Activator.cvox();

		this.ttsEngine = Activator.getServiceHandler().getTTSService();
		this.textualContext = Activator.getServiceHandler().getTextualContext().get(editor);
		this.codeLine = textualContext.getLine();
	}

	@Override
	protected IStatus run(final IProgressMonitor arg0) {
		this.request = analyzeAndCompose(this.request);

		if (this.request != null) {
			switch (this.request.getSource()) {
			case EDITOR_READ_LINE:
				readLine();
				break;
			case EDITOR_READ_CHAR:
				readCharacter();
				break;
			case EDITOR_READ_WORD:
				readWord();
				break;
			case EDITOR_TYPING:
				readTypedWord();
				break;
			case EDITOR_SELECTION:
				readSelection();
				break;
			default:
				// VoxLogger.log(LogLevel.WARNING, String.format("The source %s
				// is not currently supported from this job",
				// this.request.getSource()));
				break;
			}
		}

		return Status.OK_STATUS;
	}

	// read the current piece of text selection
	private void readSelection() {
		final ITextEditor currentEditor = this.request.getEditor();
		if (!currentEditor.equals(this.editor)) {
			// editor different, reset
			currentTextSelection = "";
		}

		if (!(request.getSelection() instanceof ITextSelection))
			return;

		final ITextSelection sel = (ITextSelection) request.getSelection();
		final String newText = sel.getText();
		// is adding or removing text to selection?
		boolean adding = false;
		if (newText.length() == ReadEditorJob.currentTextSelection.length())
			return;
		else
			adding = newText.length() > currentTextSelection.length();

		// compose message
		String result = "";
		if (adding) {
			final String substr = newText.substring(currentTextSelection.length());
			final Map<SentenceToken, String> data = new HashMap<>();
			if (substr.length() < MAX_SELECTION_READ)
				data.put(SentenceToken.NAME, substr);
			else
				data.put(SentenceToken.NAME, "" + substr.length() + " characters");
			result = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.TEXT_SELECTED, data);
		} else {
			final String substr = currentTextSelection.substring(newText.length());
			final Map<SentenceToken, String> data = new HashMap<>();
			if (substr.length() < MAX_SELECTION_READ)
				data.put(SentenceToken.NAME, substr);
			else
				data.put(SentenceToken.NAME, "" + substr.length() + " characters");
			result = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.TEXT_DESELECTED,
					data);
		}

		// update data and read
		currentTextSelection = newText;

		this.request.appendMessage(result);
		this.ttsEngine.read(this.request);
	}

	// read the character or last typed word
	private void readTypedWord() {
		String text = this.request.getMessage();
		final boolean del = this.request.isTextDeleted();

		if (del) {
			// when deleting something, clean buffer
			clearTypingBuffer();

			final Map<SentenceToken, String> data = new HashMap<>();
			data.put(SentenceToken.NAME, text);
			text = Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.DELETED, data);
			this.ttsEngine.read(text, MessageSource.EDITOR_TYPING);
		} else {
			// user is typing, check if by char or auto-completion
			if (text.length() > 1) {
				// code completion in action
				clearTypingBuffer();
				text = Activator.getServiceHandler().getReplacerService().replaceSymbols(text);
				this.ttsEngine.read(text, MessageSource.EDITOR_TYPING);
			} else {
				if (text.isEmpty())
					return;

				final char c = text.charAt(0);
				// user typing normally
				if (Character.isLetterOrDigit(c)) {
					synchronized (ReadEditorJob.class) {
						typingBuffer.append(c);
					}
					if (PreferenceHelper.sayChar()) {
						text = Activator.getServiceHandler().getReplacerService().replaceSymbols("" + c);
						this.ttsEngine.read(text, MessageSource.EDITOR_TYPING);
						;
					}
				} else {
					// extract
					synchronized (ReadEditorJob.class) {
						typingBuffer.append(c);
						text = typingBuffer.toString();
						clearTypingBuffer();
					}
					text = Activator.getServiceHandler().getReplacerService().replaceSymbols(text);
					this.ttsEngine.read(text, MessageSource.EDITOR_TYPING);
				}

			}

		}

	}

	// reset the buffer
	private void clearTypingBuffer() {
		synchronized (ReadEditorJob.class) {
			typingBuffer.delete(0, typingBuffer.length());
		}
	}

	private void readWord() {
		String text = this.textualContext.lastTouchedWord();

		text = Activator.getServiceHandler().getReplacerService().replaceSymbols(text);
		this.ttsEngine.read(text, MessageSource.EDITOR_READ_WORD);
	}

	private void readCharacter() {
		String message = this.textualContext.lastTouchedWord();

		// process message
		if (message.length() == 1 && Messages.translationExists(message)) {
			message = Messages.getSingleMessage(message);
		}

		this.request.appendMessage(message);
		this.ttsEngine.read(this.request);
	}

	private void checkLineMarker(final ITextEditor editor) {
		// get all file markers
		final IEditorInput editorInput = editor.getEditorInput();
		final IResource res = ResourceUtil.getResource(editorInput);
		IMarker[] problems = null;
		try {
			problems = res.findMarkers(IMarker.PROBLEM, true, IResource.DEPTH_INFINITE);
			// compare marker lines
			final int line = this.textualContext.getCurrentLineNumber();
			for (final IMarker item : problems) {
				if (item.exists()) {
					final Integer markerLine = (Integer) item.getAttribute(IMarker.LINE_NUMBER);
					if (markerLine.compareTo(line + 1) == 0) {
						// play sound
						SoundManager.playSound(SoundManager.SoundType.PROBLEM);
						break;
					}
				}
			}
			// breakpoints
			// TODO use hasBreakpoint to skip this check (performance)
			final IBreakpointManager bm = DebugPlugin.getDefault().getBreakpointManager();
			problems = res.findMarkers(IMarker.MARKER, true, IResource.DEPTH_INFINITE);
			for (final IMarker item : problems) {
				final IBreakpoint breakpoint = bm.getBreakpoint(item);
				if (breakpoint != null) {
					final Integer markerLine = (Integer) item.getAttribute(IMarker.LINE_NUMBER);
					if (markerLine.compareTo(line + 1) == 0) {
						// play sound
						SoundManager.playSound(SoundManager.SoundType.BREAKPOINT);
					}
				}
			}
		} catch (final CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void readLine() {
		// play sound if there is a marker at line
		if (PreferenceHelper.isSoundMarkerEnabled())
			checkLineMarker(request.getEditor());
		// sound line indentation
		// FIXME method removed, use the textual context
		// if (PreferenceHelper.isSoundIndentationEnabled())
		// checkLineIndentation();

		if (isJava() && ContextVoxProgram.isHighLevelReading()) {
			// read high level
			ASTNode node = null;

			node = getCurrentNode();

			if (node != null) {
				// found a node, collect info
				final String resp = collectInfo(node);
				this.ttsEngine.read(resp, MessageSource.EDITOR_READ_LINE);
			} else {
				// no abstract reading mode for this node, low level
				// readLowLevel();
				// FIXME anche prima non funzionava, ricevere linea e leggere
				// quella (tex cont)
			}
		} else {
			String message = this.textualContext.getLine();
			message = switchCase(message);
			this.ttsEngine.read(message, MessageSource.EDITOR_READ_LINE);
		}
	}

	// check if is a java editor
	private boolean isJava() {
		// FIXME try another way to check if is java
		return editor.getTitle().contains(".java");
	}

	private String collectInfo(final ASTNode startNode) {
		final Map<SentenceToken, String> summaryData = new HashMap<>();
		final SentenceAlternative alt = (ContextVoxProgram.getReadMode() == ReadMode.OVERVIEW)
				? SentenceAlternative.LIGHT : SentenceAlternative.FULL;

		// statement analysis
		String analysis = ASTInfoConverter.fullAnalysis(startNode, codeLine, alt);
		if (analysis == null || analysis.equals("")) {
			analysis = this.textualContext.getLine();
			if (!analysis.equals("")) {
				summaryData.put(SentenceToken.STATEMENT, analysis);
			}
		} else {
			summaryData.put(SentenceToken.STATEMENT, analysis);
		}

		return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.SUMMARY, summaryData);
	}

	// returns the node of the cursor current position
	private ASTNode getCurrentNode() {
		// // TODO Refactor: move it on ASTProvider
		// // get last motion/selection event
		// final AbstractSelection last =
		// ContextState.getJavaEditorState().getLastMotionEvent(true);
		// if (last == null)
		// return null;
		//
		// // get last read line
		// final int lineOffset = last.getLineOffset();
		// final int lineLength = last.getLineLenght();
		//
		// // get the line of code from the editor
		// try {
		// codeLine = document.get(lineOffset, lineLength);
		// } catch (final BadLocationException e) {
		// // VoxLogger.log(LogLevel.ERROR, "ReadEditorJob exploded while
		// // trying to get some data from the document", e);
		// }

		final int lineOffset = this.textualContext.getLineOffset();
		return ASTProvider.getASTNodeAt(editor, (ICompilationUnit) this.typeRoot, codeLine, lineOffset);
	}

	private String switchCase(final String message) {
		if (isEmptyLine(message))
			return Messages.getSingleMessage("emptyLine");

		return Activator.getServiceHandler().getReplacerService().replaceSymbols(message);
	}

	private boolean isEmptyLine(final String message) {
		for (int i = 0; i < message.length(); i++) {
			if (!Character.isWhitespace(message.charAt(i)))
				return false;
		}
		return true;
	}

	/*
	 * Analyze user behavior, if the request has to be forwarded will be
	 * returned, otherwise this method will return NULL
	 */
	private AnalyzeRequest analyzeAndCompose(final AnalyzeRequest request) {
		// skip if the source is "known"
		final MessageSource source = request.getSource();
		if (source == MessageSource.EDITOR_TYPING || source == MessageSource.EDITOR_SELECTION)
			return request;

		boolean forward = false;
		// TODO put this in multiple actions
		if (this.textualContext.cursorMovingVer()) {
			request.setSource(MessageSource.EDITOR_READ_LINE);
			forward = true;
		} else if (this.textualContext.cursorMovingHor()) {
			// check if it is a simple-char or a fast motion
			if (this.textualContext.isWordMoving())
				request.setSource(MessageSource.EDITOR_READ_WORD);
			else if (this.textualContext.isCharMoving())
				request.setSource(MessageSource.EDITOR_READ_CHAR);

			forward = true;
		}

		return (forward) ? request : null;

	}

	/**
	 * Reset the editor and text selection info.
	 */
	public static void resetSelection() {
		if (!currentTextSelection.isEmpty()) {
			// read last piece of text deselected
			final Map<SentenceToken, String> data = new HashMap<>();
			data.put(SentenceToken.NAME, currentTextSelection);
			Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.TEXT_DESELECTED, data);
		}
		currentTextSelection = "";

	}
}
