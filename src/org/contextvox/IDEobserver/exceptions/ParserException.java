package org.contextvox.IDEobserver.exceptions;

import java.io.IOException;

public class ParserException extends IOException {

	private static final long serialVersionUID = 1125911707201321472L;

	public ParserException(final String msg) {
		super(msg);
	}

}
