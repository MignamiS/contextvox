package org.contextvox.IDEobserver.exceptions;

/**
 * Exception thrown when the method isn't found while parsing
 */
public class MethodNotFoundException extends Exception {

	private static final long serialVersionUID = 295803645581457801L;

}
