package org.contextvox.IDEobserver.exceptions;

/**
 * Exception thrown when the selected element is not valid for a command
 */
public class InvalidSelectionException extends Exception {

	private static final long serialVersionUID = -4899407210600304631L;

}
