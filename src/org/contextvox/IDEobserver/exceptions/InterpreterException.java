package org.contextvox.IDEobserver.exceptions;

public class InterpreterException extends RuntimeException {

	private static final long serialVersionUID = 1125911707201321472L;

	public InterpreterException(final String msg) {
		super(msg);
	}

}