package org.contextvox.IDEobserver.exceptions;

/**
 * Exception thrown when the project isn't found while parsing
 */
public class ProjectNotFoundException extends Exception {

	private static final long serialVersionUID = -6886614980453181321L;

}
