package org.contextvox.IDEobserver.exceptions;

public class TokenizerException extends RuntimeException {

	private static final long serialVersionUID = -5464778690620867971L;

	public TokenizerException(final String msg) {
		super(msg);
	}

}