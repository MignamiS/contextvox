package org.contextvox.IDEobserver.exceptions;

/**
 * Exception thrown when multiple classes are found while parsing
 */
public class ClassMultipleException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
