package org.contextvox.IDEobserver.actions;

import org.contextvox.Activator;
import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.ttsengine.TTSService;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

/**
 * Toggle voice.
 */
public class StopSpeechAction extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final TTSService ttsService = Activator.getServiceHandler().getTTSService();
		final ContextVox cVox = Activator.cvox();
		if (cVox.isVoiceEnabled()) {
			ttsService.read("Voice off", MessageSource.GENERIC_FREE);
			cVox.enableVoice(false);
		} else {
			cVox.enableVoice(true);
			ttsService.read("Voice on", MessageSource.GENERIC_READ);
		}

		return null;
	}

	@Override
	public void dispose() {
	}
}
