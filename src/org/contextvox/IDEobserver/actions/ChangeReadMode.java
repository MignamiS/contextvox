package org.contextvox.IDEobserver.actions;

import org.contextvox.Activator;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

public class ChangeReadMode extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent arg0) throws ExecutionException {
		Activator.getDefault().contextVox().switchAbstractionLevel();
		// final ReadMode current = ContextVoxProgram.getReadMode();
		// ReadMode next = ReadMode.next(current);
		// if (!PreferenceHelper.isOverviewAvailable() && next ==
		// ReadMode.OVERVIEW)
		// next = ReadMode.next(next);
		// ContextVoxProgram.setReadMode(next);
		//

		return null;
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
