package org.contextvox.IDEobserver.actions;

import org.contextvox.plugin.PluginElements;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

/**
 * Activates console view.
 */
public class SetFocusOnConsole extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		activePage.activate(PluginElements.getConsole());

		// Activator.getDefault().contextVox().read(Messages.getSingleMessage("consoleFocused"),
		// MessageSource.CONSOLE);
		return null;
	}

	@Override
	public void dispose() {
	}
}
