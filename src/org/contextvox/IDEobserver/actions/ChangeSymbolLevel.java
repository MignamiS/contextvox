package org.contextvox.IDEobserver.actions;

import org.contextvox.Activator;
import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.ContextVoxProgram.SymbolLevel;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.ttsengine.TTSService;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

public class ChangeSymbolLevel extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent arg0) throws ExecutionException {
		// TODO refactor
		final SymbolLevel[] levels = ContextVoxProgram.SymbolLevel.values();
		final SymbolLevel currentLevel = Activator.cvox().getSymbolLevel();
		final ContextVox cVox = Activator.cvox();
		final TTSService ttsService = Activator.getServiceHandler().getTTSService();

		// look for current level and choose the next, cycling if necessary
		for (int i = 0; i < levels.length; i++) {
			if (levels[i] == currentLevel) {
				if (i == levels.length - 1) // array limit reached
					cVox.setSymbolLevel(levels[0]);
				else
					cVox.setSymbolLevel(levels[i + 1]);

				break;
			}
		}

		switch (currentLevel) {
		case ALL:
			ttsService.read(Messages.getSingleMessage("symbolLevelAll"), MessageSource.GENERIC_READ);
			break;
		case SOME:
			ttsService.read(Messages.getSingleMessage("symbolLevelSome"), MessageSource.GENERIC_READ);
			break;
		default:
			throw new UnsupportedOperationException("Symbol level matching not defined");

		}

		return null;
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
