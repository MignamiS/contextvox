package org.contextvox.IDEobserver.actions;

import org.contextvox.Activator;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

/**
 * Send to Core to re-send the last message.
 */
public class ReadLastCommand extends AbstractHandler {

	public ReadLastCommand() {
	}

	@Override
	public void dispose() {

	}

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		Activator.getServiceHandler().getTTSService().readLastRequest();
		return null;
	}
}