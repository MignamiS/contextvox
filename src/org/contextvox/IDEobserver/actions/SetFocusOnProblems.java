package org.contextvox.IDEobserver.actions;

import org.contextvox.plugin.PluginElements;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.views.markers.ProblemsView;

/**
 * Activates problem view
 */
@SuppressWarnings("restriction")
public class SetFocusOnProblems extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ProblemsView problemView = PluginElements.getProblems();
		final IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		activePage.activate(problemView);

		// Activator.getDefault().contextVox().read(Messages.getSingleMessage("problemViewFocused"),
		// MessageSource.PROBLEMS_VIEW);
		return null;
	}

	@Override
	public void dispose() {
	}

}
