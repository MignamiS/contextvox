package org.contextvox.IDEobserver.actions;

import org.contextvox.Activator;
import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

/**
 * Activates outline view.
 */
public class SetFocusOnOutline extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		activePage.activate(PluginElements.getOutline());

		Activator.getServiceHandler().getTTSService().read(Messages.getSingleMessage("outlineFocused"),
				MessageSource.OUTLINE);
		return null;
	}

	@Override
	public void dispose() {
	}

}
