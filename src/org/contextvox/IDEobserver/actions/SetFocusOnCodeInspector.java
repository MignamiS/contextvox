package org.contextvox.IDEobserver.actions;

import org.contextvox.plugin.PluginElements;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

/**
 * Our sample action implements workbench action delegate. The action proxy will
 * be created by the workbench and shown in the UI. When the user tries to use
 * the action, this delegate will be created and execution will be delegated to
 * it.
 *
 * @see IWorkbenchWindowActionDelegate
 */
public class SetFocusOnCodeInspector extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		activePage.activate(PluginElements.getCodeInspectorView());
		// Activator.getDefault().contextVox().read(Messages.getSingleMessage("codeInspectorFocused"),
		// MessageSource.CODEINSPECTOR);
		return null;
	}

	@Override
	public void dispose() {
	}

}