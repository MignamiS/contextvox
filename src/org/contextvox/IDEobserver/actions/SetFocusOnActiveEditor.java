package org.contextvox.IDEobserver.actions;

import org.contextvox.plugin.PluginElements;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

public class SetFocusOnActiveEditor extends AbstractHandler {

	@Override
	public void dispose() {
	}

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IEditorPart editor = PluginElements.getActiveEditor();
		if (editor == null)
			return null;

		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().activate(editor);

		// final Map<SentenceToken, String> data = new HashMap<SentenceToken,
		// String>();
		// data.put(SentenceToken.NAME, editor.getTitle());
		// Activator.getDefault().contextVox().read(Activator.cVox().makeSentence(SentenceType.EDITOR_FOCUSED,
		// data, null),
		// MessageSource.EDITOR);
		return null;
	}
}