package org.contextvox.IDEobserver.actions;

import org.contextvox.plugin.PluginElements;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.PlatformUI;

/**
 * Activates the package explorer view
 */
public class SetFocusOnPackageExplorer extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.activate(PluginElements.getPackageExplorer());

		// Activator.getDefault().contextVox().read(Messages.getSingleMessage("packageExplorerFocused"),
		// MessageSource.PACKAGE_EXPLORER);
		return null;
	}

	@Override
	public void dispose() {
	}
}