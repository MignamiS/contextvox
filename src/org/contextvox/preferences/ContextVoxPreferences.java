package org.contextvox.preferences;

import org.contextvox.Activator;
import org.contextvox.services.ttsengine.CoreType;
import org.contextvox.services.ttsengine.TTSSwitcher;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the
 * preference store that belongs to the main plug-in class. That way,
 * preferences can be accessed directly via the preference store.
 */
public class ContextVoxPreferences extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/**
	 * The constructor
	 */
	public ContextVoxPreferences() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("The preferences for the ContextVox Plugin:");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {
		// TODO let's try to add some grouping operation
		// TabFolder folder = new TabFolder(getFieldEditorParent(), SWT.None);
		// folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		//
		// TabItem item = new TabItem(folder, SWT.NONE);
		// item.setText("CVox");
		// Control control;
		// control.
		// item.setControl(control);

		// Core choice drop-down
		final ComboFieldEditor comboCore = new ComboFieldEditor(PreferenceConstants.CORE_SWITCH_KEY,
				PreferenceConstants.CORE_SWITCH_LABEL,
				new String[][] { { PreferenceConstants.CORE_VB_LABEL, CoreType.VOX_BRIDGE.getLabel() },
						{ PreferenceConstants.CORE_NATIVE_LABEL, CoreType.NATIVE_VOX.getLabel() },
						{ PreferenceConstants.CORE_NONE_LABEL, CoreType.NONE.getLabel() } },
				getFieldEditorParent());

		// Core language drop-down
		// final ComboFieldEditor comboLanguage = new
		// ComboFieldEditor(PreferenceConstants.MULTILANGUAGE,
		// PreferenceConstants.MULTILANGUAGE_TEXT,
		// new String[][] { { PreferenceConstants.ENGLISH_LABEL,
		// PreferenceConstants.ENGLISH_VALUE },
		// { PreferenceConstants.ITALIAN_LABEL,
		// PreferenceConstants.ITALIAN_VALUE } },
		// getFieldEditorParent());

		// TODO fix this
		// disable language switch when the VoxBridge and Ghost core are
		// selected
		// comboCore.setPropertyChangeListener(new IPropertyChangeListener() {
		//
		// @Override
		// public void propertyChange(PropertyChangeEvent event) {
		// String newVal = (String) event.getNewValue();
		// if (newVal.equals(PreferenceConstants.CORE_VB_LABEL)
		// || newVal.equals(PreferenceConstants.CORE_NONE_LABEL)) {
		// // disable language switch
		// comboLanguage.setEnabled(false, getFieldEditorParent());
		// } else {
		// // enable language switch
		// comboLanguage.setEnabled(true, getFieldEditorParent());
		// }
		// }
		// });

		addField(comboCore);
		// addField(comboLanguage);

		// server port
		final Group portGroup = new Group(getFieldEditorParent(), SWT.SHADOW_NONE);
		portGroup.setText(PreferenceConstants.SERVER_PORT_LABEL);
		final String[][] values = { { PreferenceConstants.PRIMARY_PORT_TEXT, PreferenceConstants.PRIMARY_PORT_VALUE },
				{ PreferenceConstants.SECONDARY_PORT_TEXT, PreferenceConstants.SECONDARY_PORT_VALUE } };
		addField(new RadioGroupFieldEditor(PreferenceConstants.SERVER_PORT_KEY, PreferenceConstants.SERVER_PORT_LABEL,
				2, values, portGroup));

		// dynamic reader checkbox
		addField(new BooleanFieldEditor(PreferenceConstants.DYNAMIC_READER_KEY,
				PreferenceConstants.DYNAMIC_READER_LABEL, getFieldEditorParent()));

		// Compulsive expander checkbox
		addField(new BooleanFieldEditor(PreferenceConstants.COMPULSIVE_EXPANDER_KEY,
				PreferenceConstants.COMPULSIVE_EXPANDER_LABEL, getFieldEditorParent()));

		// say typed word
		final Group g = new Group(getFieldEditorParent(), SWT.SHADOW_NONE);
		g.setText(PreferenceConstants.SAY_TYPED_LABEL);
		final String[][] portValues = { { PreferenceConstants.SAY_WORD_TEXT, PreferenceConstants.SAY_WORD_VALUE },
				{ PreferenceConstants.SAY_WORD_CHAR_TEXT, PreferenceConstants.SAY_WORD_CHAR_VALUE } };
		addField(new RadioGroupFieldEditor(PreferenceConstants.SAY_TYPED_KEY, PreferenceConstants.SAY_TYPED_LABEL, 2,
				portValues, g));

		// enable overview read mode
		addField(new BooleanFieldEditor(PreferenceConstants.ENABLE_OVERVIEW_KEY,
				PreferenceConstants.ENABLE_OVERVIEW_LABEL, getFieldEditorParent()));

		// sound effect for markers
		addField(new BooleanFieldEditor(PreferenceConstants.SOUND_MARKER_KEY, PreferenceConstants.SOUND_MARKER_LABEL,
				getFieldEditorParent()));

		// sound effect for indentation change
		// addField(new
		// BooleanFieldEditor(PreferenceConstants.SOUND_INDENTATION,
		// PreferenceConstants.SOUND_INDENTATION_TEXT, getFieldEditorParent()));

		// java model changes notification
		addField(new BooleanFieldEditor(PreferenceConstants.NOTIFY_JAVACHANGE_KEY,
				PreferenceConstants.NOTIFY_JAVACHANGE_LABEL, getFieldEditorParent()));

		// debug mode selector
		addField(new BooleanFieldEditor(PreferenceConstants.DEBUG_MODE_KEY, PreferenceConstants.DEBUG_MODE_LABEL,
				getFieldEditorParent()));

		// checkbox prompt timestamps
		addField(new BooleanFieldEditor(PreferenceConstants.PROMPT_TIMESTAMP_KEY,
				PreferenceConstants.PROMPT_TIMESTAMP_LABEL, getFieldEditorParent()));
		// checkbox prompt message source
		addField(new BooleanFieldEditor(PreferenceConstants.PROMPT_MESSAGESOURCE_KEY,
				PreferenceConstants.PROMPT_MESSAGESOURCE_LABEL, getFieldEditorParent()));
		// checkbox beautify commands
		addField(new BooleanFieldEditor(PreferenceConstants.BEAUTIFY_COMMAND_KEY,
				PreferenceConstants.BEAUTIFY_COMMAND_LABEL, getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(final IWorkbench workbench) {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {
		final boolean ret = super.performOk();
		if (ret) {
			manageCore();
			manageLanguage();
		}
		return ret;
	}

	private void manageLanguage() {
		switch (this.getPreferenceStore().getString(PreferenceConstants.MULTILANGUAGE_KEY)) {
		case PreferenceConstants.ENGLISH_VALUE:
			// CoreProxy.switchCoreLanguage(Messages.Language.ENGLISH);
			// Messages.reinitializeMessages(Messages.BUNDLE_NAME_EN);
			break;
		case PreferenceConstants.ITALIAN_VALUE:
			// CoreProxy.switchCoreLanguage(Messages.Language.ITALIAN);
			// Messages.reinitializeMessages(Messages.BUNDLE_NAME_IT);
			// SpeechHandler.getInstance().setLanguage(Language.ITALIAN);
			break;
		}
	}

	private void manageCore() {
		final String coreString = this.getPreferenceStore().getString(PreferenceConstants.CORE_SWITCH_KEY);
		final CoreType newType = CoreType.getCoreTypeFromLabel(coreString);
		final TTSSwitcher ttsSwitcher = Activator.cvox().getTTSSwitcher();
		final CoreType type = ttsSwitcher.getCurrentCoreType();
		if (newType == type) {
			if (newType == CoreType.VOX_BRIDGE) {
				// check if is a port change
				final int newPort = PreferenceHelper.getVoxBridgeServerPort();
				// final int port = CoreProxy.getServerPort();
				final int port = -1;
				if (newPort != -1 && newPort != port)
					ttsSwitcher.switchCore(newType);
			} else {
				ttsSwitcher.switchCore(newType);
			}
		}
	}

}