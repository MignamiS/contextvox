package org.contextvox.preferences;

import org.contextvox.Activator;
import org.contextvox.services.ttsengine.voxBridge.VoxBridge;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Helper class responsible to return the Preference values.
 */
public class PreferenceHelper {

	/**
	 * Check if the sounds related to markers are enabled.
	 *
	 * @return boolean
	 */
	public static boolean isSoundMarkerEnabled() {
		return Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.SOUND_MARKER_KEY);
	}

	/**
	 * Check if the sounds for line indentation are enabled.
	 *
	 * @return boolean
	 */
	public static boolean isSoundIndentationEnabled() {
		// return
		// Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.SOUND_INDENTATION);
		return false;
	}

	/**
	 * @return TRUE if the plug-in is in debug mode.
	 */
	public static boolean isDebugMode() {
		return Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.DEBUG_MODE_KEY);

	}

	/**
	 * Preference value if CV has to announce single character or just the
	 * entire word.
	 *
	 * @return <code>true</code> if also character have to be read, otherwise
	 *         just word are read.
	 */
	public static boolean sayChar() {
		final String val = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.SAY_TYPED_KEY);
		return val.equals(PreferenceConstants.SAY_WORD_CHAR_VALUE);
	}

	/**
	 * Returns if the Overview read mode should be available to the user.
	 *
	 * @return boolean
	 */
	public static boolean isOverviewAvailable() {
		return Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.ENABLE_OVERVIEW_KEY);
	}

	/**
	 * Returns the user selected server port.
	 *
	 * @return server port or <code>-1</code> if the VoxBridge is not selected
	 *         as current core
	 */
	public static int getVoxBridgeServerPort() {
		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		// check if the VoxBridge is selected
		final String core = store.getString(PreferenceConstants.CORE_SWITCH_KEY);
		if (!core.equals(PreferenceConstants.CORE_VB_LABEL))
			return -1;

		final String val = store.getString(PreferenceConstants.SERVER_PORT_KEY);
		// convert value
		if (val.equals(PreferenceConstants.PRIMARY_PORT_VALUE))
			return VoxBridge.PRIMARY_SERVER_PORT;
		else if (val.equals(PreferenceConstants.SECONDARY_PORT_VALUE))
			return VoxBridge.SECONDARY_SERVER_PORT;

		return -1;
	}
}
