package org.contextvox.preferences;

import org.contextvox.services.ttsengine.CoreType;

/**
 * Constant definitions for plug-in preferences. Below are listed each constant
 * suffix meaning:
 * <ul>
 * <li><code>KEY</code> indicates a key to retreive the preference data on the
 * store</li>
 * <li><code>LABEL</code> indicates the text associated to the key, displayied
 * on the panel</li>
 * <li><code>TEXT</code> indicates the text of a subvalue of a preference, for
 * example a type of core</li>
 * <li><code>VALUE</code> indicates the value of a subpreference</li>
 * </ul>
 */
public class PreferenceConstants {

	// Dynamic reading
	public static final String DYNAMIC_READER_KEY = "dynamicReader";
	public static final String DYNAMIC_READER_LABEL = "&Enable dynamic reader on active editor.";

	// Compulsive expander
	public static final String COMPULSIVE_EXPANDER_KEY = "compulsiveExp";
	public static final String COMPULSIVE_EXPANDER_LABEL = "&Enable Package Explorer compulsive expansion.";

	// Change language
	public static final String MULTILANGUAGE_KEY = "multilanguage";
	public static final String MULTILANGUAGE_LABEL = "&Select the voice language:";
	public static final String ENGLISH_TEXT = "English";
	public static final String ITALIAN_TEXT = "Italian";
	public static final String ENGLISH_VALUE = "en";
	public static final String ITALIAN_VALUE = "it";

	// change core type
	public static final String CORE_SWITCH_KEY = "coreSwitch";
	public static final String CORE_SWITCH_LABEL = "&Select the type of core:";
	public static final String CORE_VB_LABEL = CoreType.VOX_BRIDGE.getLabel();
	public static final String CORE_NONE_LABEL = CoreType.NONE.getLabel();
	public static final String CORE_NATIVE_LABEL = CoreType.NATIVE_VOX.getLabel();

	// Server port
	public static final String SERVER_PORT_KEY = "serverPort";
	public static final String SERVER_PORT_LABEL = "Server port";
	public static final String PRIMARY_PORT_VALUE = "primary";
	public static final String PRIMARY_PORT_TEXT = "Default port";
	public static final String SECONDARY_PORT_VALUE = "secondary";
	public static final String SECONDARY_PORT_TEXT = "Alternative port";

	// Overview read mode
	public static final String ENABLE_OVERVIEW_KEY = "enableOverview";
	public static final String ENABLE_OVERVIEW_LABEL = "Enable Overview as Read Mode";

	// feedback on key strokes
	public static final String SAY_TYPED_KEY = "sayTypedWord";
	public static final String SAY_TYPED_LABEL = "Say typed";
	public static final String SAY_WORD_VALUE = "stw";
	public static final String SAY_WORD_TEXT = "word only";
	public static final String SAY_WORD_CHAR_VALUE = "stwc";
	public static final String SAY_WORD_CHAR_TEXT = "word and char";

	// Debug mode
	public static final String DEBUG_MODE_KEY = "debugMode";
	public static final String DEBUG_MODE_LABEL = "&Enable Debug Mode (only for developers)";

	// Prompt timestamp
	public static final String PROMPT_TIMESTAMP_KEY = "promptTimestamp";
	public static final String PROMPT_TIMESTAMP_LABEL = "&Prompt timestamp on VoxConsole";

	// Prompt Message Source
	public static final String PROMPT_MESSAGESOURCE_KEY = "promptMessageSource";
	public static final String PROMPT_MESSAGESOURCE_LABEL = "&Prompt Message Source on VoxConsole";

	// Beautify command
	public static final String BEAUTIFY_COMMAND_KEY = "beautifyCommand";
	public static final String BEAUTIFY_COMMAND_LABEL = "&Beautify commands prompt (VoxBridge only)";

	// Sound effects
	public static final String SOUND_MARKER_KEY = "soundMarker";
	public static final String SOUND_MARKER_LABEL = "Toggle sound effect for line markers";
	public static final String SOUND_INDENTATION_KEY = "soundIndentation";
	public static final String SOUND_INDENTATION_LABEL = "Toggle sound effect for indentation change";

	// Java model changes
	public static final String NOTIFY_JAVACHANGE_KEY = "notifyJC";
	public static final String NOTIFY_JAVACHANGE_LABEL = "Enable notifications for Java Model changes";

}
