package org.contextvox.preferences;

import org.contextvox.Activator;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.CORE_SWITCH_KEY, PreferenceConstants.CORE_NONE_LABEL);
		store.setDefault(PreferenceConstants.SERVER_PORT_KEY, PreferenceConstants.PRIMARY_PORT_VALUE);
		// store.setDefault(PreferenceConstants.MULTILANGUAGE,
		// PreferenceConstants.ENGLISH_VALUE);
		store.setDefault(PreferenceConstants.DYNAMIC_READER_KEY, true);

		store.setDefault(PreferenceConstants.SAY_TYPED_KEY, PreferenceConstants.SAY_WORD_VALUE);

		store.setDefault(PreferenceConstants.ENABLE_OVERVIEW_KEY, false);
		store.setDefault(PreferenceConstants.SOUND_INDENTATION_KEY, false);
		store.setDefault(PreferenceConstants.SOUND_MARKER_KEY, true);
		store.setDefault(PreferenceConstants.DEBUG_MODE_KEY, false);
		store.setDefault(PreferenceConstants.PROMPT_MESSAGESOURCE_KEY, false);
		store.setDefault(PreferenceConstants.PROMPT_TIMESTAMP_KEY, true);
		store.setDefault(PreferenceConstants.BEAUTIFY_COMMAND_KEY, true);
		store.setDefault(PreferenceConstants.NOTIFY_JAVACHANGE_KEY, true);

	}

}
