package org.contextvox.services;

/**
 * Represents a service that a module offers to others. Each service should be
 * acquired by client with a getter on the {@link ServiceHandler}.
 *
 * @author Simone Mignami
 *
 */
public interface Service {
}
