package org.contextvox.services.text;

/**
 * A caret motion event inside a text editor.
 *
 * Each setter should be called before passing this event to the editor state,
 * to avoid wrong interpretations.
 *
 * This class is not thread-safe.
 *
 * @author Simone Mignami
 *
 */
public class MotionEvent extends AbstractEditorEvent {

	// line that contains the cursor
	private int currentLine = -1;

	// offset where the current line starts
	private int lineOffset = -1;
	// current line length
	private int lineLenght = -1;
	// cursor offset
	private int textOffset = -1;
	private String line;

	public MotionEvent() {
		super(EventType.MOTION);
	}

	public int getCurrentLine() {
		return currentLine;
	}

	public String getLine() {
		return line;
	}

	public int getLineLenght() {
		return lineLenght;
	}

	public int getLineOffset() {
		return lineOffset;
	}

	public int getTextOffset() {
		return textOffset;
	}

	/**
	 * Number of the line containing the caret. Usually line numbering starts
	 * from 1.
	 *
	 * @param currentLine
	 */
	public void setCurrentLine(final int currentLine) {
		this.currentLine = currentLine;
	}

	/**
	 * Line as a string.
	 *
	 * @param line
	 */
	public void setLine(final String line) {
		this.line = (line != null) ? line : "";
	}

	/**
	 * Length of the line (without end-of-line characters).
	 *
	 * @param lineLenght
	 */
	public void setLineLenght(final int lineLenght) {
		this.lineLenght = lineLenght;
	}

	/**
	 * Global offset on which the current line starts.
	 *
	 * @param lineOffset
	 */
	public void setLineOffset(final int lineOffset) {
		this.lineOffset = lineOffset;
	}

	/**
	 * Caret offset (global).
	 *
	 * @param textOffset
	 */
	public void setTextOffset(final int textOffset) {
		this.textOffset = textOffset;
	}

}
