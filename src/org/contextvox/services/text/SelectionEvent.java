package org.contextvox.services.text;

/**
 * Represent a selection change on the current text editor.
 *
 * @author Simone Mignami
 *
 */
public class SelectionEvent extends AbstractEditorEvent {

	public SelectionEvent() {
		super(EventType.SELECTION);
	}

}
