package org.contextvox.services.text;

enum EventType {

	SELECTION, MOTION, TYPING;
}
