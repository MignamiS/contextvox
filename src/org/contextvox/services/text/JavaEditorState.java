package org.contextvox.services.text;

/**
 * State for a java editor. This class is not thread-safe.
 *
 * @author Simone Mignami
 *
 */
class JavaEditorState implements EditorState {
	// TODO public int indentationlevelChange()

	// line that contains the cursor
	private int currentLine = -1;
	// offset where the current line starts
	private final int lineOffset = -1;
	// current line length
	private final int lineLenght = -1;
	// cursor offset (global)
	private int textOffset = -1;
	// relative to line
	private int relativeTextOffset = -1;

	private boolean verticalMotion;
	private boolean horizontalMotion;

	private String lastTouchedWord;

	// the current line as a string
	private String line;

	@Override
	public boolean cursorMovingHor() {
		return this.horizontalMotion;
	}

	@Override
	public boolean cursorMovingVer() {
		return this.verticalMotion;
	}

	@Override
	public int getCurrentLineNumber() {
		return currentLine;
	}

	@Override
	public int getIndentationLevel() {
		int n = 0;
		for (int i = 0; i < this.line.length(); i++) {
			if (this.line.charAt(i) == '\t')
				n++;
			else
				break;
		}
		return n;
	}

	@Override
	public String getLine() {
		return line;
	}

	@Override
	public int getLineLenght() {
		return lineLenght;
	}

	@Override
	public int getLineOffset() {
		return lineOffset;
	}

	@Override
	public boolean isCharMoving() {
		return this.lastTouchedWord.length() == 1;
	}

	@Override
	public boolean isWordMoving() {
		return this.lastTouchedWord.length() > 1;
	}

	@Override
	public String lastTouchedWord() {
		return this.lastTouchedWord;
	}

	/**
	 * Adds the given event to the history of the state.
	 *
	 * @param event
	 *            a new event
	 */
	void set(final MotionEvent event) {
		// vertical motion
		final int newLine = event.getCurrentLine();
		this.verticalMotion = newLine != this.currentLine;

		// horizontal motion
		final int newTextOffset = event.getTextOffset();
		final boolean hor = newTextOffset != this.textOffset;
		this.horizontalMotion = hor && !this.verticalMotion;

		// last touched word
		final int newRelativeTextOffset = newTextOffset - event.getLineOffset();
		if (this.textOffset != -1 && this.relativeTextOffset != -1) {
			if (!this.verticalMotion) {
				final int start = Math.min(this.relativeTextOffset, newRelativeTextOffset);
				final int end = Math.max(this.relativeTextOffset, newRelativeTextOffset);
				final String line = event.getLine();
				this.lastTouchedWord = line.substring(start, end);
			}
		}

		this.relativeTextOffset = newRelativeTextOffset;
		this.currentLine = newLine;
		this.textOffset = newTextOffset;
		this.line = event.getLine();
	}

}
