package org.contextvox.services.text;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.texteditor.ITextEditor;

/**
 * This class keeps track of user behavior on the text widgets, such as code
 * editors and text field. This class is not thread-safe.
 *
 * @author Simone Mignami
 *
 */
public class TextualContextHandler implements TextualContextService {
	// TODO check for thread-safety and document it

	private final Map<ITextEditor, EditorState> editors;

	public TextualContextHandler() {
		this.editors = new HashMap<>();
	}

	/**
	 * Adds a state event for the given editor. This will update the history.
	 *
	 * @param editor
	 *            a text editor
	 * @param event
	 *            a state event containing information about the new situation
	 */
	public void addEvent(final ITextEditor editor, final MotionEvent event) {
		final EditorState state = get(editor);
		if (state instanceof JavaEditorState)
			((JavaEditorState) state).set(event);
	}

	@Override
	public EditorState get(final ITextEditor editor) {
		EditorState temp = this.editors.get(editor);
		if (temp == null) {
			temp = new JavaEditorState();
			this.editors.put(editor, temp);
		}
		return temp;
	}

}
