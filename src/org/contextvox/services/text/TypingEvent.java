package org.contextvox.services.text;

/**
 * Event of text modification.
 *
 * @author Simone Mignami
 */
public class TypingEvent extends AbstractEditorEvent {

	public TypingEvent() {
		super(EventType.TYPING);
	}

}
