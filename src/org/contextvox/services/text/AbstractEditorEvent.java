package org.contextvox.services.text;

// TODO javadoc
public abstract class AbstractEditorEvent {

	protected final EventType type;

	AbstractEditorEvent(final EventType type) {
		this.type = type;
	}

	EventType getType() {
		return type;
	}

}
