package org.contextvox.services.text;

import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Provides information about user's behavior inside text widgets.
 *
 * @author Simone Mignami
 *
 */
public interface TextualContextService {

	/**
	 * Returns the state of the given editor.
	 *
	 * @param editor
	 *            a text editor
	 * @return the corresponding state. If the editor is not already mapped, a
	 *         new context is returned
	 */
	public EditorState get(ITextEditor editor);

}