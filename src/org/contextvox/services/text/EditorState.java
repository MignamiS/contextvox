package org.contextvox.services.text;

/**
 * This interface provides read access of editor context state. The state
 * represent the actual cursor position and the past user behavior, such as the
 * direction of the caret motion (vertical or horizontal). Each context is meant
 * for a single editor and is the client that has to provide this mapping and
 * access the corresponding one.
 *
 * The user has to provide an event to the state and immediately read the state
 * change. This class is not thread-safe.
 *
 * @author Simone Mignami
 *
 */
public interface EditorState {

	/**
	 * Horizontal caret motion.
	 *
	 * @return whether the caret is moving horizontal
	 */
	public boolean cursorMovingHor();

	/**
	 * Returns whether the caret has changed line. This means an explicit line
	 * change (arrow down/up, page up, etc) or caused by line change when moving
	 * the caret horizontally and reaching the end.
	 *
	 * @return whether the cursor is moving vertically
	 */
	public boolean cursorMovingVer();

	/**
	 * Returns the line number of the line that contains the cursor.
	 *
	 * @return the line number, starting from 1
	 */
	public int getCurrentLineNumber();

	/**
	 * Returns the indentation level of the current line of code. It counts only
	 * consecutives tabs at the beginning of the line.
	 *
	 * @return indentation level, might be 0 or more
	 */
	public int getIndentationLevel();

	/**
	 * Returns the current line as a string
	 *
	 * @return the line
	 */
	public String getLine();

	/**
	 * Returns the length of the current line.
	 * 
	 * @return line length
	 */
	public int getLineLenght();

	/**
	 * Returns the offset at which the current line starts.
	 * 
	 * @return line offset
	 */
	public int getLineOffset();

	/**
	 * Returns whether the caret has moved by 1 step (a char).
	 *
	 * @return <code>true</code> if the text cursor has been moved by a single
	 *         char
	 */
	public boolean isCharMoving();

	/**
	 * Returns whether the caret has been moved for 2 or more characters.
	 *
	 * @return <code>true</code> if the text cursor jumped over two or more
	 *         character
	 */
	public boolean isWordMoving();

	/**
	 * Returns the last string between the previous caret position and the
	 * current one. This is useful for reading the characters overpassed by
	 * cursor when holding the CTRL key. Moving the cursor backward produces
	 * still a left-to-right string.
	 *
	 * @return the string between the previous and current caret position
	 */
	public String lastTouchedWord();

}
