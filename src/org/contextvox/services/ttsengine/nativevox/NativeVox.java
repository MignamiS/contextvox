package org.contextvox.services.ttsengine.nativevox;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.replacer.dictionaries.Messages.Language;
import org.contextvox.services.ttsengine.Core;
import org.contextvox.services.ttsengine.CoreType;
import org.contextvox.services.ttsengine.FeedbackData;
import org.contextvox.services.ttsengine.FeedbackFilter;

public class NativeVox implements Core {

	private static final Logger logger = Logger.getLogger(NativeVox.class.getName());

	private final BlockingQueue<FeedbackData> queue;
	private final FreeTTSReader reader;

	static {
		logger.setFilter(new FeedbackFilter());
	}

	public NativeVox(final BlockingQueue<FeedbackData> queue) {
		this.reader = new FreeTTSReader();
		this.queue = queue;
	}

	@Override
	public CoreType getCoreType() {
		return CoreType.NATIVE_VOX;
	}

	@Override
	public void readString(String message, final MessageSource source) {
		message = sanitize(message);
		reader.speak(message);
	}

	@Override
	public void run() {
		logger.fine("NativeVox run method started");
		while (true) {
			try {
				final FeedbackData data = queue.take();
				logger.fine(data.getMessage());
				readString(data.getMessage(), data.getSource());
				if (data.isBomb())
					break;
			} catch (final InterruptedException e) {
				logger.log(Level.SEVERE, "NativeVox thread terminated", e);
			}
		}
		this.reader.shutdown();
		logger.fine("NativeVox run method stop");
	}

	/**
	 * Execute some security operation on the request to avoid crashes.
	 *
	 * @param message
	 * @return a new message
	 */
	private String sanitize(final String message) {
		// avoid null pointer exception
		if (message == null)
			return "";
		return message;
	}

	@Override
	public boolean switchLanguage(final Language language) {
		logger.warning("Language switch not supported");
		return false;
	}

	@Override
	public void silence() {
		reader.cancel();
	}

}
