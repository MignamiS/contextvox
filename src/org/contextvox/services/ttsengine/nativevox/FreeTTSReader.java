package org.contextvox.services.ttsengine.nativevox;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.audio.AudioPlayer;
import com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory;

class FreeTTSReader {

	private static final String VOICE_NAME = "kevin16";
	private boolean active;
	private AudioPlayer player;
	private Voice voice;

	public FreeTTSReader() {
		for (final Voice voice : new KevinVoiceDirectory().getVoices()) {
			if (voice.getName().equals(VOICE_NAME))
				this.voice = voice;
		}
		if (this.voice != null) {
			this.voice.allocate();
			this.active = true;
		}
	}

	public void cancel() {
		voice.getAudioPlayer().cancel();
	}

	public void shutdown() {
		this.voice.deallocate();
		this.active = false;
	}

	public void speak(final String text) {
		if (!active)
			throw new IllegalStateException("Speaker not initialized");
		// avoid empty lines
		if (text.equals("\n"))
			return;
		voice.speak(text);
	}

}
