package org.contextvox.services.ttsengine;

import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.replacer.dictionaries.Messages;

/**
 * A core represents the 'voice' of the program; given a string, it will
 * reproduce (directly or indirectly) the vocal representation.
 */
public interface Core extends Runnable {

	public CoreType getCoreType();

	/**
	 * Reads the given string.
	 *
	 * @param String
	 *            the message to be read
	 * @param MessageSource
	 *            the source of the message
	 */
	public void readString(String message, MessageSource source);

	/**
	 * Master switch for all languages available.
	 *
	 * @param language
	 * @return TRUE if the language has been switched successfully
	 */
	public boolean switchLanguage(Messages.Language language);

	/**
	 * Interrupts the current speech (if supported). The speech is cancelled.
	 */
	public void silence();
}
