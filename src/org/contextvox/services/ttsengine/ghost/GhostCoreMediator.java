package org.contextvox.services.ttsengine.ghost;

import java.util.logging.Logger;

import org.contextvox.Activator;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.replacer.dictionaries.Messages.Language;
import org.contextvox.services.ttsengine.Core;
import org.contextvox.services.ttsengine.CoreType;
import org.contextvox.services.ttsengine.FeedbackData;

/**
 * Core Mediator for CoreType.NONE
 */
public class GhostCoreMediator implements Core {

	private static final Logger logger = Logger.getLogger(GhostCoreMediator.class.getName());

	public static Core getInstance() {
		return new GhostCoreMediator();
	}

	@Override
	public CoreType getCoreType() {
		return CoreType.NONE;
	}

	@Override
	public void readString(final String message, final MessageSource source) {
		Activator.getDefault().contextVox().log(logger, new FeedbackData(false, source, message));
	}

	@Override
	public void run() {
	}

	@Override
	public boolean switchLanguage(final Language language) {
		return true;
	}

	@Override
	public void silence() {
		logger.fine("Silence!");
	}

}
