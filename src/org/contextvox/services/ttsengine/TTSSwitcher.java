package org.contextvox.services.ttsengine;

import org.contextvox.services.replacer.dictionaries.Messages.Language;

/**
 * This interface contains some method to modify the current TTS engine
 * behavior, such as the running engine or its language.
 *
 * @author Simone Mignami
 *
 */
public interface TTSSwitcher {

	/**
	 * Returns the current active TTS engine.
	 *
	 * @return the type of core (engine)
	 */
	public CoreType getCurrentCoreType();

	/**
	 * Change core.
	 *
	 * @param type
	 *            the core type, if it is equals to the previous one nothing
	 *            happen.
	 * @return <code>true</code> if the switch success
	 */
	public boolean switchCore(CoreType type);

	public boolean switchLanguage(Language lang);

}