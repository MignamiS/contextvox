package org.contextvox.services.ttsengine;

import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.services.Service;

/**
 * Offers a text-to-speech translation for the given string. It also reproduces
 * the audio output.
 *
 * @author Simone Mignami
 *
 */
public interface TTSService extends Service {

	/**
	 * Returns the current active TTS engine.
	 *
	 * @return the type of core (engine)
	 */
	public CoreType getCurrentCoreType();

	/**
	 * Reads the given string with the current TTS engine. This method is
	 * intended for service and debug purpose.
	 *
	 * @param message
	 *            the string
	 * @param source
	 *            a real or hypothetical source of the message
	 */
	public void read(String message, MessageSource source);

	/**
	 * Reads the given request. This is the preferred way to access this
	 * service.
	 *
	 * @param request
	 */
	public void read(VoxRequest request);

	/**
	 * Re-send the previous relevant message to the TTS engine. Note that for
	 * "relevant" is meant the last announced string that is not a notification
	 * to the user (such as symbol or abstraction level change).
	 */
	public void readLastRequest();

	/**
	 * Stops the current speech.
	 */
	public void silence();

}
