package org.contextvox.services.ttsengine.voxBridge;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.Messages.Language;
import org.contextvox.services.ttsengine.Core;
import org.contextvox.services.ttsengine.CoreType;
import org.contextvox.services.ttsengine.FeedbackData;
import org.contextvox.services.ttsengine.voxBridge.SocketBridge.Command;
import org.contextvox.utils.VoxLogger;

/**
 * The VoxBridge Core uses a socket communication to transmit a string to an
 * external program, that has to produce the audio feedback.
 */
public class VoxBridge implements Core {

	public static final int PRIMARY_SERVER_PORT = 23418;

	public static final int SECONDARY_SERVER_PORT = 23421;
	// last sent command, for performance purpose
	private static Command lastCommand;

	// last message source that sent a message (used for log purpose)
	private static MessageSource lastState;

	private static final Logger logger = Logger.getLogger(VoxBridge.class.getName());
	// milliseconds of buffer storage
	private static final int THRESHOLD = 2;
	// used for vox request exchange
	private final BlockingQueue<FeedbackData> queue;

	/** Defines how to read each message */
	private final Map<MessageSource, ReadCommand> selector;

	private SocketBridge server = null;

	/**
	 * Factory method, builds a new core.
	 *
	 * @param port
	 *            the specific server port
	 *
	 * @return the core or <code>null</code> if something goes wrong, for
	 *         example when the port is already use
	 */
	public static Core build(final int port, final BlockingQueue<FeedbackData> queue) {
		logger.info("VoxBridge setup procedure started");
		// init server
		final SocketBridge server = SocketBridge.getNewServer(port);

		if (server != null) {
			// server has been instantiated correctly
			logger.info("VoxBridge setup successful");
			return new VoxBridge(queue, server);
		} else {
			// TODO instead of returning null, throw a custom exception.
			// something goes wrong during the server setup
			return null;
		}
	}

	public static MessageSource getLastState() {
		return lastState;
	}

	private VoxBridge(final BlockingQueue<FeedbackData> queue, final SocketBridge server) {
		this.queue = queue;
		this.server = server;
		// init message source selector map
		this.selector = new HashMap<>();
		populateSelector();
	}

	@Override
	public CoreType getCoreType() {
		return CoreType.VOX_BRIDGE;
	}

	/**
	 * Returns the current server port.
	 *
	 * @return the server port or <code>-1</code> if there is no address bound
	 *         or server in use.
	 */
	public int getServerPort() {
		if (server != null)
			return server.getServerPort();
		else
			return -1;
	}

	@Override
	public void readString(final String message, final MessageSource source) {
		// look into selector if the message-source is mapped
		if (selector.containsKey(source)) {
			// cleanup and read
			lastState = source;
			selector.get(source).read(message);
		} else {
			logger.severe("Selector map has no operation for " + source);
		}
	}

	@Override
	public void run() {
		logger.fine("VoxBridge run method started");
		while (true) {
			try {
				FeedbackData data = queue.take();
				logger.fine("Got request from queue: " + data.toString());

				// concatenate other request
				String message = data.getMessage();
				MessageSource lastSource = data.getSource();
				boolean isBomb = data.isBomb();
				// wait thrashold
				Thread.sleep(THRESHOLD);
				do {
					data = queue.poll();
					if (data != null) {
						logger.fine("+ " + data.toString() + " (concatenated");
						message += data.getMessage();
						lastSource = data.getSource();
						// flag must stay true when is true
						isBomb = (isBomb) ? true : data.isBomb();
					}
				} while (data != null);
				readString(message, lastSource);

				if (isBomb) {
					logger.info("VoxBridge queue bombed, exiting");
					break;
				}
			} catch (final InterruptedException e) {
				logger.log(Level.SEVERE, "VoxBridgetMediator thread has been interrupted", e);
				this.realShutdown();
			}
		}

		// send EXIT command to client
		server.writeMessage(Command.EXIT, Messages.getSingleMessage("disconnectingServer"));

		// terminate
		realShutdown();
	}

	@Override
	public boolean switchLanguage(final Language language) {
		logger.warning("Language switch not supported");
		// at the moment it is not possible to switch this core's language
		return false;
	}

	private void populateSelector() {
		// some standard read command
		final ReadCommand onlyFree = new ReadCommand() {
			@Override
			public void read(final String message) {
				// VoxBridge bridge = null;
				// if (CoreProxy.getCurrentMediator() instanceof VoxBridge)
				// bridge = (VoxBridge) CoreProxy.getCurrentMediator();
				// else
				// return;

				if (lastCommand != Command.FREE) {
					server.writeMessage(Command.FREE, message);
					lastCommand = Command.FREE;
				} else {
					VoxLogger.log(logger, "(Free)" + message);
				}
			}
		};

		final ReadCommand selectHighOrLowLevel = new ReadCommand() {
			@Override
			public void read(final String message) {
				// VoxBridge bridge = null;
				// if (CoreProxy.getCurrentMediator() instanceof VoxBridge)
				// bridge = (VoxBridge) CoreProxy.getCurrentMediator();
				// else
				// return;

				if (ContextVoxProgram.isHighLevelReading()) {
					// read a high level read
					server.writeMessage(Command.READ, message);
					lastCommand = Command.READ;
				} else {
					// read freely the GUI
					if (lastCommand != Command.FREE) {
						server.writeMessage(Command.FREE, message);
						lastCommand = Command.FREE;
					} else {
						VoxLogger.log(logger, "(Free) " + message);
					}
				}
			}
		};

		final ReadCommand onlyRead = new ReadCommand() {
			@Override
			public void read(final String message) {
				// VoxBridge bridge = null;
				// if (CoreProxy.getCurrentMediator() instanceof VoxBridge)
				// bridge = (VoxBridge) CoreProxy.getCurrentMediator();
				// else
				// return;

				lastCommand = Command.READ;
				server.writeMessage(Command.READ, message);
			}
		};

		// populates
		selector.put(MessageSource.GENERIC_READ, onlyRead);
		selector.put(MessageSource.GENERIC_FREE, onlyFree);
		selector.put(MessageSource.MENU, onlyRead);
		selector.put(MessageSource.JAVACHANGE_EVENT, onlyRead);
		selector.put(MessageSource.VIEW, onlyRead);

		// mapping editor
		selector.put(MessageSource.EDITOR, onlyRead);
		selector.put(MessageSource.EDITOR_TYPING, onlyRead);
		selector.put(MessageSource.EDITOR_READ_LINE, onlyRead);
		selector.put(MessageSource.EDITOR_READ_WORD, onlyRead);
		selector.put(MessageSource.EDITOR_READ_CHAR, onlyRead);
		selector.put(MessageSource.EDITOR_SELECTION, onlyRead);

		// mapping active shell
		selector.put(MessageSource.TEXT, onlyFree);
		selector.put(MessageSource.DIALOG, onlyFree);
		selector.put(MessageSource.TREE, onlyFree);
		selector.put(MessageSource.TREE_ITEM, selectHighOrLowLevel);
		selector.put(MessageSource.LINK, onlyFree);
		selector.put(MessageSource.TABLE, onlyFree);
		selector.put(MessageSource.TABLE_ITEM, onlyFree);
		selector.put(MessageSource.WINDOW, onlyFree);

		// mapping views
		selector.put(MessageSource.CODEINSPECTOR, onlyRead);
		selector.put(MessageSource.COMMANDLINE, onlyRead);
		selector.put(MessageSource.CONSOLE, onlyFree);
		selector.put(MessageSource.OUTLINE, onlyRead);
		selector.put(MessageSource.PACKAGE_EXPLORER, onlyRead);
		selector.put(MessageSource.JUNIT, onlyFree);
		selector.put(MessageSource.PROBLEMS_VIEW, onlyFree);
	}

	private void realShutdown() {
		// close connection
		server.disconnect();
		server = null;
	}

	/**
	 * Determines how to read a message, its call is based on te message source
	 */
	private interface ReadCommand {
		public void read(String message);
	}

	@Override
	public void silence() {
		readString(" ", MessageSource.GENERIC_FREE);
	}
}
