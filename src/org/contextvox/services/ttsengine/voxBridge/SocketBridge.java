package org.contextvox.services.ttsengine.voxBridge;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.contextvox.Activator;
import org.contextvox.core.nativeVox.translator.Symbol;
import org.contextvox.preferences.PreferenceConstants;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.ttsengine.FeedbackFilter;
import org.contextvox.utils.LogLevel;
import org.contextvox.utils.VoxLogger;

/**
 * Responsible for managing low-level connection with the client. Note that only
 * a single client at the time is considered.
 */
class SocketBridge {

	private static final String EOL = System.getProperty("line.separator");
	private static final Logger logger = Logger.getLogger(SocketBridge.class.getName());
	private static final String NO_CLIENT_CONNECTED = "No client connected";

	static {
		logger.setFilter(new FeedbackFilter());
	}

	// amount of seconds that the accepter-thread stays blocked on the accept()
	// method on server socket
	private static final int WAIT_TIMEOUT_SECS = 30;
	private Socket client = null;

	private volatile boolean running = false;
	private final ServerSocket server;
	private PrintWriter writer = null;

	/**
	 * Return a new server running on the specified port.
	 *
	 * @param port
	 *            the specific server port
	 *
	 * @return SocketBridge or NULL if something goes wrong during the
	 *         connection setup, for example if the server port is already
	 *         bound.
	 */
	public static SocketBridge getNewServer(final int port) {
		logger.info("Setup socket bridge on port " + port);
		final String serverName = String.format("Server[%d]:", port);

		// building server
		ServerSocket server = null;
		try {
			server = new ServerSocket();
		} catch (final IOException e1) {
			logger.log(Level.SEVERE, serverName + "error when opening the socket", e1);
			return null;
		}

		// attempt to bind the address
		logger.fine("Try to bind address...");
		try {
			server.bind(new InetSocketAddress(port));
		} catch (final IOException e) {
			// the address is in use
			final String msg = String.format("%s the port %d is already in use", serverName, port);
			logger.warning(msg);
			// cleanup resources
			if (server != null && !server.isClosed())
				try {
					server.close();
				} catch (final IOException e1) {
					logger.log(Level.SEVERE, serverName + "Error when closing the broken server socket", e);
				}
			return null;
		}
		logger.info(String.format("%s listening on address 127.0.0.1:%d", serverName, port));

		// accept timeout
		logger.fine("Setting server socket timeout...");
		try {
			server.setSoTimeout(WAIT_TIMEOUT_SECS * 1000);
		} catch (final SocketException e) {
			logger.log(Level.SEVERE, serverName + "Error when setting the server accepter timeout", e);
		}

		final SocketBridge socketBridge = new SocketBridge(server);

		if (socketBridge != null)
			logger.fine("Initializing socket bridge...");
		socketBridge.init();

		return socketBridge;
	}

	private SocketBridge(final ServerSocket server) {
		this.server = server;
	}

	/**
	 * Disconnect the client
	 */
	public void disconnect() {
		logger.info("Disconnecting socket bridge");
		this.running = false;
		try {
			if (client != null)
				client.close();
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "Error when disconnecting the SocketBridge", e);
		}

		logger.info("VoxBridge disconnected");
	}

	public int getServerPort() {
		return this.server.getLocalPort();
	}

	/**
	 * Write message through socket, client must specify the command for the
	 * screen reader and the relative message, but not the end-of-line (will be
	 * added automatically).
	 *
	 * @param command
	 * @param message
	 */
	public synchronized void writeMessage(final Command command, String message) {
		final PrintWriter wr = getWriter();

		// log
		final StringBuilder sb = new StringBuilder();
		if (wr == null) {
			sb.append(String.format("[%s]", NO_CLIENT_CONNECTED));

		}
		// message source
		if (Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.PROMPT_MESSAGESOURCE_KEY))
			sb.append(String.format("[%s]", VoxBridge.getLastState()));

		// choose beautify prompt or just the raw string
		if (Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.BEAUTIFY_COMMAND_KEY))
			sb.append(String.format("[%s]", command));
		else
			sb.append(command.getValue());
		sb.append(Symbol.space);
		sb.append(message);
		VoxLogger.log(logger, sb.toString());

		// sending real message to the client
		if (wr != null) {
			message = command.getValue() + message;
			// FIXME too many EOL send!
			if (!message.endsWith("" + EOL)) {
				// ends correctly the line, so can be interpreted by the other
				// device
				message = message + EOL;
			}
			wr.println(message);
		}
	}

	protected synchronized Socket getClient() {
		return client;
	}

	protected synchronized PrintWriter getWriter() {
		return writer;
	}

	protected synchronized void setClient(final Socket client) {
		if (this.client != null) {
			try {
				this.client.close();
			} catch (final IOException e) {
				VoxLogger.log(LogLevel.ERROR, "Error when closing client connection", e);
			}
		}
		this.client = client;
	}

	protected synchronized void setWriter(final PrintWriter writer) {
		this.writer = writer;
	}

	/**
	 * Start server, wait for client.
	 */
	private void init() {
		this.running = true;

		// start thread that waits for clients
		final Thread daemon = new Thread(new Runnable() {

			@Override
			public void run() {
				logger.fine("Starting client-accepter daemon");
				while (running) {
					try {
						// wait for client
						final Socket newClient = server.accept();
						logger.info("New client connected");
						// add client and output reference
						setClient(newClient);
						setWriter(new PrintWriter(newClient.getOutputStream(), true));
						// send welcome message
						writeMessage(Command.FREE, Messages.getSingleMessage("welcomeMessage"));
					} catch (final java.io.InterruptedIOException ioe) {
						// catch timeout exception
						continue;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "Error accepting new client", e);
					}
				}

				// stop server
				try {
					if (!server.isClosed())
						server.close();
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "Error closing server socket", e);
				}

				logger.info(String.format("Server stopped (%d)", getServerPort()));
			}
		});
		daemon.setName("Accepter");
		daemon.start();
	}

	public enum Command {
		EXIT {
			@Override
			public char getValue() {
				return 'e';
			}
		},
		FREE {
			@Override
			public char getValue() {
				return 'f';
			}
		},
		READ {
			@Override
			public char getValue() {
				return 'r';
			}
		};
		public abstract char getValue();
	}

}