package org.contextvox.services.ttsengine;

import org.contextvox.plugin.requests.MessageSource;

/**
 * Used to pass data inside the core modules. This class is immutable. This
 * class should not be used by client outside TTS engine package.
 */
public class FeedbackData {

	private final boolean bomb;
	private final String message;
	private final MessageSource source;

	public FeedbackData(final boolean bomb, final MessageSource source, final String message) {
		super();
		this.bomb = bomb;
		this.source = source;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public MessageSource getSource() {
		return source;
	}

	public boolean isBomb() {
		return bomb;
	}

	@Override
	public String toString() {
		String str = "";
		if (this.bomb)
			str = "(BOMB) ";
		str += String.format("FeedbackData: [%s] %s", source.toString(), this.message);
		return str;
	}

}
