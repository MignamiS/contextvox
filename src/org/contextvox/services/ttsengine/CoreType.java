package org.contextvox.services.ttsengine;

/**
 * Cores supported.
 */
public enum CoreType {

	NATIVE_VOX("NativeVox"), NONE("None"), VOX_BRIDGE("VoxBridge");

	private final String label;

	/**
	 * Get corresponding type from the given label
	 *
	 * @param label
	 * @return CoreType or <code>null</code> if noone corresponds
	 */
	public static CoreType getCoreTypeFromLabel(final String label) {
		final CoreType[] vals = CoreType.values();
		for (final CoreType item : vals) {
			if (item.getLabel().equalsIgnoreCase(label))
				return item;
		}
		return null;
	}

	private CoreType(final String label) {
		this.label = label;
	}

	/**
	 * Return the property label (value)
	 *
	 * @return value
	 */
	public String getLabel() {
		return label;
	}
}
