package org.contextvox.services.ttsengine;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.preferences.PreferenceHelper;
import org.contextvox.services.replacer.dictionaries.Messages.Language;
import org.contextvox.services.ttsengine.ghost.GhostCoreMediator;
import org.contextvox.services.ttsengine.nativevox.NativeVox;
import org.contextvox.services.ttsengine.voxBridge.VoxBridge;

/**
 * Manages the TTS engines.
 *
 * @author Simone Mignami
 *
 */
public class TTSHandler implements TTSService, TTSSwitcher {
	// TODO check for thread-safety and document it

	private static final int QUEUE_SIZE = 25;

	private final static Logger logger = Logger.getLogger(TTSHandler.class.getName());

	// current active core
	private Core core = null;

	private final AtomicReference<FeedbackData> lastRequest;

	private final BlockingQueue<FeedbackData> queue;

	public TTSHandler(final ContextVox contextVox) {
		this.queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
		this.lastRequest = new AtomicReference<>();
	}

	@Override
	public CoreType getCurrentCoreType() {
		if (this.core != null)
			return this.core.getCoreType();
		return null;
	}

	/**
	 * Setup operations
	 */
	public void init() {
		// TODO get default from preference store
		realInit(CoreType.NATIVE_VOX);
	}

	@Override
	public void read(final String message, final MessageSource source) {
		// TODO Auto-generated method stub
		read(new VoxRequest(source, message));
	}

	@Override
	public void read(final VoxRequest request) {
		if (core == null && request == null)
			return;

		final FeedbackData packet = new FeedbackData(false, request.getSource(), request.getMessage());
		if (core instanceof GhostCoreMediator) {
			// skip queue and forward directly (no other threads)
			core.readString(request.getMessage(), request.getSource());
			// TODO filter
			lastRequest.set(packet);
		} else {
			// cancel previous speech
			if (core instanceof NativeVox)
				((NativeVox) core).silence();
			// post new message
			final boolean success = this.queue.offer(packet);
			if (success) {
				// TODO filter using message source (when the source hierarchy
				// implemented
				lastRequest.set(packet);
			} else {
				logger.warning("Queue capacity exceeded");
			}
		}
	}

	@Override
	public void readLastRequest() {
		this.queue.offer(lastRequest.get());
	}

	protected boolean realInit(final CoreType type) {
		logger.info("Starting " + type + " initialization");
		// TODO check eventual core explosion with exceptions
		switch (type) {
		case VOX_BRIDGE:
			// TODO change using contextvox.getpreferencestore()
			final int port = PreferenceHelper.getVoxBridgeServerPort();
			core = VoxBridge.build(port, queue);
			break;

		case NATIVE_VOX:
			core = new NativeVox(this.queue);
			break;

		case NONE:
			core = GhostCoreMediator.getInstance();
			break;
		}

		startCoreThread();
		return false;
	}

	/**
	 * Stops the current core and clean-up
	 */
	public void shutdown() {
		logger.info("Starting core shutdown procedure for " + core.getCoreType());
		if (core instanceof GhostCoreMediator) {
			core = null;
		} else {
			final FeedbackData bomb = new FeedbackData(true, MessageSource.GENERIC_FREE, "");
			// bomb the queue
			final boolean success = this.queue.offer(bomb);
			if (!success)
				logger.severe("Unable to bomb the queue and shutdown the core " + core.getCoreType().toString());
		}
	}

	@Override
	public void silence() {
		this.queue.clear();
		this.core.silence();
	}

	private void startCoreThread() {
		if (core != null) {
			final Thread t = new Thread(core);
			t.setName("Core thread (" + core.getCoreType().toString() + ")");
			t.start();
			logger.fine("Core thread started");
		}
	}

	@Override
	public boolean switchCore(final CoreType type) {
		logger.info("Starting core switch from " + core.getCoreType() + " to " + type);
		// reset previous core
		if (core != null) {
			this.shutdown();
			core = null;
		}
		realInit(type);
		return true;
	}

	@Override
	public boolean switchLanguage(final Language lang) {
		logger.info("Switching core language to " + lang);
		if (core != null)
			return core.switchLanguage(lang);
		logger.info("This core does not support language switch");
		return false;
	}

}
