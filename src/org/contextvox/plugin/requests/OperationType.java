package org.contextvox.plugin.requests;

/**
 * Represent an operation that is performed inside a GUI element, such as a Tree
 * or on the text editor.
 */
public enum OperationType {

	NONE, GAIN_FOCUS, LOOSE_FOCUS, TREEITEM_SELECTED, DESELECTED, TREEITEM_COLLAPSED, TREEITEM_EXPANDED;
}
