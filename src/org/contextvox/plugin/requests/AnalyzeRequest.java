package org.contextvox.plugin.requests;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.texteditor.ITextEditor;

// TODO javadoc
// no thread-safe
public class AnalyzeRequest extends VoxRequest {

	private ISelection selection;

	private ITextEditor editor;

	// SWT event
	private Event event;
	// event from an IDocument
	private DocumentEvent documentEvent;
	// when a document event is provided, indicates if the text has been deleted
	// (true) or typed (false)
	private boolean textDeleted;

	private int offset = -1;

	public AnalyzeRequest(final MessageSource source) {
		super(source);
	}

	public DocumentEvent getDocumentEvent() {
		return documentEvent;
	}

	public ITextEditor getEditor() {
		return editor;
	}

	public Event getEvent() {
		return event;
	}

	/**
	 * Returns current caret's offset inside the document. The offset is global
	 * and not related to the line in which is contained.
	 *
	 * @return the global caret offset. The value of <code>-1</code> means "not
	 *         set"
	 */
	public int getOffset() {
		return offset;
	}

	public ISelection getSelection() {
		return selection;
	}

	public boolean isTextDeleted() {
		return textDeleted;
	}

	public void setDocumentEvent(final DocumentEvent documentEvent) {
		this.documentEvent = documentEvent;
	}

	public void setEditor(final ITextEditor editor) {
		this.editor = editor;
	}

	public void setEvent(final Event event) {
		this.event = event;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public void setSelection(final ISelection selection) {
		this.selection = selection;
	}

	public void setTextDeleted(final boolean textDeleted) {
		this.textDeleted = textDeleted;
	}

}
