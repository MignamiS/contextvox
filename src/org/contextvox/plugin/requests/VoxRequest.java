package org.contextvox.plugin.requests;

/**
 * Information packed to be analyzed. This class is not thread-safe.
 */
public class VoxRequest {

	private static int id_counter = 0;

	private final int id;
	private String message;

	private OperationType operation;

	private MessageSource source;
	private State state = State.FORWARD;

	public VoxRequest(final MessageSource source) {
		this.id = id_counter++;
		this.source = source;
		this.message = "";
		this.operation = OperationType.NONE;
	}

	/**
	 * Constructor
	 *
	 * @param source
	 *            the component that generate the request
	 * @param message
	 *            the message to read
	 *
	 * @see BaseVoxState
	 */
	public VoxRequest(final MessageSource source, final String message) {
		this.id = id_counter++;
		this.source = source;
		this.message = message;
		this.operation = OperationType.NONE;
	}

	public VoxRequest(final String message, final MessageSource source, final OperationType operation) {
		this.id = id_counter++;
		this.message = message;
		this.source = source;
		this.operation = operation;
	}

	/**
	 * Appends the given message to the current.
	 *
	 * @param message
	 */
	public void appendMessage(final String message) {
		this.message += " " + message;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(obj instanceof VoxRequest))
			return false;
		final VoxRequest other = (VoxRequest) obj;
		return this.id == other.id;
	}

	public String getMessage() {
		return message;
	}

	public OperationType getOperation() {
		return operation;
	}

	public MessageSource getSource() {
		return source;
	}

	/**
	 * Returns the current request state of processing.
	 *
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	/**
	 * Append the given string to the beginning of the message.
	 *
	 * @param str
	 */
	public void prepend(final String str) {
		this.message = str + " " + this.message;
	}

	/**
	 * Sets the request as "ready".
	 */
	public void ready() {
		this.state = State.READY;
	}

	/**
	 * Replaces the current message with the given one. To just add the string
	 * to the current, consider use {@link #appendMessage(String)} or
	 * {@link #prepend(String)} methods.
	 *
	 * @param message
	 *
	 * @see #appendMessage(String)
	 * @see #prepend(String)
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	public void setOperation(final OperationType operation) {
		this.operation = operation;
	}

	public void setSource(final MessageSource source) {
		this.source = source;
	}

	/**
	 * Changes the request's state. This is useful to propagate the request to
	 * many sequential processors without a return value from them (such as null
	 * to inform the next processor that the request should be stopped).
	 *
	 * @param state
	 *            the new state of the request
	 */
	public void setState(final State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		final String prefix = String.format("(%s)VoxRequest message source: %s", this.state.toString().charAt(0),
				this.source.toString());
		String suffix = "";
		if (this.message != null)
			suffix = " message: " + this.message;
		return prefix + suffix;
	}

	public enum State {
		/**
		 * The request should not proceed and can be deleted.
		 */
		ABORT,
		/**
		 * The request processing has been stopped, but it can continue if
		 * another process may request.
		 */
		SUSPEND,
		/**
		 * Default, the request is processed normally.
		 */
		FORWARD,
		/**
		 * The request has been processed. This is used to inform the manager of
		 * the process that a processor processed the request and that others
		 * should not continue. But this is just an advice...
		 */
		READY;
	}
}
