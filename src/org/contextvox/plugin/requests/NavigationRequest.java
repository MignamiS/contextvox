package org.contextvox.plugin.requests;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;

/**
 * A request send by the Navigation listener group. It contains many elements
 * that can be null, so be careful when using the get() methods.
 */
public class NavigationRequest extends VoxRequest {
	// TODO remove constructors, use builders where possible or simply setters

	public Widget widget;

	// active shell element (text, link, tree or table item)
	private Control activeShellElement;

	// outline selection event
	private ContentOutline contentOutline;
	private ISelection elementSelection;

	// used by part selection listener
	private IWorkbenchPart part;
	// used to transmit a selection, such as tree selection
	private SelectionEvent selection;

	// package explorer selection
	private SelectionChangedEvent selectionChangeEvent;

	// package explorer tree navigation
	private TreeExpansionEvent treeEvent;
	// menu item selection listener
	private Event widgetEvent;

	public static NavigationRequest buildOutlineRequest(final MessageSource source, final String message,
			final ContentOutline contentOutline) {
		final NavigationRequest request = new NavigationRequest(source, message);
		request.setContentOutline(contentOutline);
		return request;
	}

	/**
	 * Builds a request containing information about a treee item expansion or
	 * collapsation.
	 *
	 * @param source
	 *            the workbench part that triggers this event
	 * @param event
	 *            the collapsation/expansion event
	 * @param expanded
	 *            commodity that indicates if the event is a collapsation or
	 *            expansion type
	 * @return
	 */
	public static NavigationRequest buildTreeExpansionRequest(final MessageSource source,
			final TreeExpansionEvent event, final boolean expanded) {
		final NavigationRequest request = new NavigationRequest(source);
		request.setTreeEvent(event);
		final OperationType op = (expanded) ? OperationType.TREEITEM_EXPANDED : OperationType.TREEITEM_COLLAPSED;
		request.setOperation(op);
		return request;
	}

	/**
	 * Construct a standard navigation request.
	 *
	 * @param source
	 * @param message
	 */
	public NavigationRequest(final MessageSource source, final String message) {
		super(source, message);
	}

	public NavigationRequest(final MessageSource source) {
		super(source);
	}

	public NavigationRequest(final MessageSource source, final String message, final Control activeShellElement) {
		super(source, message);
		this.activeShellElement = activeShellElement;
	}

	public NavigationRequest(final MessageSource source, final String message, final Event widgetSelectionEvent) {
		super(source, message);
		this.widgetEvent = widgetSelectionEvent;
	}

	public NavigationRequest(final MessageSource source, final String message, final IWorkbenchPart part) {
		super(source, message);
		this.part = part;
	}

	public NavigationRequest(final MessageSource source, final String message,
			final SelectionChangedEvent selectionChangeEvent) {
		super(source, message);
		this.selectionChangeEvent = selectionChangeEvent;
	}

	public NavigationRequest(final MessageSource source, final String message, final SelectionEvent selection) {
		super(source, message);
		this.selection = selection;
	}

	/**
	 * Return the active shell element used for reading elements such as text,
	 * tree item or table item.
	 *
	 * @return Control
	 */
	public Control getActiveShellElement() {
		return activeShellElement;
	}

	/**
	 * Return a Content outline selection, used by outline view processors.
	 *
	 * @return content outline selection
	 */
	public ContentOutline getContentOutline() {
		return contentOutline;
	}

	public ISelection getElementSelection() {
		return elementSelection;
	}

	public IWorkbenchPart getPart() {
		return part;
	}

	/**
	 * Return a selection, such a tree selection when browsing a generic tree
	 * (e.g. not the package explorer, that has a personal listener)
	 *
	 * @return SelectionEvent
	 */
	public SelectionEvent getSelection() {
		return selection;
	}

	/**
	 * Return a selection change event, used by the Package Explorer navigation.
	 *
	 * @return SelectionChangedEvent a selection change event of the package
	 *         explorer tree
	 */
	public SelectionChangedEvent getSelectionChangeEvent() {
		return selectionChangeEvent;
	}

	/**
	 * Return the event of expansion/collapse of an element of the package
	 * explorer tree.
	 *
	 * @return TreeExpansionEvent
	 */
	public TreeExpansionEvent getTreeEvent() {
		return treeEvent;
	}

	/**
	 * REturn a selection event of a widget, used by the Menu bar selector.
	 *
	 * @return Event
	 */
	public Event getWidgetEvent() {
		return widgetEvent;
	}

	public void setContentOutline(final ContentOutline contentOutline) {
		this.contentOutline = contentOutline;
	}

	public void setElementSelection(final ISelection elementSelection) {
		this.elementSelection = elementSelection;
	}

	public void setTreeEvent(final TreeExpansionEvent treeEvent) {
		this.treeEvent = treeEvent;
	}

	public void setSelectionChangeEvent(final SelectionChangedEvent selectionChangeEvent) {
		this.selectionChangeEvent = selectionChangeEvent;
	}
}
