package org.contextvox.plugin.requests;

/**
 * Request used for transmitting special or internal messages such the "stop"
 * command.
 */
public class SpecialRequest extends VoxRequest {

	private final Type type;

	private SpecialRequest(final MessageSource source, final Type type) {
		super(source);
		this.type = type;
	}

	/**
	 * Factory method that builds a stop request.
	 *
	 * @param source
	 *            listener generator
	 * @return a new request
	 */
	public static SpecialRequest buildStopRequest(final MessageSource source) {
		return new SpecialRequest(source, Type.STOP);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(obj instanceof SpecialRequest))
			return false;
		final SpecialRequest other = (SpecialRequest) obj;
		return super.equals(other) && this.type == other.type;
	}

	public Type getType() {
		return type;
	}

	@Override
	public int hashCode() {
		return super.hashCode() + this.type.hashCode();
	}

	@Override
	public String toString() {
		if (this.type == Type.STOP)
			return "stop request";
		return super.toString();
	}

	public enum Type {
		/**
		 * Stop command
		 */
		STOP;
	}
}
