package org.contextvox.plugin.requests;

import java.util.ArrayList;
import java.util.List;

import org.contextvox.plugin.state.VoxState;

/**
 * Represent an element of the user interface that can produce a feedback.
 * Combined with an {@link OperationType}, it is possible to describe the user
 * behavior and act.
 */
public enum MessageSource {

	/**
	 * A generic element of the current active shell
	 */
	WINDOW(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * A dialog in the current active shell
	 */
	DIALOG(WINDOW, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event triggered by the selection of the workbench part (views)
	 */
	VIEW(WINDOW, VoxState.VoxStateType.NAVIGATION),
	/**
	 * An element of the UI, such as trees, tables, links or text
	 */
	UI_ELEMENT(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * A link selected in the active shell
	 */
	LINK(UI_ELEMENT, VoxState.VoxStateType.NAVIGATION),
	/**
	 * A generic table selected in the active shell
	 */
	TABLE(UI_ELEMENT, VoxState.VoxStateType.NAVIGATION),
	/**
	 * An item of a table selected in the active shell
	 */
	TABLE_ITEM(UI_ELEMENT, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Text from a generic active shell element
	 */
	TEXT(UI_ELEMENT, VoxState.VoxStateType.NAVIGATION),
	/**
	 * A generic tree selected in an active shell
	 */
	TREE(UI_ELEMENT, VoxState.VoxStateType.NAVIGATION),
	/**
	 * An element selected in a generic tree of the active shell
	 */
	TREE_ITEM(UI_ELEMENT, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the Code inspector view
	 */
	CODEINSPECTOR(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the CommandLine of ContextVox
	 */
	COMMANDLINE(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the Console of Eclipse
	 */
	CONSOLE(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Editor view focus
	 */
	EDITOR(null, VoxState.VoxStateType.ANALYZE),
	/**
	 * Read an editor character
	 */
	EDITOR_READ_CHAR(EDITOR, VoxState.VoxStateType.ANALYZE),
	/**
	 * Read an editor line
	 */
	EDITOR_READ_LINE(EDITOR, VoxState.VoxStateType.ANALYZE),
	/**
	 * REad an editor word
	 */
	EDITOR_READ_WORD(EDITOR, VoxState.VoxStateType.ANALYZE),
	/**
	 * Text selection on editor
	 */
	EDITOR_SELECTION(EDITOR, VoxState.VoxStateType.ANALYZE),
	/**
	 * The user is typing on the active editor
	 */
	EDITOR_TYPING(EDITOR, VoxState.VoxStateType.ANALYZE),
	/**
	 * A generic message that can be read from the core, but may also be skipped
	 * in some particular situation
	 */
	GENERIC_FREE(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Generic message that will be explicitly read from the core
	 */
	GENERIC_READ(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Change into the java model
	 */
	JAVACHANGE_EVENT(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the JUnit view of Eclipse
	 */
	JUNIT(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Menu bar element
	 */
	MENU(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the Outline of Eclipse
	 */
	OUTLINE(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the Package Explorer of Eclipse
	 */
	PACKAGE_EXPLORER(null, VoxState.VoxStateType.NAVIGATION),
	/**
	 * Event from the Problem view of Eclipse
	 */
	PROBLEMS_VIEW(null, VoxState.VoxStateType.NAVIGATION);

	private final List<MessageSource> children = new ArrayList<>();
	private final MessageSource parent;
	private final VoxState.VoxStateType state;

	private MessageSource(final MessageSource parent, final VoxState.VoxStateType state) {
		this.parent = parent;
		this.state = state;
		if (parent != null)
			parent.children.add(this);
	}

	public List<MessageSource> getChildren() {
		return children;
	}

	public MessageSource getParent() {
		return parent;
	}

	public VoxState.VoxStateType getState() {
		return state;
	}

}