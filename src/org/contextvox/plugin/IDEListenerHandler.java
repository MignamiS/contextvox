package org.contextvox.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.contextvox.IDEobserver.listeners.EditorCaretListener;
import org.contextvox.IDEobserver.listeners.FineGrainListener;
import org.contextvox.IDEobserver.listeners.IDEListener;
import org.contextvox.IDEobserver.listeners.MenuBarListener;
import org.contextvox.IDEobserver.listeners.PackageExplorerListener;
import org.contextvox.IDEobserver.listeners.PartSelectionListener;
import org.contextvox.IDEobserver.listeners.UiElementListener;
import org.contextvox.IDEobserver.listeners.VoxTreeListener;
import org.contextvox.IDEobserver.listeners.WindowListener;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

class IDEListenerHandler {

	private static final Logger logger = Logger.getLogger(IDEListenerHandler.class.getName());

	private final ContextVox contextVox;

	private final Map<MessageSource, IDEListener> listeners;
	private Widget lastSelectedWidget;

	public IDEListenerHandler(final ContextVox contextVox) {
		this.contextVox = contextVox;
		this.listeners = new HashMap<>();
	}

	public void registerListener(final NavigationRequest request) {
		final Widget widget = request.widget;
		if (widget == null || widget.equals(lastSelectedWidget))
			return;

		if (widget instanceof Tree) {
			final VoxTreeListener listener = new VoxTreeListener((StimulusReceiver) this.contextVox);
			final Tree tree = (Tree) widget;
			tree.addTreeListener(listener);
			tree.addSelectionListener(listener);
			this.lastSelectedWidget = widget;
		}
	}

	private void setupDocumentListener() {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().addPartListener(new PartListener());
	}

	private void setupFineGrainListener(final StimulusReceiver cv) {
		final FineGrainListener fg = new FineGrainListener(cv);
		if (fg.start())
			listeners.put(MessageSource.EDITOR, fg);
		else
			logger.warning("something goes wrong when connecting listener for user input");
	}

	public void setupListener() {
		final StimulusReceiver cv = (StimulusReceiver) this.contextVox;

		setupPackageExplorer(cv);
		setupPartSelListener(cv);
		setupMenuBarListener(cv);
		setupWindowListener(cv);
		setupUIElementListener(cv);
		setupFineGrainListener(cv);

		// PackageExplorerListener.getInstance().start();
		// JavaChangeListener.getInstance().start();
		// CursorSelectionListener.getInstance().start();
		// ActiveShellListener.getInstance().start();
		// ActiveShellElementsListener.getInstance().start();
		// MenuBarListener.getInstance().start();
		// PartSelectionListener.getInstance().start();

		setupDocumentListener();

		// TODO remove this or change it, it isn't useful
		final int n = this.listeners.size();
		logger.info("" + n + " IDEObservers connected");
	}

	private void setupMenuBarListener(final StimulusReceiver cv) {
		final MenuBarListener mb = new MenuBarListener(cv);
		if (mb.start())
			listeners.put(MessageSource.MENU, mb);
		else
			logger.warning("something goes wrong when connecting listener for menu bar");
	}

	private void setupPackageExplorer(final StimulusReceiver cv) {
		final PackageExplorerListener packageExplorer = new PackageExplorerListener(cv);
		if (packageExplorer.start())
			listeners.put(MessageSource.PACKAGE_EXPLORER, packageExplorer);
		else
			logger.warning("something goes wrong when connecting listener for package explorer");
	}

	private void setupPartSelListener(final StimulusReceiver cv) {
		final PartSelectionListener ps = new PartSelectionListener(cv);
		if (ps.start())
			listeners.put(MessageSource.VIEW, ps);
		else
			logger.warning("something goes wrong when connecting listener for part selection");
	}

	private void setupUIElementListener(final StimulusReceiver cv) {
		final UiElementListener ase = new UiElementListener(cv);
		if (ase.start())
			listeners.put(MessageSource.UI_ELEMENT, ase);
		else
			logger.warning("something goes wrong when connecting listener for active shell elements");
	}

	private void setupWindowListener(final StimulusReceiver cv) {
		final WindowListener as = new WindowListener(cv);
		if (as.start())
			listeners.put(MessageSource.WINDOW, as);
		else
			logger.warning("something goes wrong when connecting listener for active shell focus");
	}

	public void tearDownListeners() {
		for (final MessageSource source : this.listeners.keySet()) {
			final boolean success = this.listeners.get(source).stop();
			if (!success)
				logger.warning("Something goes wrong when disconnecting Observer for " + source.toString());
		}

		// close all listener
		// PackageExplorerListener.getInstance().stop();
		// JavaChangeListener.getInstance().stop();
		// CursorSelectionListener.getInstance().stop();
		// ActiveShellListener.getInstance().stop();
		// ActiveShellElementsListener.getInstance().stop();
		// MenuBarListener.getInstance().stop();
		// PartSelectionListener.getInstance().stop();
		logger.info("IDEObservers disconnected");
	}

	/**
	 * Attaches the caret listener to each newly opened text editor.
	 */
	private final class PartListener implements IPartListener2 {

		private final Map<StyledText, EditorCaretListener> cache;

		public PartListener() {
			cache = new HashMap<>();
		}

		@Override
		public void partBroughtToTop(final IWorkbenchPartReference partRef) {
		}

		@Override
		public void partClosed(final IWorkbenchPartReference partRef) {
		}

		@Override
		public void partDeactivated(final IWorkbenchPartReference partRef) {
		}

		@Override
		public void partHidden(final IWorkbenchPartReference partRef) {
		}

		@Override
		public void partInputChanged(final IWorkbenchPartReference partRef) {
		}

		@Override
		public void partOpened(final IWorkbenchPartReference partRef) {
		}

		@Override
		public void partActivated(final IWorkbenchPartReference partRef) {
			/*
			 * http://stackoverflow. com/questions/8196143 /how-do-i-register-a-
			 * caret-listener-with- an-eclipse-editor
			 */
			if (partRef instanceof IEditorReference) {
				final AbstractTextEditor e = (AbstractTextEditor) ((IEditorReference) partRef).getEditor(false);
				final StyledText editor = (StyledText) e.getAdapter(Control.class);
				if (cache.get(editor) == null) {
					final EditorCaretListener listener = new EditorCaretListener((StimulusReceiver) contextVox, e);
					editor.addCaretListener(listener);
					cache.put(editor, listener);
					final IDocumentProvider docProv = ((ITextEditor) e).getDocumentProvider();
					final IEditorInput input = ((ITextEditor) e).getEditorInput();
					final IDocument doc = docProv.getDocument(input);
					doc.addDocumentListener(listener);
				}

			}
		}

		@Override
		public void partVisible(final IWorkbenchPartReference partRef) {
		}
	}
}
