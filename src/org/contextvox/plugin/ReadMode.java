package org.contextvox.plugin;

import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.MsgKey;

/**
 * ContextVox's read mode.
 */
public enum ReadMode {
	/**
	 * No abstraction, used for example to read punctual symbols.
	 */
	INSPECTION, /**
				 * First level of abstraction, with all possible information
				 * about a node.
				 */
	READ, /**
			 * Some "useless" information are omitted to reach a better
			 * performance when reading many lines of code.
			 */
	OVERVIEW;
	/**
	 * Given a read mode, returns the lower mode into the hierarchy.
	 * 
	 * @param mode
	 * @return the lower mode or the same if is the lowest
	 */
	public static ReadMode getLower(ReadMode mode) {
		if (mode == ReadMode.OVERVIEW)
			return ReadMode.READ;
		else if (mode == ReadMode.READ)
			return ReadMode.INSPECTION;
		else
			return mode;
	}

	/**
	 * Given a mode, returns the upper read mode on the hierarchy.
	 * 
	 * @param mode
	 * @return the upper read mode, or the same if no upper exists
	 */
	public static ReadMode getUpper(ReadMode mode) {
		if (mode == ReadMode.INSPECTION)
			return ReadMode.READ;
		else if (mode == ReadMode.READ)
			return ReadMode.OVERVIEW;
		else
			return mode;
	}

	/**
	 * Returns the next read mode on hierarchy, in an increasing order of
	 * abstraction high.
	 * 
	 * @param mode
	 * @return the upper mode on hierarchy or the lowest when top reached
	 */
	public static ReadMode next(ReadMode mode) {
		if (mode == ReadMode.OVERVIEW)
			return ReadMode.INSPECTION;
		else
			return getUpper(mode);
	}

	/**
	 * Returns the message related to the given read mode.
	 * 
	 * @param mode
	 * @return message
	 */
	public static String getMessage(ReadMode mode) {
		String key = "";
		switch (mode) {
		case INSPECTION:
			key = MsgKey.INSPECTION_MODE;
			break;
		case READ:
			key = MsgKey.READ_MODE;
			break;
		case OVERVIEW:
			key = MsgKey.OVERVIEW_MODE;
			break;
		}

		return Messages.getSingleMessage(key);
	}
}
