package org.contextvox.plugin;

import org.contextvox.IDEobserver.views.ContextVoxView;
import org.contextvox.IDEobserver.views.VoxConsoleView;
import org.contextvox.contextualizer.codeInspector.views.CodeInspectorView;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.internal.console.ConsoleView;
import org.eclipse.ui.internal.views.markers.ProblemsView;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.views.contentoutline.ContentOutline;

/**
 * Helper class that retreives Workbench and plugin elements.
 */
@SuppressWarnings("restriction")
public class PluginElements {

	/**
	 * Returns the active editor or the most recently opened one.
	 *
	 * @return the active editor or <code>null</code> if noone is opened
	 */
	public static IEditorPart getActiveEditor() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return null;
		final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
		if (workbenchWindow == null)
			return null;
		final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
		if (workbenchPage == null)
			return null;
		return workbenchPage.getActiveEditor();
	}

	/**
	 * Returns a text selection containing all the information about the cursor
	 * on the given editor.
	 *
	 * @param editor
	 *            the editor which contains the cursor. If <code>null</code>,
	 *            the active editor is taken.
	 * @return a text selection, or <code>null</code> if the cursor is not in a
	 *         text editor.
	 */
	public static ITextSelection getCurrentCursorPosition(ITextEditor editor) {
		if (editor == null) {
			if (getActiveEditor() instanceof ITextEditor)
				editor = (ITextEditor) getActiveEditor();
			else
				return null;
		}

		final ISelection sel = editor.getSelectionProvider().getSelection();
		if (sel != null && sel instanceof ITextSelection)
			return (ITextSelection) sel;
		else
			return null;
	}

	/**
	 * Returns the {@link IDocument} enclosed on the active editor.
	 *
	 * @return the document on the active editor or <code>null</code> if there
	 *         is not opened editor or if it is not a text editor.
	 */
	public static IDocument getDocumentOnActiveEditor() {
		final IEditorPart part = getActiveEditor();
		if (part instanceof ITextEditor) {
			final ITextEditor editor = (ITextEditor) part;
			return editor.getDocumentProvider().getDocument(editor.getEditorInput());
		}
		return null;
	}

	/**
	 * Returns the title of the current opened editor. This code has to run
	 * inside the UI-Thread.
	 *
	 * @return a string containing the current editor title, or an empty string
	 *         if no editor is active
	 */
	public static String getActiveEditorFilename() {
		final IEditorPart editor = getActiveEditor();
		if (editor != null)
			return editor.getTitle();
		else
			return "";
	}

	public static ContentOutline getOutline() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
		final String viewId = IPageLayout.ID_OUTLINE;
		final IViewReference viewReference = workbenchPage.findViewReference(viewId);
		return (ContentOutline) viewReference.getView(true);
	}

	public static ConsoleView getConsole() {
		ConsoleView consoleView = null;
		try {
			final IWorkbench workbench = PlatformUI.getWorkbench();
			final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
			final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
			final String viewId = IConsoleConstants.ID_CONSOLE_VIEW;
			consoleView = (ConsoleView) workbenchPage.showView(viewId);
		} catch (final PartInitException e) {
		}
		return consoleView;
	}

	public static ProblemsView getProblems() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
		final String viewId = IPageLayout.ID_PROBLEM_VIEW;
		final IViewReference viewReference = workbenchPage.findViewReference(viewId);
		return (ProblemsView) viewReference.getView(true);
	}

	/**
	 * Returns the Package Explorer or makes it visible if it is hidden.
	 *
	 * @return the Package Explorer or <code>null</code> if the current
	 *         perspective doesn't have it.
	 */
	public static PackageExplorerPart getPackageExplorer() {
		final PackageExplorerPart pxp = PackageExplorerPart.getFromActivePerspective();
		if (pxp == null)
			PackageExplorerPart.openInActivePerspective();
		return PackageExplorerPart.getFromActivePerspective();
	}

	/**
	 * Return the CV's command line
	 *
	 * @return the command line view
	 */
	public static ContextVoxView getContextVoxView() {
		ContextVoxView contextVoxView = null;
		try {
			final IWorkbench workbench = PlatformUI.getWorkbench();
			final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
			final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
			final String viewId = "org.contextvox.IDEobserver.views.ContextVoxView";
			contextVoxView = (ContextVoxView) workbenchPage.showView(viewId);
		} catch (final PartInitException e) {
		}
		return contextVoxView;
	}

	public static VoxConsoleView getVoxConsoleView() {
		final IWorkbenchWindow wbw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (wbw != null) {
			final IViewPart view = wbw.getActivePage().findView("org.contextvox.IDEobserver.views.VoxConsoleView");
			if (view != null)
				return (VoxConsoleView) view;
		}

		return null;
	}

	/**
	 * This method get the code inspector
	 *
	 * @return the code inspector view
	 */
	public static CodeInspectorView getCodeInspectorView() {
		CodeInspectorView codeInspectorView = null;
		try {
			final IWorkbench workbench = PlatformUI.getWorkbench();
			final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
			final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
			final String viewId = "org.contextvox.contextualizer.codeInspector.views.CodeInspectorView";
			codeInspectorView = (CodeInspectorView) workbenchPage.showView(viewId);
		} catch (final PartInitException e) {
		}
		return codeInspectorView;
	}

	/**
	 * This method get the junit view
	 *
	 * @return junit view
	 */
	public static IViewPart getJunit() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
		final String viewId = "org.eclipse.jdt.junit.ResultView";
		IViewPart viewPart = null;
		try {
			viewPart = workbenchPage.showView(viewId);
		} catch (final PartInitException e) {
			e.printStackTrace();
		}
		return viewPart;
	}

	public static IWorkbenchPart getActivePart() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage workbenchPage = workbenchWindow.getActivePage();
		return workbenchPage.getActivePart();
	}

	/**
	 * Returns the current selection using the selection service.
	 *
	 * @return the current selection, or <code>null</code>
	 */
	public ISelection getSelection() {
		final IWorkbench wb = PlatformUI.getWorkbench();
		if (wb != null) {
			final IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
			if (window != null) {
				return window.getSelectionService().getSelection();
			}
		}
		return null;
	}
}
