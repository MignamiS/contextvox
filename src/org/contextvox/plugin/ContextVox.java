package org.contextvox.plugin;

import java.util.logging.Logger;

import org.contextvox.plugin.ContextVoxProgram.SymbolLevel;
import org.contextvox.plugin.state.AbstractionLevel;
import org.contextvox.services.ttsengine.FeedbackData;
import org.contextvox.services.ttsengine.TTSSwitcher;

// TODO remove interface TTSService
public interface ContextVox {

	public void enableVoice(final boolean enable);

	public AbstractionLevel getAbstractionLevel();

	public SymbolLevel getSymbolLevel();

	/**
	 * ContextVox startup
	 */
	public void init();

	public boolean isDebugMode();

	public boolean isVoiceEnabled();

	/**
	 * This method has to be used only for logging vocal messages. Other log
	 * messages must be logged using the respective class logger.
	 *
	 * @param logger
	 *            the logger of the class that called this method
	 * @param request
	 *            the request
	 */
	public void log(Logger logger, FeedbackData request);

	/**
	 * Sets the symbol level that will be used to read the code. This feature
	 * reduces verbosity when reading without less useful symbols, such as
	 * semicolons.
	 *
	 * @param level
	 */
	public void setSymbolLevel(SymbolLevel level);

	/**
	 * Stops ContextVox
	 */
	public void shutdown();

	/**
	 * Cycles through abstraction levels available on the current state.
	 */
	public void switchAbstractionLevel();

	public TTSSwitcher getTTSSwitcher();
}
