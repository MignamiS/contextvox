package org.contextvox.plugin;

import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.services.ttsengine.CoreType;

public interface CoreMediator {

	/**
	 * Change core.
	 *
	 * @param type
	 *            the core type.
	 */
	public void switchCore(CoreType type);

	public void read(VoxRequest request);

	public void read(String message, MessageSource source);

	public CoreType getCurrentCoreType();

	public void readLastRequest();

	/**
	 * Stop the current speech.
	 */
	public void silence();
}
