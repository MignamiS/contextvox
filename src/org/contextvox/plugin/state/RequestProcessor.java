package org.contextvox.plugin.state;

import org.contextvox.plugin.requests.VoxRequest;

interface RequestProcessor {

	/**
	 * Process the request.
	 * 
	 * @param request
	 * @return whether the chain processed the request. If <code>false</code> is
	 *         returned, then there are no processor able to satisfy the
	 *         requirements
	 */
	boolean process(VoxRequest request);

}