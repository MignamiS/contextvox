package org.contextvox.plugin.state;

import org.contextvox.Activator;
import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.plugin.requests.VoxRequest.State;
import org.contextvox.preferences.PreferenceHelper;
import org.contextvox.services.ServiceHandler;
import org.contextvox.services.replacer.ReplacerService;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.text.EditorState;
import org.contextvox.services.text.TextualContextService;
import org.contextvox.services.ttsengine.TTSService;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * ContextVox state enabled when the user read and modify the source code.
 */
class AnalyzeState implements VoxState {

	// services
	private final TextualContextService texConService;
	private final TTSService ttsService;
	private final ReplacerService replacer;

	private AbstractionLevel abstraction;

	public AnalyzeState(final ContextVox contextVox) {
		final ServiceHandler handler = Activator.getServiceHandler();
		this.ttsService = handler.getTTSService();
		this.texConService = handler.getTextualContext();
		this.replacer = Activator.getServiceHandler().getReplacerService();

		this.abstraction = AbstractionLevel.INSPECTION;
	}

	private String checkLineIsEmpty(final String rawLine) {
		boolean isEmpty = true;
		for (int i = 0; i < rawLine.length(); i++) {
			if (!Character.isWhitespace(rawLine.charAt(i)))
				isEmpty = false;
		}

		if (isEmpty)
			return Messages.getSingleMessage("emptyLine");
		else
			return rawLine;
	}

	/*
	 * Determines which behavior to adopt, if it isn't set.
	 */
	private void determineBehavior(final AnalyzeRequest request, final ITextEditor editor) {
		if (request.getSource() != MessageSource.EDITOR)
			return;
		final EditorState context = texConService.get(editor);

		if (context.cursorMovingVer()) {
			request.setSource(MessageSource.EDITOR_READ_LINE);
		} else if (context.cursorMovingHor()) {
			if (context.isCharMoving())
				request.setSource(MessageSource.EDITOR_READ_CHAR);
			else if (context.isWordMoving())
				request.setSource(MessageSource.EDITOR_READ_WORD);
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null)
			return obj instanceof AnalyzeState;
		return false;
	}

	@Override
	public AbstractionLevel getLevel() {
		return this.abstraction;
	}

	@Override
	public VoxStateType getType() {
		return VoxStateType.ANALYZE;
	}

	@Override
	public void handle(final VoxRequest request) {
		if (request.getSource() == MessageSource.EDITOR_TYPING || request.getSource() == MessageSource.EDITOR_SELECTION)
			return;

		final AnalyzeRequest req = (AnalyzeRequest) request;
		// extract information
		final ITextEditor editor = req.getEditor();
		determineBehavior(req, editor);
		switch (request.getSource()) {
		case EDITOR_READ_LINE:
			readLine(request);
			break;

		case EDITOR_READ_WORD:
			readWord(request, texConService.get(editor));
			break;

		case EDITOR_READ_CHAR:
			readChar(request, texConService.get(editor));
			break;

		default:
			break;
		}
	}

	@Override
	public int hashCode() {
		return this.getType().hashCode();
	}

	@Override
	public void nextAbstractionLevel() {
		switch (abstraction) {
		case INSPECTION:
			this.abstraction = AbstractionLevel.READ;
			break;

		case READ:
			if (PreferenceHelper.isOverviewAvailable())
				this.abstraction = AbstractionLevel.OVERVIEW;
			else
				this.abstraction = AbstractionLevel.INSPECTION;
			break;

		case OVERVIEW:
			this.abstraction = AbstractionLevel.INSPECTION;
			break;

		default:
			throw new IllegalStateException("Illegal state " + this.abstraction.toString());
		}

		// TODO remove hardcoded message
		final String msg = "" + this.abstraction + " abstraction level selected";
		this.ttsService.read(msg, MessageSource.GENERIC_READ);
	}

	private void processAbstraction(final VoxRequest request) {
		// TODO Auto-generated method stub

	}

	private void processWithNoAbstraction(final AnalyzeRequest request) {
		final TextualContextService context = Activator.getServiceHandler().getTextualContext();
		final EditorState state = context.get(request.getEditor());
		String rawLine = state.getLine();
		rawLine = checkLineIsEmpty(rawLine);
		final String line = this.replacer.replaceSymbols(rawLine);
		request.setMessage(line);
		request.ready();
	}

	private void readChar(final VoxRequest request, final EditorState context) {
		final char c = context.lastTouchedWord().charAt(0);
		String character = "" + c;
		if (!Character.isLetter(c) && !Character.isDigit(c))
			// process symbols
			character = Messages.getSingleMessage(character);
		request.appendMessage(character);
		request.ready();
	}

	private void readLine(final VoxRequest genericRequest) {
		final AnalyzeRequest request = (AnalyzeRequest) genericRequest;
		// TODO sound effects

		if (abstraction == AbstractionLevel.READ || abstraction == AbstractionLevel.OVERVIEW) {
			processAbstraction(request);
			final State state = request.getState();
			if (state != State.READY || state != State.FORWARD)
				processWithNoAbstraction(request);
		} else if (abstraction == AbstractionLevel.INSPECTION) {
			processWithNoAbstraction(request);
		}
	}

	private void readWord(final VoxRequest request, final EditorState context) {
		String word = context.lastTouchedWord();
		word = Activator.getServiceHandler().getReplacerService().replaceSymbols(word);
		request.appendMessage(word);
		request.ready();
	}
}
