package org.contextvox.plugin.state;

import java.util.logging.Logger;

import org.contextvox.plugin.requests.VoxRequest;

/**
 * Implements the chain-of-responsability pattern. Each component of the chain
 * tries to process the request and, when it is not possible, the successor is
 * called. When the last discovers that there are no other processor, a value is
 * returned to inform the caller that the chain did not process the given
 * request.
 */
abstract class AbstractRequestProcessor implements RequestProcessor {

	protected final Logger logger = Logger.getLogger(AbstractRequestProcessor.class.getName());

	protected final RequestProcessor successor;

	protected AbstractRequestProcessor(final RequestProcessor successor) {
		this.successor = successor;
	}

	@Override
	public final boolean process(final VoxRequest request) {
		boolean proceed = check(request);
		if (proceed) {
			logger.fine("Request caught by " + getClass().getSimpleName());
			proceed = handle(request);
			if (proceed) {
				if (this.successor != null)
					return this.successor.process(request);
			}
			return true;
		} else {
			if (this.successor != null)
				return this.successor.process(request);
		}
		return proceed;
	}

	/**
	 * Returns whether the current processor is able to process the request.
	 *
	 * @param request
	 * @return <code>true</code> if the processor can catch the request,
	 *         otherwise the next element of the chain should be called
	 */
	protected abstract boolean check(VoxRequest request);

	/**
	 * The method that handles the request.
	 *
	 * @param request
	 * @return whether proceed or not. If <code>false</code> the chain stops
	 *         processing the request. Otherwise, the process continue normally
	 */
	protected abstract boolean handle(VoxRequest request);

}
