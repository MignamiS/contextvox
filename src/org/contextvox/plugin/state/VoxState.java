package org.contextvox.plugin.state;

import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.services.ServiceHandler;

/**
 * State pattern associate with ContextVox's states.
 */
public interface VoxState {

	/**
	 * Returns the abstraction level of this state.
	 *
	 * @return the abstraction level
	 */
	public AbstractionLevel getLevel();

	public VoxStateType getType();

	/**
	 * Handle the given request.
	 *
	 * @param request
	 */
	public void handle(VoxRequest request);

	/**
	 * Cycle to the next abstraction level. Also announces vocally the change.
	 */
	public void nextAbstractionLevel();

	/**
	 * ContextVox' states.
	 */
	public enum VoxStateType {

		/**
		 * GUI navigation, views and wizards.
		 */
		NAVIGATION,
		/**
		 * Code analysis
		 */
		ANALYZE;

		/**
		 * Builds a new instance of a VoxState, corresponding to the given type.
		 *
		 * @param type
		 * @param contextVox
		 * @return new instance
		 */
		public static VoxState getInstance(final VoxStateType type, final ContextVox contextVox,
				final ServiceHandler services) {
			switch (type) {
			case NAVIGATION:
				return new NavigationState(contextVox);

			case ANALYZE:
				return new AnalyzeState(contextVox);

			default:
				throw new UnsupportedOperationException("Type " + type.toString() + " not yet supported");
			}
		}
	}

}