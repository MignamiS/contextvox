package org.contextvox.plugin.state;

import java.util.logging.Logger;

import org.contextvox.plugin.ContextVox;

/**
 * Chain-of-responsability pattern.
 */
abstract class AnalyzeRequestProcessor implements RequestProcessor {
	// TODO refactor using abstract request processor instead

	protected static final Logger logger = Logger.getLogger(AnalyzeRequestProcessor.class.getName());

	protected final RequestProcessor successor;
	protected final ContextVox contextvox;
	protected NavigationState state;

	public AnalyzeRequestProcessor(final RequestProcessor successor, final ContextVox contextvox,
			final NavigationState state) {
		this.successor = successor;
		this.contextvox = contextvox;
		this.state = state;
	}

}
