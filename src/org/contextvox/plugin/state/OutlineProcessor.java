package org.contextvox.plugin.state;

import org.contextvox.IDEobserver.helpers.JavaElementsHelper;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.VoxRequest;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * Processes outline related requests.
 */
class OutlineProcessor extends NavigationRequestProcessor {

	public OutlineProcessor(final RequestProcessor successor, final NavigationState state) {
		super(successor, state);
	}

	@Override
	protected boolean check(final VoxRequest request) {
		final MessageSource source = request.getSource();
		return source == MessageSource.OUTLINE;
	}

	@Override
	protected boolean handle(final VoxRequest req) {
		final NavigationRequest request = (NavigationRequest) req;
		final ISelection sel = request.getSelectionChangeEvent().getSelection();
		if (!(sel instanceof TreeSelection))
			return false;

		final TreeSelection selected = (TreeSelection) sel;
		final Tree tree = ((TreeViewer) request.getSelectionChangeEvent().getSource()).getTree();
		final TreeItem[] items = tree.getSelection();

		if (items.length == 0 || items[0] == null)
			return false;
		final TreeItem treeItem = items[0];

		final Object element = selected.getFirstElement();
		if (!(element instanceof IJavaElement))
			return false;

		// build message
		String msg = "";
		final AbstractionLevel absLevel = this.state.getLevel();
		if (absLevel == AbstractionLevel.RICH) {
			msg = JavaElementsHelper.extractInfo((IJavaElement) element);
		} else {
			msg = treeItem.getText();
		}
		// msg = readOutlineElement(selected, items, treeItem, msg);

		request.appendMessage(msg);
		request.ready();
		return false;
	}

}
