package org.contextvox.plugin.state;

import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.VoxRequest;

/**
 * Processes Editor's events.
 */
class EditorProcessor extends AnalyzeRequestProcessor {

	public EditorProcessor(final RequestProcessor successor, final ContextVox contextvox, final NavigationState state) {
		super(successor, contextvox, state);
	}

	@Override
	public boolean process(final VoxRequest request) {
		final MessageSource source = request.getSource();
		if (source == MessageSource.EDITOR) {
			logger.fine("Request caught by Editor processor");
			handle(request);
			return true;
		} else {
			if (this.successor == null)
				return false;
			return this.successor.process(request);
		}
	}

	private void handle(final VoxRequest request) {
		// TODO move here ReadEditorJob's body.
	}

}
