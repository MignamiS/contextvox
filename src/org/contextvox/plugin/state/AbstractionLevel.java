package org.contextvox.plugin.state;

/**
 * Represent a way of announcing information to the user.
 *
 * @author Simone Mignami
 *
 */
public enum AbstractionLevel {

	/**
	 * Read GUI elements without extra information. (Navigation state).
	 */
	BARE,
	/**
	 * Read GUI elements adding information extracted from context. (Navigation
	 * state).
	 */
	RICH,
	/**
	 * Read code without interpretation. (Analyze state).
	 */
	INSPECTION,
	/**
	 * Read code in abstract way, but without omitting information. (Analyze
	 * state).
	 */
	READ,
	/**
	 * Fastly read code. (Analyze state).
	 */
	OVERVIEW;
}