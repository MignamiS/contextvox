package org.contextvox.plugin.state;

import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.VoxRequest;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Widget;

/**
 * Processes menu requests.
 */
class MenuBarProcessor extends NavigationRequestProcessor {

	public MenuBarProcessor(final RequestProcessor successor, final NavigationState state) {
		super(successor, state);
	}

	@Override
	protected boolean check(final VoxRequest request) {
		final MessageSource source = request.getSource();
		return source == MessageSource.MENU;
	}

	@Override
	protected boolean handle(final VoxRequest req) {
		final NavigationRequest request = (NavigationRequest) req;
		// TODO read also if item is available or not, mnemonic and other stuff
		final Widget widget = request.getWidgetEvent().widget;
		// Check if there is an event catch for another widget with this
		// listener, that is not a menubar item
		if (!(widget instanceof MenuItem))
			return false;
		String menuItem = ((MenuItem) widget).getText();
		menuItem = menuItem.replaceAll("&", "").replaceAll("\t", " ");
		request.appendMessage(menuItem);
		request.ready();
		return false;
	}

}
