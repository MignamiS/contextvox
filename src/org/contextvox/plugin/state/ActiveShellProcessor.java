package org.contextvox.plugin.state;

import org.contextvox.IDEobserver.helpers.WidgetsHelper;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.VoxRequest;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Processes request related to window and dialog.
 */
class ActiveShellProcessor extends NavigationRequestProcessor {

	public ActiveShellProcessor(final RequestProcessor successor, final NavigationState state) {
		super(successor, state);
	}

	@Override
	protected boolean check(final VoxRequest request) {
		final MessageSource source = request.getSource();
		return source == MessageSource.WINDOW || source.getParent() == MessageSource.WINDOW;
	}

	@Override
	protected boolean handle(final VoxRequest request) {
		final MessageSource source = request.getSource();
		String message = "";
		if (source == MessageSource.WINDOW) {
			// read view or part
			message = handleWindow(message);
		} else if (source == MessageSource.DIALOG) {
			// read dialog
			final Shell shell = WidgetsHelper.getDisplay().getActiveShell();
			// TODO read better
			message = shell.getText();
		} else if (source == MessageSource.VIEW) {
			// read view title
			final IWorkbenchPart part = ((NavigationRequest) request).getPart();
			message = part.getTitle();
			// message = MessageMaker.replaceSymbol(message, null);
			// this.contextvox.read(new VoxRequest(MessageSource.UI_VIEW,
			// message));
		}
		request.appendMessage(message);
		request.ready();
		return false;

	}

	private String handleWindow(String message) {
		final IWorkbenchPart activePart = WidgetsHelper.getActivePart();
		if (activePart != null)
			message = activePart.getTitle();
		return message;
	}

	// TODO implement as possible way to read wizards
	// private void putLablesToQueue(final Shell shell) {
	// final Control[] controls = shell.getTabList();
	// for (final Control control : controls) {
	// if (control instanceof Label) {
	// final String message = ((Label) control).getText();
	// contextVox.processRequest(
	// new VoxRequest(MessageSource.ACTIVESHELL_ELEMENT,
	// MessageMaker.replaceSymbol(message, null)));
	// }
	// }
	// }
}
