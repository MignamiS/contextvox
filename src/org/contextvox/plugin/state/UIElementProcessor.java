package org.contextvox.plugin.state;

import org.contextvox.IDEobserver.helpers.WidgetsHelper;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.OperationType;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * Processes request from UI elements such as normal text, links, tables and
 * trees.
 */
class UIElementProcessor extends NavigationRequestProcessor {

	// used to determine if the last processed part is the same
	private Object lastSourceContainingTree;
	private int level;

	public UIElementProcessor(final RequestProcessor successor, final NavigationState state) {
		super(successor, state);
	}

	@Override
	protected boolean check(final VoxRequest request) {
		final MessageSource source = request.getSource();
		final OperationType op = request.getOperation();
		boolean result = false;
		// tree event
		result = result || checkTreeEvent(source, op);
		return result;
	}

	private boolean checkTreeEvent(final MessageSource source, final OperationType op) {
		return source == MessageSource.TREE || op == OperationType.TREEITEM_EXPANDED
				|| op == OperationType.TREEITEM_COLLAPSED || op == OperationType.TREEITEM_SELECTED;
	}

	@Override
	protected boolean handle(final VoxRequest request) {
		final OperationType op = request.getOperation();
		final MessageSource source = request.getSource();
		if (checkTreeEvent(source, op)) {
			processTreeEvent((NavigationRequest) request);
		}

		// proceed only if the request is not ready
		return request.getState() != VoxRequest.State.READY;
	}

	private void processTreeEvent(final NavigationRequest request) {
		final MessageSource src = request.getSource();
		final OperationType op = request.getOperation();

		if (src == MessageSource.TREE) {
			if (request.widget instanceof Tree) {
				final TreeItem[] sel = ((Tree) request.widget).getSelection();
				if (sel.length > 0) {
					request.appendMessage(sel[0].getText());
					request.ready();
					return;
				}
			}
		} else if (op == OperationType.TREEITEM_EXPANDED) {
			final String msg = Messages.getSingleMessage("expanded");
			request.prepend(msg);
		} else if (op == OperationType.TREEITEM_COLLAPSED) {
			final String msg = Messages.getSingleMessage("collapsed");
			request.prepend(msg);
		} else if (op == OperationType.TREEITEM_SELECTED) {
			final String msg = processTreeLevel(request);
			request.prepend(msg);
		}
	}

	private String processTreeLevel(final NavigationRequest request) {
		String msg = "";
		final SelectionChangedEvent event = request.getSelectionChangeEvent();
		final Object src = event.getSource();
		if (event != null && src != null) {
			// check is the same tree
			if (src.equals(this.lastSourceContainingTree)) {
				// check level
				final ITreeSelection selection = (ITreeSelection) event.getSelection();
				final int lvl = WidgetsHelper.computeTreeLevel(selection);
				if (lvl != -1 && lvl != this.level)
					msg = Messages.getSingleMessage("level") + " " + lvl;
				this.lastSourceContainingTree = src;
				this.level = lvl;
			} else {
				// different tree, reset
				this.level = -1;
				this.lastSourceContainingTree = src;
			}
		}
		return msg;
	}

}
