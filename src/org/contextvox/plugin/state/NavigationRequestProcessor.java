package org.contextvox.plugin.state;

/**
 * Abstract processor of Navigation requests.
 */
abstract class NavigationRequestProcessor extends AbstractRequestProcessor {

	protected NavigationState state;

	public NavigationRequestProcessor(final RequestProcessor successor, final NavigationState state) {
		super(successor);
		this.state = state;
	}

}
