package org.contextvox.plugin.state;

import java.util.logging.Logger;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.listeners.selections.ProblemsSelection;
import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.core.nativeVox.translator.Symbol;
import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.services.replacer.ReplacerService;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.ttsengine.TTSService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.internal.views.markers.ProblemsView;

/**
 * State activated when navigating the GUI (package explorer, outline, menu,
 * ...).
 */
@SuppressWarnings("restriction")
class NavigationState implements VoxState {

	private static final Logger logger = Logger.getLogger(NavigationState.class.getName());

	private final ContextVox contextVox;

	private RequestProcessor processors;

	private AbstractionLevel abstractionLevel = AbstractionLevel.BARE;

	private final TTSService ttsService = Activator.getServiceHandler().getTTSService();

	@Override
	public AbstractionLevel getLevel() {
		return abstractionLevel;
	}

	public NavigationState(final ContextVox contextVox) {
		this.contextVox = contextVox;
		init();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null)
			return obj instanceof NavigationState;
		return false;
	}

	public ContextVox getContextVox() {
		return contextVox;
	}

	@Override
	public VoxStateType getType() {
		return VoxStateType.NAVIGATION;
	}

	@Override
	public void handle(final VoxRequest aRequest) {
		boolean success = this.processors.process(aRequest);
		if (!success || aRequest.getState() != VoxRequest.State.FORWARD
				|| aRequest.getState() != VoxRequest.State.READY) {
			// notify problem
			logger.severe("Request not handled by navigation state: " + aRequest);
			return;
		}

		final ReplacerService replacer = Activator.getServiceHandler().getReplacerService();
		final String newMessage = replacer.replaceSymbols(aRequest.getMessage());
		aRequest.setMessage(newMessage);

		// TODO brutal way to cut the code here (remove it, dead code)
		success = true;
		if (success)
			return;
		final NavigationRequest request = (NavigationRequest) aRequest;

		// TODO move these methods and delete this code
		switch (aRequest.getSource()) {
		case LINK:
		case TEXT:
			readSimpleASElement((NavigationRequest) aRequest);
			break;
		case WINDOW:
			readASElement(request);
			break;
		case TREE_ITEM:
			readASElementTreeItem(request);
			break;
		default:
			throw new UnsupportedOperationException(String.format("Source %s not yet supported", aRequest.getSource()));
		}
	}

	@Override
	public int hashCode() {
		return this.getType().hashCode();
	}

	private String extractText(final Link link) {
		final String text = link.getText();
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			if (ch == '<') {
				while (ch != '>' && i < text.length())
					ch = text.charAt(++i);
				if (i < text.length() - 1)
					ch = text.charAt(++i);
			}
			if (ch != '>')
				sb.append(ch);
		}
		return sb.toString();
	}

	// extract info method to speak

	// used by treeItem read event
	private boolean focusOnProblemsView(final TreeItem treeItem) {
		final boolean isProblems = PluginElements.getActivePart() instanceof ProblemsView;
		return !isExpandable(treeItem) && isProblems;

	}

	private void init() {
		// setup processors
		final PackageExplorerProcessor pe = new PackageExplorerProcessor(null, this);
		final OutlineProcessor op = new OutlineProcessor(pe, this);
		final MenuBarProcessor mb = new MenuBarProcessor(op, this);
		final ActiveShellProcessor asp = new ActiveShellProcessor(mb, this);
		final UIElementProcessor ui = new UIElementProcessor(asp, this);
		processors = ui;

	}

	// used by treeItem read event
	private boolean isExpandable(final TreeItem treeItem) {
		return treeItem.getItemCount() > 0;
	}

	private void readASElement(final NavigationRequest request) {
		// TODO Auto-generated method stub

	}

	private void readASElementTreeItem(final NavigationRequest request) {
		/*
		 * TODO move this multiple selection function into the Selection State,
		 * and keep the navigation only for a single item.
		 */
		// final TreeItem[] treeItems = tree.getSelection();
		String message = "";
		if (request.getSelection() != null) {
			// read the selected item normally
			final TreeItem item = (TreeItem) request.getSelection().item;

			// TODO improve this solution using another Message builder
			final MessageBuilder mb = new MessageBuilder();
			mb.add(item.getText());
			if (isExpandable(item))
				mb.add(Messages.getSingleMessage("isExpandable"));
			message = Activator.getServiceHandler().getReplacerService().replaceSymbols(mb.toString());

			// if the tree is the Problem View, read also the resource involved.
			if (focusOnProblemsView(item)) {
				message += Symbol.spaceStr + Messages.getSingleMessage("inResource");
				message += Symbol.spaceStr + ProblemsSelection.getInstance().getResourceName();
			}
		} else if (request.getWidgetEvent() != null) {
			// tree item has been expanded / collapsed
			final int eventType = request.getWidgetEvent().type;
			if (eventType == SWT.Expand) {
				final MessageBuilder mb = new MessageBuilder();
				final TreeItem treeItem = (TreeItem) request.getWidgetEvent().item;
				mb.add(treeItem.getText());
				mb.add(Messages.getSingleMessage("expanded"));
				request.appendMessage(mb.toString());
				this.ttsService.read(request);
			} else if (eventType == SWT.Collapse) {
				final MessageBuilder mb = new MessageBuilder();
				final TreeItem treeItem = (TreeItem) request.getWidgetEvent().item;
				mb.add(treeItem.getText());
				mb.add(Messages.getSingleMessage("collapsed"));
				request.appendMessage(mb.toString());
				this.ttsService.read(request);
			}
		}

		// finally read the message
		request.appendMessage(message);
		this.ttsService.read(request);

		// for (final TreeItem treeItem : treeItems) {
		// final MessageBuilder mb = new MessageBuilder();
		// mb.add(treeItem.getText());
		// if (isExpandable(treeItem))
		// mb.add(Messages.getMessage("isExpandable"));
		// String message = MessageMaker.replaceSymbol(mb.toString(),
		// JavaSymbolReplacer.configFastRead());
		// if (focusOnProblemsView(treeItem))
		// message += Symbol.spaceStr +
		// ProblemsSelection.getInstance().getResourceName();
		// CoreProxy.readString(message,
		// MessageSource.ACTIVESHELL_ELEMENT_TREE_ITEM);
		// }
	}

	/*
	 * Read a simple Active Shell element, such as text or link
	 */
	private void readSimpleASElement(final NavigationRequest request) {
		final Control element = request.getActiveShellElement();
		if (element == null)
			return;

		if (element instanceof Text) {
			// read as text
			final String message = ((Text) element).getMessage();
			if (message != null && !message.isEmpty())
				this.ttsService.read(message, MessageSource.TEXT);
			if (((Text) element).getEditable())
				this.ttsService.read(Messages.getSingleMessage("insertText"), MessageSource.TEXT);
		} else if (element instanceof Link) {
			final String text = extractText((Link) element);
			this.ttsService.read(text, MessageSource.LINK);
		}
	}

	@Override
	public void nextAbstractionLevel() {
		final AbstractionLevel tmp = this.abstractionLevel;
		if (this.abstractionLevel == AbstractionLevel.BARE)
			this.abstractionLevel = AbstractionLevel.RICH;
		else
			this.abstractionLevel = AbstractionLevel.BARE;
		// TODO remove hardcoded message
		final String str = "Switched abstraction level from " + tmp + " to " + this.abstractionLevel;
		logger.info(str);
		// announce
		final String resp = "" + this.abstractionLevel + " abstraction level selected";
		ttsService.read(new VoxRequest(MessageSource.GENERIC_READ, resp));
	}

}
