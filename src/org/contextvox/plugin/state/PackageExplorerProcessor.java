package org.contextvox.plugin.state;

import org.contextvox.IDEobserver.helpers.JavaElementsHelper;
import org.contextvox.IDEobserver.helpers.WidgetsHelper;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.OperationType;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;

/**
 * Processes package explorer related requests.
 */
class PackageExplorerProcessor extends NavigationRequestProcessor {

	public PackageExplorerProcessor(final RequestProcessor successor, final NavigationState state) {
		super(successor, state);
	}

	@Override
	protected boolean check(final VoxRequest request) {
		return request.getSource() == MessageSource.PACKAGE_EXPLORER;
	}

	@Override
	protected boolean handle(final VoxRequest aRequest) {
		final NavigationRequest request = (NavigationRequest) aRequest;
		final OperationType op = request.getOperation();
		if (op == OperationType.TREEITEM_SELECTED && request.getSelectionChangeEvent() != null) {
			// read selection modification
			final ITreeSelection sel = (ITreeSelection) request.getSelectionChangeEvent().getSelection();
			extractInfoFromTreeItem(sel, request);
		} else if ((op == OperationType.TREEITEM_COLLAPSED || op == OperationType.TREEITEM_EXPANDED)
				&& request.getTreeEvent() != null) {
			final ITreeSelection sel = (ITreeSelection) WidgetsHelper.getActivePart().getSite().getSelectionProvider()
					.getSelection();
			extractInfoFromTreeItem(sel, request);
		}
		request.ready();
		return false;
	}

	private void extractInfoFromTreeItem(final ITreeSelection item, final NavigationRequest request) {
		final Object sel = item.getFirstElement();
		if (sel == null)
			return;

		String msg;
		final AbstractionLevel absLevel = this.state.getLevel();
		if (absLevel == AbstractionLevel.RICH) {
			// add extra info
			if (sel instanceof IJavaElement) {
				// read java element
				msg = JavaElementsHelper.extractInfo((IJavaElement) sel);
			} else {
				// no java element
				msg = Messages.getSingleMessage("noJavaElement");
				readLabel();
			}
		} else {
			// read just the label
			msg = readLabel();
		}

		request.appendMessage(msg);
	}

	private String readLabel() {
		final Control control = Display.getCurrent().getFocusControl();
		if (control == null || !(control instanceof Tree))
			return "";

		final Tree tree = (Tree) control;
		if (tree.getSelection().length > 0)
			return tree.getSelection()[0].getText();
		return "";
	}

}
