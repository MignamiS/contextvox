package org.contextvox.plugin;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.helpers.EditorInformations;
import org.contextvox.IDEobserver.helpers.WorkbenchHelper;
import org.contextvox.plugin.requests.AnalyzeRequest;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.plugin.requests.NavigationRequest;
import org.contextvox.plugin.requests.SpecialRequest;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.plugin.requests.VoxRequest.State;
import org.contextvox.plugin.state.AbstractionLevel;
import org.contextvox.plugin.state.VoxState;
import org.contextvox.plugin.state.VoxState.VoxStateType;
import org.contextvox.services.ServiceHandler;
import org.contextvox.services.replacer.MessageMaker;
import org.contextvox.services.replacer.ReplacerOption;
import org.contextvox.services.replacer.ReplacerService;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.SentenceAlternative;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.contextvox.services.text.MotionEvent;
import org.contextvox.services.text.TextualContextHandler;
import org.contextvox.services.text.TextualContextService;
import org.contextvox.services.ttsengine.CoreType;
import org.contextvox.services.ttsengine.FeedbackData;
import org.contextvox.services.ttsengine.TTSHandler;
import org.contextvox.services.ttsengine.TTSService;
import org.contextvox.services.ttsengine.TTSSwitcher;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.texteditor.ITextEditor;

public class ContextVoxProgram implements ContextVox, StimulusReceiver, ServiceHandler, ReplacerService {

	private static final Logger logger = Logger.getLogger(ContextVoxProgram.class.getName());

	private final Status contextVoxStatus;
	private final IDEListenerHandler listenerHandler;

	// services
	private final TTSHandler ttsHandler;
	private final TextualContextHandler textualContext;

	private final Map<VoxState.VoxStateType, VoxState> states;
	private VoxStateType currentState = VoxStateType.NAVIGATION;

	@Deprecated
	public static ReadMode getReadMode() {
		return null;
	}

	public SymbolLevel getSymbolLevel() {
		// TODO implement
		return SymbolLevel.ALL;
	}

	/**
	 * Returns if CVox's abstract language is enabled.
	 *
	 * @return <code>true</code> if the abstract language is enabled,
	 *         <code>false</code> otherwise.
	 */
	@Deprecated
	public static boolean isHighLevelReading() {
		// return readMode != ReadMode.INSPECTION;
		return true;
	}

	@Deprecated
	public static boolean isShowWhitespacesOnConsole() {
		return false;
	}

	/**
	 * Start a read command, that can change the program state.
	 *
	 * @param request
	 *            a VoxRequest without message, that will be filled when the
	 *            request will be processed.
	 */
	@Deprecated
	public static void readFocus(final VoxRequest request) {
		// BaseVoxState state = null;
		// // be sure to pass the request to the right state
		// synchronized (ContextVoxProgram.class) {
		// if (CVState.getType() == request.getState()) {
		// state = CVState;
		// } else {
		// // chainge state
		// final BaseVoxState.Type previousState = CVState.getType();
		// CVState = request.getState().getStateInstance();
		// state = CVState;
		// VoxLogger.log(LogLevel.DEBUG, String.format("State %s (previous %s)",
		// CVState.getType(), previousState),
		// null);
		// }
		// }
		//
		// // now read with the right state
		// state.readFocus(request);
	}

	@Deprecated
	public static void setReadMode(final ReadMode readMode) {
		// final ReadMode prev = ContextVoxProgram.readMode;
		// ContextVoxProgram.readMode = readMode;
		//
		// // print debug information
		// final String str = String.format("Read mode switched from %s to %s",
		// prev, readMode);
		// VoxLogger.log(LogLevel.DEBUG, str);
	}

	@Deprecated
	public static void setShowWhitespacesOnConsole(final boolean showWhitespacesOnConsole) {
		// TODO remove it
		// ContextVoxProgram.showWhitespacesOnConsole =
		// showWhitespacesOnConsole;
	}

	public void setSymbolLevel(final SymbolLevel symbolLevel) {
		// TODO implement
	}

	public ContextVoxProgram() {
		this.listenerHandler = new IDEListenerHandler(this);
		this.ttsHandler = new TTSHandler(this);
		this.contextVoxStatus = new Status(Activator.getDefault().getPreferenceStore());
		this.states = new HashMap<>();
		this.textualContext = new TextualContextHandler();
	}

	/**
	 * @param enable
	 * @see org.contextvox.plugin.Status#enableVoice(boolean)
	 */
	@Override
	public void enableVoice(final boolean enable) {
		contextVoxStatus.enableVoice(enable);
	}

	@Override
	public void init() {
		logger.info("ContextVox instance initialization");
		this.ttsHandler.init();
		this.listenerHandler.setupListener();
		setupStates();
	}

	/**
	 * @return
	 * @see org.contextvox.plugin.Status#isDebugMode()
	 */
	@Override
	public boolean isDebugMode() {
		return contextVoxStatus.isDebugMode();
	}

	/**
	 * @return
	 * @see org.contextvox.plugin.Status#isVoiceEnabled()
	 */
	@Override
	public boolean isVoiceEnabled() {
		return contextVoxStatus.isVoiceEnabled();
	}

	@Override
	public void log(final Logger logger, final FeedbackData request) {
		// check preferences, if DEBUG enabled log in vox console
		final String str = String.format("[%s] %s", request.getSource(), request.getMessage());
		// System.out.println(str);
	}

	@Override
	public void processRequest(final VoxRequest request) {
		if (!isVoiceEnabled())
			return;

		preprocessRequest(request);

		State state = request.getState();
		if (state != State.ABORT && state != State.SUSPEND) {
			// standard
			updateContext(request);
			this.currentState = request.getSource().getState();
			this.states.get(this.currentState).handle(request);
		}

		state = request.getState();
		if (state == State.FORWARD || state == State.READY) {
			ttsHandler.read(request);
		}
	}

	private void updateContext(final VoxRequest request) {
		if (request instanceof AnalyzeRequest) {
			final AnalyzeRequest req = (AnalyzeRequest) request;
			ITextEditor editor = req.getEditor();
			if (editor == null)
				/*
				 * TODO warning, this might not be the correct editor. Try
				 * another way to get it. Also check if the text selection is
				 * not a text field selection or such...
				 */
				editor = (ITextEditor) PluginElements.getActiveEditor();
			final int newTextOffset = req.getOffset();
			if (newTextOffset == -1)
				return;
			if (WorkbenchHelper.getSelectionService().getSelection() instanceof ITextSelection) {
				final ITextSelection selection = ((ITextSelection) WorkbenchHelper.getSelectionService()
						.getSelection());
				// caret motion event only
				if (selection.getLength() == 0) {
					final IDocument doc = EditorInformations.getDocumentFromEditor(editor);
					if (doc == null)
						return;

					final MotionEvent e = new MotionEvent();
					try {
						/*
						 * Because the caret listener triggers BEFORE that the
						 * document is updated, the selection shows the old
						 * cursor position. therefore we need to use the new
						 * caret position to infer the new information palette.
						 */
						final int startLine = doc.getLineOfOffset(newTextOffset);
						final IRegion lineInfo = doc.getLineInformationOfOffset(newTextOffset);
						final int lineOffset = lineInfo.getOffset();
						final int lineLenght = lineInfo.getLength();

						e.setCurrentLine(startLine);
						final String line = doc.get(lineOffset, lineLenght);
						e.setLine(line);
						e.setLineLenght(lineLenght);
						e.setLineOffset(lineOffset);
						e.setTextOffset(newTextOffset);
					} catch (final BadLocationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					this.textualContext.addEvent(editor, e);
				}
				// TODO other motion events, such as selection
			}
		}

	}

	/**
	 * Some request may change the program state and should be processed before.
	 *
	 * @param request
	 */
	private void preprocessRequest(final VoxRequest request) {
		// a generic tree is selected, register listener
		if (request.getSource() == MessageSource.TREE)
			this.listenerHandler.registerListener((NavigationRequest) request);

		// special request
		if (request instanceof SpecialRequest) {
			final SpecialRequest special = (SpecialRequest) request;
			// stop command
			if (special.getType() == SpecialRequest.Type.STOP) {
				this.ttsHandler.silence();
				request.setState(VoxRequest.State.ABORT);
			}
		}
	}

	@Override
	public void shutdown() {
		logger.fine("ContextVox shutdown procedure started...");
		this.listenerHandler.tearDownListeners();
		// close the inpustream of cache
		// Speech.myClose();

	}

	private void setupStates() {
		for (final VoxStateType type : VoxState.VoxStateType.values()) {
			final VoxState value = VoxState.VoxStateType.getInstance(type, this, this);
			states.put(type, value);
		}
	}

	/**
	 * Which symbols can be read when java code is processed by symbol replacer
	 */
	public enum SymbolLevel {
		/**
		 * All symbols are processed and read.
		 */
		ALL,
		/**
		 * Only the most relevant symbols are processed.
		 */
		SOME;

	}

	@Override
	public void switchAbstractionLevel() {
		states.get(currentState).nextAbstractionLevel();
	}

	@Override
	public AbstractionLevel getAbstractionLevel() {
		return this.states.get(this.currentState).getLevel();
	}

	@Override
	public String replaceSymbols(final String str) {
		final EnumSet<ReplacerOption> config = produceReplacerConfiguration(false);
		return MessageMaker.replaceSymbols(str, config);
	}

	private EnumSet<ReplacerOption> produceReplacerConfiguration(final boolean sentences) {
		final EnumSet<ReplacerOption> config = EnumSet.noneOf(ReplacerOption.class);
		// symbol level
		final SymbolLevel symbolLevel = getSymbolLevel();
		switch (symbolLevel) {
		case ALL:
			config.add(ReplacerOption.SYMBOL_ALL);
			break;
		case SOME:
			config.add(ReplacerOption.SYMBOL_FAST);
		default:
			break;
		}
		// sentences
		if (sentences)
			config.add(ReplacerOption.SENTENCES);

		config.add(ReplacerOption.COMPLEX_SYMBOL);

		// camle case support
		if (this.ttsHandler.getCurrentCoreType() == CoreType.NATIVE_VOX)
			config.add(ReplacerOption.CAMEL_CASE);

		return config;
	}

	@Override
	public String makeSentence(final SentenceType type, final Map<SentenceToken, String> data) {
		if (type == null || data == null)
			throw new NullPointerException();
		final EnumSet<ReplacerOption> config = produceReplacerConfiguration(true);
		return MessageMaker.makeSentence(type, data, config);
	}

	@Override
	public String makeSentence(final SentenceType type, final Map<SentenceToken, String> data,
			final SentenceAlternative alternative) {
		if (type == null || data == null || alternative == null)
			throw new NullPointerException();
		final EnumSet<ReplacerOption> config = produceReplacerConfiguration(true);
		return MessageMaker.makeSentence(type, data, alternative, config);
	}

	@Override
	public TTSService getTTSService() {
		return this.ttsHandler;
	}

	@Override
	public ReplacerService getReplacerService() {
		return this;
	}

	@Override
	public TextualContextService getTextualContext() {
		return this.textualContext;
	}

	@Override
	public TTSSwitcher getTTSSwitcher() {
		return this.ttsHandler;
	}

}
