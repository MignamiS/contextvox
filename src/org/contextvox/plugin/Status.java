package org.contextvox.plugin;

import org.contextvox.preferences.PreferenceConstants;
import org.eclipse.jface.preference.IPreferenceStore;

public class Status {

	private final IPreferenceStore preferences;
	private volatile boolean voiceEnabled = true;

	public Status(final IPreferenceStore preferenceStore) {
		preferences = preferenceStore;
	}

	public void enableVoice(final boolean enable) {
		this.voiceEnabled = enable;
	}

	public boolean isDebugMode() {
		return this.preferences.getBoolean(PreferenceConstants.DEBUG_MODE_KEY);
	}

	public boolean isVoiceEnabled() {
		return this.voiceEnabled;
	}
}
