package org.contextvox.plugin;

import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.resources.IMarker;

/**
 * Handles the anchor point on the editor. The class is not thread-safe, but the
 * visibility of the changes is guaranteed.
 */
public class BookmarkManager {
	private static final AtomicReference<IMarker> bookmark = new AtomicReference<>();

	public static IMarker getBookmark() {
		return bookmark.get();
	}

	public static void setBookmark(final IMarker bookmark) {
		BookmarkManager.bookmark.set(bookmark);
	}

	public static void deleteBookmark() {
		bookmark.set(null);
	}
}
