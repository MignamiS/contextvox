package org.contextvox;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.contextvox.plugin.ContextVox;
import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.VoxLogFormatter;
import org.contextvox.services.ServiceHandler;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.ttsengine.FeedbackFilter;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchListener;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class Activator extends AbstractUIPlugin {

	// plug-in ID
	public static final String PLUGIN_ID = "org.contextvox";// "org.contextvox_v2"; //$NON-NLS-1$
	// shared instance of the program activator
	private static Activator plugin;
	// program instance
	private static ContextVox contextVoxInstance;
	private static ServiceHandler serviceHandler;

	private static WBListener wbl;

	/**
	 * Returns the ContextVox's instance.
	 *
	 * @return the singleton instance
	 */
	public static ContextVox cvox() {
		if (contextVoxInstance == null)
			throw new IllegalStateException("No ContextVox instance found");
		return contextVoxInstance;
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns the current service handler.
	 *
	 * @return the service handler
	 */
	public static ServiceHandler getServiceHandler() {
		return serviceHandler;
	}

	public Activator() {
	}

	/**
	 * Returns the ContextVox instance.
	 *
	 * @return plugin instance
	 * @deprecated use the static method {@link #cvox()}
	 */
	@Deprecated
	public ContextVox contextVox() {
		if (contextVoxInstance == null)
			throw new IllegalStateException("No ContextVox instance found");
		return contextVoxInstance;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
	 * BundleContext )
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

		// logger
		// TODO preferences setup
		final Logger root = LogManager.getLogManager().getLogger("");
		root.setLevel(Level.FINE);
		final Handler handler = root.getHandlers()[0];
		handler.setLevel(Level.FINE);
		handler.setFormatter(new VoxLogFormatter());
		handler.setFilter(new FeedbackFilter());

		// messages initialization
		Messages.load(Messages.DEFAULT_LANGUAGE);

		// add listener to workbench for close correctly all listener
		wbl = new WBListener();
		plugin.getWorkbench().addWorkbenchListener(wbl);

		final ContextVoxProgram instance = new ContextVoxProgram();
		contextVoxInstance = instance;
		serviceHandler = instance;
		contextVoxInstance.init();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.
	 * BundleContext )
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		plugin.getWorkbench().removeWorkbenchListener(wbl);
		plugin = null;
		super.stop(context);
	}

	/**
	 * The activator class controls the plug-in life cycle
	 */
	class WBListener implements IWorkbenchListener {

		@Override
		public void postShutdown(final IWorkbench workbench) {
		}

		/**
		 * http://stackoverflow.com/questions/10579716/eclipse-shut-down-hook-able-
		 * to-stop-the-termination
		 */
		@Override
		public boolean preShutdown(final IWorkbench workbench, final boolean forced) {
			// TODO move to postShutdown to avoid fake shutdown when aborting
			// workspace close
			contextVoxInstance.shutdown();
			return true;
		}

	}
}