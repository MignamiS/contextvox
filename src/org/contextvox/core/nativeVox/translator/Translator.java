package org.contextvox.core.nativeVox.translator;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.contextvox.services.replacer.dictionaries.Messages;

import com.gtranslate.Audio;


/**
 * @desc This class receive a Text with a language and transform a string to InputStream
 */

public class Translator {
	private final List<String> subTexts;

	public Translator(final String text) {
		//get string until 100 words
		subTexts = new TextSplitter(text).split(100);
	}
	
	//get all inputstream (Audio) of a string
	public List<InputStream> messageToAudio(){
		List<InputStream> listAudio=new ArrayList<InputStream>();
		for (final String subText : subTexts) {
			if (subText.equals(""))
				continue;
			
			final Audio audio = Audio.getInstance();
			String encodedText=null;
			try {
				encodedText = URLEncoder.encode(subText, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				//FAR DIRE L'ERRORE 
				System.err.println("Error encoding");
				e.printStackTrace();
			}
			try {
				//add inputStream of string 
				listAudio.add(audio.getAudio(encodedText, Messages.getCurrentLanguage().getIdentifier()));
			} catch (IOException e) {
				System.err.println("Error get audio");
				//FAR DIRE L'ERRORE 
				e.printStackTrace();
			}
		}
		return listAudio;
	}
	
}
