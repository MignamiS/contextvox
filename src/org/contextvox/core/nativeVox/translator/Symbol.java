package org.contextvox.core.nativeVox.translator;

/**
 * Contains some useful symbol.
 */
public class Symbol {

	public static final char space = ' ';
	public static final char comma = ',';
	public static final char colon = ':';
	public static final char point = '.';

	public static final String spaceStr = String.valueOf(space);
	public static final String commaStr = String.valueOf(comma);
	public static final String colonStr = String.valueOf(colon);
	public static final String pointStr = String.valueOf(point);

	/**
	 * Plattform dependent carriage return
	 */
	public static final String carriageReturnStr = System.lineSeparator();
}
