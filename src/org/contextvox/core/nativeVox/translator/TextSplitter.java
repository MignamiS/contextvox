package org.contextvox.core.nativeVox.translator;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @desc This class split the text, principally used per google  
 *
 */
public class TextSplitter {

	private final String text;

	public TextSplitter(final String text) {
		this.text = text;
	}

	/**
	 * 
	 * @param bound the max length of character 
	 * @return List of String with the final text
	 */
	public List<String> split(final int bound) {
		if (text.length() <= bound) {
			final List<String> subTexts = new ArrayList<>();
			subTexts.add(text);
			return subTexts;
		}
		return splitText(bound);
	}

	/**
	 * 
	 * @param bound the max length of character 
	 * @return List of String with the final text
	 */
	private List<String> splitText(final int bound) {
		final List<String> subTexts = new ArrayList<>();
		int min = 0, max;
		while (min + bound < text.length()) {
			max = bound;
			final String subText = text.substring(min, min + max);
			boolean containsSeparator = false;
			//verify presence of a point
			if (subText.contains(Symbol.pointStr)) {
				containsSeparator = true;
				do {
					max--;
				} while (subText.charAt(max) != Symbol.point);
				//other case verify presence of a space
			} else if (subText.contains(Symbol.spaceStr)) {
				containsSeparator = true;
				do {
					max--;
				} while (subText.charAt(max) != Symbol.space);
			}
			//save the final text
			if (max > 0)
				subTexts.add(subText.substring(0, max));
			min += max;
			if (containsSeparator)
				min++;
		}
		subTexts.add(text.substring(min, text.length()));
		return subTexts;
	}

}
