package org.contextvox.core.nativeVox.translator;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * FIXME LG: Trovare una soluzione pi� pulita. Fatta in questa maniera per
 * evitare di chiudere la stream dopo averla riprodotta (Audio.play() chiama
 * automaticamente close() quando la stream � finita). Evitando la chiusura la
 * stream pu� essere riutilizzata.
 */
class SydBufferedInputStream extends BufferedInputStream {
	public SydBufferedInputStream(final InputStream in) {
		super(in);
		// Permette di usare il metodo reset() per riportare la stream
		// all'inizio
		markpos = 0;
	}

	@Override
	public void close() throws IOException {
	}

	public void closeForReal() throws IOException {
		super.close();
	}
}

/**
 * This class implements the single char cache
 */

public class CharCache {
	public static final String LETTERS_PATH = "mp3/letters/";
	public static final String NUMBERS_PATH = "mp3/numbers/";
	public static final String SPECIAL_CHARS_PATH = "mp3/specialChars/";
	public static final String ERRORS_PATH = "mp3/errors/";
	public static final String SPECIAL_CHARS[] = { "<", ">", "&", "=", "\\",
		"!", "(", ")", "[", "]", "{", "}", ";", ",", ".", "'", "\"", "~",
		"?", ":", "+", "-", "_", "*", "/", "|", "^", "�", "%" };
	
	private Map<String, Map<String, SydBufferedInputStream>> charMap = null;
	private Map<String, SydBufferedInputStream> currentCharMap = null;
	private static CharCache cache;
	
	public void setLanguage(String language){
		loadCharMap(language);
		currentCharMap = charMap.get(language);
	}
	
	public static synchronized CharCache getInstance() {
		if (cache == null)
			cache = new CharCache();
		return cache;
	}
	
	
	public Map<String, Map<String, SydBufferedInputStream>> getCharMap() {
		return charMap;
	}

	public void setCurrentCharMap(Map<String, SydBufferedInputStream> currentCharMap) {
		this.currentCharMap = currentCharMap;
	}

	public Map<String, SydBufferedInputStream> getCurrentCharMap() {
		return currentCharMap;
	}



	//caching character
	private void loadCharMap(final String language) {
		Map<String, SydBufferedInputStream> map = null;

		if (charMap == null)
			charMap = new HashMap<>();
		else
			map = charMap.get(language);

		if (map == null) {
			map = new HashMap<String, SydBufferedInputStream>();
			charMap.put(language, map);
		}

		// Load letters
		for (char i = 'a'; i <= 'z'; i++) {
			map.put("" + i,
					new SydBufferedInputStream(this.getClass()
							.getResourceAsStream(
									"/" + LETTERS_PATH + language + "/" + i
											+ ".mp3")));
		}

		// Load numbers
		for (int i = 0; i <= 9; i++) {
			map.put("" + i,
					new SydBufferedInputStream(this.getClass()
							.getResourceAsStream(
									"/" + NUMBERS_PATH + language + "/" + i
											+ ".mp3")));
		}
		/*
		// Load special characters
		for (int i = 0; i < SPECIAL_CHARS.length; i++) {
			map.put(NaturalLanguageReplacer.getInstance()
					.replace(SPECIAL_CHARS[i]).toLowerCase(),
					new SydBufferedInputStream(this.getClass()
							.getResourceAsStream(
									"/" + SPECIAL_CHARS_PATH + language + "/"
											+ i + ".mp3")));
		}
		*/
	}

	public void disposeCharMap() {
		if (charMap == null)
			return;
		for (final Map<String, SydBufferedInputStream> map : charMap.values()) {
			for (final SydBufferedInputStream stream : map.values())
				try {
					stream.closeForReal();
				} catch (final IOException e) {
					// FIXME Gestire eccezione
					e.printStackTrace();
				}
			map.clear();
		}
		charMap.clear();
		charMap = null;
	}

}
