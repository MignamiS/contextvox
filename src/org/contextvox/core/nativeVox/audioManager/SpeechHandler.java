package org.contextvox.core.nativeVox.audioManager;

import java.io.InputStream;
import java.util.List;

import org.contextvox.Activator;
import org.contextvox.core.nativeVox.translator.CharCache;
import org.contextvox.core.nativeVox.translator.Translator;
import org.contextvox.preferences.PreferenceConstants;
import org.eclipse.jface.preference.IPreferenceStore;

import com.gtranslate.Language;



public class SpeechHandler {
	private static SpeechHandler speechHandler;
	private SpeechQueue speechQueue;
	private CharCache cache;
	private String language;

	

	private SpeechHandler() {
		speechQueue = new SpeechQueue();
		speechQueue.start();
		initLanguage();
	}

	public static synchronized SpeechHandler getInstance() {
		if (speechHandler == null)
			speechHandler = new SpeechHandler();
		return speechHandler;
	}

	public void initLanguage() {
		switch (getPreferedLanguage()) {
		case PreferenceConstants.ENGLISH_VALUE:
			this.language = Language.ENGLISH;
			break;
		case PreferenceConstants.ITALIAN_VALUE:
			this.language = Language.ITALIAN;
			break;
		}
		//create cache
		cache=CharCache.getInstance();
		//populate cache
		cache.setLanguage(language);
		
	}

	public String getPreferedLanguage() {
		final Activator activator = Activator.getDefault();
		if(activator==null){
			return "";
		}
		final IPreferenceStore preferenceStore = activator.getPreferenceStore();
		final String name = PreferenceConstants.MULTILANGUAGE_KEY;
		return preferenceStore.getString(name);
	}
	
	

	public void stopAndPlay(final String message) {
		stopAndStart();
		InputStream stream = null;
		if (cache != null){
			stream = cache.getCurrentCharMap().get(message.trim().toLowerCase());
		}
		if (stream != null){
			Speech.cacheFlag=true;
			speechQueue.put(new Speech(stream));
		}else{
			Speech.cacheFlag=false;
			Translator t = new Translator(message);
			//translate message
			List<InputStream> l = t.messageToAudio();
			//get all Speeches 
			for (InputStream audio: l) {
				speechQueue.put(new Speech(audio));
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void stopAndStart() {
		//da sistemare lo stop
		speechQueue.stop();
		speechQueue = new SpeechQueue();
		speechQueue.start();
	}

	public void putToQueue(final String message) {
		InputStream stream = null;
		if (cache != null)
			stream = cache.getCurrentCharMap().get(message.toLowerCase());
		if (stream != null){
			speechQueue.put(new Speech(stream));
		}
		else{
			Translator t = new Translator(message);
			//translate message
			List<InputStream> l = t.messageToAudio();
			//get all Speeches 
			for (InputStream audio: l) {
				speechQueue.put(new Speech(audio));
			}
		}
			
	}

	public void setLanguage(final String language) {
		this.language = language;
		if (cache.getCharMap().get(language) == null)
			cache.setLanguage(language);
		cache.setCurrentCharMap(cache.getCharMap().get(language));
	}

	public String getLanguage() {
		return language;
	}
}