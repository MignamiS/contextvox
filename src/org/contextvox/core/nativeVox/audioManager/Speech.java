package org.contextvox.core.nativeVox.audioManager;

import java.io.IOException;
import java.io.InputStream;

import org.contextvox.core.nativeVox.translator.CharCache;
import org.contextvox.services.replacer.dictionaries.Messages;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Speech {

	public static volatile boolean cacheFlag = false; // true ->Stream cache;
														// false ->always stream
														// close
	private static InputStream streamCache;// singleton only per cache

	private InputStream stream;
	private Player player;

	public Speech(final InputStream stream) {
		if (cacheFlag) {
			streamCache = stream;
		} else {
			this.stream = stream;
		}

	}

	public static InputStream getStreamCache() {
		return streamCache;
	}

	public static void setStreamCache(final InputStream streamCache) {
		Speech.streamCache = streamCache;
	}

	public static void myClose() {
		if (streamCache != null) {
			try {
				streamCache.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	// play audio of the stream
	public void play() {
		InputStream stream = null;
		if (cacheFlag) {
			stream = streamCache;
		} else {
			stream = this.stream;
		}
		if (stream != null) {
			try {
				player = new Player(stream);
				player.play();
			} catch (final JavaLayerException e) {
				e.printStackTrace();
			} catch (final NoClassDefFoundError ex1) {
				final InputStream errorStream = this.getClass().getResourceAsStream("/" + CharCache.ERRORS_PATH
						+ Messages.getCurrentLanguage().getIdentifier() + "/" + "errorLoadingContextVox.mp3");
				// say the error
				try {
					player = new Player(errorStream);
					player.play();
				} catch (final JavaLayerException e1) {
					System.err.println("Error Play");
					e1.printStackTrace();
				} finally {
					if (player != null)
						player.close();
				}
				// printBug();
				ex1.printStackTrace();
			} finally {
				// close all
				if (player != null) {
					// check if you have a stream cache and check if the stream
					// support reset
					if (cacheFlag && stream.markSupported()) {
						try {
							stream.reset();
							player.close();
						} catch (final IOException e) {
							e.printStackTrace();
						}
					} else {
						player.close();
						try {
							stream.close();
						} catch (final IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	// private void printBug() {
	// System.out.println("/*");
	// System.out.println(" * LIBRARY BUG?");
	// System.out.println(" *");
	// System.out.println(" * java.lang.NoClassDefFoundError");
	// System.out.println(" * in:
	// sun.net.www.protocol.http.Handler.openConnection(Handler.java:62)");
	// System.out.println(" *");
	// System.out.println(" * This is caused because there is a class file that
	// depends on");
	// System.out.println(" * and it is present at compile time but not found at
	// runtime.");
	// System.out.println(" */");
	// }
}
