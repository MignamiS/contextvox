package org.contextvox.core.nativeVox.audioManager;

import java.util.concurrent.LinkedBlockingQueue;

public class SpeechQueue extends Thread {

	private final LinkedBlockingQueue<Speech> queue;

	public SpeechQueue() {
		queue = new LinkedBlockingQueue<>();
	}

	@Override
	public void run() {
		while (true) {
			if (!queue.isEmpty()) {
				try {
					queue.take().play();
				} catch (final Exception e) {
				}
			}
		}
	}

	public void put(final Speech speech) {
		try {
			queue.put(speech);
		} catch (final InterruptedException e) {
		}
	}

}