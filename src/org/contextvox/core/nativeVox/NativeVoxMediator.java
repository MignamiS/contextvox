package org.contextvox.core.nativeVox;

/**
 * This class use the elements of package Core, it is a "mediator" for replacer,
 * translator and audioManager
 */
public class NativeVoxMediator {
	// // FIXME error while running this core, crash
	// private static NativeVoxMediator coreSingleton = null;
	// private volatile boolean running = true;
	// private BlockingQueue<VoxRequest> queue;
	//
	// private NativeVoxMediator() {
	// }
	//
	// public static synchronized Core getInstance() {
	// if (coreSingleton == null) {
	// coreSingleton = new NativeVoxMediator();
	// }
	// return coreSingleton;
	// }
	//
	// @Override
	// public void readString(final String message) {
	// readString(message, MessageSource.GENERIC_READ);
	// }
	//
	// @Override
	// public boolean switchLanguage(final Messages.Language language) {
	// final boolean result = Messages.switchLanguage(language);
	// // change core voice language
	// if (result) {
	// switch (language) {
	// case ENGLISH:
	// SpeechHandler.getInstance().setLanguage(Language.ENGLISH);
	// // lettura single-char
	// CharCache.getInstance().setLanguage(Language.ENGLISH);
	// break;
	// case ITALIAN:
	// SpeechHandler.getInstance().setLanguage(Language.ITALIAN);
	// // lettura single-char
	// CharCache.getInstance().setLanguage(Language.ITALIAN);
	// break;
	// }
	// }
	// return result;
	// }
	//
	// /**
	// * Read a single char, accorgin with the corresponding translation.
	// *
	// * @param char
	// */
	// public void readSingleChar(final String ch) {
	// // TODO introdurre la cache (LG). Utilizzare solo MP3 di lettere e
	// // numeri, i simboli vanno messi in cache dinamicamente quando vengono
	// // utilizzati in quanto la loro traduzione pu� variare.
	// readString(ch);
	// // TODO push up on interface
	// }
	//
	// @Override
	// public void readString(final String message, final MessageSource source)
	// {
	// if (!Activator.getDefault().contextVox().isVoiceOn())
	// return;
	//
	// if (message.length() == 1)
	// readSingleChar(message);
	// else
	// SpeechHandler.getInstance().stopAndPlay(message);
	// }
	//
	// @Override
	// public void shutdown() {
	// // TODO check if it is necessary to shutdown the cache
	// this.running = false;
	// // bombing queue
	// try {
	// this.queue.put(new VoxRequest(MessageSource.GENERIC_READ, "", true));
	// } catch (final InterruptedException e) {
	// VoxLogger.log(LogLevel.ERROR, "Error while bombing mediator queue", e);
	// }
	//
	// // realShutdown();
	// }
	//
	// @Override
	// public void run() {
	// queue = CoreProxy.getQueue();
	// // read messages from queue
	// while (running) {
	// try {
	// final VoxRequest request = queue.take();
	// readString(request.getMessage(), request.getSource());
	// } catch (final InterruptedException e) {
	// VoxLogger.log(LogLevel.ERROR, "Error while reading from mediator queue,
	// thread interrupted", e);
	// }
	//
	// // terminate
	// // realShutdown();
	// }
	// }
	//
	// @Override
	// public boolean isRunning() {
	// return this.running;
	// }
	//
	// @Override
	// public CoreType getCoreType() {
	// return CoreType.NATIVE_VOX;
	// }
}
