package org.contextvox.contextualizer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.contextualizer.visitors.SimpleExpressionVisitor;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceAlternative;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

/**
 * helper class that will return the corresponding string representation from a
 * given ASTNode.
 */
public class ASTInfoConverter {
	/**
	 * Analyze the given node and return the corresponding sentence. The
	 * codeline is an extra parameter that increase analysis precision when
	 * processing complex nodes (e.g. try-catch-finally).
	 *
	 * @param node
	 *            to analyze
	 * @param codeLine
	 *            corresponding line of code, may be empty or null
	 * @return sentence
	 */
	public static String fullAnalysis(final ASTNode node, final String codeLine) {
		return fullAnalysis(node, codeLine, SentenceAlternative.FULL);
	}

	public static String fullAnalysis(final ASTNode node, final String codeLine,
			final SentenceAlternative alternative) {
		// analyze
		final AnalysisReport report = switchNodeType(node, codeLine);
		if (report == null)
			return null;

		filterData(report, alternative);
		return injectData(report, alternative);
	}

	private static void filterData(final AnalysisReport report, final SentenceAlternative alternative) {
		switch (report.type) {
		case FOR:
			ASTInfoFilter.filterForLoop(report.data);
			break;
		default:
			return;
		}
	}

	private static String injectData(final AnalysisReport report, final SentenceAlternative alternative) {
		if (alternative == SentenceAlternative.PARTIAL)
			throw new UnsupportedOperationException(
					"Partial sentence alternative is not supported from Full-analysis method");

		if (report.elsePrefix)
			return "else " + Activator.getServiceHandler().getReplacerService().makeSentence(report.type, report.data);
		else
			return Activator.getServiceHandler().getReplacerService().makeSentence(report.type, report.data);
	}

	/**
	 * Analyze a given node providing uniquely information contained inside the
	 * list.
	 *
	 * @param required
	 *            the required token. If is not present on the list, is skipped.
	 * @param node
	 *            the specific ASTNode to check
	 * @param codeLine
	 *            the line of code corresponding to the current position of the
	 *            cursor. It can help the analysis.
	 * @return a string containing the whole list of information required, or an
	 *         empty list if something goes wrong.
	 */
	public static String selectiveAnalysis(final List<SentenceToken> required, final ASTNode node,
			final String codeLine) {
		// analyze
		final AnalysisReport report = switchNodeType(node, codeLine);
		if (report.data == null)
			return "";

		// compose message
		final StringBuilder sb = new StringBuilder();
		required.stream().forEach(token -> {
			if (report.data.containsKey(token)) {
				final Map<SentenceToken, String> map = new HashMap<>();
				map.put(token, report.data.get(token));
				final String str = Activator.getServiceHandler().getReplacerService().makeSentence(report.type, map);
				sb.append(str);
			}
		});

		return sb.toString();
	}

	/**
	 * Extract information from the given node.
	 */
	private static AnalysisReport switchNodeType(final ASTNode node, final String codeLine) {
		// TODO fragment switch into functions, more readable
		Map<SentenceToken, String> data = null;
		boolean elsePrefix = false;
		SentenceType type = SentenceType.CATCH; // don't care the init value...
		// intermediate message
		String message = null;

		// select the correct node type
		switch (node.getNodeType()) {
		case ASTNode.IF_STATEMENT:
			data = chooseRightIfStatement(node, codeLine);
			switch (ifKind((IfStatement) node, codeLine)) {
			case 0: // if
				type = SentenceType.IF;
				break;
			case 1: // else-if
				elsePrefix = true;
				type = SentenceType.IF;
				break;
			case 2: // else
				type = SentenceType.ELSE;
				break;
			default:
				message = "";
			}
			break;
		case ASTNode.BLOCK_COMMENT:
			ASTInfoInterpreter.getInfo((Comment) node);
			break;
		case ASTNode.FOR_STATEMENT:
			// skip "strange" for loop without updater
			final ForStatement forNode = (ForStatement) node;
			if (forNode.updaters().isEmpty())
				return null;

			data = ASTInfoInterpreter.getInfoForLoop(forNode);
			type = SentenceType.FOR;
			break;
		case ASTNode.ENHANCED_FOR_STATEMENT:
			data = ASTInfoInterpreter.getInfoEnhancedForLoop(((EnhancedForStatement) node));
			type = SentenceType.FOREACH;
			break;
		case ASTNode.SWITCH_STATEMENT:
			data = ASTInfoInterpreter.getInfo(((SwitchStatement) node));
			type = SentenceType.SWITCH;
			break;
		case ASTNode.WHILE_STATEMENT:
			data = ASTInfoInterpreter.getInfo(((WhileStatement) node));
			type = SentenceType.WHILE;
			break;
		case ASTNode.DO_STATEMENT:
			data = ASTInfoInterpreter.getInfo(((DoStatement) node));
			if (codeLine.contains("while")) {
				// raed only the while statement
				type = SentenceType.WHILE;
			} else {
				type = SentenceType.DO;
			}
			break;
		case ASTNode.TRY_STATEMENT:
			if (isCatchClause(codeLine)) {
				// process only catch clause
				final CatchClause c = getCatchClause(node, codeLine);
				if (c == null)
					return null;
				data = ASTInfoInterpreter.getInfo((c));
				type = SentenceType.CATCH;
			} else if (isFinallyClause(codeLine)) {
				// process only finally clause
				message = Messages.getSingleMessage("finallyClause");
			} else {
				// process entire try statement
				data = ASTInfoInterpreter.getInfo(((TryStatement) node));
				type = SentenceType.TRY_CATCH;
			}
			break;
		case ASTNode.METHOD_DECLARATION:
			// skip if it is a method annotation or javadoc
			if (codeLine.contains("@") || codeLine.contains("/**"))
				return null;
			final MethodDeclaration method = (MethodDeclaration) node;
			type = (method.isConstructor()) ? SentenceType.CONSTRUCTOR : SentenceType.METHOD;
			data = ASTInfoInterpreter.getInfo(method);
			break;
		case ASTNode.EXPRESSION_STATEMENT:
		case ASTNode.VARIABLE_DECLARATION_STATEMENT:
			final SimpleExpressionVisitor vs = new SimpleExpressionVisitor();
			node.accept(vs);
			data = vs.getData();
			type = SentenceType.SIMPLE_EXPRESSION;
			break;
		default:
			// TODO type not already mapped, notice developer
			message = "";
		}

		return new AnalysisReport(data, elsePrefix, type, message);

	}

	// select and process the statement corresponding to the correct if-part
	private static Map<SentenceToken, String> chooseRightIfStatement(final ASTNode node, final String codeLine) {
		Map<SentenceToken, String> map = null;
		// get all conditions as string
		IfStatement tmp = (IfStatement) node;
		do {
			if (tmp.getExpression() != null) {
				// compare with line of code
				if (codeLine.contains(tmp.getExpression().toString())) {
					map = ASTInfoInterpreter.getInfo((tmp));
					break;
				}
			}

			// look for next (else)if statement
			if (tmp.getElseStatement() instanceof IfStatement) {
				tmp = (IfStatement) tmp.getElseStatement();
			} else {
				// else reached
				map = ASTInfoInterpreter.getInfo((tmp));
				break;
			}
		} while (tmp != null);

		return map;
	}

	/**
	 * Return the corresponding type of if-statement using a code number.
	 *
	 * @param node
	 * @param codeLine
	 * @return code 0=IF, 1=ELSE-IF and 2=ELSE; -1 is returned when the line
	 *         does not contains an if-related keyword (may just be the end
	 *         brace)
	 */
	private static int ifKind(final IfStatement node, final String line) {
		if (line.contains("else if"))
			return 1;
		else if (line.contains("if"))
			return 0;
		else if (line.contains("else"))
			return 2;
		else
			return -1;
		//
		// if (node.getElseStatement() == null) {
		// return 0; // IF
		// } else {
		// if (node.getElseStatement().getNodeType() == ASTNode.IF_STATEMENT)
		// return 1; // ELSE-IF
		// else
		// return 2; // ELSE
		// }
	}

	// look for the catch-clause that appears in code
	private static CatchClause getCatchClause(final ASTNode node, final String codeLine) {
		// get all catch-clauses and confront
		if (node instanceof TryStatement) {
			final List<?> cc = ((TryStatement) node).catchClauses();
			for (final Object clause : cc) {
				if (clause instanceof CatchClause) {
					// confront exception signature
					final String type = ((CatchClause) clause).getException().getType().toString();
					if (codeLine.contains(type)) {
						// found the correct catch
						return (CatchClause) clause;
					}
				}
			}
			// something goes wrong
			return null;
		} else {
			return null;
		}
	}

	// check if the line of code contains the "finally" declaration
	private static boolean isFinallyClause(final String codeLine) {
		return codeLine.contains("finally");
	}

	// check if the line correspond to a catch clause
	private static boolean isCatchClause(final String codeLine) {
		return codeLine.contains("catch");
	}
}

class AnalysisReport {
	final Map<SentenceToken, String> data;
	final boolean elsePrefix;
	final SentenceType type;
	final String message;

	public AnalysisReport(final Map<SentenceToken, String> data, final boolean elsePrefix, final SentenceType type,
			final String mess) {
		this.data = data;
		this.elsePrefix = elsePrefix;
		this.type = type;
		this.message = mess;
	}

}