package org.contextvox.contextualizer;

import java.util.Map;

import org.contextvox.plugin.ContextVoxProgram;
import org.contextvox.plugin.ReadMode;
import org.contextvox.services.replacer.SentenceToken;

/**
 * Contains helper methods to filter a particular type of node information. This
 * is useful when dealing with the automatic choose of token to inject into
 * sentences.
 */
public class ASTInfoFilter {

	public static Map<SentenceToken, String> filterForLoop(Map<SentenceToken, String> data) {
		// if overview is enabled, remove updater from light sentence
		// alternative
		if (ContextVoxProgram.getReadMode() == ReadMode.OVERVIEW) {
			if (data.containsKey(SentenceToken.UPDATE)) {
				final String up = data.get(SentenceToken.UPDATE);
				if (up.contains("++") || up.contains("--"))
					data.remove(SentenceToken.UPDATE);
			}
		}

		return data;
	}
}
