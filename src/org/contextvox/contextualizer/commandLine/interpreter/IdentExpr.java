package org.contextvox.contextualizer.commandLine.interpreter;

/**
 * 
 * This class is used when in the command line use other keyboard
 *
 */
public class IdentExpr implements Expr {

	private final String value;

	public IdentExpr(final String value) {
		this.value = value;
	}

	@Override
	public String eval() {
		return value;
	}

}
