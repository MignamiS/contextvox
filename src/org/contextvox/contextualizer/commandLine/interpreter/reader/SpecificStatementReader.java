package org.contextvox.contextualizer.commandLine.interpreter.reader;

import java.util.List;

import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Statement;

/**
 * 
 * This class build a message to playing, in a specific situation of statement 
 *
 */
public class SpecificStatementReader {

	private static SpecificStatementReader ssr = null;

	private SpecificStatementReader() {
	}

	public static synchronized SpecificStatementReader getInstance() {
		if (ssr == null)
			ssr = new SpecificStatementReader();
		return ssr;
	}
	
	
	public String readBlockStatement(final int i, final boolean complete) {
		final ASTNode node = Context.getInstance().getCurrentNode();
		final MessageBuilder mb = new MessageBuilder();
		if (node.getNodeType() != ASTNode.BLOCK) {
			mb.add(Messages.getSingleMessage("selectedNodeisntABlock"));
			return mb.toString();
		}
		final List<?> statements = ((Block) node).statements();
		if (i > statements.size() - 1) {
			//choose a not existing statement
			mb.add(Messages.getSingleMessage("contains"));
			mb.add(statements.size());
			if (statements.size() == 1)
				mb.addAndPause(Messages.getSingleMessage("statement"));
			else
				mb.addAndPause(Messages.getSingleMessage("statements"));
			return mb.toString();
		}
		mb.add(Messages.getSingleMessage("statements"));
		mb.addAndPause(i);
		final Statement stmt = (Statement) statements.get(i);
		mb.add(new GenericStatementReader(stmt, complete).read());
		
		return mb.toString();
	}

	// TODO FUTURE DEV: remove exception throw, complete method (See
	// GenericStatementReader for help)
	public String readIfCondition(final int i, final boolean complete) {
		throw new UnsupportedOperationException(Messages.getSingleMessage("notImplementedYet"));
	}

	// TODO FUTURE DEV: remove exception throw, complete method (See
	// GenericStatementReader for help)
	public String readTryCatch(final int i, final boolean complete) {
		throw new UnsupportedOperationException(Messages.getSingleMessage("notImplementedYet"));
	}

	// TODO FUTURE DEV: remove exception throw, complete method (See
	// GenericStatementReader for help)
	public String readSwitchCase(final int i, final boolean complete) {
		throw new UnsupportedOperationException(Messages.getSingleMessage("notImplementedYet"));
	}

}
