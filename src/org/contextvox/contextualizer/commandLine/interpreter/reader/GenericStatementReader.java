package org.contextvox.contextualizer.commandLine.interpreter.reader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.exceptions.InterpreterException;
import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

/**
 * This class build a message to playing. It read the type the ASTNode and
 * choose the correct method.
 */
public class GenericStatementReader {

	private final ASTNode node;
	private final MessageBuilder mb;

	public GenericStatementReader(final ASTNode node, final boolean complete) {
		this.node = node;
		mb = new MessageBuilder();
	}

	public String read() {
		if (node == null)
			throw new InterpreterException(Messages.getSingleMessage("noNodeSelected"));
		switchStmt(node);
		return mb.toString();
	}

	private void switchStmt(final ASTNode node) {
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		switch (node.getNodeType()) {
		case ASTNode.BLOCK:
			blockCase(node);
			break;
		case ASTNode.IF_STATEMENT:
			ifStatementCase(node);
			break;
		case ASTNode.DO_STATEMENT:
			doStatementCase(node);
			break;
		case ASTNode.WHILE_STATEMENT:
			whileStatementCase(node);
			break;
		case ASTNode.FOR_STATEMENT:
			forStatementCase(node);
			break;
		case ASTNode.TRY_STATEMENT:
			tryStatementCase(node);
			break;
		case ASTNode.SWITCH_STATEMENT:
			switchStatementCase(node);
			break;
		case ASTNode.SYNCHRONIZED_STATEMENT:
			synchronizedStatementCase(node);
			break;
		case ASTNode.VARIABLE_DECLARATION_STATEMENT:
			final VariableDeclarationStatement temp = (VariableDeclarationStatement) node;
			System.err
					.println(node.toString() + " " + temp.getType().toString() + " " + Flags.toString(temp.getFlags()));
			data.put(SentenceToken.NAME, temp.fragments().get(0).toString());
			final String type = temp.getType().toString();
			if (type.contains("<")) {
				// type = TokenReplacer.replaceGenerics(type);
			}
			data.put(SentenceToken.TYPE, type);
			data.put(SentenceToken.MODIFIERS, Flags.toString(temp.getFlags()));
			mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FIELD, data));
			break;
		default:
			mb.add(node.toString().replace("\n", ""));
		}
	}

	private void blockCase(final ASTNode node) {
		final List<?> statements = ((Block) node).statements();
		if (statements.size() == 0) {
			mb.add("");
			return;
		}
		for (int i = 0; i < statements.size(); i++) {
			switchStmt((ASTNode) statements.get(i));
		}
	}

	@SuppressWarnings("deprecation")
	private void ifStatementCase(final ASTNode node) {
		IfStatement ifStatement = (IfStatement) node;
		final List<IfStatement> ifStmts = new ArrayList<IfStatement>();
		Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		ifStmts.add(ifStatement);
		ASTNode child = null;
		Statement elseStmt = null;
		while ((child = ifStatement.getElseStatement()) != null) {
			if (child instanceof IfStatement) {
				final IfStatement subIf = (IfStatement) child;
				ifStmts.add(subIf);
				ifStatement = subIf;
			} else {
				elseStmt = (Statement) child;
				break;
			}
		}

		if (ifStmts.size() == 1) {
			final IfStatement ifStmt = ifStmts.get(0);
			data.put(SentenceToken.CONDITION, ifStmt.getExpression().toString());
			mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.IF, data));
		} else {
			// parse every if statement
			for (int i = 0; i < ifStmts.size(); i++) {
				final IfStatement ifStmt = ifStmts.get(i);
				data.put(SentenceToken.CONDITION, ifStmt.getExpression().toString());
				mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.IFELSE, data));
			}
		}
		if (elseStmt != null) {
			data = new HashMap<SentenceToken, String>();
			mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.IFELSE, data));
		}
	}

	private void doStatementCase(final ASTNode node) {
		final DoStatement doStatement = (DoStatement) node;
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		data.put(SentenceToken.CONDITION, doStatement.getExpression().toString());
		mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.DO, data));
	}

	private void whileStatementCase(final ASTNode node) {
		final WhileStatement whileStatement = (WhileStatement) node;
		// create map for create a while message
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		data.put(SentenceToken.CONDITION, whileStatement.getExpression().toString());
		mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.WHILE, data));
	}

	private void forStatementCase(final ASTNode node) {
		final ForStatement forStatement = (ForStatement) node;
		// create map for create a for message
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		final List<?> initializers = forStatement.initializers();
		switch (initializers.size()) {
		case 0:
			break;
		case 1:
			final ASTNode init = (ASTNode) initializers.get(0);
			data.put(SentenceToken.ITERATOR, init.toString());
			break;
		default:
			String str = "";
			for (int i = 0; i < initializers.size(); i++) {
				str = ((ASTNode) initializers.get(i)).toString() + " ";
			}
			data.put(SentenceToken.ITERATOR, str);
		}
		final Expression expression = forStatement.getExpression();
		if (expression != null) {
			data.put(SentenceToken.CONDITION, expression.toString());
		}
		final List<?> updaters = forStatement.updaters();
		if (updaters.size() != 0) {
			String str = "";
			for (int i = 0; i < updaters.size(); i++) {
				str = updaters.get(i).toString() + " ";
			}
			data.put(SentenceToken.UPDATE, str);
		}
		mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FOR, data));
	}

	@SuppressWarnings("deprecation")
	private void tryStatementCase(final ASTNode node) {
		final TryStatement tryStatement = (TryStatement) node;
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		final List<?> catchClauses = tryStatement.catchClauses();
		switch (catchClauses.size()) {
		case 1:
			CatchClause cc = (CatchClause) catchClauses.get(0);
			data.put(SentenceToken.EXCEPTION, cc.toString());
			data.put(SentenceToken.NUMBER, catchClauses.size() + "");
			break;
		default:
			String str = "";
			for (int i = 0; i < catchClauses.size(); i++) {
				cc = (CatchClause) catchClauses.get(i);
				str = cc.toString() + " ";
			}
			data.put(SentenceToken.NUMBER, catchClauses.size() + "");
			data.put(SentenceToken.EXCEPTION, str);
		}
		if (tryStatement.getFinally() != null) {
			mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.TRY_CATCH_FINALLY,
					data));
		} else {
			mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.TRY_CATCH, data));
		}
	}

	private void switchStatementCase(final ASTNode node) {
		final SwitchStatement switchStatement = (SwitchStatement) node;
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		data.put(SentenceToken.SELECTOR, switchStatement.getExpression().toString());
		final List<?> stmts = switchStatement.statements();
		String str = "";
		for (int i = 0; i < stmts.size(); i++) {
			final Statement stmt = (Statement) stmts.get(i);
			if (stmt.getNodeType() == ASTNode.SWITCH_CASE) {
				final SwitchCase switchCase = (SwitchCase) stmts.get(i);
				if (switchCase.isDefault()) {
					str += "default";
				} else {
					str += switchCase.getExpression().toString() + " ";
				}
			}
			data.put(SentenceToken.LIST, str);
		}
		mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.SWITCH, data));
	}

	private void synchronizedStatementCase(final ASTNode node) {
		final SynchronizedStatement synchStmt = (SynchronizedStatement) node;
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		data.put(SentenceToken.CONDITION, synchStmt.getExpression().toString());
		mb.add(Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.SYNCHRONIZE_BLOCK, data));
	}

}
