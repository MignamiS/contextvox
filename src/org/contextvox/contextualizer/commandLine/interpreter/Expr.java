package org.contextvox.contextualizer.commandLine.interpreter;

public interface Expr {
	public String eval();
}
