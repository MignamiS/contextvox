package org.contextvox.contextualizer.commandLine.interpreter;

/**
 * 
 * This class is used when in the command line insert a positive number  
 *
 */
public class NumExpr implements Expr {

	private final int num;

	public NumExpr(final int num) {
		this.num = num;
	}

	@Override
	public String eval() {
		return String.valueOf(num);
	}

}
