package org.contextvox.contextualizer.commandLine.interpreter;



import java.util.ArrayList;

import org.contextvox.IDEobserver.exceptions.InvalidSelectionException;
import org.contextvox.IDEobserver.exceptions.MethodNotFoundException;
import org.contextvox.IDEobserver.exceptions.ProjectNotFoundException;
import org.contextvox.IDEobserver.helpers.ClassInformations;
import org.contextvox.IDEobserver.helpers.MethodInformations;
import org.contextvox.IDEobserver.helpers.PackageInformations;
import org.contextvox.IDEobserver.helpers.ProjectInformations;
import org.contextvox.contextualizer.commandLine.interpreter.reader.SpecificStatementReader;
import org.contextvox.contextualizer.commandLine.parser.Token.Type;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.internal.core.JavaElement;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * This class is used when in the command line use the keyword "info" with others parameters 
 *
 */
@SuppressWarnings("restriction")
public class SpecificInfoExpr implements Expr {

	private final Type type;
	private IdentExpr ident;
	private int num;
	private boolean complete;

	public SpecificInfoExpr(final IdentExpr ident, final Type type) {
		this.type = type;
		this.ident = ident;
	}

	public SpecificInfoExpr(final NumExpr num, final Type type,
			final boolean complete) {
		this.type = type;
		this.num = Integer.valueOf(num.eval());
		this.complete = complete;
	}

	@Override
	public String eval() {
		switch (type) {
		case Project:
			return caseProject(ident.eval());
		case Package:
			return casePackage(ident.eval());
		case Class:
			return caseClass(ident.eval());
		case Method:
			return caseMethod(ident.eval());
		case Statement:
			return SpecificStatementReader.getInstance().readBlockStatement(
					num, complete);
		case Condition:
			return SpecificStatementReader.getInstance().readIfCondition(num,
					complete);
		case Catch:
			return SpecificStatementReader.getInstance().readTryCatch(num,
					complete);
		case Case:
			return SpecificStatementReader.getInstance().readSwitchCase(num,
					complete);
		default:
			throw new RuntimeException();
		}
	}

	private String caseProject(final String projectName) {
		return ProjectInformations.getPunctalInfo(projectName);
	}
	
	private String casePackage(final String packageName) {
		IJavaProject javaProject = null;
		try {
			javaProject = getJavaProjectFromSelection();
		} catch (final ProjectNotFoundException e) {
			return Messages.getSingleMessage("projectNotSelected");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		return PackageInformations.getPunctalInfo(javaProject, packageName);
	}

	private String caseClass(final String className) {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection selection = (TreeSelection) service.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		if (selection == null || selection.isEmpty())
			return Messages.getSingleMessage("projectNotSelected");
		final JavaElement element = (JavaElement) selection.getFirstElement();
		IJavaProject javaProject = null;
		IPackageFragment thePackage = null;
		if (element.getElementType() == IJavaElement.JAVA_PROJECT) {
			javaProject = (JavaProject) element;
		} else if (element.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
			thePackage = (IPackageFragment) element;
			try {
				javaProject = getJavaProjectFromSelection();
			} catch (final ProjectNotFoundException e) {
				return Messages.getSingleMessage("projectNotSelected");
			} catch (final InvalidSelectionException e) {
				return Messages.getSingleMessage("invalidSelection");
			}
		} else {
			try {
				javaProject = getJavaProjectFromSelection();
			} catch (final ProjectNotFoundException e) {
				return Messages.getSingleMessage("projectNotSelected");
			} catch (final InvalidSelectionException e) {
				return Messages.getSingleMessage("invalidSelection");
			}
		}
		return ClassInformations.getPunctalInfo(javaProject, thePackage,
				className);
	}

	private String caseMethod(final String methodName) {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection selection = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		JavaElement element = null;
		if (selection == null || selection.isEmpty())
			return Messages.getSingleMessage("classNotSelected");
		element = (JavaElement) selection.getFirstElement();
		ArrayList<IMethod> methods = null;
		if (element == null)
			return Messages.getSingleMessage("classNotSelected");
		try {
			methods = MethodInformations.getMethod(element, methodName);
		} catch (final MethodNotFoundException e) {
			return Messages.getSingleMessage("methodNotFoundInClass");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		return MethodInformations.getPunctalInfo(methods);
	}

	private IJavaProject getJavaProjectFromSelection()
			throws ProjectNotFoundException, InvalidSelectionException {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection structured = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		if (structured == null || structured.isEmpty())
			throw new ProjectNotFoundException();
		final Object element = structured.getFirstElement();
		if (!(element instanceof IJavaElement))
			throw new InvalidSelectionException();
		final IJavaProject javaProject = (IJavaProject) ((IJavaElement) element)
				.getAncestor(IJavaElement.JAVA_PROJECT);
		if (javaProject == null)
			throw new ProjectNotFoundException();
		return javaProject;
	}

}
