package org.contextvox.contextualizer.commandLine.interpreter;

import org.contextvox.contextualizer.commandLine.ReadingState;
import org.contextvox.services.replacer.dictionaries.Messages;


/**
 * 
 * This class change the read line from partial to complete and the opposite 
 *
 */
public class SwitchExpr implements Expr {

	@Override
	public String eval() {
		ReadingState.getInstance().switchState();
		if (ReadingState.getInstance().isPartial())
			return Messages.getSingleMessage("readingStatePartial");
		return Messages.getSingleMessage("readingStateComplete");
	}

}
