package org.contextvox.contextualizer.commandLine.interpreter;


import org.contextvox.IDEobserver.exceptions.InvalidSelectionException;
import org.contextvox.IDEobserver.exceptions.ProjectNotFoundException;
import org.contextvox.IDEobserver.helpers.ClassInformations;
import org.contextvox.IDEobserver.helpers.CursorInformations;
import org.contextvox.IDEobserver.helpers.EditorInformations;
import org.contextvox.IDEobserver.helpers.MethodInformations;
import org.contextvox.IDEobserver.helpers.PackageInformations;
import org.contextvox.IDEobserver.helpers.ProjectInformations;
import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.contextualizer.commandLine.interpreter.reader.GenericStatementReader;
import org.contextvox.contextualizer.commandLine.parser.Token.Type;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;
/**
 * 
 * This class is used when in the command line use the keyword "info" 
 *
 */
public class GenericInfoExpr implements Expr {
	private final Type type;

	public GenericInfoExpr(final Type type) {
		this.type = type;
	}
	
	@Override
	public String eval() {
		switch (type) {
		case Partial:
			return casePartial();
		case Complete:
			return caseComplete();
		case Projects:
			return caseProjects();
		case Packages:
			return casePackages();
		case Classes:
			return caseClasses();
		case Methods:
			return caseMethods();
		case Editor:
			return caseEditor();
		case Cursor:
			return caseCursor();
		default:
			throw new RuntimeException();
		}
	}
	
	private String casePartial() {
		final ASTNode node = Context.getInstance().getCurrentNode();
		return new GenericStatementReader(node, false).read();
	}
	
	private String caseComplete() {
		final ASTNode node = Context.getInstance().getCurrentNode();
		return new GenericStatementReader(node, true).read();
	}

	private String caseProjects() {
		return ProjectInformations.getGeneralInfo();
	}

	private String casePackages() {
		IJavaProject javaProject = null;
		try {
			javaProject = getJavaProjectFromSelection();
		} catch (final ProjectNotFoundException e) {
			return Messages.getSingleMessage("projectNotSelected");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		return PackageInformations.getGeneralInfo(javaProject);
	}

	private String caseClasses() {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection selection = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		IJavaProject javaProject = null;
		if (selection == null || selection.isEmpty())
			return Messages.getSingleMessage("projectNotSelected");
		final Object element = selection.getFirstElement();
		IPackageFragment thePackage = null;
		if (element instanceof IJavaProject) {
			javaProject = (IJavaProject) element;
		} else if (element instanceof IPackageFragment) {
			thePackage = (IPackageFragment) element;
			try {
				javaProject = getJavaProjectFromSelection();
			} catch (final ProjectNotFoundException e) {
				return Messages.getSingleMessage("projectNotSelected");
			} catch (final InvalidSelectionException e) {
				return Messages.getSingleMessage("invalidSelection");
			}
		} else {
			try {
				javaProject = getJavaProjectFromSelection();
			} catch (final ProjectNotFoundException e) {
				return Messages.getSingleMessage("projectNotSelected");
			} catch (final InvalidSelectionException e) {
				return Messages.getSingleMessage("invalidSelection");
			}
		}
		return ClassInformations.getGeneralInfo(javaProject, thePackage);
	}

	private String caseMethods() {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection selection = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		if (selection == null || selection.isEmpty())
			return Messages.getSingleMessage("classNotSelected");
		final Object firstElement = selection.getFirstElement();
		if (firstElement == null || !(firstElement instanceof IJavaElement))
			return Messages.getSingleMessage("invalidSelection");
		try {
			return MethodInformations.getGeneralInfo((IJavaElement) selection
					.getFirstElement());
		} catch (final JavaModelException e) {
			return Messages.getSingleMessage("classNotSelected");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
	}

	private String caseEditor() {
		return EditorInformations.getGeneralInformations();
	}

	private String caseCursor() {
		return CursorInformations.getGeneralInfo();
	}

	private IJavaProject getJavaProjectFromSelection()
			throws ProjectNotFoundException, InvalidSelectionException {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection structured = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		if (structured == null || structured.isEmpty())
			throw new ProjectNotFoundException();
		final Object element = structured.getFirstElement();
		if (!(element instanceof IJavaElement))
			throw new InvalidSelectionException();
		final IJavaProject javaProject = (IJavaProject) ((IJavaElement) element).getAncestor(IJavaElement.JAVA_PROJECT);
		if (javaProject == null)
			throw new ProjectNotFoundException();
		return javaProject;
	}

}
