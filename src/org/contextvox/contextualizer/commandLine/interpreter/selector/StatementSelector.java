package org.contextvox.contextualizer.commandLine.interpreter.selector;

import java.util.List;

import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;

/**
 * 
 * This class browse from the ASTNode and create the message, so we can play this in an other moment
 *
 */
public class StatementSelector {

	private static StatementSelector ss = null;

	private StatementSelector() {
	}

	public static synchronized StatementSelector getInstance() {
		if (ss == null)
			ss = new StatementSelector();
		return ss;
	}

	//TODO da vedere con simo come implementarlo 
	public String selectBlockStatement(final int i) {
		final ASTNode node = Context.getInstance().getCurrentNode();
		final MessageBuilder mb = new MessageBuilder();
		if (node.getNodeType() != ASTNode.BLOCK) {
			mb.add(Messages.getSingleMessage("selectedNodeisntABlock"));
			return mb.toString();
		}
		final List<?> statements = ((Block) node).statements();
		if (i > statements.size() - 1) {
			//choose a not existing statement
			mb.add(Messages.getSingleMessage("contains"));
			mb.add(statements.size());
			if (statements.size() == 1)
				mb.addAndPause(Messages.getSingleMessage("statement"));
			else
				mb.addAndPause(Messages.getSingleMessage("statements"));
			return mb.toString();
		}
		Context.getInstance().addNode((ASTNode) statements.get(i));
		mb.add(Messages.getSingleMessage("statement"));
		mb.add(i);
		mb.add(Messages.getSingleMessage("selected"));
		return mb.toString();
	}

	// TODO FUTURE DEV: remove exception throw, complete method (See
	// GenericStatementReader for help)
	public String selectIfCondition(final int i) {
		//throw new UnsupportedOperationException(Messages.notImplementedYet);
		throw new UnsupportedOperationException("Not implemented yet");
	}

	// TODO FUTURE DEV: remove exception throw, complete method (See
	// GenericStatementReader for help)
	public String selectTryCatch(final int i) {
		//throw new UnsupportedOperationException(Messages.notImplementedYet);
		throw new UnsupportedOperationException("Not implemented yet");
	}

	// TODO FUTURE DEV: remove exception throw, complete method (See
	// GenericStatementReader for help)
	public String selectSwitchCase(final int i) {
		//throw new UnsupportedOperationException(Messages.notImplementedYet);
		throw new UnsupportedOperationException("Not implemented yet");
	}

}
