package org.contextvox.contextualizer.commandLine.interpreter;

import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.contextualizer.commandLine.parser.Token.Type;
import org.contextvox.services.replacer.dictionaries.Messages;



/**
 * 
 * This class is used when in the command line use the keyword "get" 
 *
 */
public class GetExpr implements Expr {

	private final Type type;

	public GetExpr(final Type type) {
		this.type = type;
	}

	@Override
	public String eval() {
		switch (type) {
		case Parent:
			Context.getInstance().addParentNode();
			return Messages.getSingleMessage("parentSelected");
		case Previous:
			Context.getInstance().removeLastNode();
			return Messages.getSingleMessage("previousSelected");
		default:
			throw new RuntimeException();
		}

	}

}
