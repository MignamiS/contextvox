package org.contextvox.contextualizer.commandLine.interpreter;

import java.util.ArrayList;

import org.contextvox.IDEobserver.exceptions.ClassMultipleException;
import org.contextvox.IDEobserver.exceptions.InvalidSelectionException;
import org.contextvox.IDEobserver.exceptions.MethodNotFoundException;
import org.contextvox.IDEobserver.exceptions.ProjectNotFoundException;
import org.contextvox.IDEobserver.helpers.ClassInformations;
import org.contextvox.IDEobserver.helpers.MethodInformations;
import org.contextvox.IDEobserver.helpers.PackageInformations;
import org.contextvox.contextualizer.commandLine.interpreter.selector.StatementSelector;
import org.contextvox.contextualizer.commandLine.parser.Token.Type;
import org.contextvox.plugin.PluginElements;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.JavaElement;
import org.eclipse.jdt.internal.ui.packageview.PackageExplorerPart;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * This class is used when in the command line use the keyword "select" 
 *
 */

@SuppressWarnings("restriction")
public class SelectExpr implements Expr {

	private final Type type;
	private final IdentExpr ident;
	private int num;

	public SelectExpr(final IdentExpr ident, final Type type) {
		this.type = type;
		this.ident = ident;
	}

	public SelectExpr(final NumExpr num, final Type type) {
		this.type = type;
		this.ident = null;
		this.num = Integer.valueOf(num.eval());
	}

	@Override
	public String eval() {
		switch (type) {
		case Project:
			return caseProject();
		case Package:
			return casePackage();
		case Class:
			return caseClass();
		case Method:
			return caseMethod();
		case Statement:
			return StatementSelector.getInstance().selectBlockStatement(num);
		case Condition:
			return StatementSelector.getInstance().selectIfCondition(num);
		case Catch:
			return StatementSelector.getInstance().selectTryCatch(num);
		case Case:
			return StatementSelector.getInstance().selectSwitchCase(num);
		default:
			throw new RuntimeException();
		}
	}
	
	private String caseProject() {
		final PackageExplorerPart packageExplorer = PluginElements.getPackageExplorer();
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot root = workspace.getRoot();
		final IProject project = root.getProject(ident.eval());
		if (!project.exists())
			return Messages.getSingleMessage("projectNotExist");
		packageExplorer.setFocus();
		packageExplorer.selectAndReveal(project);
		return "";//Messages.done;
	}

	private String casePackage() {
		final PackageExplorerPart packageExplorer = PluginElements
				.getPackageExplorer();
		IJavaProject javaProject = null;
		IPackageFragment thePackage = null;
		try {
			javaProject = getJavaProjectFromSelection();
		} catch (final ProjectNotFoundException e) {
			return Messages.getSingleMessage("projectNotSelected");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		try {
			thePackage = PackageInformations.getPackage(javaProject,
					ident.eval());
		} catch (final JavaModelException e) {
			return Messages.getSingleMessage("packageNotFound");
		}
		if (thePackage == null)
			return Messages.getSingleMessage("packageNotFound");
		packageExplorer.setFocus();
		packageExplorer.selectAndReveal(thePackage);
		return "";//Messages.done;
	}

	private String caseClass() {
		final PackageExplorerPart packageExplorer = PluginElements
				.getPackageExplorer();
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection selection = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		JavaElement element = null;
		IJavaProject javaProject = null;
		IPackageFragment thePackage = null;
		ICompilationUnit theClass = null;
		try {
			javaProject = getJavaProjectFromSelection();
		} catch (final ProjectNotFoundException e) {
			return Messages.getSingleMessage("projectNotSelected");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		element = (JavaElement) selection.getFirstElement();
		if (element.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
			thePackage = (IPackageFragment) element;
			try {
				javaProject = getJavaProjectFromSelection();
			} catch (final ProjectNotFoundException e) {
				return Messages.getSingleMessage("projectNotSelected");
			} catch (final InvalidSelectionException e) {
				return Messages.getSingleMessage("invalidSelection");
			}
		}
		try {
			theClass = ClassInformations.getClass(javaProject, thePackage,
					ident.eval());
		} catch (final ClassMultipleException e) {
			return Messages.getSingleMessage("multipleClass");
		}
		if (theClass == null)
			return Messages.getSingleMessage("noClass");
		packageExplorer.setFocus();
		packageExplorer.selectAndReveal(theClass);
		return "";//Messages.done;
	}

	private String caseMethod() {
		final PackageExplorerPart packageExplorer = PluginElements
				.getPackageExplorer();
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection selection = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		JavaElement element = null;
		if (selection == null || selection.isEmpty())
			return Messages.getSingleMessage("classNotSelected");
		element = (JavaElement) selection.getFirstElement();
		ArrayList<IMethod> methods = null;
		if (element == null)
			return Messages.getSingleMessage("classNotSelected");
		try {
			methods = MethodInformations.getMethod(element, ident.eval());
		} catch (final MethodNotFoundException e) {
			return Messages.getSingleMessage("methodNotFoundInClass");
		} catch (final InvalidSelectionException e) {
			return Messages.getSingleMessage("invalidSelection");
		}
		packageExplorer.setFocus();
		packageExplorer.selectAndReveal(methods.get(0));
		return "";//Messages.done;
	}

	private IJavaProject getJavaProjectFromSelection()
			throws ProjectNotFoundException, InvalidSelectionException {
		final ISelectionService service = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getSelectionService();
		final TreeSelection structured = (TreeSelection) service
				.getSelection("org.eclipse.jdt.ui.PackageExplorer");
		if (structured == null || structured.isEmpty())
			throw new ProjectNotFoundException();
		final Object element = structured.getFirstElement();
		if (!(element instanceof IJavaElement))
			throw new InvalidSelectionException();
		final IJavaProject javaProject = (IJavaProject) ((IJavaElement) element)
				.getAncestor(IJavaElement.JAVA_PROJECT);
		if (javaProject == null)
			throw new ProjectNotFoundException();
		return javaProject;
	}

}
