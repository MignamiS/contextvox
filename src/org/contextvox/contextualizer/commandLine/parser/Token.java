package org.contextvox.contextualizer.commandLine.parser;
/**
 * 
 * This class is used for read the command line 
 *
 */
public class Token {

	public enum Type {
		Info, Select, Get, Ident, Num, Projects, Packages, Classes, Methods, Editor, Cursor, Project, Package, Class, Method, Complete, Partial, Parent, Previous, Statement, Condition, Catch, Case, Switch, Reading, Unknown, Eos
	}

	private final Type type;
	private final String value;
	private final int num;

	public Token(final Type type, final String value) {
		this.type = type;
		this.value = value;
		this.num = -1;
	}

	public Token(final Type type, final Integer num) {
		this.type = type;
		this.value = null;
		this.num = num;
	}

	public Token(final Type type) {
		this(type, "");
	}

	public Type type() {
		return type;
	}

	public String value() {
		return value;
	}

	public int num() {
		return num;
	}

	@Override
	public String toString() {
		switch (type()) {
		case Num:
			return type() + " [" + num() + "]";
		default:
			return type().toString();
		}
	}

}
