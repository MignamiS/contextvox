package org.contextvox.contextualizer.commandLine.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

import org.contextvox.IDEobserver.exceptions.TokenizerException;
import org.contextvox.contextualizer.commandLine.parser.Token.Type;

/**
 * 
 * This class make the lexical analysis
 * It use the class Token to make the analysis
 *
 */
public class Tokenizer {

	private static final int EOS = -1;

	private final BufferedReader in;
	private final StringBuilder builder = new StringBuilder();
	private int ch;
	private boolean stepBack;
	private Token token;
	private Token prevToken;

	public Tokenizer(final Reader in) {
		this.in = new BufferedReader(in);
	}

	public Token prev() {
		if (stepBack)
			throw new TokenizerException("Already in step back mode");
		stepBack = true;
		return prevToken;
	}

	public Token token() {
		return stepBack ? prevToken : token;
	}

	public Token next() throws IOException {
		if (stepBack) {
			stepBack = false;
			return token;
		}
		prevToken = token;
		skipWhites();
		if ((token = value()) != null)
			return token;
		if ((token = eos()) != null)
			return token;
		return token = new Token(Type.Unknown);
	}

	private Token value() throws IOException {
		markAndRead(1);
		if (!isPart())
			return resetAndToken(null);
		while (isPart())
			appendMarkAndRead();
		final String str = toStringAndReset();
		try {
			final int num = Integer.valueOf(str);
			return resetAndToken(new Token(Type.Num, num));
		} catch (final NumberFormatException e) {
			final Type type = switchType(str);
			return resetAndToken(new Token(type, str));
		}
	}

	private Type switchType(final String str) {
		Type type = null;
		try {
			type = Type.valueOf(firstCharToUpperCase(str));
		} catch (final IllegalArgumentException e) {
			type = Type.Ident;
		}
		return type;
	}

	private String firstCharToUpperCase(final String str) {
		final String s = str.toLowerCase();
		return Character.toString(s.charAt(0)).toUpperCase() + s.substring(1);
	}

	private Token eos() throws IOException {
		markAndRead(1);
		if (ch == EOS)
			return new Token(Type.Eos);
		return resetAndToken(null);
	}

	private void skipWhites() throws IOException {
		do
			markAndRead(1);
		while (isWhite());
		reset();
	}

	private void markAndRead(final int readAheadLimit) throws IOException {
		in.mark(readAheadLimit);
		read();
	}

	private void read() throws IOException {
		ch = in.read();
	}

	private void reset() throws IOException {
		in.reset();
	}

	private void append() {
		builder.append((char) ch);
	}

	private void appendMarkAndRead() throws IOException {
		append();
		markAndRead(1);
	}

	private Token resetAndToken(final Token tok) throws IOException {
		reset();
		return tok;
	}

	private boolean isPart() {
		return !(isWhite() || isEos());
	}

	private boolean isWhite() {
		return Character.isWhitespace(ch);
	}

	private boolean isEos() {
		return ch == EOS;
	}

	private String toStringAndReset() {
		final String s = builder.toString();
		builder.setLength(0);
		return s;
	}

}
