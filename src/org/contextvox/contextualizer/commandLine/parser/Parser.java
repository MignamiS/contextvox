package org.contextvox.contextualizer.commandLine.parser;

import java.io.IOException;
import java.io.Reader;

import org.contextvox.IDEobserver.exceptions.ParserException;
import org.contextvox.contextualizer.commandLine.interpreter.Expr;
import org.contextvox.contextualizer.commandLine.interpreter.GenericInfoExpr;
import org.contextvox.contextualizer.commandLine.interpreter.GetExpr;
import org.contextvox.contextualizer.commandLine.interpreter.IdentExpr;
import org.contextvox.contextualizer.commandLine.interpreter.NumExpr;
import org.contextvox.contextualizer.commandLine.interpreter.SelectExpr;
import org.contextvox.contextualizer.commandLine.interpreter.SpecificInfoExpr;
import org.contextvox.contextualizer.commandLine.interpreter.SwitchExpr;
import org.contextvox.contextualizer.commandLine.parser.Token.Type;
import org.contextvox.services.replacer.dictionaries.Messages;

/**
 * 
 * This class parse from ASTNode
 * It is the "Client" of Interpreter Pattern (modified) 
 *
 */
public class Parser {

	private final Tokenizer tokenizer;

	public Parser(final Reader in) {
		tokenizer = new Tokenizer(in);
	}

	public Expr parse() throws IOException {
		tokenizer.next();
		final Expr expr = parseExpr();
		check(Type.Eos);
		return expr;
	}

	private Expr parseExpr() throws IOException {
		switch (tokenizer.token().type()) {
		case Info:
			tokenizer.next();
			return switchInfoCase();
		case Select:
			tokenizer.next();
			return parseSelectExpr();
		case Get:
			tokenizer.next();
			return parseGetExpr();
		case Switch:
			tokenizer.next();
			return parseSwitchExpr();
		default:
			throw new ParserException(Messages.getSingleMessage("syntaxError"));
		}
	}

	private Expr switchInfoCase() throws IOException {
		switch (tokenizer.next().type()) {
		case Ident:
		case Num:
			tokenizer.prev();
			return parseSpecificInfoExpr();
		default:
			tokenizer.prev();
			return parseGenericInfoExpr();
		}
	}

	private Expr parseGenericInfoExpr() throws IOException {
		Type type = null;
		switch (type = tokenizer.token().type()) {
		case Complete:
		case Projects:
		case Packages:
		case Classes:
		case Methods:
		case Editor:
		case Cursor:
			tokenizer.next();
			return new GenericInfoExpr(type);
		case Eos:
			return new GenericInfoExpr(Type.Partial);
		default:
			throw new ParserException(Messages.getSingleMessage("syntaxError"));
		}
	}

	private Expr parseSpecificInfoExpr() throws IOException {
		Type type = null;
		switch (type = tokenizer.token().type()) {
		case Project:
		case Package:
		case Class:
		case Method:
			tokenizer.next();
			check(Type.Ident);
			final IdentExpr ident = new IdentExpr(tokenizer.token().value());
			tokenizer.next();
			return new SpecificInfoExpr(ident, type);
		case Statement:
		case Condition:
		case Catch:
		case Case:
			tokenizer.next();
			check(Type.Num);
			final NumExpr num = new NumExpr(tokenizer.token().num());
			boolean complete = false;
			if (tokenizer.next().type() == Type.Complete) {
				complete = true;
				tokenizer.next();
			}
			return new SpecificInfoExpr(num, type, complete);
		default:
			throw new ParserException(Messages.getSingleMessage("syntaxError"));
		}
	}

	private Expr parseSelectExpr() throws IOException {
		Type type = null;
		switch (type = tokenizer.token().type()) {
		case Project:
		case Package:
		case Class:
		case Method:
			tokenizer.next();
			check(Type.Ident);
			final IdentExpr ident = new IdentExpr(tokenizer.token().value());
			tokenizer.next();
			return new SelectExpr(ident, type);
		case Statement:
		case Condition:
		case Catch:
		case Case:
			tokenizer.next();
			check(Type.Num);
			final NumExpr num = new NumExpr(tokenizer.token().num());
			tokenizer.next();
			return new SelectExpr(num, type);
		default:
			throw new ParserException(Messages.getSingleMessage("syntaxError"));
		}
	}

	private Expr parseGetExpr() throws IOException {
		Type type = null;
		switch (type = tokenizer.token().type()) {
		case Parent:
		case Previous:
			tokenizer.next();
			return new GetExpr(type);
		default:
			throw new ParserException(Messages.getSingleMessage("syntaxError"));
		}
	}

	private Expr parseSwitchExpr() throws IOException {
		check(Type.Reading);
		tokenizer.next();
		return new SwitchExpr();
	}

	private void check(final Type type) throws ParserException {
		final Type found = tokenizer.token().type();
		if (found != type)
			throw new ParserException(Messages.getSingleMessage("syntaxError"));
	}

}
