package org.contextvox.contextualizer.commandLine;

import org.contextvox.Activator;
import org.contextvox.core.nativeVox.translator.Symbol;

/**
 * This class implements some functionality for build a message, so it is
 * adapted to reading in an other moment
 *
 */
public class MessageBuilder {

	private final StringBuffer sb;

	public MessageBuilder() {
		sb = new StringBuffer();
	}

	public void add(final String str) {
		sb.append(str);
		sb.append(Symbol.space);
	}

	public void addAndNaturalReplace(final String str) {
		sb.append(Activator.getServiceHandler().getReplacerService().replaceSymbols(str));
	}

	public void addAndPause(final String str) {
		sb.append(str);
		sb.append(Symbol.colon);
	}

	public void add(final int i) {
		sb.append(String.valueOf(i));
		sb.append(Symbol.space);
	}

	public void addAndPause(final int i) {
		sb.append(String.valueOf(i));
		sb.append(Symbol.colon);
	}

	public void addEnd() {
		sb.append(Symbol.point);
	}

	@Override
	public String toString() {
		return sb.toString();
	}

}
