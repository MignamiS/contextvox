package org.contextvox.contextualizer.commandLine.commandlinev2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.listeners.SpotChangesListener;
import org.contextvox.IDEobserver.views.ContextVoxView;
import org.contextvox.contextualizer.ASTInfoConverter;
import org.contextvox.contextualizer.ASTProvider;
import org.contextvox.plugin.BookmarkManager;
import org.contextvox.plugin.PluginElements;
import org.contextvox.plugin.requests.MessageSource;
import org.contextvox.preferences.PreferenceHelper;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.MsgKey;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.contextvox.services.ttsengine.TTSService;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * This class contains all the static method called by the Command Line parser
 * when the command has been recognized. Each of these methods is the starting
 * point of the command execution.
 */
public class CommandExecutor {

	private static final TTSService core;
	static {
		core = Activator.getServiceHandler().getTTSService();
	}

	private static final String ERROR_NO_LISTENER_REGISTERED = "Error: you must before use the command to start registering editor's events";

	private static final String STATUS_SYMBOL = "symbol";
	private static final String STATUS_ABSTRACTION = "abstraction";
	private static final String STATUS_DEBUG = "debug";
	private static final String STATUS_CORE = "core";

	// text associated to the ContextVox's quick bookmark
	private static final String BOOKMARK_LABEL = "<quick bookmark>";

	/*
	 * Landmarks representing the offset of the document's region to select with
	 * the Select Line command.
	 */
	private static int startLandmark = -1;
	private static int endLandmark = -1;
	// editor in which there is the start landmark
	private static ITextEditor startLandmarkEditor = null;

	// spot change listener currently on action
	private static SpotChangesListener spotChangesListener = null;

	public static void help(final Cmd command) {
		System.out.println(command);
	}

	public static void infoElement(final Cmd command) {
		// convert option in sentence tokens
		final List<SentenceToken> tokens = convertInfoElementOptions(command.getOptions());
		// get current node
		final IEditorPart part = PluginElements.getActiveEditor();
		if (part == null || !(part instanceof ITextEditor))
			return;

		final ASTNode node = ASTProvider.getNodeOnCursorPosition((ITextEditor) part);
		// analyze node
		// TODO provide code line
		final String result = ASTInfoConverter.selectiveAnalysis(tokens, node, null);
		// TODO insert codeline
		if (result.equals("")) {
			// print error message
			final String errorMessage = "Error on the command, see the help";
			PluginElements.getContextVoxView().writeOnConsole(errorMessage);
			core.read(errorMessage, MessageSource.COMMANDLINE);
		} else {
			PluginElements.getContextVoxView().writeOnConsole(result);
			core.read(result, MessageSource.COMMANDLINE);
		}
	}

	private static List<SentenceToken> convertInfoElementOptions(final ArrayList<String> options) {
		final ArrayList<SentenceToken> tokens = new ArrayList<>();
		options.stream().forEach(item -> {
			final SentenceToken tok = SentenceToken.getTokenFromOption(item);
			if (tok != null)
				tokens.add(tok);
		});

		return tokens;
		// return options.stream().map(option -> {
		// switch (option) {
		// case "n":
		// case "name":
		// return SentenceToken.NAME;
		// case "p":
		// case "param":
		// return SentenceToken.PARAMETERS;
		// case "t":
		// case "type":
		// return SentenceToken.TYPE;
		//
		// case "i":
		// case "iterator":
		// return SentenceToken.ITERATOR;
		// case "c":
		// case "condition":
		// return SentenceToken.CONDITION;
		// case "e":
		// case "else":
		// return SentenceToken.ELSE_STATEMENT;
		// case "sel":
		// case "selector":
		// return SentenceToken.SELECTOR;
		// case "cs":
		// case "caselist":
		// return SentenceToken.LIST;
		// // case "str":
		// // case "structure":
		// // return SentenceToken.
		//
		// default:
		// // TODO create an exception ad-hoc
		// throw new IllegalArgumentException("Option unsupported");
		// }
		// }).collect(Collectors.toList());
	}

	public static void status(final Cmd command) {
		// TODO change
		String result = "";
		if (command.getArguments().isEmpty()) {
			// say all
			// final String abstraction =
			// (ContextVoxProgram.isHighLevelReading()) ? "high" : "low";
			final String abstraction = "unknown";
			final String symbol = Activator.cvox().toString();
			result = String.format("Context vox is reading in %s level of abstraction and with a %s symbol level",
					abstraction, symbol);
		} else {
			// analyze only the given element
			final String arg = command.getArguments().get(0).toLowerCase();
			switch (arg) {
			case STATUS_ABSTRACTION:
				// final String state = (ContextVoxProgram.isHighLevelReading())
				// ? "high" : "low";
				final String state = "unknown";
				result = String.format("Context Vox is reading on %s level of abstraction", state);
				break;
			case STATUS_SYMBOL:
				result = String.format("Context Vox is reading %s symbols", Activator.cvox());
				break;
			case STATUS_DEBUG:
				if (PreferenceHelper.isDebugMode())
					result = "Context Vox is in debug mode";
				else
					result = "Context Vox is not in debug mode";
				break;
			case STATUS_CORE:
				final String label = core.getCurrentCoreType().getLabel();
				result = String.format("The current core is the %s", label);
				break;
			}
		}

		core.read(result, MessageSource.COMMANDLINE);
		writeOnConsole(result);
	}

	public static void selectToEnd() {
		selectCodeOnEditor(false);
		PluginElements.getActiveEditor().setFocus();

	}

	private static void selectCodeOnEditor(final boolean toStart) {
		// get node where the cursor is
		final IEditorPart part = PluginElements.getActiveEditor();
		if (!(part instanceof ITextEditor))
			return;

		final ASTNode currentNode = ASTProvider.getNodeOnCursorPosition((ITextEditor) part);

		// navigate to the first enclosing block
		ASTNode tmp = currentNode;
		ASTNode found = null;
		while (tmp != null) {
			// found block, TypeDeclaration, AnonymusType
			if (tmp.getNodeType() == ASTNode.BLOCK || tmp.getNodeType() == ASTNode.TYPE_DECLARATION
					|| tmp.getNodeType() == ASTNode.ANONYMOUS_CLASS_DECLARATION) {
				found = tmp;
				break;
			}
			tmp = tmp.getParent();
		}

		final int blockOffset = found.getStartPosition();
		final int blockLength = found.getLength();
		if (toStart) {
			final int start = blockOffset + 1;// don't select the brace
			final int length = currentNode.getStartPosition() - start;
			// select the code (-1 means "do not select the end of block")
			((ITextEditor) part).selectAndReveal(start, length);
		} else {
			final int start = currentNode.getStartPosition();
			final int length = blockLength - (start - blockOffset);
			// select the code (-1 means "do not select the end of block")
			((ITextEditor) part).selectAndReveal(start, length - 1);
		}
	}

	public static void selectToStart() {
		selectCodeOnEditor(true);
		PluginElements.getActiveEditor().setFocus();
	}

	private static void writeOnConsole(final String text) {
		/*
		 * check if the thread executing this method is the UI-Thread. If yes,
		 * write simply on console, otherwise schedule a UIJob.
		 */
		if (Thread.currentThread().equals(Display.getCurrent().getThread())) {
			PluginElements.getContextVoxView().writeOnConsole(text);
		} else {
			new UIJob("write on command line console") {

				@Override
				public IStatus runInUIThread(final IProgressMonitor arg0) {
					PluginElements.getContextVoxView().writeOnConsole(text);
					return Status.OK_STATUS;
				}
			}.schedule();
		}

		// announce text
		core.read(text, MessageSource.COMMANDLINE);
	}

	/**
	 * Jump command.
	 *
	 * @param cmd
	 */
	public static void jumpTo(final Cmd cmd) {
		final String val = cmd.getOptions().get(0);
		if (val != null) {
			// jump to bookmark
			if (isTargetABookmark(val))
				return;

			// get current node
			final IEditorPart editor = PluginElements.getActiveEditor();
			if (editor != null && editor instanceof ITextEditor) {
				final ASTNode currentNode = ASTProvider.getNodeOnCursorPosition((ITextEditor) editor);
				if (currentNode instanceof Statement) {
					// get jump target node
					final Statement target = getTargetNode(val, currentNode);

					// jump to node
					if (target != null) {
						final int start = target.getStartPosition();
						((ITextEditor) editor).selectAndReveal(start, 0);
						editor.setFocus();
					} else {
						// no next node
						writeOnConsole(MsgKey.ERROR_NO_NEXT_NODE);
					}
				} else {
					// no statement selected
					writeOnConsole(MsgKey.ERROR_NO_STATEMENT);
				}
			} else {
				// no editor opened or no textual editor selected
				writeOnConsole(MsgKey.ERROR_NO_EDITOR);
			}
		}
	}

	// check if the user wants to jump to a bookmark
	private static boolean isTargetABookmark(final String val) {
		final String bookmarkOption = "bm";
		if (val.equals(bookmarkOption)) {
			jumpToBookmark();
			return true;
		}
		return false;

	}

	// switch between option and return the corresponding target node, or null.
	private static Statement getTargetNode(final String val, final ASTNode currentNode) {
		Statement target = null;
		switch (val) {
		case "next":
		case "n":
			target = ASTProvider.getTargetStatement((Statement) currentNode, ASTProvider.NEXT);
			break;
		case "prev�":
		case "p":
			target = ASTProvider.getTargetStatement((Statement) currentNode, ASTProvider.PREVIOUS);
			break;
		case "in":
			target = ASTProvider.getTargetStatement((Statement) currentNode, ASTProvider.IN);
			break;
		case "out":
			target = ASTProvider.getTargetStatement((Statement) currentNode, ASTProvider.OUT);
			break;
		case "last":
		case "l":
			target = ASTProvider.getTargetStatement((Statement) currentNode, ASTProvider.LAST);
			break;
		case "first":
		case "f":
			target = ASTProvider.getTargetStatement((Statement) currentNode, ASTProvider.FIRST);
			break;
		default:
			// TODO display better errore
			core.read("This option is not supported", MessageSource.COMMANDLINE);
			break;

		}
		return target;
	}

	private static void jumpToBookmark() {
		final IMarker mark = BookmarkManager.getBookmark();
		if (mark != null && mark.exists()) {
			// jump to
			try {
				final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				// Integer line= (Integer)
				// mark.getAttribute(IMarker.LINE_NUMBER);
				IDE.openEditor(page, mark);
				PluginElements.getActiveEditor().setFocus();
			} catch (final CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// error message
		}
	}

	public static void createBookmar() {
		// get cursor position
		final ITextSelection sel = PluginElements.getCurrentCursorPosition(null);
		if (sel == null)
			return;
		final int line = sel.getStartLine() + 1;

		// delete previously created quick bookmark
		final IResource resource = ResourceUtil.getResource(PluginElements.getActiveEditor().getEditorInput());
		deletePreviousBookkmark();

		try {
			final IMarker mark = resource.createMarker(IMarker.BOOKMARK);
			mark.setAttribute(IMarker.LINE_NUMBER, line);
			mark.setAttribute(IMarker.TRANSIENT, true);
			mark.setAttribute(IMarker.MESSAGE, BOOKMARK_LABEL);
			BookmarkManager.setBookmark(mark);
			// print and say
			final String mess = Messages.getSingleMessage("bookmarkCreated");
			PluginElements.getContextVoxView().writeOnConsole(mess);
			core.read(mess, MessageSource.COMMANDLINE);
			PluginElements.getActiveEditor().setFocus();
		} catch (final CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// removes previously created bookmarks
	private static void deletePreviousBookkmark() {
		final IMarker mark = BookmarkManager.getBookmark();
		if (mark != null) {
			try {
				mark.delete();
			} catch (final CoreException e) {
				e.printStackTrace();
			}
			BookmarkManager.deleteBookmark();
		}
	}

	public static void deleteBookmark() {
		final IMarker mark = BookmarkManager.getBookmark();
		if (mark != null && mark.exists()) {
			try {
				mark.delete();
			} catch (final CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		final String mess = Messages.getSingleMessage("bookmarkDeleted");
		core.read(mess, MessageSource.COMMANDLINE);
		PluginElements.getContextVoxView().writeOnConsole(mess);
	}

	public static void addShortcut(final Cmd cmd) {
		// parse integer
		final String str = cmd.getOptions().get(0);
		final int key = Integer.parseInt(str);
		final ContextVoxView view = PluginElements.getContextVoxView();
		final String command = view.getLastCommand();
		if (command != null) {
			view.addShortcut(key, command);
			// notify user
			final String message = "Shortcut added";
			core.read(message, MessageSource.COMMANDLINE);
			view.writeOnConsole(message);
		}
	}

	public static void getShortcut() {
		// print on console
		final ContextVoxView view = PluginElements.getContextVoxView();
		final ArrayList<String> list = view.getAllShortcuts();
		list.stream().forEach(item -> {
			view.writeOnConsole(item);
		});
		// notify user
		final String message = String.format("Printed %d shortcuts on the output area", list.size());
		core.read(message, MessageSource.COMMANDLINE);
	}

	/**
	 * Processs some extra command, used for development purpose. Commands have
	 * to be inserted in the form "keyword=value". Spaces are not considered.
	 * Command accepted:
	 * <ul>
	 * <li>clear - clears the command line output</li>
	 * </ul>
	 *
	 * @param cmd
	 *            the raw command from the input line
	 * @return <code>true</code> if the command has been recognized as "extra"
	 *         and the processing should be skipped. Otherwise the parser is
	 *         activated.
	 */
	public static boolean extraCommand(final String cmd) {
		// clear command
		if (cmd.trim().equalsIgnoreCase("clear")) {
			clearCommandLine();
			return true;
		}

		// other commands
		final String[] token = cmd.replaceAll("\\s+", "").split("=");
		switch (token[0]) {
		case "whitespace":
			// ContextVoxProgram.setShowWhitespacesOnConsole(Boolean.parseBoolean(token[1]));
			return true;
		}

		return false;
	}

	// clears the output area of the command line
	private static void clearCommandLine() {
		if (Thread.currentThread().equals(Display.getCurrent().getThread())) {
			PluginElements.getContextVoxView().clearOutputArea();
		} else {
			new UIJob("clear command line") {

				@Override
				public IStatus runInUIThread(final IProgressMonitor arg0) {
					PluginElements.getContextVoxView().clearOutputArea();
					return Status.OK_STATUS;
				}
			}.schedule();
		}
	}

	public static void selectLine(final Cmd cmd) {
		// choose operation
		final ArrayList<String> option = cmd.getOptions();
		if (!option.isEmpty()) {
			switch (option.get(0)) {
			case "s":
				setLandmark(true);
				break;

			case "e":
				setLandmark(false);
				break;

			case "r":
				startLandmark = endLandmark = -1;
				writeOnConsole(MsgKey.INFO_LANDMARK_RESET);
				break;

			case "status":
				informStatus();
				break;

			default:
				break;
			}
		}
	}

	// inform the user about the landmark selected
	private static void informStatus() {
		final Map<SentenceToken, String> data = new HashMap<>();
		if (startLandmark != -1)
			data.put(SentenceToken.START, "" + startLandmark);
		else
			data.put(SentenceToken.BLANK, "");
		final String msg = Activator.getServiceHandler().getReplacerService()
				.makeSentence(SentenceType.SELECT_LINE_STATUS, data);
		writeOnConsole(msg);
	}

	private static void setLandmark(final boolean start) {
		// get current editor
		final IEditorPart part = PluginElements.getActiveEditor();
		if (part == null || !(part instanceof ITextEditor)) {
			writeOnConsole(MsgKey.ERROR_NO_EDITOR);
			return;
		}

		final ITextEditor editor = (ITextEditor) part;

		// save cursor line number
		final ISelection sel = editor.getSelectionProvider().getSelection();
		if (sel instanceof ITextSelection) {
			final ITextSelection textSel = (ITextSelection) sel;
			final IDocument doc = editor.getDocumentProvider().getDocument(editor.getEditorInput());
			if (start) {
				// retreive line start
				final int line = textSel.getStartLine();
				try {
					startLandmark = doc.getLineOffset(line);
				} catch (final BadLocationException e) {
					e.printStackTrace();
				}
				startLandmarkEditor = editor;
				endLandmark = -1;
				editor.setFocus();
			} else {
				// check 2 landmark are in same editor
				if (editor.equals(startLandmarkEditor)) {
					// retreive line end
					final int offset = textSel.getOffset();
					try {
						final int line = doc.getLineOfOffset(offset);
						final IRegion lineInfo = doc.getLineInformation(line);
						endLandmark = lineInfo.getOffset() + lineInfo.getLength();
					} catch (final BadLocationException e) {
						e.printStackTrace();
					}
				} else {
					// landmark in different editor, notice user
					writeOnConsole(MsgKey.ERROR_LANDMARK_DIFFERENT_EDITOR);
				}

			}

			// if end then select area
			if (!start) {
				/*
				 * For the moment only tob-down selection is available. Warn the
				 * user, but swap the landmarks and proceed.
				 */
				if (startLandmark != -1 && endLandmark != -1) {
					if (startLandmark > endLandmark) {
						final int tmp = startLandmark;
						startLandmark = endLandmark;
						endLandmark = tmp;

						writeOnConsole(MsgKey.WARNING_BOTTOM_UP_SELECTION);
						core.read(MsgKey.WARNING_BOTTOM_UP_SELECTION, MessageSource.COMMANDLINE);
					}

					final int length = endLandmark - startLandmark;
					editor.selectAndReveal(startLandmark, length);
					editor.setFocus();

					// reset landmarks
					startLandmark = endLandmark = -1;
				}
			}
		}

	}

	public static void spotChanges() {
		if (spotChangesListener != null) {
			final String report = spotChangesListener.getSpottedChanges();
			writeOnConsole("Spotted changes: ");
			writeOnConsole(report);
			spotChangesListener.detach();
			spotChangesListener = null;
		} else {
			// warn the user
			writeOnConsole(ERROR_NO_LISTENER_REGISTERED);
		}
	}

	public static void spotRecord() {
		// get document
		final IDocument doc = PluginElements.getDocumentOnActiveEditor();
		if (doc == null) {
			final String message = Messages.getSingleMessage(MsgKey.ERROR_NO_EDITOR);
			writeOnConsole(message);
			;
			return;
		}

		// attach new listener
		spotChangesListener = new SpotChangesListener(doc);
		doc.addDocumentListener(spotChangesListener);
		PluginElements.getActiveEditor().setFocus();
		;
	}
}
