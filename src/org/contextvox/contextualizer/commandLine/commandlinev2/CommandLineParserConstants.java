/* Generated By:JavaCC: Do not edit this line. CommandLineParserConstants.java */
package org.contextvox.contextualizer.commandLine.commandlinev2;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface CommandLineParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int SELEND = 1;
  /** RegularExpression Id. */
  int SELSTART = 2;
  /** RegularExpression Id. */
  int SELLINE = 3;
  /** RegularExpression Id. */
  int JUMP = 4;
  /** RegularExpression Id. */
  int INFOELEM = 5;
  /** RegularExpression Id. */
  int HELP = 6;
  /** RegularExpression Id. */
  int STATUS = 7;
  /** RegularExpression Id. */
  int ADDSHORTCUT = 8;
  /** RegularExpression Id. */
  int GETSHORTCUT = 9;
  /** RegularExpression Id. */
  int CREATEBOOKM = 10;
  /** RegularExpression Id. */
  int DELETEBOOKM = 11;
  /** RegularExpression Id. */
  int SPOTREC = 12;
  /** RegularExpression Id. */
  int SPOTCHANGES = 13;
  /** RegularExpression Id. */
  int EOL = 15;
  /** RegularExpression Id. */
  int NUMERICOPTION = 16;
  /** RegularExpression Id. */
  int OPTION = 17;
  /** RegularExpression Id. */
  int WORD = 18;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "<SELEND>",
    "<SELSTART>",
    "<SELLINE>",
    "<JUMP>",
    "<INFOELEM>",
    "<HELP>",
    "\"status\"",
    "\"addShortcut\"",
    "\"getShortcut\"",
    "<CREATEBOOKM>",
    "<DELETEBOOKM>",
    "<SPOTREC>",
    "<SPOTCHANGES>",
    "\" \"",
    "<EOL>",
    "<NUMERICOPTION>",
    "<OPTION>",
    "<WORD>",
  };

}
