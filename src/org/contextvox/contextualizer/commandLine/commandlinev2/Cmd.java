package org.contextvox.contextualizer.commandLine.commandlinev2;

import java.util.ArrayList;

class Cmd {

	private final String command;
	private final ArrayList<String> options;
	private final ArrayList<String> arguments;

	public Cmd(String command) {
		this.command = command;
		this.options = new ArrayList<>();
		this.arguments = new ArrayList<>();
	}

	public void addOption(String opt) {
		if (opt.length() <= 1)
			return;
		if (opt.startsWith("-")) {
			this.options.add(opt.substring(1));
		}
	}

	public void addArgument(String arg) {
		this.arguments.add(arg);
	}

	public ArrayList<String> getArguments() {
		return arguments;
	}

	public ArrayList<String> getOptions() {
		return options;
	}

	@Override
	public String toString() {
		return this.command + " " + this.options.toString() + " " + this.arguments.toString();
	}
}
