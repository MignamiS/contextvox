package org.contextvox.contextualizer.commandLine;

import java.util.ArrayList;
import java.util.List;

import org.contextvox.IDEobserver.exceptions.InterpreterException;
import org.eclipse.jdt.core.dom.ASTNode;

/**
 * This class save the ASTNode, to all execution, this node are visited from command line  
 * This class is a component of the Interpreter pattern (Modified)
 */
public class Context {

	private static Context context = null;
	private final List<ASTNode> nodes = new ArrayList<ASTNode>();

	private Context() {
	}

	public static synchronized Context getInstance() {
		if (context == null)
			context = new Context();
		return context;
	}

	public void setStartNode(final ASTNode node) {
		nodes.clear();
		nodes.add(node);
	}

	public void addNode(final ASTNode node) {
		nodes.add(node);
	}

	public void addParentNode() throws InterpreterException {
		final ASTNode node = getCurrentNode().getParent();
		if (node == null)
			throw new InterpreterException("No parent Node");
		nodes.add(node);
	}

	public ASTNode getCurrentNode() throws InterpreterException {
		if (nodes.isEmpty())
			throw new InterpreterException("No Node selected");
		return nodes.get(nodes.size() - 1);
	}

	public void removeLastNode() throws InterpreterException {
		if (nodes.size() < 2)
			throw new InterpreterException("No Previously Selected Node");
		nodes.remove(nodes.size() - 1);
	}

}