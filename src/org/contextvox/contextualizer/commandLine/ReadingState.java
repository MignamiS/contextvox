package org.contextvox.contextualizer.commandLine;

/**
 * 
 * @desc Save the state of the read line: partial or complete
 *
 */
public class ReadingState {

	private static ReadingState readingState = null;
	private boolean isPartial = false;

	private ReadingState() {
	}

	public static synchronized ReadingState getInstance() {
		if (readingState == null)
			readingState = new ReadingState();
		return readingState;
	}

	public boolean isPartial() {
		return isPartial;
	}

	public void switchState() {
		isPartial = isPartial ? false : true;
	}

}
