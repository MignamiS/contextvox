package org.contextvox.contextualizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;

/**
 * This class will provide the corresponding node of a AST for the given
 * starting node.<br/>
 * <b>The class is thread-safe</b>
 */
public class ASTInfoCollector {

	/**
	 * Return the enclosing method of the start node.
	 * 
	 * @param startNode
	 *            the node corresponding to the cursor line
	 * @return MethodDeclaration if found, null otherwise
	 */
	public static MethodDeclaration getParentMethod(final ASTNode startNode) {
		ASTNode tmp = startNode;
		do {
			tmp = tmp.getParent();
			if (tmp == null) {
				// root reached
				break;
			}
		} while (tmp.getNodeType() != ASTNode.METHOD_DECLARATION);

		return (MethodDeclaration) tmp;
	}

	/**
	 * Return the node corresponding to the class declaration, starting from the
	 * given start node.
	 * 
	 * @param startNode
	 * @return TypeDeclaration if found, null otherwise
	 */
	public static TypeDeclaration getParentClassDeclaration(
			final ASTNode startNode) {
		ASTNode tmp = startNode;
		do {
			tmp = tmp.getParent();
			if (tmp == null) {
				// root reached
				break;
			}
		} while (tmp.getNodeType() != ASTNode.TYPE_DECLARATION);

		return (TypeDeclaration) tmp;
	}

	/**
	 * Return the node corresponding to the package declaration, starting from
	 * the given start node.
	 * 
	 * @param startNode
	 * @return PackageDeclaration if found, null otherwise
	 */
	public static PackageDeclaration getParentPackage(final ASTNode startNode) {
		ASTNode tmp = startNode.getParent();
		// look for compilation unit, packagedeclaration doesn't work
		do {
			if (tmp.getParent() == null) {
				// root reached
				break;
			} else {
				tmp = tmp.getParent();
			}
		} while (tmp.getNodeType() != ASTNode.COMPILATION_UNIT);
		CompilationUnit cu = (CompilationUnit) tmp;

		return cu.getPackage();
	}

	/**
	 * Return the current case of the switch, starting from a generic statement.
	 * 
	 * @param startNode
	 *            a generic statement
	 * @return CaseStatement or null if the statement isn't in a switch
	 */
	public static SwitchCase getCurrentCaseStatement(final Statement startNode) {
		// go to the switch parent
		SwitchStatement par = null;
		if (startNode.getParent().getNodeType() == ASTNode.SWITCH_STATEMENT)
			par = (SwitchStatement) startNode.getParent();
		else
			return null;

		// look for the current case statement
		@SuppressWarnings("unchecked")
		List<Statement> list = par.statements();
		int index = -1;
		for (int i = 0; i < list.size(); i++) {
			// look for the start node
			Statement s = list.get(i);
			if (s.equals(startNode)) {
				index = i;
				break;
			}
		}

		// something goes wrong
		if (index == -1)
			return null;

		// look for the "parent" case, searching backward into the list
		for (int i = index; i >= 0; i--) {
			if (list.get(i).getNodeType() == ASTNode.SWITCH_CASE)
				return (SwitchCase) list.get(i);
		}

		return null;
	}

	/**
	 * Return the first switch declaration that enclose the given statement.
	 * 
	 * @param startNode
	 *            a statement inside the switch
	 * @return SwitchStatement or null if there is no parent switch-declaration
	 */
	public static SwitchStatement getEnclosingSwitchNode(
			final Statement startNode) {
		ASTNode tmp = startNode;
		do {
			tmp = tmp.getParent();
			if (tmp == null)
				break;
		} while (tmp.getNodeType() != ASTNode.SWITCH_STATEMENT);

		return (SwitchStatement) tmp;
	}

	/**
	 * Check if the given node is enclosed in a loop.
	 * 
	 * @param node
	 * @return Statement corresponding to the first loop-statement declaration
	 *         found, null otherwise
	 */
	public static Statement isInALoop(final Statement node) {
		Statement tmp;
		// do not check the starting node, we don't care if it is a loop
		if (node.getParent() instanceof Statement)
			tmp = (Statement) node.getParent();
		else
			return null; // not in a loop

		// now loop upward looking for a loop-statement
		do {
			if (tmp.getParent() instanceof Statement) {
				tmp = (Statement) tmp.getParent();
				// check loop-statement
				int type = tmp.getNodeType();
				if (type == ASTNode.FOR_STATEMENT
						|| type == ASTNode.ENHANCED_FOR_STATEMENT
						|| type == ASTNode.WHILE_STATEMENT
						|| type == ASTNode.DO_STATEMENT)
					return tmp;
			} else {
				break;
			}
		} while (tmp != null);

		return null;
	}

	/**
	 * Checks if the given statement is in a double for-loop. If there is only a
	 * single for-loop enclosing, the method will return <b>null</b>.<br/>
	 * Note that the given node is considered as a starting point not inclusive,
	 * so if it is a for-loop, won't be considered in the count.
	 * 
	 * @param node
	 * @return Statement[] or null if there are less than 2 for-loops
	 */
	public static Statement[] isInADoubleFor(final Statement node) {
		Statement[] result = new Statement[2];
		Statement tmp = isInALoop(node);
		if (tmp != null && tmp instanceof ForStatement) {
			// found first loop
			result[0] = tmp;
		} else {
			return null;
		}

		tmp = isInALoop(tmp);
		if (tmp != null && tmp instanceof ForStatement) {
			// found the second one
			result[1] = tmp;
		} else {
			return null;
		}

		return result;
	}

	/**
	 * Return if the given node is in a try-catch statement. If the given node
	 * is a catch clause, it is ugually considered in a try statement.<br/>
	 * Note that if the editor's cursor is on the keyword "try" it is still
	 * considered <b>inside</b> the try statement.
	 * 
	 * @param node
	 * @return TryStatement
	 */
	public static TryStatement isInTryCatchBlock(final ASTNode node) {
		ASTNode tmp = node;
		while (tmp.getNodeType() != ASTNode.TRY_STATEMENT
				&& tmp.getNodeType() != ASTNode.CATCH_CLAUSE) {
			tmp = tmp.getParent();
			if (tmp == null)
				return null;
		}

		if (tmp instanceof CatchClause)
			return (TryStatement) tmp.getParent();
		else
			return (TryStatement) tmp;
	}

	/**
	 * Return all the variables declarated on the given node's scope.
	 * 
	 * @param node
	 * @param scopeLevels
	 *            number of parent scopes processed; if there are not enough
	 *            parents, the method will return before causing errors
	 * @param params
	 *            if true also the method parameters is considered. Note that
	 *            the metho's parameters are returned only if the method
	 *            declaration is currently in the scope took in account.
	 * @return List of variable declaration statement, or null if none is found
	 */
	public static List<VariableDeclaration> variablesOnScope(
			final ASTNode node, int scopeLevels, final boolean params) {
		if (scopeLevels < 1)
			scopeLevels = 1; // no less than 1 scope level

		// get the parent blocks
		ASTNode tmp = node;

		// iterate each possible scope
		boolean foundLimit = false;
		List<VariableDeclaration> vars = new ArrayList<>();
		for (int n = 0; n < scopeLevels; n++) {
			do {
				tmp = tmp.getParent();
				if (tmp == null
						|| tmp.getNodeType() == ASTNode.METHOD_DECLARATION) {
					foundLimit = true;
					break;
				}
			} while (tmp.getNodeType() != ASTNode.BLOCK);

			// if it is a method, get parameters
			List<?> parameters = null;
			if (params) {
				if (tmp.getParent().getNodeType() == ASTNode.METHOD_DECLARATION) {
					parameters = ((MethodDeclaration) tmp.getParent())
							.parameters();
				}
			}

			// found a block, get variable declaration
			List<?> ss = ((Block) tmp).statements();
			for (int i = 0; i < ss.size(); i++) {
				ASTNode s = (ASTNode) ss.get(i);
				if (s.getNodeType() == ASTNode.VARIABLE_DECLARATION_STATEMENT)
					vars.add((VariableDeclaration) s);
			}

			// look for eventually for-loop iterators
			VariableDeclaration iterator = null;
			if (tmp.getParent().getNodeType() == ASTNode.FOR_STATEMENT) {
				List<?> init = ((ForStatement) tmp.getParent()).initializers();
				if (init.size() == 1
						&& init.get(0) instanceof VariableDeclaration) {
					iterator = (VariableDeclaration) init.get(0);
				}
			}
			vars.add(iterator);
			// TODO foreach iterator

			// add method parameters, if found
			if (params && parameters != null && parameters.size() > 0) {
				for (int i = 0; i < parameters.size(); i++) {
					if (((ASTNode) parameters.get(i)).getNodeType() == ASTNode.VARIABLE_DECLARATION_STATEMENT) {
						vars.add((VariableDeclaration) parameters.get(i));
					}
				}

				// method found, no more scopes upward
				if (foundLimit)
					break;
			}
		}

		return (vars.size() > 0) ? vars : null;
	}
}
