package org.contextvox.contextualizer.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contextvox.services.replacer.SentenceToken;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

/**
 * Use this ASTVisitor to look for Simple Expressions
 */
public class SimpleExpressionVisitor extends ASTVisitor {

	private final Map<SentenceToken, String> data = new HashMap<>();

	public Map<SentenceToken, String> getData() {
		return data;
	}

	public SimpleExpressionVisitor() {
	}

	public SimpleExpressionVisitor(boolean visitDocTags) {
		super(visitDocTags);
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		final List<?> frag = node.fragments();
		if (frag == null || frag.size() == 0)
			return false;

		/*
		 * TODO Extract a possible concatenation of variable declaration. For
		 * now just a simple variable is extracted (e.g. String str = ...), but
		 * should be fine something like "int a = b = ...", using the variable
		 * declaration fragment list provided.
		 */
		final VariableDeclarationFragment var = (VariableDeclarationFragment) frag.get(0);
		final Expression init = var.getInitializer();
		final ArrayList<MethodInvocation> functions = getFunctions(init);

		if (evaluate(functions)) {
			final String type = node.getType().toString();
			final String name = var.getName().toString();
			final String lhe = String.format("%s %s = ", type, name);
			extractInfo(functions, lhe);
		}

		return false;
	}

	// determines if the node fit the specifications
	private boolean evaluate(ArrayList<MethodInvocation> functions) {
		// 2+ function concatenated
		if (functions.size() < 2)
			return false;

		// no function call in parameter list
		for (final MethodInvocation item : functions) {
			final List<?> args = item.arguments();
			// iterate arguments
			for (int i = 0; i < args.size(); i++) {
				if (args.get(i) instanceof MethodInvocation)
					return false;
			}
		}

		return true;
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		// retreive expression data
		final Expression expression = node.getExpression();
		final ArrayList<MethodInvocation> functions = getFunctions(expression);

		if (evaluate(functions)) {
			String rhe = "";
			if (expression.getNodeType() == ASTNode.ASSIGNMENT)
				rhe = ((Assignment) expression).getLeftHandSide().toString() + " = ";
			extractInfo(functions, rhe);
		}

		return false;
	}

	private void extractInfo(final ArrayList<MethodInvocation> functions, String leftHandExp) {
		data.put(SentenceToken.LEFT_HAND, leftHandExp);

		// number of concatenated functions
		final String number = "" + functions.size();
		data.put(SentenceToken.NUMBER, number);

		// name of last function
		final String name = functions.get(0).getName().toString();
		data.put(SentenceToken.NAME, name);

		// list of parameters of the last function
		final String list = getArgumentsAsString(functions.get(0));
		if (list != null && !list.isEmpty())
			data.put(SentenceToken.LIST, list);
	}

	private String getArgumentsAsString(MethodInvocation method) {
		final StringBuilder sb = new StringBuilder();
		final List<?> args = method.arguments();
		for (int i = 0; i < args.size(); i++) {
			final Expression arg = (Expression) args.get(i);
			final int type = arg.getNodeType();
			if (type == ASTNode.NUMBER_LITERAL || type == ASTNode.BOOLEAN_LITERAL || type == ASTNode.CHARACTER_LITERAL
					|| type == ASTNode.NULL_LITERAL || type == ASTNode.STRING_LITERAL || type == ASTNode.TYPE_LITERAL
					|| type == ASTNode.SIMPLE_NAME)
				sb.append(arg.toString() + " ");
			else
				return null;
		}

		return sb.toString();
	}

	private ArrayList<MethodInvocation> getFunctions(Expression node) {
		final ArrayList<MethodInvocation> functions = new ArrayList<>();

		Expression exp = node;
		// take care of var assignement
		if (exp.getNodeType() == ASTNode.ASSIGNMENT)
			exp = ((Assignment) exp).getRightHandSide();

		do {
			if (exp.getNodeType() == ASTNode.METHOD_INVOCATION) {
				final MethodInvocation mi = (MethodInvocation) exp;
				functions.add(mi);
				exp = mi.getExpression();
			} else {
				break;
			}
		} while (exp != null);

		return functions;
	}

}
