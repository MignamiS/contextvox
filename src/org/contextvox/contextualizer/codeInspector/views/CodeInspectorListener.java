package org.contextvox.contextualizer.codeInspector.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contextvox.Activator;
import org.contextvox.contextualizer.codeInspector.views.MethodParser.StateStatement;
import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.plugin.PluginElements;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
//TODO: da sistemare code inspector, quando si aggiungono progetti sul momento o quando
//si eliminano dei progetti con il code inspector aperto -->error!
//Per risolvere--> Evento Package Explorer richiama metodo getJavaProjects()

/**
 *
 * The listener of Code Inspector
 *
 */
public class CodeInspectorListener implements KeyListener {
	private int counter = 0, childLevel = 0, nrChildren = 0;
	private List<IJavaElement> allParents;
	private IJavaElement parent = null;
	private boolean statement = false;
	private StateStatement stateStatement = StateStatement.STAY;
	private MethodParser methodParser = null;
	private final CodeInspectorView codeInspectorView;

	public CodeInspectorListener() {
		codeInspectorView = PluginElements.getCodeInspectorView();
		getJavaProjects();
	}

	@Override
	public void keyPressed(final KeyEvent event) {
		String rensponse = "";

		if (parent == null && allParents.size() == 0) {
			getJavaProjects();
		}
		stateStatement = StateStatement.STAY;
		switch (event.keyCode) {
		// choose the father
		case SWT.ARROW_UP:
			if (allParents.size() == 0) {
				rensponse = Messages.getSingleMessage("notExistingProject");
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse, MessageSource.CODEINSPECTOR);
			} else if (statement && counter > 0) {
				counter--;
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			} else if (counter > 0) {
				counter--;
				parent = allParents.get(counter);
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			}
			break;
		case SWT.ARROW_DOWN:
			if (allParents.size() == 0) {
				rensponse = Messages.getSingleMessage("notExistingProject");
				// CoreProxy.readString(rensponse, MessageSource.CODEINSPECTOR);

			} else if (statement && !methodParser.getStop()) {
				counter++;
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			} else if (counter < allParents.size() - 1 && !statement) {
				counter++;
				parent = allParents.get(counter);
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			}
			break;
		case SWT.ARROW_LEFT:
			if (statement && childLevel >= 1) {
				childLevel--;
				counter = 0;
				stateStatement = StateStatement.COLLAPSED;
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			} else if (parent != null && parent.getParent() != null) {// close
																		// the
																		// info
				counter = 0;
				setElements(false);
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			}
			break;
		case SWT.ARROW_RIGHT:
			if (statement && !methodParser.getStop()) {
				childLevel++;
				counter = 0;
				stateStatement = StateStatement.EXPANDED;
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			} else if (parent != null && parent.getElementType() <= IJavaElement.METHOD) {// open
																							// the
																							// info
				counter = 0;
				setElements(true);
				rensponse = getInfoParent();
				codeInspectorView.addTextOnCommandArea(rensponse);
				// CoreProxy.readString(rensponse + " " +
				// Messages.getSingleMessage("selected"),
				// MessageSource.CODEINSPECTOR);
			}
			break;
		}
	}

	/**
	 * get all projects
	 */
	private void getJavaProjects() {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot root = workspace.getRoot();
		final IProject[] iProjects = root.getProjects();

		final IJavaProject[] projects = new IJavaProject[iProjects.length];
		// get all information of the projects and show in the code inspector
		for (int i = 0; i < iProjects.length; i++) {
			try {
				if (iProjects[i].hasNature(JavaCore.NATURE_ID)) {
					// IProject is not a IJavaProject, but all IJavaProjects
					// have an IProject
					projects[i] = JavaCore.create(iProjects[i]);
				}
			} catch (final CoreException e) {
				e.printStackTrace();
			}
		}
		allParents = new ArrayList<IJavaElement>();
		// get all elements
		for (int i = 0; i < projects.length; i++) {
			allParents.add(projects[i]);
		}
		if (allParents.size() >= 1) {
			parent = allParents.get(0);
			final String rensponse = getInfoParent();
			codeInspectorView.addTextOnCommandArea(rensponse);
			// CoreProxy.readString(rensponse + " " +
			// Messages.getSingleMessage("selected"),
			// MessageSource.CODEINSPECTOR);
		}
	}

	/**
	 * get all packages
	 *
	 * @param parent
	 *            the parent element to get all children
	 */
	private void getPackages(final IJavaElement parent) {
		final IPackageFragmentRoot tempProject = (IPackageFragmentRoot) parent;
		try {
			final IJavaElement[] temp = tempProject.getChildren();
			if (temp.length > 0) {
				allParents = new ArrayList<IJavaElement>();
				for (int i = 0; i < temp.length; i++) {
					if (!temp[i].getElementName().equals("")) {
						allParents.add(temp[i]);
					}
				}
			}
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * get all folders
	 *
	 * @param parent
	 *            the parent element to get all children
	 */
	private void getFolders(final IJavaElement parent) {
		final IJavaProject tempProject = parent.getJavaProject();
		try {

			final IJavaElement[] temp = tempProject.getChildren();
			if (temp.length > 0) {
				allParents = new ArrayList<IJavaElement>();
				for (int i = 0; i < temp.length; i++) {
					// not get jar or zip file
					if (!temp[i].getElementName().equals("") && !temp[i].getElementName().contains(".jar")
							&& !temp[i].getElementName().contains(".zip")) {
						allParents.add(temp[i]);
					}
				}
			}

		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * get all CompilationUnits = the Source file is always below the package
	 * node
	 */
	private void getCompilationUnit() {
		final IPackageFragment tempPackage = (IPackageFragment) parent;
		try {
			nrChildren = tempPackage.getChildren().length;
			if (nrChildren > 0) {
				allParents = new ArrayList<IJavaElement>(Arrays.asList(tempPackage.getChildren()));
			}
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * get all IType = class information
	 *
	 * @param parent
	 *            the parent element to get all children
	 */
	private void getClasstype(final IJavaElement parent) {
		final ArrayList<IJavaElement> temp = new ArrayList<IJavaElement>();
		try {
			for (int i = 0; i < allParents.size(); i++) {
				temp.addAll(Arrays.asList(((ICompilationUnit) allParents.get(i)).getAllTypes()));
			}
			if (temp.size() > 0) {
				allParents = new ArrayList<IJavaElement>(temp);
			}
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * get all methods and fields
	 *
	 * @param parent
	 *            the parent element to get all children
	 */
	private void getMethods(final IJavaElement parent) {
		final IType tempClass = (IType) parent;
		try {
			final int nrChildren = tempClass.getFields().length + tempClass.getMethods().length;
			if (nrChildren > 0) {
				allParents = new ArrayList<IJavaElement>(Arrays.asList(tempClass.getFields()));
				allParents.addAll(Arrays.asList(tempClass.getMethods()));
			}
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	private void createASTNode(final IMethod method) {
		final ICompilationUnit compilationUnit = method.getCompilationUnit();
		final ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(compilationUnit);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		final ASTNode rootNode = parser.createAST(null);
		final CompilationUnit compilationUnitNode = (CompilationUnit) rootNode;
		final ASTNode node = NodeFinder.perform(compilationUnitNode, rootNode.getStartPosition(), rootNode.getLength());
		Context.getInstance().setStartNode(node);
		methodParser = new MethodParser(method.getElementName());
	}

	/**
	 * get all information of the actual parent
	 *
	 * @return the information of the parent
	 */
	private String getInfoParent() {
		// part of statements
		if (statement) {
			// in base al counter mostra info
			return methodParser.returnAllStatement(counter, stateStatement);
		}

		// part of the java element
		final Map<SentenceToken, String> data = new HashMap<SentenceToken, String>();
		data.put(SentenceToken.NAME, parent.getElementName());
		switch (parent.getElementType()) {
		case IJavaElement.JAVA_PROJECT:
			final IJavaProject tempProject = (IJavaProject) parent;
			try {
				// check if is expandible
				if (tempProject.getChildren().length > 0) {
					data.put(SentenceToken.STATUS, Messages.getSingleMessage("expandable"));
					data.put(SentenceToken.NUMBER, tempProject.getChildren().length + "");
				} else {
					data.put(SentenceToken.STATUS, Messages.getSingleMessage("notexpandable"));
				}
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.JAVAPROJECT, data);
		case IJavaElement.PACKAGE_FRAGMENT_ROOT:
			final IPackageFragmentRoot tempFolder = (IPackageFragmentRoot) parent;
			try {
				// check if is expandible
				if (tempFolder.getChildren().length > 0) {
					data.put(SentenceToken.STATUS, Messages.getSingleMessage("expandable"));
					data.put(SentenceToken.NUMBER, tempFolder.getChildren().length - 1 + "");
				} else {
					data.put(SentenceToken.STATUS, Messages.getSingleMessage("notexpandable"));
				}
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FOLDER, data);
		case IJavaElement.PACKAGE_FRAGMENT:
			final IPackageFragment tempPackage = (IPackageFragment) parent;
			try {
				// check if is expandible
				if (tempPackage.getChildren().length > 0) {
					data.put(SentenceToken.STATUS, Messages.getSingleMessage("expandable"));
					data.put(SentenceToken.NUMBER, tempPackage.getChildren().length + "");
				} else {
					data.put(SentenceToken.STATUS, Messages.getSingleMessage("notexpandable"));
				}
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.PACKAGE, data);
		case IJavaElement.TYPE:
			final IType tempClass = (IType) parent;
			try {
				data.put(SentenceToken.MODIFIERS, Flags.toString(tempClass.getFlags()));
				// check the type of the class
				if (tempClass.isInterface()) {
					return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.INTERFACE,
							data);
				} else if (tempClass.isEnum()) {
					return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.ENUM, data);
				}
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.CLASS, data);
		case IJavaElement.FIELD:
			final IField tempField = (IField) parent;
			try {
				data.put(SentenceToken.MODIFIERS, Flags.toString(tempField.getFlags()));
				final String temp = Signature.getSignatureSimpleName(tempField.getTypeSignature());
				if (temp.contains("<")) {
					// temp = TokenReplacer.replaceGenerics(temp);
				}
				data.put(SentenceToken.TYPE, temp);
			} catch (final JavaModelException e) {
				e.printStackTrace();
			}
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.FIELD, data);
		case IJavaElement.METHOD:
			final IMethod method = (IMethod) parent;
			// create the astnode with the current parent
			createASTNode(method);
			// extract info method
			extractInfoMethod(method, data);
			return Activator.getServiceHandler().getReplacerService().makeSentence(SentenceType.METHOD, data);
		}
		return "";
	}

	/**
	 *
	 * @param javaElement
	 *            parent element to get all informations
	 * @param data
	 *            map to put all informations
	 */
	private void extractInfoMethod(final IMethod method, final Map<SentenceToken, String> data) {
		try {
			data.put(SentenceToken.MODIFIERS, Flags.toString(method.getFlags()));
		} catch (final JavaModelException e2) {
			e2.printStackTrace();
		}
		data.put(SentenceToken.NAME, method.getElementName());
		try {
			final String temp = Signature.getSignatureSimpleName(method.getReturnType());
			// check if this type is a generic
			if (temp.contains("<")) {
				// temp = TokenReplacer.replaceGenerics(temp);
			}
			data.put(SentenceToken.RETURNTYPE, temp);
		} catch (final JavaModelException e1) {
			e1.printStackTrace();
		}
		try {
			if (method.getParameters().length != 0) {
				final MessageBuilder mb = new MessageBuilder();
				for (int i = 0; i < method.getParameters().length; i++) {
					mb.add(method.getParameterNames()[i]);
				}
				data.put(SentenceToken.PARAMETERS, mb.toString());
			}
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * set all elements of the specific type, for example search all folders, so
	 * this method set in a List all folders and set the first element with
	 * parent.
	 *
	 * @param down
	 *            specific if you want up or down in the package explorer
	 */
	private void setElements(final boolean down) {
		final int value = parent.getElementType();
		if (!down) {
			parent = parent.getParent();
		}
		switch (value) {
		case IJavaElement.JAVA_PROJECT:
			if (down) {
				getFolders(parent);
			}
			break;
		case IJavaElement.PACKAGE_FRAGMENT_ROOT:
			if (down) {
				getPackages(parent);
			} else {
				getJavaProjects();
			}
			break;
		case IJavaElement.PACKAGE_FRAGMENT:
			if (down) {
				getCompilationUnit();
				if (nrChildren > 0) {
					getClasstype(parent);
				}
			} else {
				getFolders(parent);
			}
			break;
		case IJavaElement.TYPE:
			if (down) {
				getMethods(parent);
			} else {
				getPackages(parent.getParent().getParent());
			}
			break;
		case IJavaElement.METHOD:
			if (down) {
				statement = true;
			} else {
				if (!statement) {
					parent = parent.getParent().getParent();
					getCompilationUnit();
					if (nrChildren > 0) {
						getClasstype(parent.getParent());
					}
				}
				statement = false;
			}
			break;
		case IJavaElement.FIELD:
			if (!down) {
				if (!statement) {
					parent = parent.getParent().getParent();
					getCompilationUnit();
					if (nrChildren > 0) {
						getClasstype(parent.getParent());
					}
				}
				statement = false;
			}
			break;
		}
		if (allParents.size() > 0) {
			parent = allParents.get(counter);
		}

	}

	@Override
	public void keyReleased(final KeyEvent e) {
	}

}
