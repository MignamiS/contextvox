package org.contextvox.contextualizer.codeInspector.views;


import java.util.List;

import org.contextvox.contextualizer.commandLine.Context;
import org.contextvox.contextualizer.commandLine.interpreter.reader.GenericStatementReader;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

/**
 * This class Parse every method of the astnode
 *
 */
public class MethodParser {
	private String statement="";
	private boolean stop=false;
	private Statement s;
	private List<?> statements;

	enum StateStatement {EXPANDED, COLLAPSED, STAY}

	public MethodParser(String method) {
		getStartNode(method);
	}
	
	public boolean getStop(){
		return stop;
	}
	

	
	/**
	 * get the content of the statement
	 * @param selectNode the statement selected
	 */
	private void expandStatement(int selectNode){
		switch(s.getNodeType()){
		case ASTNode.IF_STATEMENT:
			s=((IfStatement)s).getThenStatement();
			break;
		case ASTNode.DO_STATEMENT:
			s=((DoStatement)s).getBody();
			break;
		case ASTNode.WHILE_STATEMENT:
			s=((WhileStatement)s).getBody();
			break;
		case ASTNode.FOR_STATEMENT:
			s=((ForStatement)s).getBody();
			break;
		case ASTNode.TRY_STATEMENT:
			s=((TryStatement)s).getBody();
			break;
		case ASTNode.SWITCH_STATEMENT:
			statements=((SwitchStatement)s).statements();
			s=(Statement) statements.get(0);
			break;
		case ASTNode.SYNCHRONIZED_STATEMENT:
			s=((SynchronizedStatement)s).getBody();
			break;
		case ASTNode.BLOCK:
			statements=((Block)s).statements();
			if(statements.size()>0 &&selectNode<=statements.size()-1){
				s=(Statement) statements.get(0);
			}else{
				s=(Statement) s.getParent();
				statement=null;
				stop=true;
			}
			break;
		default:
			s=(Statement) s.getParent();
			statement=null;
			stop=true;
		}
	}
	
	private void getStartNode(String methodName){
		final ASTNode node = Context.getInstance().getCurrentNode();
		node.accept(new ASTVisitor() {			
			public boolean visit(MethodDeclaration node) {
				if(node.getName().getIdentifier().equals(methodName)){
					Block block = node.getBody();
					if(block!=null){
						statements = block.statements();
					}else{
						statements=null;
					}
					return true;
				}
				return false;
			}
		});
	}
	
	/**
	 * parse the ASTNode and return the element specific by selectNode
	 * @param selectNode the statement selected
	 * @return the message of the statement
	 */
	public String returnAllStatement(int selectNode, StateStatement stateStatement){
		stop=false;
		if(statements ==null){
			//statement not expandable 
			statement=Messages.getSingleMessage("notexpanded");
			stop=true;
			return statement;
		}
		if(selectNode>statements.size()-1){
			if(statements.size()==0){
				//get the content of the current statement
				statements=((Block)s.getParent()).statements();
				stateStatement=StateStatement.STAY;
				statement="";
			}else{
				//statement not expandable 
				statement=Messages.getSingleMessage("notexpanded");
				stop=true;
				return statement;
			}
		}
		//expanded or collapsed the select statement if you want
		if(stateStatement==StateStatement.EXPANDED){
			expandStatement(selectNode);
		}else if(stateStatement==StateStatement.COLLAPSED){
			if(s.getParent().getParent()!=null){
				//level block
				if(s.getParent().getParent().getParent().getNodeType()==ASTNode.BLOCK){
					statements=((Block)s.getParent().getParent().getParent()).statements();
				}
				//case have a variable
				if(s.getParent().getParent().getNodeType()==ASTNode.METHOD_DECLARATION){
					s=(Statement)s.getParent();
				}else{
					s=(Statement)s.getParent().getParent();
				}
				statement="";
			}
		}else{
			//get the statement selected 
			s = (Statement)statements.get(selectNode);
		}
		//if the statement is a block-->get the content
		if(s.getNodeType()==ASTNode.BLOCK){
			expandStatement(selectNode);
		}
		
		if(statement==null){
			return Messages.getSingleMessage("notexpanded");
		}
		//get the message
		statement=new GenericStatementReader(s, false).read();
		return statement;
	}
	
}
