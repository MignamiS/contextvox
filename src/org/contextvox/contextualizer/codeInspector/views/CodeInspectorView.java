package org.contextvox.contextualizer.codeInspector.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;


/**
 * 
 * This class is used for the code inspector view
 *
 */
public class CodeInspectorView extends ViewPart{

	/**
	 * TextArea in which there are written the commands and the answers
	 */
	private StyledText commandArea;
	
	
	/**
	 * Field in which the user use the arrow
	 */
	private Text commandLine;
	
	public CodeInspectorView() {
		super();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		final GridLayout gridLay = new GridLayout();
		final GridData horizontalFill = new GridData(SWT.FILL, SWT.FILL, true,false);
		final GridData fill = new GridData(SWT.FILL, SWT.FILL, true, true);
		parent.setLayout(gridLay);

		commandArea = new StyledText(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.BORDER | SWT.READ_ONLY | SWT.WRAP);
		commandArea.setText("Vox, ready!");
		commandArea.setLayoutData(fill);

		commandLine = new Text(parent, SWT.SINGLE | SWT.BORDER);
		commandLine.setEditable(false);
		commandLine.setText("");
		commandLine.setLayoutData(horizontalFill);
		
		//add event of the code inspector
		commandLine.addKeyListener(new CodeInspectorListener());
//		commandLine.addKeyListener(new MyCodeInspectorListener());
	}

	
	/**
	 * Adds the text on the command area
	 *
	 * @param text
	 *            the text to add
	 */
	public void addTextOnCommandArea(final String text) {
		commandArea.setText(commandArea.getText()
				+ System.getProperty("line.separator") + text);
		scrollCommandAreaBottom();
	}
	
	/**
	 * Scroll the command area to the bottom
	 */
	private void scrollCommandAreaBottom() {
		commandArea.setTopIndex(commandArea.getLineCount() - 1);
	}
	
	@Override
	public void setFocus() {
		commandLine.setFocus();		
	}

}
