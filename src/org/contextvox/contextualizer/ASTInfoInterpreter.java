package org.contextvox.contextualizer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contextvox.contextualizer.commandLine.MessageBuilder;
import org.contextvox.services.replacer.SentenceToken;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.WhileStatement;

/**
 * Helper that extracts informations from an ASTNode and provides a Collection
 * with a summary, that can be passed to the MessageMaker.<br/>
 * <b>The class is thread-safe</b>
 */
public class ASTInfoInterpreter {

	/**
	 * Get info about the given enhanced for-statement (foreach)
	 *
	 * @param startNode
	 * @return Map<SentenceToken, String>
	 */
	public static Map<SentenceToken, String> getInfoEnhancedForLoop(final EnhancedForStatement startNode) {
		final Map<SentenceToken, String> map = new HashMap<>();
		map.put(SentenceToken.COLLECTION, startNode.getExpression().toString());
		final SingleVariableDeclaration param = startNode.getParameter();
		map.put(SentenceToken.TYPE, param.getType().toString());
		map.put(SentenceToken.ITERATOR, param.getName().toString());

		return map;
	}

	/**
	 * Get all informations from the for-statement node and make a summary. Note
	 * that only the following for-loop are considered:
	 * <ul>
	 * <li>One variable as iterator</li>
	 * <li>Infinite for-loop (for (;;))</li>
	 * </ul>
	 *
	 * @param node
	 * @return map data readable from the MessageMaker, or an empty map if the
	 *         statement isn't a supported one (for example with many iterators)
	 */
	public static Map<SentenceToken, String> getInfoForLoop(final ForStatement node) {
		final Map<SentenceToken, String> map = new HashMap<>();
		VariableDeclarationFragment fragment = null;

		// iterator
		final List<?> initializers = node.initializers();
		final int iNumber = initializers.size();
		if (iNumber == 0) {
			// infinite loop
			map.put(SentenceToken.BLANK, "");
			return map;
		} else if (iNumber == 1) {
			// standard for loop
			final List<?> vars = ((VariableDeclarationExpression) initializers.get(0)).fragments();
			fragment = (VariableDeclarationFragment) vars.get(0);
			final String iterator = fragment.getName().toString();
			map.put(SentenceToken.ITERATOR, iterator);
		} else {
			// more than an initializer, return
			return map;
		}

		// start value
		if (fragment != null) {
			final String init = fragment.getInitializer().toString();
			map.put(SentenceToken.START, init);
		}

		// end value
		final String end = node.getExpression().toString().split("[<>]")[1];
		map.put(SentenceToken.END, end);

		// condition
		final Expression cond = node.getExpression();
		if (cond != null)
			map.put(SentenceToken.CONDITION, cond.toString());

		// updater
		final List<?> updaters = node.updaters();
		// check out for increment
		final String update = updaters.get(0).toString();
		map.put(SentenceToken.UPDATE, update);

		return map;
	}

	/**
	 * Get info from the while loop and provides a summary.
	 *
	 * @param startNode
	 * @return Map readable from the MessageMaker
	 */
	public static Map<SentenceToken, String> getInfo(final WhileStatement startNode) {
		final Map<SentenceToken, String> map = new HashMap<>();
		// get condition
		final Expression cond = startNode.getExpression();
		map.put(SentenceToken.CONDITION, cond.toString());

		return map;
	}

	/**
	 * Provides summary about do-while statement
	 *
	 * @param startNode
	 * @return Map<SentenceToken, String>
	 */
	public static Map<SentenceToken, String> getInfo(final DoStatement startNode) {
		final Map<SentenceToken, String> map = new HashMap<>();
		// get condition
		final Expression cond = startNode.getExpression();
		map.put(SentenceToken.CONDITION, cond.toString());

		return map;
	}

	/**
	 * Get info from the switch-statement node and produces a summary.
	 *
	 * @param node
	 * @return Map readable from a MessageMaker
	 */
	public static Map<SentenceToken, String> getInfo(final SwitchStatement node) {
		final Map<SentenceToken, String> map = new HashMap<>();
		// get condition
		final Expression cond = node.getExpression();
		map.put(SentenceToken.SELECTOR, cond.toString());

		// get case list
		@SuppressWarnings("unchecked")
		final List<Statement> statements = node.statements();
		final MessageBuilder mb = new MessageBuilder();
		final String caseSeparator = " ";
		for (final Statement s : statements) {
			if (s instanceof SwitchCase) {
				// choose between CASE and DEFAULT
				if (((SwitchCase) s).isDefault()) {
					mb.add("default");
					mb.add(caseSeparator);
				} else {
					// remove useless words
					String cs = s.toString();
					cs = cs.replace("case ", "").replace(":", "");
					mb.add(cs);
					mb.add(caseSeparator);
				}
			}
		}
		final String r = mb.toString();
		if (!r.isEmpty())
			map.put(SentenceToken.LIST, r);

		return map;
	}

	/**
	 * Get all informations from the ifStatement-node and return a summary.
	 *
	 * @param node
	 * @return Map that can be read from the MessageMaker
	 */
	public static Map<SentenceToken, String> getInfo(final IfStatement node) {
		final Map<SentenceToken, String> map = new HashMap<>();
		// get condition
		map.put(SentenceToken.CONDITION, node.getExpression().toString());
		// get statements
		// final Statement then = node.getThenStatement();
		// if (!(then instanceof Block)) {
		// // single statement, read it
		// map.put(SentenceToken.STATEMENT, then.toString());
		// }

		// else
		final Statement elseStmt = node.getElseStatement();
		if (elseStmt != null) {
			// if (!(elseStmt instanceof Block)) {
			// // simple else statement, read it
			// map.put(SentenceToken.ELSE_STATEMENT, elseStmt.toString());
			// }
			map.put(SentenceToken.ELSE_STATEMENT, "");
		}

		return map;
	}

	public static Map<SentenceToken, String> getInfo(final TryStatement startNode) {
		final Map<SentenceToken, String> map = new HashMap<>();

		// get catch clauses
		String clauses = "";
		final List<?> cc = startNode.catchClauses();
		for (final Object item : cc) {
			if (item instanceof CatchClause) {
				// get exception
				clauses = clauses.concat(((CatchClause) item).getException().getType().toString() + " ");
			}
		}
		map.put(SentenceToken.EXCEPTION, clauses);

		// check "finally" block
		if (startNode.getFinally() != null) {
			// "finally" found
			map.put(SentenceToken.FINALLY, " ");
		}

		return map;
	}

	/**
	 * Get summary for the given catch clause
	 *
	 * @param startNode
	 * @return Map<SentenceToken, String>
	 */
	public static Map<SentenceToken, String> getInfo(final CatchClause startNode) {
		final Map<SentenceToken, String> map = new HashMap<>();
		map.put(SentenceToken.EXCEPTION, startNode.getException().getType().toString());
		return map;
	}

	public static Map<SentenceToken, String> getInfo(final Comment startNode) {
		// TODO implement
		final Map<SentenceToken, String> map = null;
		return map;
	}

	public static Map<SentenceToken, String> getInfo(final MethodDeclaration node) {
		final Map<SentenceToken, String> data = new HashMap<>();
		data.put(SentenceToken.NAME, node.getName().toString());
		if (!node.isConstructor())
			data.put(SentenceToken.RETURNTYPE, node.getReturnType2().toString());
		// parameters
		final List<?> params = node.parameters();
		if (params.size() > 0) {
			// process parameters
			final StringBuilder sb = new StringBuilder();
			for (int i = 0; i < params.size(); i++) {
				if (params.get(i) instanceof SingleVariableDeclaration) {
					final SingleVariableDeclaration vd = (SingleVariableDeclaration) params.get(i);
					final String type = vd.getType().toString();
					final String name = vd.getName().toString();
					sb.append(String.format("%s %s ", type, name));
				}
			}
			data.put(SentenceToken.PARAMETERS, sb.toString());
		}

		// modifiers
		final StringBuilder sb = new StringBuilder();
		final List<?> mod = node.modifiers();
		for (int i = 0; i < mod.size(); i++) {
			if (mod.get(i) instanceof IExtendedModifier) {
				final IExtendedModifier modifier = (IExtendedModifier) mod.get(i);
				if (modifier.isModifier())
					sb.append(modifier.toString() + " ");
			}
		}
		if (sb.length() > 0) {
			// register modifiers
			data.put(SentenceToken.MODIFIERS, sb.toString());
		}

		return data;
	}
}
