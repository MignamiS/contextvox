package org.contextvox.contextualizer;

import java.util.List;

import org.contextvox.plugin.PluginElements;
import org.contextvox.utils.LogLevel;
import org.contextvox.utils.VoxLogger;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * This class contains helper methods to retrieve AST from the workspace, for
 * example the current ASTNode under the editor's cursor position.
 */
public class ASTProvider {

	// statement jump command
	// TODO refactor
	public static final int NEXT = 1;
	public static final int PREVIOUS = 2;
	public static final int FIRST = 3;
	public static final int LAST = 4;
	public static final int IN = 5;
	public static final int OUT = 6;

	/**
	 * Helper method that return the ASTNode corresponding to the given
	 * lineOffset.
	 * <p>
	 * To get the ASTNode under the current cursor's line position use the
	 * {@link #getNodeOnCursorPosition(ITextEditor)}
	 * </p>
	 *
	 *
	 * @param editor
	 *            the editor that contains the code
	 * @param compilationUnit
	 *            the unit to process to extract the node
	 * @param codeLine
	 *            the line of code that contains the searched node
	 * @param lineOffset
	 *            the offset of the starting char of the line
	 * @return the corresponding enclosing ASTNode, or null if there isn't one.
	 */
	public static ASTNode getASTNodeAt(final ITextEditor editor, final ICompilationUnit compilationUnit,
			final String codeLine, final int lineOffset) {
		// get the root (AST) of the compilation unit
		final ASTNode root = getASTFromCompilationUnit(editor, compilationUnit, false);

		if (root != null) {
			// get the exact position of the start line (without spaces)
			final String strTmp = codeLine.replaceFirst("^\\s*", "");
			final int cnt = codeLine.length() - strTmp.length();
			final int lineStart = lineOffset + cnt;
			final int lineLength = strTmp.length();

			final ASTNode node2 = NodeFinder.perform(root, lineStart, lineLength - (cnt + 2));
			ASTNode tmp = node2;
			ASTNode finalNode = node2;

			// highest node in hierarchy
			while (tmp.getStartPosition() >= lineStart) {
				finalNode = tmp;
				tmp = tmp.getParent();
				if (tmp == null)
					break;
			}
			return finalNode;
		} else {
			return null;
		}

	}

	/**
	 * Helper method that return an AST from the given Compilation Unit.
	 *
	 * @param editor
	 *            the editor that contains the compilation unit. If
	 *            <code>null</code> the active editor will be taken
	 * @param compilationUnit
	 *            the source
	 * @param resolveBindings
	 *            specify if the bindings should be processed (use
	 *            <code>false</code> to obtain better performances).
	 * @return ASTNode corresponding to the root of the compilation unit
	 */
	public static ASTNode getASTFromCompilationUnit(ITextEditor editor, final ICompilationUnit compilationUnit,
			final boolean resolveBindings) {
		if (editor == null)
			editor = (ITextEditor) PluginElements.getActiveEditor();

		// return ContextState.parseCompilationUnit(editor, compilationUnit);
		// FIXME the holder contains the root of the parset ASTNode
		return null;

		// final ASTParser parser = ASTParser.newParser(AST.JLS8);
		// parser.setSource(compilationUnit);
		// parser.setKind(ASTParser.K_COMPILATION_UNIT);
		// parser.setResolveBindings(resolveBindings);
		// final ASTNode rootNode = parser.createAST(null);
		// final CompilationUnit compilationUnitNode = (CompilationUnit)
		// rootNode;
		// final ASTNode node = NodeFinder.perform(compilationUnitNode,
		// rootNode.getStartPosition(), rootNode.getLength());
		//
		// return node;
	}

	/**
	 * Helper method that return the smallest Java element enclosed on the given
	 * TypeRoot.
	 *
	 * @param ITypeRoot
	 *            the root of the hierarchy to process
	 * @param lineOffset
	 *            start of the element in code
	 * @return corresponding Java element (IField, IMethod, etc). Return null if
	 *         the element correspond to the Compilation Unit.
	 */
	public static IJavaElement getCurrentJavaElement(final ITypeRoot root, final int lineOffset) {
		if (root instanceof ICompilationUnit) {
			final ICompilationUnit unit = (ICompilationUnit) root;
			IJavaElement selected = null;
			try {
				selected = unit.getElementAt(lineOffset);
			} catch (final JavaModelException e) {
				VoxLogger.log(LogLevel.ERROR,
						"ReadEditorJob exploded trying to get the corresponding element of the editor's offset", e);
			}

			// if (selected instanceof IType || selected instanceof IMethod)
			return selected;
		}
		return null;
	}

	/**
	 * Help method that return the Java Element from the given Editor.
	 *
	 * @param Editor
	 * @return the enclosed java element. It may return null if there isn't a
	 *         java element inside the editor passed.
	 */
	public static ITypeRoot getJavaInput(final IEditorPart part) {
		final IEditorInput editorInput = part.getEditorInput();
		if (editorInput != null) {
			final IJavaElement input = JavaUI.getEditorInputJavaElement(editorInput);
			if (input instanceof ITypeRoot)
				return (ITypeRoot) input;
		}
		return null;
	}

	/**
	 * Helper method that return the IDocument processed by the given editor.
	 *
	 * @param text
	 *            editor
	 * @return IDocument
	 */
	public static IDocument getDocument(final ITextEditor textEditor) {
		final IDocumentProvider provider = textEditor.getDocumentProvider();
		final IEditorInput editorInput = textEditor.getEditorInput();
		return provider.getDocument(editorInput);
	}

	/**
	 * Retrieve the ASTNode under the current cursor position, using the line
	 * and not the simply offset.
	 *
	 * @param part
	 *            the editor part
	 * @return the ASTNode corresponding to the line of code
	 */
	public static ASTNode getNodeOnCursorPosition(final ITextEditor part) {
		final IDocument document = ASTProvider.getDocument(part);
		final ISelection selection = part.getSelectionProvider().getSelection();
		final int textOffset = ((ITextSelection) selection).getOffset();
		String codeLine = null;
		int lineOffset = 0;
		int lineLen = 0;
		try {
			final int line = document.getLineOfOffset(textOffset);
			lineOffset = document.getLineOffset(line);
			lineLen = document.getLineLength(line);
			codeLine = document.get(lineOffset, lineLen);
		} catch (final BadLocationException e) {
			e.printStackTrace();
		}

		final ICompilationUnit compilationUnit = (ICompilationUnit) getJavaInput(part);
		final ASTNode currentNode = getASTNodeAt(part, compilationUnit, codeLine, lineOffset);
		return currentNode;
	}

	/**
	 * Returns the jump target statement based on the specified selector.
	 *
	 * @param node
	 *            the current node, the starting point
	 * @param selector
	 *            the ASTProvider constant representing the operation: for
	 *            example NEXT means the next statement after the given node
	 * @return the next node, or <code>null</code> if there are not next one
	 */
	public static Statement getTargetStatement(final Statement node, final int selector) {
		if (node == null)
			return node;

		if (selector == NEXT || selector == PREVIOUS || selector == LAST || selector == FIRST)
			return sameLevelTarget(node, selector);
		else if (selector == IN || selector == OUT)
			return changeLevelTarget(node, selector);
		else
			return null;
	}

	/**
	 * Returns the target statement that is in a different level from the given
	 * one, for example inside a block.
	 *
	 * @param node
	 *            the current statement
	 * @param selector
	 *            the constant representing which statement should be taken
	 * @return the target statement, or <code>null</code> if noone is found
	 */
	private static Statement changeLevelTarget(final Statement node, final int selector) {
		if (selector == ASTProvider.IN)
			return getInnerStatement(node);
		else
			return getOuterStatement(node);
	}

	// get statement outside the containing block
	private static Statement getOuterStatement(final Statement node) {
		final Block parentBlock = getParentBlock(node);
		if (parentBlock == null)
			return null;

		// get block "owner"
		final ASTNode parent = parentBlock.getParent();
		if (parent instanceof Statement)
			return (Statement) parent;
		else
			return null;
	}

	// get the first statement inside the block of the given statement
	private static Statement getInnerStatement(final Statement node) {
		// get the body (block)
		final BlockVisitor bs = new BlockVisitor();
		node.accept(bs);
		final Block block = bs.getBlock();
		if (block == null)
			return null;

		// get first element inside
		final List<?> statements = block.statements();
		Statement stmt = null;
		if (statements.size() > 0) {
			stmt = (Statement) statements.get(0);
		}

		return stmt;
	}

	// provides target node that is on the same hierarchical level on the block
	private static Statement sameLevelTarget(final Statement node, final int selector) {
		final Block parentBlock = getParentBlock(node);
		if (parentBlock == null)
			return null;

		// search the current node
		final List<?> statements = parentBlock.statements();
		final int index = getIndexOfStatement(node, statements);
		if (index == -1)
			return null;

		// look for the next node
		switch (selector) {
		case NEXT:
			return getNext(statements, index);
		case PREVIOUS:
			return getPrevious(statements, index);
		case FIRST:
			return getFirst(statements);
		case LAST:
			return getLast(statements);
		default:
			return null;
		}
	}

	// get last statement of the current block
	private static Statement getLast(final List<?> statements) {
		if (statements.size() > 0)
			return (Statement) statements.get(statements.size() - 1);
		else
			return null;
	}

	// get first statement of the current block
	private static Statement getFirst(final List<?> statements) {
		if (statements.size() > 0)
			return (Statement) statements.get(0);
		else
			return null;
	}

	// get the statement before the given one
	private static Statement getPrevious(final List<?> statements, final int index) {
		if (index > 0 && statements.size() > 0)
			return (Statement) statements.get(index - 1);
		else
			return null;
	}

	// get the statement after the given one
	private static Statement getNext(final List<?> statements, final int index) {
		if (index != statements.size() - 1)
			return (Statement) statements.get(index + 1);
		else
			return null;
	}

	/*
	 * Get the index of the given statement inside the containing block, if
	 * present.
	 */
	private static int getIndexOfStatement(final Statement node, final List<?> statements) {
		int index = -1;
		for (int i = 0; i < statements.size(); i++) {
			final Statement stmt = (Statement) statements.get(i);
			if (stmt.equals(node)) {
				// found the node, save index
				index = i;
				break;
			}
		}
		return index;
	}

	/**
	 * Return the parent block that contains the given statement.
	 *
	 * @param node
	 *            the contained node
	 * @return the parent block or <code>null</code> if there no containing
	 *         block node
	 */
	private static Block getParentBlock(final Statement node) {
		ASTNode tmp = node.getParent();
		do {
			if (tmp != null && tmp instanceof Block)
				return (Block) tmp;
			tmp = tmp.getParent();
		} while (tmp != null);

		return null;
	}
}

/**
 * Class created only to get a block from the AST.
 */
class BlockVisitor extends ASTVisitor {
	private Block block;

	public Block getBlock() {
		return block;
	}

	@Override
	public boolean visit(final Block node) {
		block = node;
		return false;
	}
}
