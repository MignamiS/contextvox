package org.contextvox.utils;

import java.util.Calendar;
import java.util.logging.Logger;

import org.contextvox.Activator;
import org.contextvox.IDEobserver.views.VoxConsoleView;
import org.contextvox.plugin.requests.VoxRequest;
import org.contextvox.preferences.PreferenceConstants;
import org.contextvox.preferences.PreferenceHelper;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.progress.UIJob;

/**
 * This class provides a Log Manager for the messages of ContextVox.
 */
public class VoxLogger {
	// TODO remove log methods, use the java.util.logger

	private static final String WARNING_PROMPT = "[WARNING] ";
	private static final String DEBUG_PROMPT = "[DEBUG] ";

	public static void log(final Logger logger, final VoxRequest request) {
		// TODO implement
	}

	/**
	 * This method should be used for logging string feedback instead a simple
	 * log.
	 *
	 * @param logger
	 *            the logger of the class that produced the message
	 * @param msg
	 *            the message to log
	 */
	public static void log(final Logger logger, final String msg) {
		// TODO preference->deviate log on VoxConsole, in this case disable the
		// printonvoxconsole() method.
		printOnVoxConsole(msg, false);
		logger.info(msg);
	}

	/**
	 * Log an error or warning, will be printed directly into the .log file. Can
	 * also be used as entry point for logging INFO messages, that are printed
	 * into the Vox Console (if opened.
	 *
	 * @param severity
	 *            LogLevel
	 * @param message
	 *            the message that will be printed
	 * @param exception
	 *            that print the stack-trace, can be null
	 */
	@Deprecated
	public static void log(final LogLevel severity, final String message, final Throwable exception) {
		switch (severity) {
		case ERROR:
			if (PreferenceHelper.isDebugMode())
				VoxLogger.printOnConsole(message, exception);
			VoxLogger.printOnFile(message, exception);
			VoxLogger.printOnVoxConsole(message, false);
			break;
		case INFO:
			printOnVoxConsole(message, false);
			break;
		case DEBUG:
			if (PreferenceHelper.isDebugMode())
				printOnVoxConsole(message, true);
			break;
		case WARNING:
			VoxLogger.printOnVoxConsole(WARNING_PROMPT + message, false);
			// VoxLogger.printOnConsole(WARNING_PROMPT + message, null);
			break;
		}
	}

	/**
	 * Shorter version of the log() method, the parameter Exception is set to
	 * NULL
	 *
	 * @param severity
	 * @param message
	 */
	@Deprecated
	public static void log(final LogLevel severity, final String message) {
		log(severity, message, null);
	}

	private static void printOnVoxConsole(final String message, final boolean debug) {
		new UIJob("write on vox console") {

			@Override
			public IStatus runInUIThread(final IProgressMonitor arg0) {
				// formatting hour
				String now = "";
				if (Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.PROMPT_TIMESTAMP_KEY))
					now = getTimestamp();

				String text = now + " " + message + System.getProperty("line.separator");
				if (debug)
					text = DEBUG_PROMPT.concat(text);
				VoxConsoleView.writeOnConsole(text);

				return Status.OK_STATUS;
			}

			// return the timestamp ready to be prompted
			private String getTimestamp() {
				final Calendar time = Calendar.getInstance();
				final int h = time.get(Calendar.HOUR_OF_DAY);
				final int m = time.get(Calendar.MINUTE);
				final int s = time.get(Calendar.SECOND);
				final String now = String.format("<%02d:%02d:%02d>", h, m, s);
				return now;
			}
		}.schedule();
	}

	private static void printOnFile(final String message, final Throwable exception) {
		// 2 status is ERROR
		Activator.getDefault().getLog().log(new Status(2, Activator.PLUGIN_ID, message, exception));
	}

	private static void printOnConsole(final String message, final Throwable exception) {
		if (exception == null) {
			// prompt normally
			// System.out.println(message);
		} else {
			// print the error
			System.err.println(message);
			exception.printStackTrace();
		}
	}

}
