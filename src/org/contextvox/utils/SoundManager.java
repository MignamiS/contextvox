package org.contextvox.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.contextvox.utils.SoundManager.SoundType;

public class SoundManager {
	/** Local folder that contains the sound pack */
	private static final String BASEPATH = "mp3/sounds/";

	private static final Map<SoundType, Sound> soundCache = new HashMap<>();

	public static void playSound(final SoundType type) {
		if (soundCache.containsKey(type)) {
			soundCache.get(type).play();
		} else {
			final Sound sound = new Sound(type.getPath(), type);
			soundCache.put(type, sound);
			sound.play();
		}
	}

	public enum SoundType {
		PROBLEM {
			@Override
			public String getPath() {
				return BASEPATH + "warning.wav";
			}
		},
		BREAKPOINT {
			@Override
			public String getPath() {
				return BASEPATH + "breakpoint.wav";
			}
		},
		EXIT_BLOCK_INDENTATION {
			@Override
			public String getPath() {
				return BASEPATH + "decrease_indentation.wav";
			}
		},
		ENTER_BLOCK_INDENTATION {
			@Override
			public String getPath() {
				return BASEPATH + "increase_indentation.wav";
			}
		};

		public abstract String getPath();
	}

}

class Sound {
	private Clip clip;
	private final SoundType type;

	public Sound(String fileName, SoundType type) {
		try {
			// URL url = this.getClass().getClassLoader().getResource(fileName);
			// File file = new File(url.getPath());
			// File file = new File(fileName);
			final InputStream s = new BufferedInputStream(this.getClass().getResourceAsStream("/" + fileName));
			// if (file.exists()) {
			final AudioInputStream sound = AudioSystem.getAudioInputStream(s);
			// load the sound into memory (a Clip)
			clip = AudioSystem.getClip();
			clip.open(sound);
			// } else {
			// throw new RuntimeException("Sound: file not found: " + fileName);
			// }
		} catch (final MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException("Sound: Malformed URL: " + e);
		} catch (final UnsupportedAudioFileException e) {
			e.printStackTrace();
			throw new RuntimeException("Sound: Unsupported Audio File: " + e);
		} catch (final IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Sound: Input/Output Error: " + e);
		} catch (final LineUnavailableException e) {
			e.printStackTrace();
			throw new RuntimeException("Sound: Line Unavailable Exception Error: " + e);
		}

		this.type = type;

	}

	public void play() {
		clip.setFramePosition(0); // Must always rewind!
		clip.start();
	}

	public void stop() {
		clip.stop();
	}

	public SoundType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		return this.type.hashCode();
	}

}