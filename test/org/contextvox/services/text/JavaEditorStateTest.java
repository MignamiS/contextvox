package org.contextvox.services.text;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JavaEditorStateTest {

	private static final String LINE1_TEST = "public static void test() {";
	private static final String LINE2_TEST = "int a = 0;";

	@Test
	public void testCursorMovingHor() {
		final JavaEditorState state = new JavaEditorState();

		// first event (setup)
		final MotionEvent e = new MotionEvent();
		e.setCurrentLine(1);
		e.setLine(LINE1_TEST);
		e.setLineLenght(LINE1_TEST.length());
		e.setLineOffset(0);
		e.setTextOffset(0);
		state.set(e);

		// move caret right of 1
		e.setTextOffset(1);
		state.set(e);
		assertTrue(state.cursorMovingHor());

		// move caret left of 1
		e.setTextOffset(0);
		state.set(e);
		assertTrue(state.cursorMovingHor());
		assertFalse(state.cursorMovingVer());
	}

	@Test
	public void testCursorMovingVer() {
		final JavaEditorState state = new JavaEditorState();

		final MotionEvent e = new MotionEvent();
		e.setCurrentLine(1);
		e.setLine(LINE1_TEST);
		e.setLineLenght(LINE1_TEST.length());
		e.setLineOffset(0);
		e.setTextOffset(0);
		state.set(e);

		// move caret down
		e.setCurrentLine(2);
		e.setLine(LINE2_TEST);
		e.setLineLenght(LINE2_TEST.length());
		e.setLineOffset(LINE1_TEST.length() + 1);
		e.setTextOffset(LINE1_TEST.length() + 1);
		state.set(e);
		assertTrue(state.cursorMovingVer());
	}

	@Test
	public void testLastTouchedWord() {
		final JavaEditorState state = new JavaEditorState();

		final MotionEvent e = new MotionEvent();
		e.setCurrentLine(1);
		e.setLine(LINE1_TEST);
		e.setLineLenght(LINE1_TEST.length());
		e.setLineOffset(0);
		e.setTextOffset(0);
		state.set(e);

		// move caret right of 1 char
		e.setTextOffset(1);
		state.set(e);
		String word = state.lastTouchedWord();
		String exp = LINE1_TEST.substring(0, 1);
		assertEquals(exp, word);

		// move caret of 5 characters
		e.setTextOffset(6);
		state.set(e);
		word = state.lastTouchedWord();
		exp = LINE1_TEST.substring(1, 6);
		assertEquals(exp, word);

		// backward motion
		e.setTextOffset(6);
		state.set(e);
		e.setTextOffset(0);
		state.set(e);
		word = state.lastTouchedWord();
		exp = "public";
		assertEquals(exp, word);
	}

	@Test
	public void testIsCharWordMoving() {
		final JavaEditorState state = new JavaEditorState();

		final MotionEvent e = new MotionEvent();
		e.setCurrentLine(1);
		e.setLine(LINE1_TEST);
		e.setLineLenght(LINE1_TEST.length());
		e.setLineOffset(0);
		e.setTextOffset(0);
		state.set(e);

		// forward
		e.setTextOffset(1);
		state.set(e);
		assertTrue(state.isCharMoving());
		assertFalse(state.isWordMoving());

		// backward
		e.setTextOffset(0);
		state.set(e);
		assertTrue(state.isCharMoving());
		assertFalse(state.isWordMoving());

		// jump
		e.setTextOffset(2);
		state.set(e);
		assertFalse(state.isCharMoving());
		assertTrue(state.isWordMoving());
	}

	@Test
	public void testGetLine() {
		final JavaEditorState state = new JavaEditorState();

		final MotionEvent e = new MotionEvent();
		e.setCurrentLine(1);
		e.setLine(LINE1_TEST);
		e.setLineLenght(LINE1_TEST.length());
		e.setLineOffset(0);
		e.setTextOffset(0);
		state.set(e);

		assertEquals(LINE1_TEST, state.getLine());
	}

	@Test
	public void testIndentationLevel() {
		final JavaEditorState state = new JavaEditorState();

		final String line = "\t\tasd\t";
		final MotionEvent e = new MotionEvent();
		e.setCurrentLine(1);
		e.setLine(line);
		e.setLineLenght(line.length());
		e.setLineOffset(0);
		e.setTextOffset(0);
		state.set(e);
		assertEquals(2, state.getIndentationLevel());
	}
}
