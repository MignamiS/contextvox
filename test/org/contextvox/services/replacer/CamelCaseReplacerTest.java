package org.contextvox.services.replacer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CamelCaseReplacerTest {

	@Test
	public void testReplace() {
		final CamelCaseReplacer ccr = new CamelCaseReplacer();

		// single upper case
		final RawMessage data = new RawMessage("String of");
		ccr.replace(data);
		assertEquals(" string of", data.getText());

		// double UC
		data.setText("TextEditor");
		ccr.replace(data);
		assertEquals(" text editor", data.getText());

		// many UC near
		data.setText("TTSEngine");
		ccr.replace(data);
		assertEquals(" t t s engine", data.getText());

		// double UC + prefix lower case
		data.setText("thisIsSparta!");
		ccr.replace(data);
		assertEquals("this is sparta!", data.getText());

		// no UC
		final String noUC = "this is a text";
		data.setText(noUC);
		ccr.replace(data);
		assertEquals(noUC, data.getText());
	}

}
