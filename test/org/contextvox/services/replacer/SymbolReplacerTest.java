package org.contextvox.services.replacer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.contextvox.services.replacer.RawMessage;
import org.contextvox.services.replacer.Replacer;
import org.contextvox.services.replacer.SymbolReplacer;
import org.contextvox.services.replacer.dictionaries.MessageType;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.MessagesTest;
import org.junit.BeforeClass;
import org.junit.Test;

public class SymbolReplacerTest {

	@BeforeClass
	public static void setupMessages() {
		Messages.load(MessagesTest.MESSAGES_PATH, MessagesTest.SENTENCES_PATH);
	}

	@Test
	public void testConcatReplace() {
		final Replacer r1 = new SymbolReplacer(MessageType.PARENTHESIS, false);
		final Replacer r2 = new SymbolReplacer(MessageType.OPERATOR, false);
		final String str = "i++)";

		final RawMessage msg = new RawMessage(str);
		r1.replace(msg);
		r2.replace(msg);
		final String temp = msg.getText();
		final String result = trimSpaces(temp);
		String exp = "i" + Messages.getSingleMessage("++") + Messages.getSingleMessage(")");
		exp = trimSpaces(exp);
		assertTrue(exp.equals(result));
	}

	@Test
	public void testSingleReplace() {
		String source = "i++";
		String exp = null;
		String result = null;

		// replace
		SymbolReplacer r = new SymbolReplacer(MessageType.OPERATOR, false);
		final RawMessage msg = new RawMessage(source);
		r.replace(msg);
		String temp = msg.getText();
		result = temp.trim();
		exp = ("i " + Messages.getSingleMessage("++")).trim();

		assertTrue(exp.equals(result));

		// remove
		r = new SymbolReplacer(MessageType.OPERATOR, true);
		msg.setText(source);
		r.replace(msg);
		temp = msg.getText();
		result = temp.trim();
		exp = "i";
		assertTrue(exp.equals(result));

		// exception remove
		final String[] exceptions = { "--" };
		r = new SymbolReplacer(MessageType.OPERATOR, false, exceptions, false);
		source = "i++";
		exp = ("i " + Messages.getSingleMessage("++")).trim();
		msg.setText(source);
		r.replace(msg);
		temp = msg.getText();
		result = temp.trim();
		assertEquals(exp, result);

		// exception skipped
		source = "i++.";
		final String[] e = { ">>" };
		r = new SymbolReplacer(MessageType.OPERATOR, true, e, true);
		exp = "i .";
		msg.setText(source);
		r.replace(msg);
		temp = msg.getText();
		result = temp.trim();
		assertTrue(exp.equals(result));

		// System.out.println(String.format("<%s><%s>", exp, result));
	}

	private static String trimSpaces(final String str) {
		return str.replaceAll("\\s+", "");
	}
}
