package org.contextvox.services.replacer;

import static org.junit.Assert.assertTrue;

import org.contextvox.services.replacer.RawMessage;
import org.contextvox.services.replacer.Replacer;
import org.contextvox.services.replacer.SpaceCleanerReplacer;
import org.junit.Test;

public class SpaceCleanerReplacerTest {

	@Test
	public void test() {
		final String src = "\t\tthis is    a   \t\nstring";
		final String exp = "this is a string";
		final Replacer r = new SpaceCleanerReplacer();
		final RawMessage msg = new RawMessage(src);
		r.replace(msg);
		String res = msg.getText();
		res = res.trim();
		System.out.println(res);
		assertTrue(exp.equals(res));
	}

}
