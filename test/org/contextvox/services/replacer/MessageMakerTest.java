package org.contextvox.services.replacer;

import static org.junit.Assert.assertEquals;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import org.contextvox.services.replacer.MessageMaker;
import org.contextvox.services.replacer.ReplacerOption;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.MessagesTest;
import org.contextvox.services.replacer.dictionaries.SentenceAlternative;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.junit.BeforeClass;
import org.junit.Test;

public class MessageMakerTest {

	@BeforeClass
	public static void setup() {
		Messages.load(MessagesTest.MESSAGES_PATH, MessagesTest.SENTENCES_PATH);
	}

	@Test
	public void testSymbolReplace() {
		// default config
		EnumSet<ReplacerOption> config = EnumSet.of(ReplacerOption.SYMBOL_ALL, ReplacerOption.COMPLEX_SYMBOL);
		final String str = "i++);";
		String result = MessageMaker.replaceSymbols(str, config);
		String exp = "i increment closed parenthesis semicolon ";
		assertEquals(exp, result);

		// fast read
		config = EnumSet.of(ReplacerOption.SYMBOL_FAST, ReplacerOption.COMPLEX_SYMBOL);
		result = MessageMaker.replaceSymbols(str, config);
		exp = "i increment ";
		assertEquals(exp, result);
	}

	@Test
	public void testMakeSentence() {
		final EnumSet<ReplacerOption> config = EnumSet.of(ReplacerOption.SYMBOL_ALL, ReplacerOption.COMPLEX_SYMBOL,
				ReplacerOption.SENTENCES);
		// just for test...
		final String exp = "class array array at index i is protected";
		final Map<SentenceToken, String> data = new HashMap<>();
		data.put(SentenceToken.NAME, "array[i]");
		data.put(SentenceToken.MODIFIERS, "protected");
		final String result = MessageMaker.makeSentence(SentenceType.CLASS, data, SentenceAlternative.FULL, config);
		assertEquals(exp, result);
	}
}
