package org.contextvox.services.replacer;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.contextvox.services.replacer.RawMessage;
import org.contextvox.services.replacer.Replacer;
import org.contextvox.services.replacer.SentenceToken;
import org.contextvox.services.replacer.TokenReplacer;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.MessagesTest;
import org.contextvox.services.replacer.dictionaries.SentenceAlternative;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.junit.BeforeClass;
import org.junit.Test;

// WARNING: editing the dictionary may break tests, remember to check expected sentences position (.get(1)).
public class TokenReplacerTest {

	@BeforeClass
	public static void setupMessages() {
		Messages.load(MessagesTest.MESSAGES_PATH, MessagesTest.SENTENCES_PATH);
	}

	@Test
	public void testReplaceWithDifferentParameters() {
		final Map<SentenceToken, String> data = new HashMap<>();

		// class declaration, same param number different type
		final String name = "Pippo";
		final String mod = "public";
		final String extend = "Ziopippo";
		data.put(SentenceToken.NAME, name);
		data.put(SentenceToken.MODIFIERS, mod);
		data.put(SentenceToken.EXTEND, extend);

		final SentenceAlternative alt = SentenceAlternative.FULL;
		final SentenceType type = SentenceType.CLASS;
		final Replacer rep = new TokenReplacer();
		final RawMessage msg = new RawMessage();
		msg.setAlternative(alt);
		msg.setType(type);
		msg.setData(data);

		rep.replace(msg);
		final String r = msg.getText();
		// building solution
		String exp = Messages.getSentence(SentenceType.CLASS, SentenceAlternative.FULL).get(2).getTranslation();
		exp = exp.replace(SentenceToken.NAME.getToken(), name);
		exp = exp.replace(SentenceToken.MODIFIERS.getToken(), mod);
		exp = exp.replace(SentenceToken.EXTEND.getToken(), extend);

		assertEquals(exp, r);
	}

	@Test
	public void testReplace() {
		// Map<SentenceToken, String> data = new HashMap<>();
		//
		// // For loop with 2 token
		// String newCond = "list.size()";
		// String newInit = "i=0";
		// data.put(SentenceToken.CONDITION, newCond);
		// data.put(SentenceToken.INITIALIZERS, newInit);
		// Replacer rep = new SentenceReplacer(null, SentenceType.FOR, data,
		// SentenceAlternative.FULL);
		// String r = rep.replace(null);
		// // building solution
		// String exp =
		// Messages.getSentence(SentenceType.FOR).get(1).getTranslation();
		// exp = exp.replace(SentenceToken.CONDITION.getToken(), newCond);
		// exp = exp.replace(SentenceToken.INITIALIZERS.getToken(), newInit);

		// assertEquals(exp, r);

		// for loop with 3 token
		// String newUpdate = "i++";
		// data.put(SentenceToken.UPDATE, newUpdate);
		// r = SentenceReplacer.replace(data, SentenceType.FOR);
		// exp = Messages.getSentence(SentenceType.FOR).get(0);
		// exp = exp.replace(SentenceToken.INITIALIZERS.getToken(), newInit);
		// exp = exp.replace(SentenceToken.CONDITION.getToken(), newCond);
		// exp = exp.replace(SentenceToken.UPDATE.getToken(), newUpdate);
		//
		// assertEquals(exp, r);
		//
		// // for loop with 0 token
		// data.clear();
		// r = SentenceReplacer.replace(data, SentenceType.FOR);
		// exp = Messages.getSentence(SentenceType.FOR).get(2);
		//
		// assertEquals(exp, r);
	}

}
