package org.contextvox.services.replacer.dictionaries;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.contextvox.services.replacer.dictionaries.MessageType;
import org.contextvox.services.replacer.dictionaries.Messages;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.junit.BeforeClass;
import org.junit.Test;

public class MessagesTest {

	public static final String SENTENCES_PATH = "/sent.test";
	public static final String MESSAGES_PATH = "/msg.test";

	@BeforeClass
	public static void setup() {
		Messages.load(MESSAGES_PATH, SENTENCES_PATH);
	}

	@Test
	public void test() {
		assertNotNull(Messages.getTranslations(MessageType.OPERATOR));
		assertNotNull(Messages.getSentence(SentenceType.FOREACH));
	}

	@Test(expected = NoSuchElementException.class)
	public void noSentencePresentTest() {
		Messages.getSentence(SentenceType.FOR);
	}

}
