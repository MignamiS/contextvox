package org.contextvox.services.replacer.dictionaries;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

public class MessagePackTest {

	private MessagePack mp;

	@Before
	public void setUp() {
		final String path = "/en.messages";
		final InputStream bundle = getClass().getResourceAsStream(path);
		mp = MessagePack.build(Messages.DEFAULT_LANGUAGE, bundle);
	}

	@Test
	public void testLoad() {
		// check punctual search
		final String t = mp.getTranslation("+");
		assertTrue(t.equals("plus"));

		// special symbol, just don't throw any exception...
		mp.getTranslation("\t");
		mp.getTranslation(" ");
		mp.getTranslation("\r\n");
	}

	@Test(expected = NoSuchElementException.class)
	public void testMissingWord() {
		mp.getTranslation("asda");
	}

}
