package org.contextvox.services.replacer.dictionaries;

import static junit.framework.Assert.assertTrue;

import java.io.InputStream;
import java.util.List;

import org.contextvox.services.replacer.dictionaries.SentenceAlternative;
import org.contextvox.services.replacer.dictionaries.SentencePack;
import org.contextvox.services.replacer.dictionaries.SentenceType;
import org.contextvox.services.replacer.dictionaries.Translation;
import org.junit.Before;
import org.junit.Test;

public class SentencePackTest {

	private SentencePack sp;

	@Before
	public void setupPack() {
		final String path = "/en_sentences.json";
		final InputStream is = getClass().getResourceAsStream(path);
		sp = SentencePack.build(is);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testLoad() {
		assertTrue(sp.size() > 0);
		// empty list
		final List<Translation> list = sp.getTranslation(SentenceType.WHILE, SentenceAlternative.LIGHT);
		assertTrue(list.isEmpty());
	}

	// @Test
	// public void testLoad() {
	// final String path = "/en.messages";
	// final InputStream is = getClass().getResourceAsStream(path);
	//
	// final MessagePack mp = MessagePack.build(Messages.Language.ENGLISH, is);
	// final SentencePack sp = SentencePack.build(mp.getSentences());
	// sp.save(null);
	// try {
	// is.close();
	// } catch (final IOException e) {
	// }
	// }

}
