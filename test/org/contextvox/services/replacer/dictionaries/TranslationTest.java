package org.contextvox.services.replacer.dictionaries;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.contextvox.services.replacer.dictionaries.MessageType;
import org.contextvox.services.replacer.dictionaries.Translation;
import org.junit.Test;

public class TranslationTest {
	
	@Test
	public void testSort() {
		List<Translation> list = new ArrayList<>();
		MessageType[] types = MessageType.values();

		// != type, = length
		list.add(new Translation("sy1", "sdfa", MessageType.OPERATOR));
		list.add(new Translation("me1", "sdfa", MessageType.SENTENCE));
		list.add(new Translation("pa1", "sdsfa", MessageType.PARENTHESIS));
		Collections.sort(list);
		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i).getType(), types[i]);
		}
		list.clear();

		// = type, != length
		list.add(new Translation("4321", "sdsafasd", MessageType.OPERATOR));
		list.add(new Translation("21", "sdsdsfsdfsdf", MessageType.OPERATOR));
		list.add(new Translation("1", "sdsa", MessageType.OPERATOR));
		list.add(new Translation("321", "sdsafasdfsfas", MessageType.OPERATOR));
		Collections.sort(list);

	}

	@Test
	public void testEquals() {
		Translation a = new Translation("+=", "plus equal", MessageType.OPERATOR);
		assertFalse(a.equals(null));
		assertFalse(a.equals(new String("test")));

		Translation b = new Translation("==", "equal", MessageType.OPERATOR);
		assertFalse(a.equals(b));

		Translation a2 = new Translation("+=", "plus equal", MessageType.OPERATOR);
		assertTrue(a.equals(a2));
		assertTrue(a2.equals(a));

		b = new Translation("+=", "plus equal", MessageType.PARENTHESIS);
		assertFalse(a.equals(b));
	}

	@Test
	public void testCompareTo() {
		// same type & length
		Translation a = new Translation("12", "asdfsadf", MessageType.OPERATOR);
		Translation b = new Translation("12", "asdfsadf", MessageType.OPERATOR);
		assertEquals(0, a.compareTo(b));

		// same type, != length
		a = new Translation("123", "sadfsadf�lkj", MessageType.OPERATOR);
		b = new Translation("12", "sdfsdfsd", MessageType.OPERATOR);
		assertTrue(a.compareTo(b) <= 1);
		assertTrue(b.compareTo(a) >= -1);

		// != type, same length
		a = new Translation("11", "s�akjdflksaj", MessageType.PARENTHESIS);
		b = new Translation("11", "q�lkjqwre", MessageType.SENTENCE);
		assertTrue(a.compareTo(b) != 0);
		assertTrue(b.compareTo(a) != 0);
	}

	@Test
	public void testHashCode() {
		final String symbol = "+=";
		final String translation = "plus equal";
		final MessageType type = MessageType.OPERATOR;
		final Translation t1 = new Translation(symbol, translation, type);
		final Translation t2 = new Translation("{", "left brace", MessageType.PARENTHESIS);
		assertFalse(t1.hashCode() == t2.hashCode());

		final Translation t1b = new Translation(symbol, translation, type);
		assertTrue(t1.hashCode() == t1b.hashCode());
	}
}
