package org.contextvox.services.replacer;

import static org.junit.Assert.assertTrue;

import org.contextvox.services.replacer.ComplexReplacer;
import org.contextvox.services.replacer.RawMessage;
import org.junit.Test;

public class ComplexReplacerTest {

	@Test
	public void testArrayNotation() {
		final ComplexReplacer r = new ComplexReplacer();

		// definition
		String src = "int[] myArr";
		String exp = "array of int myArr";
		final RawMessage msg = new RawMessage(src);
		r.replace(msg);
		String res = msg.getText();
		assertTrue(exp.equals(res));

		// spaced
		src = "func( Double[] da)";
		exp = "func( array of Double da)";
		msg.setText(src);
		r.replace(msg);
		res = msg.getText();
		assertTrue(exp.equals(res));

		// access
		src = "c[11] = asd";
		exp = "array c at index 11 = asd";
		msg.setText(src);
		r.replace(msg);
		res = msg.getText();
		assertTrue(exp.equals(res));
	}

	@Test
	public void testGenerics() {
		final ComplexReplacer r = new ComplexReplacer();

		// simple
		String src = "List<String>";
		String exp = "List of String";
		final RawMessage msg = new RawMessage(src);
		r.replace(msg);
		String res = msg.getText();
		assertTrue(exp.equals(res));

		// complex
		src = "List<Set<String>>";
		exp = "List of Set of String";
		msg.setText(src);
		r.replace(msg);
		res = msg.getText();
		assertTrue(exp.equals(res));

		// Map version
		src = "Map<String, List<Integer>>";
		exp = "Map of String and List of Integer";
		msg.setText(src);
		r.replace(msg);
		res = msg.getText();
		res = res.replaceAll("\\s+", " ");
		assertTrue(exp.equals(res));

	}

}
