package org.contextvox.plugin.requests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class SpecialRequestTest {

	@Test
	public void testHashCode() {
		final SpecialRequest v1 = SpecialRequest.buildStopRequest(MessageSource.GENERIC_FREE);
		assertEquals(v1.hashCode(), v1.hashCode());

		final SpecialRequest v2 = SpecialRequest.buildStopRequest(MessageSource.GENERIC_FREE);
		assertNotEquals(v1.hashCode(), v2.hashCode());

		final SpecialRequest v3 = SpecialRequest.buildStopRequest(MessageSource.EDITOR);
		assertNotEquals(v1.hashCode(), v3.hashCode());
	}

	@Test
	public void testEqualsObject() {
		// same
		final SpecialRequest v1 = SpecialRequest.buildStopRequest(MessageSource.GENERIC_FREE);
		assertEquals(v1, v1);
		final SpecialRequest v2 = SpecialRequest.buildStopRequest(MessageSource.COMMANDLINE);
		assertNotEquals(v1, v2);
		// null
		assertNotEquals(v1, null);
		// other type
		final NavigationRequest v3 = new NavigationRequest(MessageSource.GENERIC_FREE, "");
		assertNotEquals(v1, v3);
	}

}
