package org.contextvox.plugin.requests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class VoxRequestTest {

	@Test
	public void testHashCode() {
		// same
		final VoxRequest v1 = new VoxRequest(MessageSource.GENERIC_FREE);
		assertEquals(v1.hashCode(), v1.hashCode());
		final VoxRequest v1b = new VoxRequest(MessageSource.GENERIC_FREE, "");
		assertNotEquals(v1.hashCode(), v1b.hashCode());
		// different
		final VoxRequest v2 = new VoxRequest(MessageSource.EDITOR);
		assertNotEquals(v1.hashCode(), v2.hashCode());
		final VoxRequest v3 = new VoxRequest(MessageSource.EDITOR, "some text");
		assertNotEquals(v2.hashCode(), v3.hashCode());
	}

	@Test
	public void testEqualsObject() {
		// same
		final VoxRequest v1 = new VoxRequest(MessageSource.GENERIC_FREE);
		assertEquals(v1, v1);
		final VoxRequest v1b = new VoxRequest(MessageSource.GENERIC_FREE, "");
		assertNotEquals(v1, v1b);
		// null
		assertNotEquals(v1, null);

		// different
		final VoxRequest v3 = new VoxRequest(MessageSource.EDITOR);
		assertNotEquals(v1, v3);
		final VoxRequest v4 = new VoxRequest(MessageSource.EDITOR, "some text");
		assertNotEquals(v3, v4);
	}

}
