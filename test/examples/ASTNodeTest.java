package examples;

import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.junit.Test;

public class ASTNodeTest {

	String str = "package javaproject;" // package for all classes
			+ "class Dummy {" //
			+ "   void testSearch(String queryStr, String dateStr, SearchResources twitter1) {" //
			+ "      Query query = new Query(queryStr).until(dateStr);" //
			+ "      int j=0;" + "		 for(int i =0;i<5;i++){j+=i;} for(int i =0;i<4;i++){j+=i;}" //
			+ "      switch(j){case 0: j=1; break; case 1: j=2; break;}"
			+ " 	if(j<5){"
			+ "		}else if(j<3){"
			+ "		}else{"
			+ "		}"
			+ "   }" //
			+ "}";

	@Test
	public void ASTNodeFunctionTest() {
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);

		parser.setEnvironment( // apply classpath
				new String[] { "C:\\" }, //
				null, null, true);
		parser.setUnitName("any_name");

		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		cu.accept(new ASTVisitor() {
			public boolean visit(MethodDeclaration node) {
				if (node.getName().getIdentifier().equals("testSearch")) {
					Block block = node.getBody();
					block.accept(new ASTVisitor() {
						int counter=0;
						public boolean visit(IfStatement node){
							System.out.println("************************USANDO IFStatement****************************");
							System.out.println(node.getExpression());
							System.out.println(node.getThenStatement());
							System.out.println(node.getElseStatement());
							
							
							return true;
						}
						public boolean visit(SwitchStatement node){
							System.out.println("************************USANDO SwitchStatement****************************");
							System.out.println(node.getExpression());
							List<?> stmts = node.statements();
							for (int i = 0; i < stmts.size(); i++) {
								Statement s = (Statement) stmts.get(i);
								if(s.getNodeType()==ASTNode.SWITCH_CASE){
									SwitchCase sc = (SwitchCase) s;
									System.out.println("case-> "+sc);
									System.out.println(sc.getExpression());
								}
								System.out.println(s);
							}
							return true;
						}
						
						// cosi prende solamente i for
						public boolean visit(ForStatement node) {
							++counter;
							System.out.println(counter);
							System.out.println("************************USANDO FORSTATEMENT****************************");
							// prendo il valore che inizializza il for
							List<?> initializers = node.initializers();
							ASTNode i = (ASTNode) initializers.get(0);
							System.out.println("For: " + i);
							// prende solo la "condizione"->i < 5
							System.out.println("For: " + node.getExpression());
							// il valore di incremento
							List<?> updaters = node.updaters();
							ASTNode u = (ASTNode) updaters.get(0);
							System.out.println("For: " + u);
							return true;
						}

						// cosi prende solamente gli expressionstatement->j+=i;
						public boolean visit(ExpressionStatement node) {
							System.out
									.println("************************USANDO ExpressionStatement****************************");
							if (node.getParent().getParent() instanceof ForStatement) {
								ForStatement b = (ForStatement) node
										.getParent().getParent();
								// risultato->for (int i=0; i < 5; i++) { j+=i;}
								System.out.println("For: "
										+ node.getParent().getParent());
								// risultato->{j+=i;}
								System.out.println("For: " + node.getParent());
								// uguale a prima
								System.out.println("For: " + b.getBody());
								// prende contenuto del for ->j+=i;
								System.out.println("For: " + node);
								// prendo il valore che inizializza il for
								List<?> initializers = b.initializers();
								ASTNode i = (ASTNode) initializers.get(0);
								System.out.println("For: " + i);
								// prende solo la "condizione"->i < 5
								System.out.println("For: " + b.getExpression());
								// il valore di incremento
								List<?> updaters = b.updaters();
								ASTNode u = (ASTNode) updaters.get(0);
								System.out.println("For: " + u);
							}

							return true;
						}

						// cosi prende tutti i metodi invocati
						public boolean visit(MethodInvocation node) {
							System.out.println("Name: " + node.getName());

							Expression expression = node.getExpression();
							if (expression != null) {
								System.out.println("Expr: "
										+ expression.toString());
								ITypeBinding typeBinding = expression
										.resolveTypeBinding();
								if (typeBinding != null) {
									System.out.println("Type: "
											+ typeBinding.getName());
								}
							}

							IMethodBinding binding = node
									.resolveMethodBinding();
							if (binding != null) {
								ITypeBinding type = binding.getDeclaringClass();
								if (type != null) {
									System.out.println("Decl: "
											+ type.getName());
								}
							}

							return true;
						}
					});
				}
				return true;
			}
		});
	}
}
