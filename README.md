Project ContextVox
------------------

**Author**: Simone Mignami (<simone.mig29@gmail.com>)

**Description**: ContextVox is a plug-in for Eclipse that provides an intermediate layer between the IDE and a screen reader, allowing the user to read the Java programming language in a more effective way.

**Keywords**: Eclipse, Java, screen reader, productivity, open-source.


Project state
--------------

There is a stable version that is a summary of 3 years of students work. Now *ContextVox* has been released in an open-source policy and a new version is under development.

See [more information](https://bitbucket.org/MignamiS/contextvox/src/master/extra/info/index.md)

