package message_manager;

import java.io.ByteArrayOutputStream;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

public class MessageManager {

	public static void main(final String[] args) {
		final JsonObject obj = Json.createObjectBuilder()
				.add("asd", Json.createArrayBuilder().add("first").add("second")).build();

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		final JsonWriter writer = Json.createWriter(bos);
		writer.write(obj);
		writer.close();

		System.out.println(bos.toString());
	}

}
